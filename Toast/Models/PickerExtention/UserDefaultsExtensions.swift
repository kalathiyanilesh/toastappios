//
//  UserDefaults+Extensions.swift
//
//  Created by C110 Pinkesh on 22/11/17.
//  Copyright © 2017 C110. All rights reserved.
//

import UIKit
import Foundation

extension UserDefaults {
    
    func setCustomObject(CustomObject: AnyObject, forKey:String) {
        let defaults = UserDefaults.standard
        do {
            if #available(iOS 11.0, *) {
                let data = try NSKeyedArchiver.archivedData(withRootObject: CustomObject, requiringSecureCoding: false)
            } else {
                // Fallback on earlier versions
            }
            defaults.set(data, forKey: forKey)
        } catch {
            print("Failed to save...")
        }
        defaults.synchronize()
    }
    
    
    func getCustomObject(forKey:String) -> AnyObject? {

        let defaults = UserDefaults.standard
        if defaults.object(forKey: forKey) != nil {
            let decoded  = defaults.object(forKey: forKey) as! NSData
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data)! as AnyObject
            return decodedTeams
        }
        return nil
    }
    
    func removeCustomObject(forKey:String)
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: forKey)
        defaults.synchronize()
    }
    
}
