//Kravt
//ApplicationStyle
//Copyright © Mindscan
//Created by Igor S Yefimov (First Line Software)


import UIKit



@objcMembers
class ApplicationStyle: NSObject {

    // MARK: - dateformatter
    static var chatMessageDateFormatter = DateFormatter() {
        didSet {
            chatMessageDateFormatter.dateStyle = .medium
        }
    }

    
    //MARK: - Buttons
    static func setButtonImage(button: UIButton, imageName: String?) {
        let image = imageName != nil ? UIImage(named: imageName!) : nil
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
    }




    //MARK: - Colors

    static func color(red: UInt8, green: UInt8, blue: UInt8, alpha: CGFloat) -> UIColor {
        return UIColor.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: alpha)
    }

    static func color(red: UInt8, green: UInt8, blue: UInt8) -> UIColor {
        return color(red: red, green: green, blue: blue, alpha: 1)
    }


    static func randomColor() -> UIColor {
        return randomColor(alpha: 1)
    }

    static func randomColor(alpha: CGFloat) -> UIColor {
        return color(red: UInt8(arc4random() % 255), green: UInt8(arc4random() % 255), blue: UInt8(arc4random() % 255), alpha: alpha)
    }


    static let accentColor = color(red: 236, green: 197, blue: 64)
    static let backgroundColor = color(red: 252, green: 251, blue: 245)
    static let greenColor = color(red: 85, green: 182, blue: 18)
    static let hintColor = color(red: 149, green: 150, blue: 166)
    static let mainTextColor = color(red: 0, green: 0, blue: 0, alpha: 0.85)
    static let cornflowerBlueColor = color(red: 101, green: 126, blue: 212)

    static let noAvatarBg = color(red: 255, green: 238, blue: 235, alpha: 1)
    static let customBorderColor = color(red: 217, green: 217, blue: 217, alpha: 1)
    static let secondaryTextColor = color(red: 103, green: 104, blue: 115, alpha: 1)
    static let invitedTextColor = color(red: 101, green: 126, blue: 212, alpha: 1)


    //MARK: - Fonts

    static func SFUIDisplayMediumFont(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFUIDisplay-Medium", size: size)
    }


    static func SFUITextMediumFont(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFUIText-Medium", size: size)
    }

    static func SFUITextRegularFont(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFUIText-Regular", size: size)
    }


    // MARK: - Margins (depending on iOS version)
    static let searchBarBottomSpace: CGFloat = UIDevice.current.systemVersion.hasPrefix("10.") ?  0 : 8



    //MARK: - Navigation bar

    static func navigationBarButton(imageName: String, target: Any?, action: Selector) -> UIBarButtonItem {
        let button: UIButton = UIButton()
        button.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        button.setTitleColor(accentColor, for: .normal)
        button.setTitleColor(accentColor, for: .highlighted)
        button.sizeToFit()
        button.addTarget(target, action: action, for: .touchUpInside)

        return UIBarButtonItem(customView: button)
    }


    static func navigationBarButton(title: String, target: Any?, action: Selector) -> UIBarButtonItem {
        let button: UIButton = UIButton()
        button.setTitle(title, for: .normal);
        button.setTitle(title, for: .highlighted);
        button.setTitleColor(accentColor, for: .normal)
        button.setTitleColor(accentColor, for: .highlighted)
        button.sizeToFit()
        button.addTarget(target, action: action, for: .touchUpInside)

        return UIBarButtonItem(customView: button)
    }


    static func stylizeNavigationBar(navigationBar: UINavigationBar?) {
        guard let navigationBar = navigationBar else {
            return
        }

        navigationBar.isTranslucent = false
        navigationBar.barTintColor = backgroundColor
//        if #available(iOS 10, *) {
//            let image = UIImage(named: "navigationBarBackground")
//            navigationBar.setBackgroundImage(image, for: .default)
//            navigationBar.shadowImage = UIImage()
//        }
    }




    //MARK: - Search bar

    static func stylizeSearchBar(_ searchBar: UISearchBar?) {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([.foregroundColor : accentColor], for: .normal)
        
        guard let searchBar = searchBar else {
            return
        }

        searchBar.barTintColor = backgroundColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = backgroundColor.cgColor
        searchBar.searchBarStyle = .default
        searchBar.tintColor = accentColor

        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            let frame = CGRect(origin: CGPoint(x: 16, y: 8), size: CGSize(width: textField.width, height: 36));
            textField.frame = frame

            textField.font = SFUITextRegularFont(size: 17)
            textField.textColor = mainTextColor
            textField.backgroundColor = UIColor.white
            layoutSearchBar(searchBar)
        }
    }
    
    static func layoutSearchBar(_ searchBar: UISearchBar) {
        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            
            if let backgroundView = textField.subviews.first {
                backgroundView.clipsToBounds = true;
                backgroundView.layer.cornerRadius = backgroundView.height / 2;
            }
        }
    }




    //MARK: - Segmented control

    class func stylizeSegmentedControl(_ segmentedControl: UISegmentedControl?) {
        guard let segmentedControl = segmentedControl else {
            return
        }

        let normalAttributes = [
            NSAttributedString.Key.font.rawValue: self.SFUITextRegularFont(size: 13),
            NSAttributedString.Key.foregroundColor.rawValue: self.accentColor
        ]
        

        let selectedAttributes = [
            NSAttributedString.Key.font.rawValue: self.SFUITextRegularFont(size: 13),
            NSAttributedString.Key.foregroundColor.rawValue: UIColor.white
        ]
        segmentedControl.setTitleTextAttributes(selectedAttributes as [AnyHashable: Any] as? [NSAttributedString.Key : Any], for: .selected)

        segmentedControl.setTitleTextAttributes(normalAttributes as [AnyHashable: Any] as? [NSAttributedString.Key : Any], for: .normal)
        segmentedControl.tintColor = self.accentColor
        segmentedControl.sizeToFit()
        segmentedControl.selectedSegmentIndex = 0;
        
        //THIS METHOD WILL BE USED FOR SEGEMENT CONTROL TO UPDATE UI LIKE IOS 12
        segmentedControl.ensureiOS12Style()
    }

}


extension UISegmentedControl {
    
    func ensureiOS12Style() {
        if #available(iOS 13, *) {
            let tintColorImage = UIImage(color: tintColor)
            setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: .selected, barMetrics: .default)
            setBackgroundImage(UIImage(color: tintColor.withAlphaComponent(0.2)), for: .highlighted, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
            //setTitleTextAttributes([.foregroundColor: tintColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .regular)], for: .normal)
            setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
            layer.borderWidth = 1
            layer.borderColor = tintColor.cgColor
        }
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 14)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UILabel {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
    
    @IBInspectable
    var uppercased: Bool {
        
        set {
            if newValue {
                self.text = self.text?.uppercased()
            }
        }
        get {
            return false
        }
    }
}

extension UIButton {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = titleLabel?.attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: titleLabel?.text ?? "")
                titleLabel?.text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            titleLabel?.attributedText = attributedString
        }

        get {
            if let currentLetterSpace = titleLabel?.attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
    
    @IBInspectable
    var uppercased: Bool {
        
        set {
            if newValue {
                self.setTitle(self.titleLabel?.text?.uppercased(), for: .normal)
            }
        }
        get {
            return false
        }
    }
}

extension UITextField {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
    
    @IBInspectable
    var placeholderColor: UIColor {
        set {
            attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: newValue])
        }

        get {
            return UIColor.black
        }
    }
}

extension UITextView {
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
}
