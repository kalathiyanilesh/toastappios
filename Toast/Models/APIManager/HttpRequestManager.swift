//
//  HttpRequestManager.swift
//  Copyright © 2018 Nilesh. All rights reserved.

import UIKit
import SwiftyJSON
import Alamofire

class HttpRequestManager
{
    static let sharedInstance = HttpRequestManager()
    //var alamoFireManager = Alamofire.SessionManager.default
    let additionalHeader = ["User-Agent": "iOS"]
    
    private static var alamoFireManager : Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            SSL_URL: .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let man = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return man
    }()
    
    private static var alamoFireManagerRecipe : Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            SSL_URL_RECIPE: .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let man = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return man
    }()
    
    //MARK:- GET & POST
    func requestWithJsonParam( endpointurl:String,
                                   service:String,
                                   parameters:NSDictionary,
                                   method: HTTPMethod,
                                   getReturn:Bool = false,
                                   isrecipeapi: Bool = false,
                                   showLoader:Bool,
                                   responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            print("URL : \(endpointurl) \nParam :\(convertDictionaryToJSONString(dic: parameters) ?? "not found")")
            
            ShowNetworkIndicator(xx: true)
            var encoding: ParameterEncoding = JSONEncoding.default
            if method == .get {
                encoding = URLEncoding.queryString
            }
            var headers: HTTPHeaders = HTTPHeaders()
            if isUserLogin() {
                headers = ["Authorization": getAuthToken()]
                print(headers)
            }
            let sessionmanager: SessionManager = isrecipeapi ? HttpRequestManager.alamoFireManagerRecipe : HttpRequestManager.alamoFireManager
            sessionmanager.request(endpointurl, method: method, parameters: parameters as? Parameters, encoding: encoding, headers: headers)
                .responseString(completionHandler: { (responseString) in
                    print(responseString.value ?? "error")
                    ShowNetworkIndicator(xx: false)
                    if(showLoader) { hideLoaderHUD() }
                    if(responseString.value == nil)
                    {
                        responseData(responseString.error as NSError?, nil)
                    }
                    else
                    {
                        ShowNetworkIndicator(xx: false)
                        let strResponse = "\(responseString.value!)"
                        //let arr = strResponse.components(separatedBy: "\n")
                        //let dict =  convertStringToDictionary(str:(arr.last  ?? "")!)
                        let dict =  convertStringToDictionary(str:strResponse)
                        if dict != nil {
                            let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: dict! as NSDictionary)
                            print(dictResponse)
                            if isrecipeapi {
                                if dictResponse.object(forKey: "success") as? String == "1" {
                                    responseData(nil,dictResponse)
                                } else {
                                    if let msg = dictResponse["message"] as? String {
                                        print(msg)
                                        showMessage(msg)
                                        if msg == "Authorization token not provided" || msg == "Permission denied for this account type" {
                                            Logoutuser()
                                        } else {
                                            if getReturn {
                                                responseData(nil,nil)
                                            }
                                        }
                                    } else {
                                        responseData(nil,dictResponse)
                                    }
                                }
                            } else {
                                if let msg = dictResponse["message"] as? String {
                                    print(msg)
                                    showMessage(msg)
                                    if msg == "Authorization token not provided" || msg == "Permission denied for this account type" {
                                        Logoutuser()
                                    } else {
                                        if getReturn {
                                            responseData(nil,nil)
                                        }
                                    }
                                } else {
                                    responseData(nil,dictResponse)
                                }
                            }
                        }
                    }
                })
        }
    }
    
    func requestWithPostMultipartParam(endpointurl:String,
                                       showLoader:Bool,
                                       getReturn:Bool = false,
                                       isrecipeapi: Bool = false,
                                       parameters:NSDictionary,
                                       responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            ShowNetworkIndicator(xx: true)
            var headers: HTTPHeaders = HTTPHeaders()
            if isUserLogin() {
                headers = ["Authorization": getAuthToken()]
                print(headers)
            }
            let sessionmanager: SessionManager = isrecipeapi ? HttpRequestManager.alamoFireManagerRecipe : HttpRequestManager.alamoFireManager
            sessionmanager.upload(multipartFormData:
                { multipartFormData in
                    for (key, value) in parameters
                    {
                        if value is Data
                        {
                            let strType = self.imageType(imgData: value as! NSData)
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "ddMMyyyyHH:mm:ss"
                            let sImageName = dateFormatter.string(from: Date())
                            multipartFormData.append(value as! Data, withName: key as! String, fileName: "\(sImageName).\(strType)", mimeType: "image/\(strType)")
                        }
                        else
                        {
                            if key as! String == "review_item_images" {
                                let arrItemImages = parameters.object(forKey: "review_item_images") as! NSArray
                                for i in 0..<arrItemImages.count {
                                    let dictData: NSDictionary = arrItemImages.object(at: i) as! NSDictionary
                                    let strItemID =  dictData.object(forKey: "item_order_id") as! String
                                    let images = dictData.object(forKey: "images") as! NSArray
                                    for k in 0..<images.count {
                                        let imgData: Data = compressImage(image: images[k] as! UIImage) as Data
                                        let strType = self.imageType(imgData: imgData as NSData)
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "ddMMyyyyHH:mm:ss.SSSS"
                                        let sImageName = "\(strItemID)_" + "\(dateFormatter.string(from: Date()))" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())"
                                        print("imageName: ",sImageName)
                                        multipartFormData.append(imgData, withName: key as! String, fileName: "\(sImageName).\(strType)", mimeType: "image/\(strType)")
                                    }
                                }
                            } else {
                                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
                            }
                        }
                    }
            }, to: endpointurl, headers:headers,encodingCompletion:
                { encodingResult in
                    switch encodingResult
                    {
                    case .success(let upload, _, _):
                        upload.responseString(completionHandler:{(responseString) in
                            print(responseString.value ?? "error")
                            ShowNetworkIndicator(xx: false)
                            if(showLoader) { hideLoaderHUD() }
                            if(responseString.value == nil)
                            {
                                responseData(responseString.error as NSError?, nil)
                            }
                            else
                            {
                                let strResponse = "\(responseString.value!)"
                                let dict =  convertStringToDictionary(str:strResponse)
                                if dict != nil {
                                    let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: dict! as NSDictionary)
                                    if isrecipeapi {
                                        if dictResponse.object(forKey: "success") as? String == "1" {
                                            responseData(nil,dictResponse)
                                        } else {
                                            if let msg = dictResponse["message"] as? String {
                                                print(msg)
                                                showMessage(msg)
                                                if msg == "Authorization token not provided" || msg == "Permission denied for this account type" {
                                                    Logoutuser()
                                                } else {
                                                    if getReturn {
                                                        responseData(nil,nil)
                                                    }
                                                }
                                            } else {
                                                responseData(nil,dictResponse)
                                            }
                                        }
                                    } else {
                                        if let msg = dictResponse["message"] as? String {
                                            showMessage(msg)
                                            if msg == "Authorization token not provided" || msg == "Permission denied for this account type" {
                                                Logoutuser()
                                            } else {
                                                if getReturn {
                                                    responseData(nil,nil)
                                                }
                                            }
                                        } else {
                                            responseData(nil,dictResponse)
                                        }
                                    }
                                }
                            }
                        })
                        break
                    case .failure(let encodingError):
                        print("ENCODING ERROR: ",encodingError)
                        responseData(encodingError as NSError,nil)
                    }
            })
        }
    }
    
    func imageType(imgData : NSData) -> String
    {
        var c = [UInt8](repeating: 0, count: 1)
        imgData.getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "" //unknown
        }
        
        return ext
    }
    
    func GetRandomIntValue() -> Int {
        return Int(arc4random_uniform(UInt32(10)))
    }
}
