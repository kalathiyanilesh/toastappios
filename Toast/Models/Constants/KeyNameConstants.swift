
import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let SELECTED_MODE = "selectedmode"
let MY_ORDER_ID = "MY_ORDER_ID"
let MY_QR_CODE = "MY_QR_CODE"
let MY_JOIN_CODE = "MY_JOIN_CODE"
let MENU_DICT = "MENU_DICT"
let IS_ENTER_TABLE = "IS_ENTER_TABLE"

let TEMP_MY_ORDER_ID = "TEMP_MY_ORDER_ID"
let TEMP_MY_QR_CODE = "TEMP_MY_QR_CODE"
let TEMP_MY_JOIN_CODE = "TEMP_MY_JOIN_CODE"

//MARK:- APPNAME
public let APPNAME :String = "Toast"

//MARK:- LOADER COLOR
let loadercolor = [Color_Hex(hex: "1C2244"), Color_Hex(hex: "131835")]

//MARK:- PLACEHOLDER IMAGES
let EVENT_PLACE_HOLDER = UIImage(named: "event_placeholder")
let RESTAURANT_PLACE_HOLDER = UIImage(named: "temp9")
let FOOD_PLACE_HOLDER = UIImage(named: "fooditem_placeholder")
let USER_PLACE_HOLDER = UIImage(named: "temp4")

//MARK:- NOTIFICATION
let BILLING_PAGE_REFRESH = "BILLING_PAGE_REFRESH"
let NOTIFICATION_PAGE_REFRESH = "NOTIFICATION_PAGE_REFRESH"
let EVENTS_PAGE_REFRESH = "EVENTS_PAGE_REFRESH"
let OFFERS_PAGE_REFRESH = "OFFERS_PAGE_REFRESH"
let TOAST_PAGE_REFRESH = "TOAST_PAGE_REFRESH"
let RECIPE_PAGE_REFRESH = "RECIPE_PAGE_REFRESH"
let HOME_PAGE_REFRESH = "HOME_PAGE_REFRESH"
let ACTIVE_ORDER_PAGE_REFRESH = "ACTIVE_ORDER_PAGE_REFRESH"
let ORDER_SUCCESS_PAGE_REFRESH = "ORDER_SUCCESS_PAGE_REFRESH"

//MARK:- HEADER KEY
public let kHEADER = "token"
public let kTIMEZONE = "timezone"

//MARK:- PAGELIMIT (PAGINATION)
public let PAGE_LIMIT = 10

//MARK:- COLORDS
let APP_COLOR = Color_Hex(hex: "8F6BFF")

//MARK:- STORY BOARD NAMES
public let SB_ONBOARDING:String = "Onboarding"
public let SB_MAIN:String = "Main"
public let SB_LOGIN:String = "Login"
public let SB_TAB:String = "Tab"
public let SB_NOTIFICATION:String = "Notification"
public let SB_REVIEWORDER:String = "ReviewOrder"
public let SB_MENU:String = "Menu"
public let SB_ORDERPLACE:String = "OrderPlace"
public let SB_RECIPE:String = "Recipe"
public let SB_HOME:String = "Home"

//MARK:- USER DEFAULTS KEY
let IS_LOGIN:String = "islogin"
let UD_AUTHTOKEN:String = "UDAUTHTOKEN"
let UD_CUSTOMERID:String = "customerid"
let UD_UserData:String = "UDUserData"
let UD_UserId:String = "UDUserId"
let UD_SecretLogID:String = "UDSecretLogID"
let UD_APIToken:String = "UDUserAccessToken"
let UD_DeviceToken:String = "UDDeviceToken"
let k_CART_ITEMS:String = "cartitems"

//MARK:- NSNOTIFICATION
let k_GO_SELECTED_FOOD_SECTION:String = "gotoselectedsection"
let k_FILTER_FOOD:String = "filterfood"
let k_FILTER_SPECIAL_FOOD:String = "filterspecialfood"
let k_UPDATE_MENU_SETTING:String = "updatemenusetting"
let k_RELOAD_FOOD:String = "reloadfood"
let k_RELOAD_SPECIAL_FOOD:String = "reloadspecialfood"

//MARK:- CONTROLLERS ID
public let idNavnBoarding = "navOnboarding"

//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let EnterName = "Name is required."
let EnterLname = "Last Name is required."
let EnterEmail = "Email address is required."
let EnterValidEmail = "Enter valid email address."
let EnterPhoneNo = "Phone number is required."
let EnterValidPhoneNo = "Enter valid phone number."
let EnterPassword = "Password is required."
let EnterConfirmPassword = "Confirm password is required."
let AgreeTerms = "Please accept terms and condition"
let EnterMobile = "Mobile number is required."
let EnterValidMobile = "Mobile number is required."
let EnterOLDPassword = "Old Password is required."
let EnterNEWPassword = "New Password is required."
let PasswordMismatch = "Password Mismatch."
let EnterMinLengthPassword = "Minimum length of password is 6 character."
let EnterState = "State is required."
let DontHaveCamera = "You don't have camera."
let InternetNotAvail = "Internet Not Available."
let ErrorMSG = "Something going wrong. Please try again!"


//MARK:- NSNOTIFICATION
let k_Recipe_Create = "createRecipe"
let k_Recipe_Edit = "EditRecipe"
let k_Recipe_Delete = "DeleteRecipe"
let k_Noti_Filter_Recipe = "NotificationFilterRecipe"

//MARK:- Filter
let k_Filter_Recipe = "FilterRecipe"
