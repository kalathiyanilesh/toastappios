//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

let kDATA = "data"
let kSUCCESS = "success"
let kMESSAGE = "message"
let kRESPONSE_STATUS = "ResponseStatus"

/*
 For Main app:
 a) Staging URL: staging-be.toastapp.co
 b) Prod URL: be.toastapp.co
 
Recipe feature:
a) Staging URL: staging-be-recipes.toastapp.co
b) Prod URL: be-recipes.toastapp.co
*/


/*
//FOR STAGING
//let SERVER_URL = "https://staging-be2.toastapp.co/"
let SERVER_URL = "https://staging-be.toastapp.co/"
let SSL_URL = "staging-be.toastapp.co"
let SERVER_RECIPE_URL = "http://staging-be-recipes.toastapp.co/"
let SSL_URL_RECIPE = "staging-be-recipes.toastapp.co"
let IMAGE_FOLDER = "stagingimages"
*/


//FOR PRODUCTION
let SERVER_URL = "https://be.toastapp.co/"
let SSL_URL = "be.toastapp.co"
let SERVER_RECIPE_URL = "https://be-recipes.toastapp.co/"
let SSL_URL_RECIPE = "be-recipes.toastapp.co"
let IMAGE_FOLDER = "images"

let UPLOAD_IMAGE_URL = ""
let TERMS_CONDITION = ""

let DeviceType = "1"

//API Services
public var CUSTOMER_ID = ""

//Login
let APILogin = "login/phone/"

//Notifications
let APIEvents = "notifications/\(CUSTOMER_ID)/events"
let APIAttendUnAttend = "attendunattend"
let APIOffers = "notifications/\(CUSTOMER_ID)/offers"
let APIToast = "notifications/\(CUSTOMER_ID)/toast"
let APINotificationSettings = "notifications/settings/\(CUSTOMER_ID)"

//Table
let APIJoin = "table/\(CUSTOMER_ID)/join"
let APIJoinWithCode = "table/\(CUSTOMER_ID)/join_with_code"
let APITableCode = "table/\(CUSTOMER_ID)/get_table_code"
let APILeave = "customer/\(CUSTOMER_ID)/leave"
let APICustomizeOrder = "customer/\(CUSTOMER_ID)/customize_order"

//Reviews
let APIGetItemReview = "reviews/\(CUSTOMER_ID)/item/"
let APIPast = "reviews/\(CUSTOMER_ID)/past?"
let APIOrderReview = "reviews/\(CUSTOMER_ID)/orders/"

//Complaints
let APIAddComplaints = "complaints/\(CUSTOMER_ID)/add"

//User Profile
let APICustomerProfile = "customer_profile/\(CUSTOMER_ID)"
let APIPastOrder = "orders/customer/\(CUSTOMER_ID)/past"
let APIGetPastOrder = "GetPastOrder" //in placeorder screen
let APIActiveOrder = "orders/customer/\(CUSTOMER_ID)/active"

//Orders
let APICheckout = "checkout"
let APIAcknowledge = "acknowledge"
let APIZomatoGold = "ZomatoGold"
let APITip = "tips"
let APIPlaceOrder = "customer/\(CUSTOMER_ID)"
let APIGetCoupons = "promo/coupons/\(CUSTOMER_ID)"
let APIApplyCoupon = "coupons/\(CUSTOMER_ID)/apply"
let APIVerifyAddress = "VerifyAddress"

//Logout
let APILogout = "logout/mobile/customer/\(CUSTOMER_ID)"

let APIRestaurantDetails = "GetRestaurantDetails"

//Recipe
let APIRecipe = "api/v1/recipes/popular_recipes"
let APIBaseRecipe = "api/v1/recipes/"
let APICreateRecipe = "api/v1/recipes"


//PickUp Order (Take Away)
let APIJoinViaTakeAway = "orders/customer/\(CUSTOMER_ID)/menu/pick_up"
let APIAddTA = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/pick_up/add"
let APITAInitiataPay = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/initiate/payment"
let APITAAcknowledgePay = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/acknowledge/payment"
let APITAPickUpPlace = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/pick_up/place"

//Delivery
let APIMenuDelivery = "orders/customer/\(CUSTOMER_ID)/menu/"
let APIMenuAddDelivery = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/delivery/add"
let APIMenuPlaceDelivery = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/delivery/place"
let APIMenuDeliveryInitiataPay = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/initiate/payment"
let APIMenuDeliveryAcknowledgePay = "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/acknowledge/payment"

//Restaurant
let APIPopularRestaurant = "search/\(CUSTOMER_ID)/popular_restraunts"
let APISearchNearRestaurant = "search/\(CUSTOMER_ID)"
let APISearchRestaurant = "search/\(CUSTOMER_ID)/name"

let APIMenuPriceLess = "search/\(CUSTOMER_ID)/"
let APIGetCity = "search/\(CUSTOMER_ID)/city"


