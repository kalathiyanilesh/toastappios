//
//  constant.swift

import Foundation
import UIKit
import CoreLocation
import SystemConfiguration
import SwiftMessages
import AVFoundation
import FloatingPanel
import Toaster
import Firebase
import Photos
import GoogleMaps
import GLNotificationBar

var spinner = RPLoadingAnimationView.init(frame: CGRect.zero)
var overlayView = UIView()
var lat_currnt: Double = 0
var long_currnt: Double = 0

enum MODULE_NAME: String {
    case ACKNOWLEDGEPAYMENT = "acknowledge_payment"
    case DISCOUNT = "discount"
    case ZOMATOGOLD = "zomato_gold"
    case EVENTS = "events"
    case OFFERS = "coupons"
    case TOAST = "reviews_reply"
}

public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
        isSim = true
    #endif
    return isSim
}()

public func FONT_DISPLAY_PRO_LIGHT(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFProDisplay-Light", size: fontSize)!
}

public func FONT_PRO_BOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFProText-Bold", size: fontSize)!
}

public func FONT_SFUI_REG(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFUIText-Regular", size: fontSize)!
}

public func FONT_SFUI_SEMIBOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFUIText-Semibold", size: fontSize)!
}

/*
func isSystemInDarkMode() -> Bool {
    return APP_DELEGATE.window!.traitCollection.userInterfaceStyle == .dark ? true : false
}*/

//MARK:-  Get VC for navigation
//var banner = NotificationBanner(title: "", subtitle: "", style: .success)
public func getStoryboard(storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: storyboardName, bundle: nil)
}

public func loadVC(strStoryboardId: String, strVCId: String) -> UIViewController {
    let vc = getStoryboard(storyboardName: strStoryboardId).instantiateViewController(withIdentifier: strVCId)
    return vc
}

public var CurrentTimeStamp: String
{
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}

func convertTextToQRCode(text: String) -> UIImage {
    
    let size : CGSize = CGSize(width: 500, height: 500)
    let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
    
    let filter = CIFilter(name: "CIQRCodeGenerator")!
    
    filter.setValue(data, forKey: "inputMessage")
    filter.setValue("L", forKey: "inputCorrectionLevel")
    
    let qrcodeCIImage = filter.outputImage!
    
    let cgImage = CIContext(options:nil).createCGImage(qrcodeCIImage, from: qrcodeCIImage.extent)
    UIGraphicsBeginImageContext(CGSize(width: size.width * UIScreen.main.scale, height:size.height * UIScreen.main.scale))
    let context = UIGraphicsGetCurrentContext()
    context!.interpolationQuality = .none
    
    context?.draw(cgImage!, in: CGRect(x: 0.0,y: 0.0,width: context!.boundingBoxOfClipPath.width,height: context!.boundingBoxOfClipPath.height))
    
    let preImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    let qrCodeImage = UIImage(cgImage: (preImage?.cgImage!)!, scale: 1.0/UIScreen.main.scale, orientation: .downMirrored)
    
    return qrCodeImage
}

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func saveInUserDefault(obj: AnyObject, key: String) {
    UserDefaults.standard.set(obj, forKey: key)
    UserDefaults.standard.synchronize()
}

func getUserInfo() -> NSDictionary {
    var dictUser: NSDictionary = NSDictionary()
    if let dictData = UserDefaults.standard.value(forKey: UD_UserData) as? NSDictionary {
        dictUser = dictData
    }
    return dictUser
}

func getUserID() -> String {
    if(UserDefaults.standard.string(forKey: UD_UserId) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_UserId)!
    }
}

func getDeviceToken() -> String {
    if (UserDefaults.standard.object(forKey: "pushyToken") != nil) {
        return UserDefaults.standard.object(forKey: "pushyToken") as! String
    }
    return "IOS_SIMULATOR_TOKEN"
    //return Messaging.messaging().fcmToken != nil ? Messaging.messaging().fcmToken! : "SIMULATOR"
}

func GetAPIToken() -> String {
    if(UserDefaults.standard.string(forKey: UD_APIToken) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_APIToken)!
    }
}

func GetSecretLogID() -> String {
    if(UserDefaults.standard.string(forKey: UD_SecretLogID) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_SecretLogID)!
    }
}

func getMyQRCode() -> String {
    if (UserDefaults.standard.object(forKey: MY_QR_CODE) != nil) {
        return UserDefaults.standard.object(forKey: MY_QR_CODE) as! String
    }
    return ""
}

func getMyOrderID() -> String {
    if (UserDefaults.standard.object(forKey: MY_ORDER_ID) != nil) {
        return UserDefaults.standard.object(forKey: MY_ORDER_ID) as! String
    }
    return ""
}

func getUserDefautSavedString(_ sKey: String) -> String {
    if (UserDefaults.standard.object(forKey: sKey) != nil) {
        return UserDefaults.standard.object(forKey: sKey) as! String
    }
    return ""
}

func getMyJoinCode() -> String {
    if (UserDefaults.standard.object(forKey: MY_JOIN_CODE) != nil) {
        return UserDefaults.standard.object(forKey: MY_JOIN_CODE) as! String
    }
    return ""
}

func SaveMyQRCode(_ strValue: String) {
    UserDefaults.standard.set(strValue, forKey: MY_QR_CODE)
    UserDefaults.standard.set(strValue, forKey: TEMP_MY_QR_CODE)
    UserDefaults.standard.synchronize()
}

func SaveMyOrderID(_ strValue: String) {
    UserDefaults.standard.set(strValue, forKey: MY_ORDER_ID)
    UserDefaults.standard.set(strValue, forKey: TEMP_MY_ORDER_ID)
    UserDefaults.standard.synchronize()
}

func SaveMyJoinCode(_ strValue: String) {
    UserDefaults.standard.set(strValue, forKey: MY_JOIN_CODE)
    UserDefaults.standard.set(strValue, forKey: TEMP_MY_JOIN_CODE)
    UserDefaults.standard.synchronize()
}

func ClearTableCodes() {
    UserDefaults.standard.removeObject(forKey: MY_QR_CODE)
    UserDefaults.standard.removeObject(forKey: MY_ORDER_ID)
    UserDefaults.standard.removeObject(forKey: MY_JOIN_CODE)
    UserDefaults.standard.removeObject(forKey: MENU_DICT)
}

func RemoveAllTempData() {
    UserDefaults.standard.removeObject(forKey: TEMP_MY_QR_CODE)
    UserDefaults.standard.removeObject(forKey: TEMP_MY_ORDER_ID)
    UserDefaults.standard.removeObject(forKey: TEMP_MY_JOIN_CODE)
    UserDefaults.standard.removeObject(forKey: IS_ENTER_TABLE)
}

func compressImage(image:UIImage) -> NSData
{
    var compression:CGFloat!
    let maxCompression:CGFloat!
    compression = 0.9
    maxCompression = 0.1
    var imageData = image.jpegData(compressionQuality: compression)! as NSData
    while (imageData.length > 10 && compression > maxCompression)
    {
        compression = compression - 0.10
        imageData = image.jpegData(compressionQuality: compression)! as NSData
    }
    return imageData
}

func SetCursorColor() {
    UITextField.appearance().tintColor = APP_COLOR
    UITextView.appearance().tintColor = APP_COLOR
}

func setNoDataLabel(tableView:UITableView, array:NSMutableArray, text:String) -> Int
{
    var numOfSections = 0
    if array.count != 0 {
        tableView.backgroundView = nil
        numOfSections = 1
    } else {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-20, height: tableView.frame.size.height)
        noDataLabel.text = text
        noDataLabel.numberOfLines = 0
        noDataLabel.font = FontWithSize("Montserrat-Regular", 12)
        noDataLabel.textColor = UIColor.lightGray
        noDataLabel.textAlignment = NSTextAlignment.center
        tableView.backgroundView = noDataLabel
        tableView.separatorStyle = .none
    }
    
    return numOfSections
}

func changeDateFormat(strDate: String,FormatFrom: String, FormateTo: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = FormatFrom
    let date = dateFormat.date(from: strDate)
    dateFormat.dateFormat = FormateTo
    if date == nil {
        return strDate
    }
    return dateFormat.string(from: date!)
}

public func convert(dicData:NSDictionary,toString:(_ strData:String)->())
{
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dicData)
        if let json = String(data: jsonData, encoding: .utf8) {
            toString(json)
        }
    } catch {
        print("something went wrong with parsing json")
    }
}

public func convert(JSONstring:String,toDictionary:(_ strData:NSDictionary)->())
{
    var strJSON:String = JSONstring
    strJSON = strJSON.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    let arr = strJSON.components(separatedBy: "\n")
    var dict : [String:Any]?
    for jsonString in arr{
        if let jsonDataToVerify = jsonString.data(using: String.Encoding.utf8)
        {
            do {
                dict = try JSONSerialization.jsonObject(with: jsonDataToVerify) as? [String : Any]
                print("JSON is valid.")
                toDictionary(dict! as NSDictionary)
            } catch {
                //print("Error deserializing JSON: \(error.localizedDescription)")
            }
        }
    }
}

public func getMainBaseCoin(strCoin:String,to:(_ strMain:String, _ strBase:String)->())
{
    if strCoin.count > 0 {
        let arrCoins = strCoin.components(separatedBy: "/")
        if arrCoins.count == 2 {
            to(arrCoins[0],arrCoins[1])
        }
    }
}

func dictionaryOfFilteredBy(dict: NSDictionary) -> NSDictionary {
    let replaced: NSMutableDictionary = NSMutableDictionary(dictionary : dict)
    let blank: String = ""
    for (key, _) in dict
    {
        let object = dict[key] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[key] = blank as AnyObject?
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[key] = dictionaryOfFilteredBy(dict: object as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[key] = arrayOfFilteredBy(arr: object as! NSArray)
        }
        else
        {
            replaced[key] = "\(object)" as AnyObject?
        }
    }
    return replaced
}

func arrayOfFilteredBy(arr: NSArray) -> NSArray {
    
    let replaced: NSMutableArray = NSMutableArray(array: arr)
    let blank: String = ""
    
    for i in 0..<arr.count
    {
        let object = arr[i] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[i] = blank as AnyObject
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[i] = dictionaryOfFilteredBy(dict: arr[i] as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[i] = arrayOfFilteredBy(arr: arr[i] as! NSArray)
        }
        else
        {
            replaced[i] = "\(object)" as AnyObject
        }
        
    }
    return replaced
}

public var mostTopViewController: UIViewController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    set {
        UIApplication.shared.keyWindow?.rootViewController = newValue
    }
}

//MARK:- iOS version checking Functions
public var appDisplayName: String? {
    return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
}
public var appBundleID: String? {
    return Bundle.main.bundleIdentifier
}
public func IOS_VERSION() -> String {
    return UIDevice.current.systemVersion
}
public var statusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}
public var applicationIconBadgeNumber: Int {
    get {
        return UIApplication.shared.applicationIconBadgeNumber
    }
    set {
        UIApplication.shared.applicationIconBadgeNumber = newValue
    }
}
public var appVersion: String? {
    return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
}
public func SCREENWIDTH() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.width
}

public func SCREENHEIGHT() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.height
}


//MARK:- SwiftLoader
public func showLoaderHUD(strMessage:String)
{
    let activityData = ActivityData()
    NVActivityIndicatorView.DEFAULT_TYPE = .ballPulse
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = ""
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

public func hideLoaderHUD()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    spinner.removeFromSuperview()
    overlayView.removeFromSuperview()
}

public func showMessage(_ bodymsg:String)
{
    ToastView.appearance().backgroundColor = UIColor.black //Color_Hex(hex: "3CBCB4")
    ToastView.appearance().font = FontWithSize("Montserrat-Medium", 12)
    Toast(text: bodymsg).show()
    /*
    let view = MessageView.viewFromNib(layout: .cardView)
    //view.configureDropShadow()
    view.configureContent(body: bodymsg)
    view.layoutMarginAdditions = UIEdgeInsets(top: 25, left: 10, bottom: 20, right: 10)
    view.configureTheme(.warning, iconStyle: .light)
    
    let backgroundColor = Color_Hex(hex: "404040")
    let foregroundColor = UIColor.white
    
    view.configureTheme(backgroundColor: backgroundColor, foregroundColor: foregroundColor)
    view.titleLabel?.isHidden = true
    view.button?.isHidden = true
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    SwiftMessages.show(view: view)*/
}

//MARK:- Network indicator
public func ShowNetworkIndicator(xx :Bool)
{
    runOnMainThreadWithoutDeadlock {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

//MARK : Length validation
public func TRIM(string: Any) -> String
{
    return (string as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

public func validateTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTxtViewLength(_ txtVal: UITextView, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTextLength(_ strVal: String, withMessage msg: String) -> Bool {
    if TRIM(string: strVal).count == 0
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateAmount(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    var amount : Double = 0
    if TRIM(string: txtVal.text!).count > 0{
        amount = Double(TRIM(string: txtVal.text ?? ""))!
    }
    if amount <= 0 {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateMinTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count != 10
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    print("!=10 length")
    return true
}

public func validateMaxTxtFieldLength(_ txtVal: UITextField, lenght:Int,msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "").count > lenght
    {
        showMessage(msg)
        return false
    }
    return true
}

public func passwordMismatch(_ txtVal: UITextField, _ txtVal1: UITextField, withMessage msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "") != TRIM(string: txtVal1.text ?? "")
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateEmailAddress(_ txtVal: UITextField ,withMessage msg: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    if(emailTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validatePhoneNo(_ txtVal: UITextField ,withMessage msg: String) -> Bool
{
    let PHONE_REGEX = "^[0-9]{6,}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    if(phoneTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateTermsCondition(_ termsSelected: Bool, withMessage msg: String) -> Bool {
    if !termsSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateBool(_ isSelected: Bool, withMessage msg: String) -> Bool {
    if !isSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func isBase64(stringBase64:String) -> Bool
{
    let regex = "([A-Za-z0-9+/]{4})*" + "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)"
    let test = NSPredicate(format:"SELF MATCHES %@", regex)
    if(test.evaluate(with: stringBase64) != true)
    {
        return false
    }
    return true
}

public func getDateString(_ date: Date,format: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = format
    return dateFormat.string(from: date)
}

public func isUserLogin() -> Bool {
    if UserDefaults.standard.value(forKey: IS_LOGIN) == nil{
        return false
    }
    return true
}

public func getAuthToken() -> String {
    if UserDefaults.standard.value(forKey: UD_AUTHTOKEN) != nil{
        return UserDefaults.standard.value(forKey: UD_AUTHTOKEN) as? String ?? ""
    }
    return ""
}

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)

    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 60) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }

    return nil
}

func Logoutuser() {
    UserDefaults.standard.removeObject(forKey: IS_LOGIN)
    UserDefaults.standard.removeObject(forKey: UD_CUSTOMERID)
    UserDefaults.standard.removeObject(forKey: UD_AUTHTOKEN)
    APP_DELEGATE.strDefualtUserName = ""
    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "navlogin")
    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        APP_DELEGATE.window?.rootViewController = vc
    }, completion: nil)
}

//MARK:- - Get image from image name
public func Set_Local_Image(imageName :String) -> UIImage
{
    return UIImage(named:imageName)!
}

//MARK:- FONT
public func FontWithSize(_ fname: String,_ fsize: Int) -> UIFont
{
    return UIFont(name: fname, size: CGFloat(fsize))!
}

public func randomColor() -> UIColor {
    let r: UInt32 = arc4random_uniform(255)
    let g: UInt32 = arc4random_uniform(255)
    let b: UInt32 = arc4random_uniform(255)
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
}

//Mark : string to dictionary
public func convertStringToDictionary(str:String) -> [String: Any]? {
    if let data = str.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

//MARK: dictionary to string
public func convertDictionaryToJSONString(dic:NSDictionary) -> String? {
    do{
        let jsonData: Data? = try JSONSerialization.data(withJSONObject: dic, options: [])
        var myString: String? = nil
        if let aData = jsonData {
            myString = String(data: aData, encoding: .utf8)
        }
        return myString
    }catch{
        print(error)
    }
    return ""
}

//MARK:-Check Internet connection
func isConnectedToNetwork() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    })
    else
    {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    let available =  (isReachable && !needsConnection)
    if(available)
    {
        return true
    }
    else
    {
        showMessage(InternetNotAvail)
        return false
    }
}

func Color_Hex(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func ConvertDateInFormate(d: String, _ from: String, _ to: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = from
    let ReminderDate = dateFormat.date(from: d)
    dateFormat.dateFormat = to
    return dateFormat.string(from: ReminderDate!)
}

func popBack<T: UIViewController>(_ navController: UINavigationController?, toControllerType: T.Type) {
    if var viewControllers: [UIViewController] = navController?.viewControllers {
        viewControllers = viewControllers.reversed()
        for currentViewController in viewControllers {
            if currentViewController .isKind(of: toControllerType) {
                navController?.popToViewController(currentViewController, animated: true)
                break
            }
        }
    }
}

func GoToHomePage() {
    let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        APP_DELEGATE.window?.rootViewController = vc
    }, completion: nil)
}

//MARK:- FIND HEIGHT OR WIDTH
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension Date {
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension String {
    func Localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
    
    func RemoveSpace() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? self
    }
}

extension TimeInterval {
    var minuteSecond: String {
        return String(format: "%dm %ds", minute, second)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}
extension UIViewController {
    func pushViewController(strStoryboardId:String,strVCId:String, animation:Bool) {
        let vc = loadVC(strStoryboardId: strStoryboardId, strVCId: strVCId)
        self.navigationController?.pushViewController(vc, animated: animation)
    }
}


extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

//MARK: FloatingPanelLayout
class MyFloatingPanelLayout: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .tip
    }

    public var supportedPositions: Set<FloatingPanelPosition> {
        return [.tip, .full]
    }
    
    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .full:
            return 40
        case .tip:
            if SCREENHEIGHT() > 736 {
                return 26
            }
            return 60
        default: return nil
        }
    }
}

func AddGradient(_ view: UIView, _ color1: UIColor = Color_Hex(hex: "#78FFD6").withAlphaComponent(0.9), _ color2: UIColor = Color_Hex(hex: "#007991").withAlphaComponent(0.9)) {
    let colorTop = color1.cgColor
    let colorBottom = color2.cgColor
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [colorTop, colorBottom]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
    gradientLayer.frame = view.bounds
    view.layer.insertSublayer(gradientLayer, at:0)
}

func AddBlurEffect(_ view: UIView) {
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = view.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view.addSubview(blurEffectView)
}

extension String {

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

@IBDesignable class BigSwitch: UISwitch {

    @IBInspectable var scale : CGFloat = 1{
        didSet{
            setup()
        }
    }

    //from storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    //from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    private func setup(){
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    override func prepareForInterfaceBuilder() {
        setup()
        super.prepareForInterfaceBuilder()
    }
}

func GetItemCount(_ sItemID: String) -> String {
    var TotalCount = 0
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if sItemID == objFood.itemId {
            TotalCount = TotalCount + (Int(objFood.itemCount!) ?? 0)
            //break
        }
    }
    return "\(TotalCount)"
}

func GetItemCountWithUniqID(_ objMenu: TMenu) -> String {
    var TotalCount = 0
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objMenu.itemId == objFood.itemId && objMenu.uniqID == objFood.uniqID {
            TotalCount = TotalCount + (Int(objFood.itemCount!) ?? 0)
            break
        }
    }
    return "\(TotalCount)"
}

func GetAddOnsTotal(_ objMenu: TMenu) -> CGFloat {
    var TotalCount: CGFloat = 0
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objMenu.itemId == objFood.itemId && objMenu.uniqID == objFood.uniqID {
            let arrSelectedAddons = objMenu.selectedAddonItems ?? [AddonItems]()
            for objAdd in arrSelectedAddons {
                if objAdd.addonPrice?.count ?? 0 > 0 {
                    let modelAddon = objAdd.addonPrice?.first!
                    TotalCount = TotalCount + CGFloat(((modelAddon?.price ?? "0") as NSString).floatValue)
                }
            }
        }
    }
    return TotalCount
}

func GetSizeTotalPrice(_ objMenu: TMenu) -> CGFloat {
    var TotalCount: CGFloat = 0
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objMenu.itemId == objFood.itemId && objMenu.uniqID == objFood.uniqID {
            let arrSelectedSize = objMenu.selectedItemPriceDetails ?? [ItemPriceDetails]()
            for objAdd in arrSelectedSize {
                TotalCount = TotalCount + CGFloat(((objAdd.itemPrice ?? "0") as NSString).floatValue)
            }
        }
    }
    return TotalCount
}

func AddNewItem(_ objData: TMenu) {
    objData.itemCount = "1"
    APP_DELEGATE.arrCartItems.add(objData)
    NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
}

func PlusItem(_ objData: TMenu) {
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objData.itemId == objFood.itemId {
            objFood.itemCount = String(Int(objFood.itemCount!)! + 1)
            APP_DELEGATE.arrCartItems.replaceObject(at: i, with: objFood)
            break
        }
    }
    NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
}

func MinusItem(_ objData: TMenu) {
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objFood.itemId == objData.itemId && objFood.isLastAdded == "1" {
            var count = Int(objFood.itemCount!)! - 1
            if count <= 0 {
                count = 0
                APP_DELEGATE.arrCartItems.removeObject(at: i)
            } else {
                objFood.itemCount = String(count)
                APP_DELEGATE.arrCartItems.replaceObject(at: i, with: objFood)
            }
            NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
            return
        }
    }
    
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objFood = APP_DELEGATE.arrCartItems[i] as! TMenu
        if objData.itemId == objFood.itemId {
            var count = Int(objFood.itemCount!)! - 1
            if count <= 0 {
                count = 0
                APP_DELEGATE.arrCartItems.removeObject(at: i)
            } else {
                objFood.itemCount = String(count)
                APP_DELEGATE.arrCartItems.replaceObject(at: i, with: objFood)
            }
            break
        }
    }
    NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
}

func GetLastAddedAddonsForItem(_ objMenu: TMenu?) -> TMenu? {
    for i in 0..<APP_DELEGATE.arrCartItems.count {
        let objAdd: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
        if objAdd.itemId == objMenu?.itemId && objAdd.isLastAdded == "1" {
            return objAdd
        }
    }
    var objLastAdded: TMenu?
    if objLastAdded == nil {
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objTMenu: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            if objTMenu.itemId == objMenu?.itemId {
                objLastAdded = objTMenu
            }
        }
    }
    if objLastAdded != nil {
        objLastAdded?.isLastAdded = "1"
    }
    return objLastAdded
}

func GetEmojiFromRate(_ rate: CGFloat) -> UIImage {
    if rate >= 1 && rate <= 1.4 {
        return UIImage(named: "ic_seek")!
    } else if rate >= 1.5 && rate <= 2.4 {
        return UIImage(named: "ic_arrogant")!
    } else if rate >= 2.5 && rate <= 3.4 {
        return UIImage(named: "ic_confused")!
    } else if rate >= 3.5 && rate <= 4.4 {
        return UIImage(named: "ic_happy")!
    } else {
        return UIImage(named: "ic_very_happy")!
    }
}

func GetRateText(_ rate: Double) -> String {
    if rate >= 1 && rate <= 1.5 {
        return "Disappointed"
    } else if rate >= 1.5 && rate <= 2.5 {
        return "Bad"
    } else if rate >= 2.5 && rate <= 3.5 {
        return "Fair"
    } else if rate >= 3.5 && rate <= 4.5 {
        return "Good"
    } else {
        return "Awesome!"
    }
}

func isTableClose(_ dictOrder: NSDictionary) -> Bool {
    let orderStatus = dictOrder.object(forKey: "order_status") as? String ?? ""
    let closedByRes = dictOrder.object(forKey: "closed_by_restraunt") as? String ?? ""
    if orderStatus == "occupied" || orderStatus == "completed" || orderStatus == "closed" || orderStatus == "" || orderStatus == "null" {
        return true
    } else if closedByRes == "1" || closedByRes == "true" {
        return true
    }
    return false
}

//MARK:- PRIVATE API CALLING
func apiGetTableCode(responseData:@escaping  (_ strCode: String) -> Void) {
    let service = SERVER_URL + APITableCode
    let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR]
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APITableCode,
        parameters: dictParam,
        method: .post,
        getReturn: true,
        showLoader: false)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "")
            if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                if let strCode = dictData.object(forKey: "table_joining_code") as? String {
                    responseData(strCode)
                } else {
                    responseData("")
                }
            } else {
                responseData("")
            }
        } else {
            responseData("")
        }
    }
}

func apiGetPastOrder(_ showLoader: Bool, responseData:@escaping  (_ dictOrder: NSDictionary?) -> Void) {
    let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)"
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APIGetPastOrder,
        parameters: NSDictionary(),
        method: .get,
        getReturn: true,
        showLoader: showLoader)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "")
            if let dictOrder = responseDict?.object(forKey: kDATA) as? NSDictionary {
                responseData(dictOrder)
            } else {
                responseData(nil)
            }
        }  else {
            responseData(nil)
        }
    }
}

func unLikeRecipeAPICall(obj: TRecipeDetail) {
    let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(obj.recipeId ?? "")/likes/\(obj.likeId ?? "")"
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APIBaseRecipe,
        parameters: NSDictionary(),
        method: .delete,
        isrecipeapi: true,
        showLoader: false)
    { (error, responseDict) in
        print(responseDict ?? "")
        if error == nil {
            
        } else {
            showMessage(error?.localizedDescription ?? "Try again!" )
        }
    }
}

 func likeRecipeAPICall(obj: TRecipeDetail) {
    let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(obj.recipeId ?? "")/likes"
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APIBaseRecipe,
        parameters: NSDictionary(),
        method: .post,
        isrecipeapi: true,
        showLoader: false)
    { (error, responseDict) in
        print(responseDict ?? "")
        if error == nil {
            if let dictlike: NSDictionary = responseDict?.object(forKey: "like") as? NSDictionary {
                obj.likeId = dictlike.object(forKey: "like_id") as? String
            }
        } else {
            showMessage(error?.localizedDescription ?? "Try again!" )
        }
    }
}

func getLatLongFromCache() {
    if (UserDefaults.standard.object(forKey: "selectedCityLat") != nil) {
        lat_currnt = UserDefaults.standard.object(forKey: "selectedCityLat") as? Double ?? 0
    }
    if (UserDefaults.standard.object(forKey: "selectedCityLong") != nil) {
        long_currnt = UserDefaults.standard.object(forKey: "selectedCityLong") as? Double ?? 0
    }
}

//FilterModel
struct FilterRecipeModel: Codable {
    var strVegNoVeg = String()
    var isLiked = Bool()
}

extension UserDefaults {
    func setJsonObject<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func getJsonObject<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

func getAssetThumbnail(asset: PHAsset, _ size: CGSize) -> UIImage {
       let manager = PHImageManager.default()
       let option = PHImageRequestOptions()
       var thumbnail = UIImage()
       option.isSynchronous = true
       manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
           thumbnail = result!
       })
       return thumbnail
   }

extension UIView {

    func dropBorder(color: UIColor) {
        layer.borderColor = color.cgColor
        layer.borderWidth = 1
        clipsToBounds = true
    }
    
    func removeBorder() {
        layer.borderColor = UIColor.clear.cgColor
        layer.borderWidth = 0
    }
}

func getUniqID() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "ddMMyyyyhhmmssSSS"
    let uniqid = formatter.string(from: Date())
    return uniqid
}


func getCurrentAddress(completionHandler:@escaping  (_ strAddress: String?) -> Void) {
    let geocoder = GMSGeocoder()
    let coordinate = CLLocationCoordinate2D(latitude:lat_currnt, longitude: long_currnt)
    geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
        if let address = response?.firstResult() {
            let lines = address.lines! as [String]
            let addressString = lines.joined(separator: "\n")
            print("Current Address:",addressString)
            completionHandler(addressString)
        }
    }
}

func getLatLongFromCity(_ sCity: String, completion:@escaping  (_ location: CLLocationCoordinate2D?) -> Void) {
    let geoCoder = CLGeocoder()
    geoCoder.geocodeAddressString(sCity) { (placemarks, error) in
        guard let placemarks = placemarks,
        let location = placemarks.first?.location?.coordinate else {
            completion(nil)
            return
        }
        completion(location)
    }
}


func getCurrentCity(completionHandler:@escaping  (_ strCity: String?) -> Void) {
    let geocoder = GMSGeocoder()
    let position: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:lat_currnt, longitude: long_currnt)
    geocoder.reverseGeocodeCoordinate(position) { response, error in
        if let location = response?.firstResult() {
            let sCity = location.locality
            print("Current City:",sCity ?? "Not Found")
            UserDefaults.standard.set(sCity, forKey: "useroriginalcity")
            UserDefaults.standard.synchronize()
            completionHandler(sCity)
        } else {
            completionHandler("")
            print("reverse geodcode fail: \(error?.localizedDescription ?? "Not Found")")
        }
    }
}

func GetUserSelectedCity() -> String {
    if (UserDefaults.standard.object(forKey: "userselectedcity") != nil) {
        return UserDefaults.standard.object(forKey: "userselectedcity") as? String ?? ""
    } else {
        return ""
    }
}

func apiOpenPricelessMenu(restrauntId: String, responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void) {
    let service = SERVER_URL + APIMenuPriceLess + "browse_menu/\(restrauntId)/dine_in"
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APIMenuPriceLess,
        parameters: NSDictionary(),
        method: .get,
        getReturn: true,
        showLoader: true)
    { (error, responseDict) in
        responseData(error, responseDict)
    }
}

func CheckTableOngoing(responseData:@escaping  (_ isGoToScan: Bool?) -> Void) {
    if APP_DELEGATE.isFromLaunching {
        APP_DELEGATE.isFromLaunching = false
        let strCode = getMyJoinCode()
        if strCode.count > 0 {
            APP_DELEGATE.strScannedQR = getMyQRCode()
            APP_DELEGATE.sOrderID = getMyOrderID()
            GetPastOrder()
        } else {
            responseData(true)
            ClearTableCodes()
            if APP_DELEGATE.isShowNotificationPopup {
                APP_DELEGATE.isShowNotificationPopup = false
                let notificationBar = GLNotificationBar(title: APP_DELEGATE.PushTitle, message: APP_DELEGATE.PushMessage, preferredStyle: .detailedBanner, handler: nil)
                notificationBar.addAction(GLNotifyAction(title: "Go To Notification", style: .default) { (result) in
                    APP_DELEGATE.GoToNotificationTab(APP_DELEGATE.PushModule)
                })
            }
        }
    } else {
        responseData(true)
    }
}

func GetPastOrder() {
    if APP_DELEGATE.sOrderID.count > 0 {
        apiGetPastOrder(true) { (dictOrder) in
            if dictOrder != nil {
                if isTableClose(dictOrder!) {
                    ClearTableCodes()
                } else {
                    apiJoinWithCode(getMyJoinCode())
                }
            }
        }
    }
}

func apiJoinWithCode(_ strCode: String) {
    let service = SERVER_URL + APIJoinWithCode
    let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "table_joining_code":strCode]
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: service,
        service: APIJoinWithCode,
        parameters: dictParam,
        method: .post,
        showLoader: true)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "")
            if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                if let strStatus = dictData.object(forKey: "status") as? String {
                    if strStatus == "failure" {
                        //showMessage(dictData.object(forKey: "reason") as? String ?? "Try Again!")
                        ClearTableCodes()
                    } else {
                        showMessage("Something went wrong, try again later!")
                    }
                } else if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                    
                    if let sOrderID = dictData.object(forKey: "order_id") as? String {
                        APP_DELEGATE.sOrderID = sOrderID
                    }
                    
                    SaveMyQRCode(APP_DELEGATE.strScannedQR)
                    SaveMyOrderID(APP_DELEGATE.sOrderID)
                    SaveMyJoinCode(strCode)
                    
                    let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                    if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                        vc.dictMenu = dictMenu
                    }
                    if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                        APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                    }
                    vc.modalPresentationStyle = .fullScreen
                    mostTopViewController?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}

extension UILabel {

    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

        guard let labelText = self.text else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple

        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }

        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))


        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
    }
}
