//
//  CellMenu.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellMenu: UITableViewCell {

    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var lblReviews: UILabel!
    @IBOutlet var viewPlusMinus: UIView!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet weak var imageHappy: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
