//
//  FilterRecipeVC.swift
//  toastdemo
//
//  Created by Nikul on 06/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class FilterRecipeVC: UIViewController {

    //Outlets
    @IBOutlet weak var labelAll: UILabel!
    @IBOutlet weak var labelAllValue: UILabel!
    @IBOutlet weak var labelVeg: UILabel!
    @IBOutlet weak var labelVegValue: UILabel!
    @IBOutlet weak var labelNonVeg: UILabel!
    @IBOutlet weak var labelNonVegValue: UILabel!
    @IBOutlet weak var labelLikeRecipe: UILabel!
    @IBOutlet weak var labelLikeRecipevalue: UILabel!
    
    //Variables
    var arrayRecipe = [TRecipeDetail]()
    var objectFilter = FilterRecipeModel()
    
    //ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelAllValue.text = "\(arrayRecipe.count)"
        
        let arrayVeg = self.arrayRecipe.filter({ (model) -> Bool in
            return model.recipeType == "veg"})
        
        if arrayVeg.count > 0 {
            labelVegValue.text = "\(arrayVeg.count)"
        } else {
            labelVegValue.text = "0"
        }
        
        let arrayNonVeg = self.arrayRecipe.filter({ (model) -> Bool in
        return model.recipeType == "non_veg"})
        
        if arrayNonVeg.count > 0 {
            labelNonVegValue.text = "\(arrayNonVeg.count)"
        } else {
            labelNonVegValue.text = "0"
        }
        
        let arrayLike = self.arrayRecipe.filter({ (model) -> Bool in
        return model.isLiked == "1"})
        
        if arrayLike.count > 0 {
            labelLikeRecipevalue.text = "\(arrayLike.count)"
        } else {
            labelLikeRecipevalue.text = "0"
        }
        
        if let model = UserDefaults.standard.getJsonObject(FilterRecipeModel.self, forKey: k_Filter_Recipe)  {
            objectFilter = model
        }
        
        if objectFilter.strVegNoVeg != "" {
            if objectFilter.strVegNoVeg == "veg" {
                labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                labelVeg.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
                labelVegValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
                
                labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                labelNonVeg.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
                labelNonVegValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
                
                labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        } else if objectFilter.isLiked {
            labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            labelLikeRecipe.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
            labelLikeRecipevalue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        } else {
            labelAll.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
            labelAllValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
            
            labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        

    }
    
    
    //Button Action
    @IBAction func buttonCloseTouchUpInside(_ sender: Any) {
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        }
    }
    
    @IBAction func buttonCancelTouchUpInside(_ sender: Any) {
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        }
    }
    
    @IBAction func buttonAllTouchUpInside(_ sender: Any) {
        
        labelAll.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        labelAllValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        
        labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        objectFilter.strVegNoVeg = ""
        objectFilter.isLiked = false
        UserDefaults.standard.setJsonObject(encodable: objectFilter, forKey: k_Filter_Recipe)
        NotificationCenter.default.post(name:NSNotification.Name(k_Noti_Filter_Recipe),object: nil)
    }
    
    @IBAction func buttonVegTouchUpInside(_ sender: Any) {
        
        labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelVeg.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        labelVegValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        
        labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        objectFilter.strVegNoVeg = "veg"
        objectFilter.isLiked = false
        UserDefaults.standard.setJsonObject(encodable: objectFilter, forKey: k_Filter_Recipe)
        NotificationCenter.default.post(name:NSNotification.Name(k_Noti_Filter_Recipe),object: nil)
        
    }
    
    @IBAction func buttonNonVegTouchUpInside(_ sender: Any) {
        
        labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelNonVeg.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        labelNonVegValue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        
        labelLikeRecipe.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelLikeRecipevalue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        objectFilter.strVegNoVeg = "non_veg"
        objectFilter.isLiked = false
        UserDefaults.standard.setJsonObject(encodable: objectFilter, forKey: k_Filter_Recipe)
        NotificationCenter.default.post(name:NSNotification.Name(k_Noti_Filter_Recipe),object: nil)
        
    }
    
    @IBAction func buttonLikeRecipeTouchUpInside(_ sender: Any) {
        
        labelAll.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelAllValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelNonVeg.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        labelNonVegValue.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        labelLikeRecipe.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        labelLikeRecipevalue.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        
        objectFilter.strVegNoVeg = ""
        objectFilter.isLiked = true
        UserDefaults.standard.setJsonObject(encodable: objectFilter, forKey: k_Filter_Recipe)
        NotificationCenter.default.post(name:NSNotification.Name(k_Noti_Filter_Recipe),object: nil)
        
    }
}
