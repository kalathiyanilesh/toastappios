//
//  TableViewCellRecipeTime.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellRecipeTime: UITableViewCell {

    @IBOutlet weak var textFieldPTHours: UITextField!
    @IBOutlet weak var textFieldPTMintues: UITextField!
    @IBOutlet weak var textFieldServes: UITextField!
    @IBOutlet weak var textFieldCTHours: UITextField!
    @IBOutlet weak var textFieldCTMintues: UITextField!
    @IBOutlet weak var labelrate: UILabel!
    @IBOutlet weak var imageRate: UIImageView!
    @IBOutlet weak var buttonRate: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
