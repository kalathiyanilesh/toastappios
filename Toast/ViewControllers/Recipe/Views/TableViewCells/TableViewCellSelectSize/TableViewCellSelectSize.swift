//
//  TableViewCellSelectSize.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellSelectSize: UITableViewCell {

    @IBOutlet weak var imageSelectSize: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet var btnSelectSize: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
