//
//  TableViewCellSelectTitle.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellSelectTitle: UITableViewCell {

    @IBOutlet weak var labelSelectTitle: UILabel!
    @IBOutlet weak var labelSelectDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
