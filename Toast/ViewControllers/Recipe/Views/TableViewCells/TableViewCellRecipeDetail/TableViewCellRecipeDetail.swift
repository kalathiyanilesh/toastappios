//
//  TableViewCellRecipeDetail.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellRecipeDetail: UITableViewCell {

    
    @IBOutlet weak var textFieldRecipeName: UITextField!
    @IBOutlet weak var textViewRecipeDes: UITextView!
    @IBOutlet weak var buttonVeg: UIButton!
    @IBOutlet weak var buttonNonVeg: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
