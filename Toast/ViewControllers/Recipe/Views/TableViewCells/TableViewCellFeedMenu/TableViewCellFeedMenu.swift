//
//  TableViewCellFeedMenu.swift
//  Toast
//
//  Created by Nikul on 05/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellFeedMenu: UITableViewCell {

    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var imageViewItem: UIImageView!
    @IBOutlet weak var viewPlusAndMinus: UIView!
    @IBOutlet weak var imageAdd: UIImageView!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var imagePlus: UIImageView!
    @IBOutlet weak var imageMinus: UIImageView!
    @IBOutlet weak var labelOrderValue: UILabel!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var labelReview: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
