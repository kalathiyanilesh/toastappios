//
//  TableViewCellAddTopings.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellAddTopings: UITableViewCell {

    @IBOutlet weak var buttonSelectToppings: UIButton!
    @IBOutlet weak var imageVeg: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
