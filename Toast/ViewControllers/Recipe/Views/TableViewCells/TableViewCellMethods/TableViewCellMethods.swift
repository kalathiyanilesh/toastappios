//
//  TableViewCellMethods.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellMethods: UITableViewCell {

    @IBOutlet weak var buttonAddMethods: UIButton!
    @IBOutlet weak var textViewAddStep: UITextView!
    @IBOutlet var viewback: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
