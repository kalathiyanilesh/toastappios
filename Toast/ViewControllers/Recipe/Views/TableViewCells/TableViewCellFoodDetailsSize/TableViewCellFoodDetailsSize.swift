//
//  TableViewCellFoodDetailsSize.swift
//  Toast
//
//  Created by Nikul on 14/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellFoodDetailsSize: UITableViewCell {

    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var imagePlus: UIImageView!
    @IBOutlet weak var imageMinus: UIImageView!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var viewPlusMinus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
