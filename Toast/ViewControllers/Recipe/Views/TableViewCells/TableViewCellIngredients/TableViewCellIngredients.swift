//
//  TableViewCellIngredients.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewCellIngredients: UITableViewCell {

    @IBOutlet weak var buttonAddIngredients: UIButton!
    @IBOutlet weak var textFieldQty: UITextField!
    @IBOutlet weak var textFieldUnit: UITextField!
    @IBOutlet weak var textViewItemName: UITextView!
    @IBOutlet var viewItemBack: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
