//
//  TableViewHeaderAddOneDetails.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewHeaderAddOneDetails: UITableViewHeaderFooterView {

    @IBOutlet weak var imageVeg: UIImageView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet var collTags: UICollectionView!
    @IBOutlet var heightColl: NSLayoutConstraint!
    @IBOutlet var bottomColl: NSLayoutConstraint!
    
    var arrTags: [String]?
    
    
    func initCollectionView() {
        let nib = UINib(nibName: "CellTags", bundle: nil)
        collTags.register(nib, forCellWithReuseIdentifier: "CellTags")
        collTags.dataSource = self
        collTags.delegate = self
        collTags.reloadData()
    }
}

extension TableViewHeaderAddOneDetails: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTags?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellTags = collectionView.dequeueReusableCell(withReuseIdentifier: "CellTags", for: indexPath) as! CellTags
        cell.lblTags.text = arrTags?[indexPath.row]
        print(cell.lblTags.font ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sTag = arrTags?[indexPath.row] ?? ""
        let width = sTag.width(withConstrainedHeight: collectionView.frame.height, font: UIFont(name: "Manrope-SemiBold", size: 10)!)
        return CGSize(width: width+20, height: collectionView.frame.height)
    }
}
