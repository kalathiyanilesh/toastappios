//
//  TableViewHeaderEditRecipe.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewHeaderEditRecipe: UITableViewHeaderFooterView {

    @IBOutlet var buttonClose: UIButton!
    @IBOutlet var buttonManageImage: UIButton!
    @IBOutlet var buttonDeleteRecipe: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageAdd: UIImageView!
    @IBOutlet weak var labelManage: UILabel!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
