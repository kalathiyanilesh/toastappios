//
//  TableViewHeaderYourRecipe.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TableViewHeaderYourRecipe: UITableViewHeaderFooterView {

    @IBOutlet var buttonBack: UIButton!
    @IBOutlet var buttonShare: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
