//
//  CollectionViewCellRecipeImage.swift
//  toastdemo
//
//  Created by Nikul on 06/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CollectionViewCellRecipeImage: UICollectionViewCell {

    @IBOutlet weak var imageViewRecipe: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
