//
//  RecipeDetailsVC.swift
//  Toast
//
//  Created by iMac on 01/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SwiftyJSON

class CellRecipeDetails: UITableViewCell {
    @IBOutlet weak var imageRecipeOwner: UIImageView!
    @IBOutlet weak var labelOwnerName: UILabel!
    @IBOutlet weak var labelRecipeName: UILabel!
    @IBOutlet weak var labelRecipedec: UILabel!
    @IBOutlet weak var labelPT: UILabel!
    @IBOutlet weak var labelServes: UILabel!
    @IBOutlet weak var labelCT: UILabel!
    @IBOutlet weak var labelRecipeType: UILabel!
    @IBOutlet weak var imageViewVegNoVeg: UIImageView!
    @IBOutlet var imgVerified: UIImageView!
}

class CellRecipeComment: UITableViewCell {
    @IBOutlet var imguser: UIImageView!
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lblcomment: UILabel!
}

class CellImageRecipe: UICollectionViewCell {
    @IBOutlet weak var imageViewRecipe: UIImageView!
}

class RecipeDetailsVC: UIViewController {
    @IBOutlet weak var textFieldComment: UITextField!
    @IBOutlet weak var buttonLikeAndUnlike: UIButton!
    @IBOutlet weak var imageViewLikeAndUnlike: UIImageView!
    @IBOutlet weak var labelLike: UILabel!
    @IBOutlet weak var tableViewRecipeDetails: UITableView!
    @IBOutlet var collImageRecipe: UICollectionView!
    
    var objectRecipeDetail: TRecipeDetail!
    var modelLikeRecipe: LikeRecipeModel!
    var modelCommentRecipe: TComments!
    
    struct YourIngredientModel {
        var qty: String?
        var unit: String?
        var itemName: String?
    }
    
    struct YourMethodModel {
        var addStep: String?
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        //getRecipeDetails()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI() {
        textFieldComment.delegate = self
        setupLike()
    }
    
    private func setupModel() {
        
    }
    
    private func setupLike() {
        if objectRecipeDetail.isLiked ?? "0" == "1" {
            imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "like")
        } else {
            imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "dislike")
        }
        labelLike.text = objectRecipeDetail.likesCount ?? "0"
        
    }

    @IBAction func btnaddcomment(_ sender: Any) {
        self.view.endEditing(true)
        textFieldComment.text = TRIM(string: textFieldComment.text ?? "")
        if textFieldComment.text?.count ?? 0 > 0 {
            addReciepComment(comment: textFieldComment.text ?? "")
        }
    }
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonShareTouchUpInside(_ sender: Any) {
        var strIngredients = "Ingredients:\n"
        
        var arrayIngredients = [YourIngredientModel]()
        for model in (self.objectRecipeDetail.ingredients ?? [TIngredients]()) {
            
            var ingredientModel = YourIngredientModel()
            ingredientModel.qty = model.qty ?? ""
            ingredientModel.unit = model.unit ?? ""
            ingredientModel.itemName = model.name ?? ""
            
            arrayIngredients.append(ingredientModel)
            
        }
        
        for model in arrayIngredients {
            strIngredients = strIngredients.appending("- \(model.qty ?? "") \(model.unit ?? "") \(model.itemName ?? "")\n")
        }
        
        var strMethods = "Instructions:\n"
        
        var arrayMethods = [YourMethodModel]()
        for stringNote in (objectRecipeDetail.instructions ?? [String]()) {
            
            var methodsModel = YourMethodModel()
            methodsModel.addStep = stringNote
            
            arrayMethods.append(methodsModel)
        }
        
        var intValue = 0
        for models in arrayMethods {
            intValue += 1
            strMethods = strMethods.appending("\(intValue). \(models.addStep ?? "")\n")
        }
        
        let appLink = "Find us on Play Store: https://play.google.com/store/apps/details?id=com.rareblue.toastapp\n\nFind us on App Store: https://apps.apple.com/in/app/toastapp/id1510293968"
        
        let mainString = "Hey! I found this interesting recipe \(objectRecipeDetail.name ?? "") on ToastApp. \n\n\(strIngredients)\n\(strMethods)\n\n\(appLink)"
        
        let textShare = [ mainString ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonLikeAndUnlikeTouchUpInside(_ sender: Any) {
        if objectRecipeDetail.isLiked ?? "0" == "1" {
            if objectRecipeDetail.likeId != nil && objectRecipeDetail.likeId != "" {
                unLikeRecipeAPICall(likeID: objectRecipeDetail.likeId ?? "")
            } else if modelLikeRecipe != nil {
                if modelLikeRecipe.likeId != nil && modelLikeRecipe.likeId != "" {
                    unLikeRecipeAPICall(likeID: modelLikeRecipe.likeId ?? "")
                }
            }
        } else {
            likeRecipeAPICall()
        }
    }
    
    @IBAction func btncheckrecipe(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "CheckRecipeVC") as! CheckRecipeVC
        vc.recipeModel = objectRecipeDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func unLikeRecipeAPICall(likeID: String) {
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(objectRecipeDetail.recipeId ?? "")/likes/\(likeID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .delete,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                self.objectRecipeDetail.isLiked = "0"
                self.objectRecipeDetail.likeId = ""
                var intLikeCount = (Int(self.objectRecipeDetail.likesCount ?? "0") ?? 0)
                if intLikeCount > 0 {
                    intLikeCount -= 1
                } else {
                    intLikeCount = 0
                }
                
                self.objectRecipeDetail.likesCount = "\(intLikeCount)"
                
                if self.objectRecipeDetail.isLiked ?? "0" == "1" {
                    self.imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "like")
                } else {
                    self.imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "dislike")
                }
                self.labelLike.text = self.objectRecipeDetail.likesCount ?? "0"
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    private func likeRecipeAPICall() {
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(objectRecipeDetail.recipeId ?? "")/likes"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .post,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                let decoder = JSONDecoder()
                do {
                    self.modelLikeRecipe = try decoder.decode(LikeRecipeModel.self, from: JSON(responseDict)["like"].rawData())
                    if self.modelLikeRecipe != nil {
                        self.objectRecipeDetail.isLiked = "1"
                        
                        var intLikeCount = (Int(self.objectRecipeDetail.likesCount ?? "0") ?? 0)
                        intLikeCount += 1
                        
                        self.objectRecipeDetail.likesCount = "\(intLikeCount)"
                        
                        if self.objectRecipeDetail.isLiked ?? "0" == "1" {
                            self.imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "like")
                        } else {
                            self.imageViewLikeAndUnlike.image = #imageLiteral(resourceName: "dislike")
                        }
                        self.labelLike.text = self.objectRecipeDetail.likesCount ?? "0"
                        
                    }
                } catch {
                    showMessage("Try again!" )
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    private func addReciepComment(comment: String) {
        self.view.endEditing(true)
        let dictParam: NSDictionary = ["content":"\(comment)"]
        let dic: NSDictionary = ["comment":dictParam]
        
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(objectRecipeDetail.recipeId ?? "")/comments"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: dic,
            method: .post,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                let dic = responseDict?.object(forKey: "comment") as! NSDictionary
                let obj: TComments = TComments.init(object: dic)
                self.objectRecipeDetail.comments?.append(obj)
                self.textFieldComment.text = ""
                self.tableViewRecipeDetails.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    private func deleteRecipeComment(row: Int) {
        self.view.endEditing(true)
        let model = objectRecipeDetail.comments?[row]
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "\(objectRecipeDetail.recipeId ?? "")/comments/\(model?.commentId ?? "")"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .delete,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                if self.objectRecipeDetail.comments?.count ?? 0 > 0 {
                    self.objectRecipeDetail.comments?.remove(at: row)
                }
                self.tableViewRecipeDetails.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
}

//MARK:- UITableViewDelegate METHOD
extension RecipeDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return objectRecipeDetail.comments?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell : CellRecipeDetails = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetails", for: indexPath) as! CellRecipeDetails
            cell.imageRecipeOwner.sd_setImage(with: URL(string: objectRecipeDetail.publisher?.avatarUrl ?? ""), placeholderImage: USER_PLACE_HOLDER)
            cell.labelOwnerName.text = objectRecipeDetail.publisher?.name ?? ""
            
            if objectRecipeDetail.recipeType ?? "" == "non_veg" {
                cell.imageViewVegNoVeg.image = #imageLiteral(resourceName: "ic_nonveg")
            } else {
                cell.imageViewVegNoVeg.image = #imageLiteral(resourceName: "ic_veg")
            }
            
            cell.labelRecipeName.text = objectRecipeDetail.name ?? ""
            cell.labelRecipedec.text = objectRecipeDetail.descriptionValue ?? ""
            
            if objectRecipeDetail.preparationHours != nil && objectRecipeDetail.preparationHours != "" && Int(objectRecipeDetail.preparationHours ?? "0") != 0 {
                cell.labelPT.text = "PREP: \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.preparationHours ?? "")) ?? 0)) HOURS \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.preparationMins ?? "")) ?? 0)) MINS"
            } else {
                cell.labelPT.text = "PREP: \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.preparationMins ?? "")) ?? 0)) MINS"
            }
           
            if objectRecipeDetail.cookingHours != nil && objectRecipeDetail.cookingHours != "" && Int(objectRecipeDetail.cookingHours ?? "0") != 0 {
                cell.labelCT.text = "COOKING: \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.cookingHours ?? "")) ?? 0)) HOURS \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.cookingMins ?? "")) ?? 0)) MINS"
            } else {
                cell.labelCT.text = "COOKING: \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.cookingMins ?? "")) ?? 0)) MINS"
            }
            
            cell.labelServes.text = "SERVES: \(String(format: "%02d", Int(TRIM(string: objectRecipeDetail.recipeYield ?? "")) ?? 0))"
            cell.labelRecipeType.text = "VEG"
            if objectRecipeDetail.recipeType ?? "" == "non_veg" {
                cell.labelRecipeType.text = "NON-VEG"
            }
            
            cell.imgVerified.isHidden = true
            if objectRecipeDetail.publisher?.isverified == "1" {
                cell.imgVerified.isHidden = false
            }
            
            return cell
        default:
            let cell : CellRecipeComment = tableView.dequeueReusableCell(withIdentifier: "CellRecipeComment", for: indexPath) as! CellRecipeComment
            let model = objectRecipeDetail.comments?[indexPath.row]
            cell.imguser.sd_setImage(with: URL(string: model?.publisher?.avatarUrl ?? ""), placeholderImage: USER_PLACE_HOLDER)
            cell.lblname.text = model?.publisher?.name ?? ""
            cell.lblcomment.text = model?.content ?? ""
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return false
        default:
            let model = objectRecipeDetail.comments?[indexPath.row]
            if model?.publisher?.userId ?? "" == CUSTOMER_ID {
                return true
            }
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            break
        default:
            let model = objectRecipeDetail.comments?[indexPath.row]
            if model?.publisher?.userId ?? "" == CUSTOMER_ID {
                if editingStyle == .delete {
                    deleteRecipeComment(row: indexPath.row)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension RecipeDetailsVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            
        }
    }
    
}

extension RecipeDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectRecipeDetail.imagesUrls?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellImageRecipe = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImageRecipe", for: indexPath) as! CellImageRecipe
        let arrimages = objectRecipeDetail.imagesUrls
        cell.imageViewRecipe.sd_setImage(with: URL(string: arrimages?[indexPath.row] ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}
