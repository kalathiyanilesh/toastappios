//
//  RecipeVC.swift
//  Toast
//
//  Created by iMac on 30/04/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RecipeVC: UIViewController {

    @IBOutlet var viewPage: UIView!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var btnSearch: UIButton!
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.alpha = 0
        SetPageMenu()
        txtSearch.addTarget(self, action: #selector(searchRecipe(_ :)), for: .editingChanged)
    }
    
    @objc func searchRecipe(_ textField:UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(apiSearchRecipe), object: nil)
            self.perform(#selector(apiSearchRecipe), with: nil, afterDelay: 0.5)
        } else {
            txtSearch.text = ""
            RemoveSearchRecipe()
        }
    }
    
    func RemoveSearchRecipe() {
        UserDefaults.standard.removeObject(forKey: "searchedrecipe")
        UserDefaults.standard.synchronize()
        APP_DELEGATE.isPostNoti = true
        NotificationCenter.default.post(name: Notification.Name(RECIPE_PAGE_REFRESH), object: nil)
    }
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsearch(_ sender: Any) {
        if viewSearch.alpha == 0 {
            UIView.animate(withDuration: 0.35, animations: {
                self.viewSearch.alpha = 1
            }) { (finished) in
                self.txtSearch.becomeFirstResponder()
            }
        } else {
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.35, animations: {
                self.viewSearch.alpha = 0
            }) { (finished) in
                self.txtSearch.text = ""
                self.RemoveSearchRecipe()
            }
        }
    }
    
    @IBAction func btnoptions(_ sender: UIButton) {
        SetButtonColors(sender)
        pageMenu?.moveToPage(sender.tag-1)
    }
    
    func SetButtonColors(_ sender: UIButton) {
        for i in 1..<3 {
            let btn = self.view.viewWithTag(i) as! UIButton
            btn.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        }
        sender.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
    }
}

//MARK:- SET PAGEMENU
extension RecipeVC {
    func SetPageMenu() {
        pageMenu?.view.removeFromSuperview()
        pageMenu?.removeFromParent()
        
        var arrController : [UIViewController] = []
    
        let other = loadVC(strStoryboardId: SB_RECIPE, strVCId: "OthersRecipeVC")
        arrController.append(other)
        
        let your = loadVC(strStoryboardId: SB_RECIPE, strVCId: "YourRecipeVC")
        arrController.append(your)
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
    
    @objc func apiSearchRecipe() {
        if TRIM(string: txtSearch.text ?? "").count <= 0 { return }
        let searchText = txtSearch.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "search?term=\(searchText)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                let arrData = responseDict?.object(forKey: "recipes") as! NSArray
                UserDefaults.standard.set(arrData, forKey: "searchedrecipe")
                UserDefaults.standard.synchronize()
                APP_DELEGATE.isPostNoti = true
                NotificationCenter.default.post(name: Notification.Name(RECIPE_PAGE_REFRESH), object: nil)
            } else {
                print(error?.localizedDescription ?? "Try again!")
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}
