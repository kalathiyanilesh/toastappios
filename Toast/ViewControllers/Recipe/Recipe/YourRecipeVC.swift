//
//  YourRecipeVC.swift
//  Toast
//
//  Created by iMac on 30/04/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SwiftyJSON

class YourRecipeVC: UIViewController {

    //MARK:- Button Action
    @IBOutlet weak var buttonAddRecipe: UIButton!
    @IBOutlet weak var tableViewYourRecipe: UITableView!
    @IBOutlet var lblemptymsg: UILabel!
    
    var arrayRecipe = [TRecipeDetail]()
    var intPage = 1
    var totalPage = 0
    var isSend = false
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,selector: #selector(notificationCreate),name: NSNotification.Name (k_Recipe_Create),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(notificationCreate),name: NSNotification.Name (k_Recipe_Edit),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(notificationCreate),name: NSNotification.Name (k_Recipe_Delete),object: nil)
        
        getAllYourRecipe()
        tableViewYourRecipe.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.intPage += 1
                self.getAllYourRecipe()
            }else {
                self.tableViewYourRecipe.infiniteScrollingView.stopAnimating()
            }
        }))
    }

    //MARK:- Button Action
    @IBAction func buttonAddRecipeTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "AddRecipeVC") as! AddRecipeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func notificationCreate() {
        
        arrayRecipe = [TRecipeDetail]()
        intPage = 1
        totalPage = 0
        isSend = false
        
        getAllYourRecipe()
        tableViewYourRecipe.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.intPage += 1
                self.getAllYourRecipe()
            }else {
                self.tableViewYourRecipe.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
}

//MARK:- UITableViewDelegate METHOD
extension YourRecipeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblemptymsg.alpha = arrayRecipe.count > 0 ? 0 : 1
        return self.arrayRecipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellallrecipe = tableView.dequeueReusableCell(withIdentifier: "cellallrecipe", for: indexPath) as! cellallrecipe
        let obj: TRecipeDetail = arrayRecipe[indexPath.row]
        let arrimages = obj.imagesUrls
        if arrimages?.count ?? 0 > 0 {
            cell.imageViewRecipe.sd_setImage(with: URL(string: arrimages?[0] ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        }
        cell.labelRecipeName.text = obj.name
        cell.labelLikeCount.text = obj.likesCount
        
        let objpub: TPublisher = obj.publisher!
        cell.imgVerified.isHidden = true
        if objpub.isverified == "1" {
            cell.imgVerified.isHidden = false
        }
        cell.imageViewOwner.sd_setImage(with: URL(string: objpub.avatarUrl ?? ""), placeholderImage: USER_PLACE_HOLDER)
        cell.labelOwnerName.text = objpub.name
        if obj.recipeType == "non_veg" {
            cell.imageViewVeg.image = UIImage(named: "ic_nonveg")
        } else {
            cell.imageViewVeg.image = UIImage(named: "ic_veg")
        }
        if obj.isLiked == "1"  {
            cell.imageLike.image = UIImage(named: "like")
        } else {
            cell.imageLike.image = UIImage(named: "dislike")
        }
        cell.btlike.addTarget(self, action: #selector(btlike(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj: TRecipeDetail = arrayRecipe[indexPath.row]
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "YourRecipeDetailVC") as! YourRecipeDetailVC
        vc.tempRecipeModel = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @objc func btlike(sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableViewYourRecipe)
        let indexPath = tableViewYourRecipe.indexPathForRow(at: buttonPosition)
        let obj: TRecipeDetail = arrayRecipe[indexPath!.row]
        let totalcount = Int(obj.likesCount ?? "0")!
        if obj.isLiked == "1"  {
            unLikeRecipeAPICall(obj: obj)
            obj.isLiked = "0"
            obj.likeId = ""
            obj.likesCount = String(totalcount-1)
        } else {
            likeRecipeAPICall(obj: obj)
            obj.isLiked = "1"
            obj.likesCount = String(totalcount+1)
        }
        tableViewYourRecipe.reloadData()
    }
    
}


//MARK:- API Call
extension YourRecipeVC {
    func getAllYourRecipe() {
        self.isSend = true
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIBaseRecipe + "user_recipes?page=\(intPage)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                let dictpagination: NSDictionary = responseDict?.object(forKey: "pagination") as! NSDictionary
                if let pages = dictpagination.object(forKey: "total_pages") as? String {
                    self.totalPage = Int(pages)!
                }
                let arrData = responseDict?.object(forKey: "recipes") as! NSArray
                if self.intPage == 0 {
                    self.arrayRecipe.removeAll()
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                        self.arrayRecipe.append(obj)
                    }
                    if self.arrayRecipe.count > 0{
                        self.tableViewYourRecipe.showsInfiniteScrolling = true
                    }
                } else {
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                        self.arrayRecipe.append(obj)
                    }
                    self.tableViewYourRecipe.infiniteScrollingView.stopAnimating()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
                if self.intPage != 1 {
                    self.tableViewYourRecipe.infiniteScrollingView.stopAnimating()
                }
            }
            if self.totalPage == self.intPage {
                self.tableViewYourRecipe.showsInfiniteScrolling = false
            }
            if self.arrayRecipe.count <= 0 { self.tableViewYourRecipe.showsInfiniteScrolling = false }
            self.isSend = false
            DispatchQueue.main.async {
                self.tableViewYourRecipe.reloadData()
            }
        }
    }
    
    
}
