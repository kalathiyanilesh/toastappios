//
//  YourRecipeVC.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class YourRecipeDetailVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableViewYourRecipe: UITableView!
    @IBOutlet weak var buttonEditRecipe: UIButton!
    
    //MARK:- Enum
    enum YourRecipeSection: Int,CaseIterable {
        case Details, Ingredients, Methods
        func title() -> String {
            switch self {
            case .Details:
                return "Details"
            case .Ingredients:
                return "Ingredients"
            case .Methods:
                return "Instructions"
            }
        }
    }
    
    enum DetailsSection: Int,CaseIterable {
        case RecipeDetail, RecipeTime
    }
    
    struct YourRecipeModel {
        var recipeName: String?
        var recipeDesc: String?
        var isVegRecipe: Bool = true
        var PTHours: String?
        var PTMintues: String?
        var serves: String?
        var CTHours: String?
        var CTMintues: String?
        var rateRecipe: String?
        var ingredients: [YourIngredientModel]?
        var methods: [YourMethodModel]?
    }
    
    struct YourIngredientModel {
        var qty: String?
        var unit: String?
        var itemName: String?
    }
    
    struct YourMethodModel {
        var addStep: String?
    }
    
    //MARK:- Variable
    var objectAddRecipe = YourRecipeModel()
    var arrayIngredients = [YourIngredientModel]()
    var arrayMethods = [YourMethodModel]()
    var arrayRecipeImages = [String]()
    var tempRecipeModel: TRecipeDetail!
    
    @IBOutlet var viewedit: UIView!
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK:- Setup UI
    private func setupUI() {
        tableViewYourRecipe.delegate = self
        tableViewYourRecipe.dataSource = self
        tableViewYourRecipe.showsVerticalScrollIndicator = false
        tableViewYourRecipe.separatorStyle = .none
        tableViewYourRecipe.contentInset = UIEdgeInsets(top: -20.0, left: 0.0, bottom: 92.0, right: 0.0)
        tableViewYourRecipe.register(UINib(nibName: "TableViewCellRecipeDetail", bundle: nil), forCellReuseIdentifier: "TableViewCellRecipeDetail")
        tableViewYourRecipe.register(UINib(nibName: "TableViewCellRecipeTime", bundle: nil), forCellReuseIdentifier: "TableViewCellRecipeTime")
        tableViewYourRecipe.register(UINib(nibName: "TableViewCellIngredients", bundle: nil), forCellReuseIdentifier: "TableViewCellIngredients")
        tableViewYourRecipe.register(UINib(nibName: "TableViewCellMethods", bundle: nil), forCellReuseIdentifier: "TableViewCellMethods")
        tableViewYourRecipe.register(UINib(nibName: "TableViewHeaderYourRecipe", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderYourRecipe")
        tableViewYourRecipe.register(UINib(nibName: "TableViewHeaderTitle", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderTitle")
        
        setupData()
    }
    
    private func setupData() {
        
        objectAddRecipe.recipeName = tempRecipeModel.name ?? ""
        objectAddRecipe.recipeDesc = tempRecipeModel.descriptionValue ?? ""
        
        if tempRecipeModel.recipeType ?? "" == "non_veg" {
            objectAddRecipe.isVegRecipe = false
        } else {
            objectAddRecipe.isVegRecipe = true
        }
        
        objectAddRecipe.PTHours = tempRecipeModel.preparationHours ?? ""
        objectAddRecipe.PTMintues = tempRecipeModel.preparationMins ?? ""
        objectAddRecipe.serves = tempRecipeModel.recipeYield ?? ""
        objectAddRecipe.CTHours = tempRecipeModel.cookingHours ?? ""
        objectAddRecipe.CTMintues = tempRecipeModel.cookingMins ?? ""
        objectAddRecipe.rateRecipe = (tempRecipeModel.rating ?? "").capitalized
        
        arrayRecipeImages = tempRecipeModel.imagesUrls ?? [String]()
        
        for model in (self.tempRecipeModel.ingredients ?? [TIngredients]()) {
            
            var ingredientModel = YourIngredientModel()
            ingredientModel.qty = model.qty ?? ""
            ingredientModel.unit = model.unit ?? ""
            ingredientModel.itemName = model.name ?? ""
            
            arrayIngredients.append(ingredientModel)
            
        }
        
        for stringNote in (tempRecipeModel.instructions ?? [String]()) {
            
            var methodsModel = YourMethodModel()
            methodsModel.addStep = stringNote
            
            arrayMethods.append(methodsModel)
        }
        
        self.tableViewYourRecipe.reloadData()
        
    }
    
    //MARK:- Button Action
    @IBAction func buttonEditRecipeTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "EditRecipeVC") as! EditRecipeVC
        vc.tempRecipeModel = tempRecipeModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func buttonBackTouchUpInside(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonShareTouchUpInside(_ button: UIButton) {
        
        var strIngredients = "Ingredients:\n"
        
        for model in arrayIngredients {
            strIngredients = strIngredients.appending("- \(model.qty ?? "") \(model.unit ?? "") \(model.itemName ?? "")\n")
        }
        
        var strMethods = "Instructions:\n"
        
        var intValue = 0
        for models in arrayMethods {
            intValue += 1
            strMethods = strMethods.appending("\(intValue). \(models.addStep ?? "")\n")
        }
        
        let appLink = "Find us on Play Store: https://play.google.com/store/apps/details?id=com.rareblue.toastapp\n\nFind us on App Store: https://apps.apple.com/in/app/toastapp/id1510293968"
        
        let mainString = "Hey! I found this interesting recipe \(tempRecipeModel.name ?? "") on ToastApp. \n\n\(strIngredients)\n\(strMethods)\n\n\(appLink)"
        
        let textShare = [ mainString ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }

}

extension YourRecipeDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return YourRecipeSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch YourRecipeSection (rawValue: section)! {
        case .Details:
            return 224.0
        case .Ingredients:
            return 40.0
        case .Methods:
            return 40.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch YourRecipeSection (rawValue: section)! {
        case .Details:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderYourRecipe") as! TableViewHeaderYourRecipe
            headerView.buttonBack.addTarget(self, action: #selector(buttonBackTouchUpInside(_:)), for: .touchUpInside)
            headerView.buttonShare.addTarget(self, action: #selector(buttonShareTouchUpInside(_:)), for: .touchUpInside)
            
            headerView.collectionView.delegate = self
            headerView.collectionView.dataSource = self
            headerView.collectionView.register(UINib(nibName: "CollectionViewCellRecipeImage", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellRecipeImage")
            
            return headerView
        case .Ingredients:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderTitle") as! TableViewHeaderTitle
            headerView.labelTitle.text = YourRecipeSection(rawValue: section)?.title()
            return headerView
        case .Methods:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderTitle") as! TableViewHeaderTitle
            headerView.labelTitle.text = YourRecipeSection(rawValue: section)?.title()
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView (frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch YourRecipeSection (rawValue: section)! {
        case .Details:
            return DetailsSection.allCases.count
        case .Ingredients:
            return arrayIngredients.count
        case .Methods:
            return arrayMethods.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch YourRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                return 245.0
            case .RecipeTime:
                return 165.0
            }
        case .Ingredients:
            return UITableView.automaticDimension
        case .Methods:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch YourRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                return 245.0
            case .RecipeTime:
                return 165.0
            }
        case .Ingredients:
            return 150.0
        case .Methods:
            return 50.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch YourRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellRecipeDetail", for: indexPath) as! TableViewCellRecipeDetail
                cell.selectionStyle = .none
                
                cell.textFieldRecipeName.isEnabled = false
                cell.textFieldRecipeName.keyboardType = .default
                cell.textFieldRecipeName.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldRecipeName.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldRecipeName.text = objectAddRecipe.recipeName ?? ""
                
                cell.textViewRecipeDes.isEditable = false
                cell.textViewRecipeDes.keyboardType = .default
                if objectAddRecipe.recipeDesc != nil && objectAddRecipe.recipeDesc != "" && objectAddRecipe.recipeDesc != "Tell us something about your recipe" {
                    cell.textViewRecipeDes.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                } else {
                    cell.textViewRecipeDes.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                }
                cell.textViewRecipeDes.text = objectAddRecipe.recipeDesc ?? "Tell us something about your recipe"
                
                if objectAddRecipe.isVegRecipe {
                    cell.buttonVeg.setImage(#imageLiteral(resourceName: "ic_round_selected"), for: .normal)
                    cell.buttonNonVeg.setImage(#imageLiteral(resourceName: "ic_round_unselect"), for: .normal)
                } else {
                    cell.buttonVeg.setImage(#imageLiteral(resourceName: "ic_round_unselect"), for: .normal)
                    cell.buttonNonVeg.setImage(#imageLiteral(resourceName: "ic_round_selected"), for: .normal)
                }
                
                return cell
            case .RecipeTime:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellRecipeTime", for: indexPath) as! TableViewCellRecipeTime
                cell.selectionStyle = .none
                
                cell.textFieldPTHours.tag = 100
                cell.textFieldPTHours.keyboardType = .numberPad
                cell.textFieldPTHours.isEnabled = false
                cell.textFieldPTHours.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldPTHours.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldPTHours.text = objectAddRecipe.PTHours ?? ""
                
                cell.textFieldPTMintues.tag = 101
                cell.textFieldPTMintues.keyboardType = .numberPad
                cell.textFieldPTMintues.isEnabled = false
                cell.textFieldPTMintues.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldPTMintues.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldPTMintues.text = objectAddRecipe.PTMintues ?? ""
                
                cell.textFieldCTHours.tag = 102
                cell.textFieldCTHours.keyboardType = .numberPad
                cell.textFieldCTHours.isEnabled = false
                cell.textFieldCTHours.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldCTHours.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldCTHours.text = objectAddRecipe.CTHours ?? ""
                
                cell.textFieldCTMintues.tag = 103
                cell.textFieldCTMintues.keyboardType = .numberPad
                cell.textFieldCTMintues.isEnabled = false
                cell.textFieldCTMintues.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldCTMintues.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldCTMintues.text = objectAddRecipe.CTMintues ?? ""
                
                cell.textFieldServes.tag = 104
                cell.textFieldServes.keyboardType = .numberPad
                cell.textFieldServes.isEnabled = false
                cell.textFieldServes.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldServes.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldServes.text = objectAddRecipe.serves ?? ""
                
                cell.labelrate.text = objectAddRecipe.rateRecipe ?? ""
                cell.imageRate.isHidden = true
                
                return cell
            }
        case .Ingredients:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellIngredients", for: indexPath) as! TableViewCellIngredients
            cell.selectionStyle = .none
            
            let model = arrayIngredients[indexPath.row] as YourIngredientModel
            
            cell.textFieldQty.keyboardType = .numbersAndPunctuation
            cell.textFieldQty.isEnabled = false
            cell.textFieldQty.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            cell.textFieldQty.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            cell.textFieldQty.text = model.qty ?? ""
            
            cell.textFieldUnit.keyboardType = .default
            cell.textFieldUnit.isEnabled = false
            cell.textFieldUnit.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            cell.textFieldUnit.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            cell.textFieldUnit.text = model.unit ?? ""
            
            cell.textViewItemName.keyboardType = .default
            cell.textViewItemName.isEditable = false
            if model.itemName != nil && model.itemName != "" && model.itemName != "Item name" {
                cell.textViewItemName.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            } else {
                cell.textViewItemName.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            }
            cell.textViewItemName.text = model.itemName ?? "Item name"
            
            cell.buttonAddIngredients.setImage(#imageLiteral(resourceName: "minus_inactive"), for: .normal)
            
            return cell
        case .Methods:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellMethods", for: indexPath) as! TableViewCellMethods
            cell.selectionStyle = .none
            
            let model = arrayMethods[indexPath.row] as YourMethodModel
            
            cell.textViewAddStep.isEditable = false
            cell.textViewAddStep.keyboardType = .default
            if model.addStep != nil && model.addStep != "" && model.addStep != "Add step" {
                cell.textViewAddStep.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            } else {
                cell.textViewAddStep.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            }
            cell.textViewAddStep.text = model.addStep ?? "Add step"
            cell.buttonAddMethods.setImage(#imageLiteral(resourceName: "minus_inactive"), for: .normal)
            
            return cell
        }
    }
    
}

extension YourRecipeDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayRecipeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellRecipeImage", for: indexPath) as! CollectionViewCellRecipeImage
        
        let url = arrayRecipeImages[indexPath.row]
        
        cell.imageViewRecipe.sd_setImage(with: URL(string: url), placeholderImage: FOOD_PLACE_HOLDER)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}
