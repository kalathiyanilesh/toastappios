//
//  OthersRecipeVC.swift
//  Toast
//
//  Created by iMac on 30/04/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SwiftyJSON

class cellallrecipe: UITableViewCell {
    
    @IBOutlet weak var labelOwnerName: UILabel!
    @IBOutlet weak var imageViewOwner: UIImageView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var labelLikeCount: UILabel!
    @IBOutlet weak var labelRecipeName: UILabel!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewRecipe: UIImageView!
    @IBOutlet var btlike: UIButton!
    @IBOutlet var imgVerified: UIImageView!
}

class OthersRecipeVC: UIViewController {
    
    @IBOutlet weak var tableViewOtherRecipe: UITableView!
    @IBOutlet weak var buttonFilter: UIButton!
    
    var storedOffsets = [Int: CGFloat]()
    var arrpopularrcipe = [TRecipeDetail]()
    var arrayRecipe = [TRecipeDetail]()
    var intPage = 1
    var totalPage = 0
    var isSend = false
    var isSearchRecipe = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOtherRecipe.keyboardDismissMode = .onDrag

        NotificationCenter.default.addObserver(self,selector: #selector(notificationFilter),name: NSNotification.Name (k_Noti_Filter_Recipe),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(refreshAllRecipe),name: NSNotification.Name (RECIPE_PAGE_REFRESH),object: nil)

        apiGetPopularRecipe()
        apiGetAllRecipe()
        tableViewOtherRecipe.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.intPage += 1
                self.apiGetAllRecipe()
            }else {
                self.tableViewOtherRecipe.infiniteScrollingView.stopAnimating()
            }
        }))
        
    }

    @IBAction func buttonFilterTouchUpInside(_ sender: Any) {
        if arrayRecipe.count <= 0 {
            return
        }
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "FilterRecipeVC") as! FilterRecipeVC
        vc.arrayRecipe = arrayRecipe
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @objc func refreshAllRecipe() {
        if let arrData = UserDefaults.standard.object(forKey: "searchedrecipe") as? NSArray {
            isSearchRecipe = true
            arrayRecipe.removeAll()
            for i in 0..<arrData.count {
                let dictData = arrData[i] as! NSDictionary
                let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                arrayRecipe.append(obj)
            }
            tableViewOtherRecipe.showsInfiniteScrolling = false
            DispatchQueue.main.async {
                self.tableViewOtherRecipe.reloadData()
            }
        } else {
            isSearchRecipe = false
            notificationFilter()
        }
    }
    
    @objc func notificationFilter() {
        
        arrayRecipe = [TRecipeDetail]()
        intPage = 1
        totalPage = 0
        isSend = false
        
        apiGetAllRecipe()
        tableViewOtherRecipe.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.intPage += 1
                self.apiGetAllRecipe()
            }else {
                self.tableViewOtherRecipe.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
}

//MARK:- UITableViewDelegate METHOD
extension OthersRecipeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return nil
        default:
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 33))
            headerView.backgroundColor = self.view.backgroundColor

            let labelDate = UILabel()
            labelDate.frame = CGRect.init(x: 20, y: 0, width: SCREENWIDTH()-0, height: 20)
            labelDate.text = "All Recipes"
            labelDate.font = FontWithSize("Manrope-SemiBold", 16)
            labelDate.textColor = Color_Hex(hex: "#404040")
            labelDate.backgroundColor = self.view.backgroundColor
            
            headerView.addSubview(labelDate)

            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 33
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return arrayRecipe.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell : CellRecipehome = tableView.dequeueReusableCell(withIdentifier: "CellRecipehome", for: indexPath) as! CellRecipehome
            cell.collrecipes.reloadData()
            return cell
        default:
            let cell : cellallrecipe = tableView.dequeueReusableCell(withIdentifier: "cellallrecipe", for: indexPath) as! cellallrecipe
            
            let obj: TRecipeDetail = arrayRecipe[indexPath.row]
            let arrimages = obj.imagesUrls
            if arrimages?.count ?? 0 > 0 {
                cell.imageViewRecipe.sd_setImage(with: URL(string: arrimages?[0] ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
            }
            cell.labelRecipeName.text = obj.name
            cell.labelLikeCount.text = obj.likesCount
            
            let objpub: TPublisher = obj.publisher!
            cell.imgVerified.isHidden = true
            if objpub.isverified == "1" {
                cell.imgVerified.isHidden = false
            }
            cell.imageViewOwner.sd_setImage(with: URL(string: objpub.avatarUrl ?? ""), placeholderImage: USER_PLACE_HOLDER)
            cell.labelOwnerName.text = objpub.name
            if obj.recipeType == "non_veg" {
                cell.imageViewVeg.image = UIImage(named: "ic_nonveg")
            } else {
                cell.imageViewVeg.image = UIImage(named: "ic_veg")
            }
            if obj.isLiked == "1"  {
                cell.imageLike.image = UIImage(named: "like")
            } else {
                cell.imageLike.image = UIImage(named: "dislike")
            }
            
            cell.btlike.addTarget(self, action: #selector(btlike(sender:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrayRecipe[indexPath.row] as TRecipeDetail
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeDetailsVC") as! RecipeDetailsVC
        vc.objectRecipeDetail = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellRecipehome else { return }
            promoCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            promoCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 260
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellRecipehome else { return }
            storedOffsets[indexPath.row] = promoCell.collectionViewOffset
        default:
            break
        }
    }

    @objc func btlike(sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableViewOtherRecipe)
        let indexPath = tableViewOtherRecipe.indexPathForRow(at: buttonPosition)
        let obj: TRecipeDetail = arrayRecipe[indexPath!.row]
        likeunlike(obj)
    }
    
    @objc func btnpopularlike(sender: UIButton!) {
        let indexPath = IndexPath(item: sender.tag-555, section: 0)
        let obj: TRecipeDetail = arrpopularrcipe[indexPath.row]
        likeunlike(obj)
    }
    
    func likeunlike(_ obj: TRecipeDetail) {
        let totalcount = Int(obj.likesCount ?? "0")!
        if obj.isLiked == "1"  {
            unLikeRecipeAPICall(obj: obj)
            obj.isLiked = "0"
            obj.likeId = ""
            obj.likesCount = String(totalcount-1)
        } else {
            likeRecipeAPICall(obj: obj)
            obj.isLiked = "1"
            obj.likesCount = String(totalcount+1)
        }
        tableViewOtherRecipe.reloadData()
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension OthersRecipeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrpopularrcipe.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellCollRecipe = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollRecipe", for: indexPath) as! CellCollRecipe
        
        let model = arrpopularrcipe[indexPath.row] as TRecipeDetail
        
        let arrimages = model.imagesUrls
        if arrimages?.count ?? 0 > 0 {
            cell.imgitem.sd_setImage(with: URL(string: arrimages?[0] ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        }
        
        if model.isLiked ?? "0" == "1" {
            cell.imglikedislike.image = #imageLiteral(resourceName: "like")
        } else {
            cell.imglikedislike.image = #imageLiteral(resourceName: "dislike")
        }
        
        cell.lbltotallike.text = model.likesCount ?? "0"
        
        if model.recipeType ?? "" == "non_veg" {
            cell.imgvegnonveg.image = #imageLiteral(resourceName: "ic_nonveg")
        } else {
            cell.imgvegnonveg.image = #imageLiteral(resourceName: "ic_veg")
        }
        
        cell.lblitemname.text = model.name ?? ""
        cell.imgVerified.isHidden = true
        if model.publisher?.isverified == "1" {
            cell.imgVerified.isHidden = false
        }
        cell.imgowner.sd_setImage(with: URL(string: model.publisher?.avatarUrl ?? ""), placeholderImage: USER_PLACE_HOLDER)
        cell.lblnameowner.text = model.publisher?.name ?? ""
        
        cell.btnlike.tag = indexPath.row+555
        cell.btnlike.addTarget(self, action: #selector(btnpopularlike(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        let model = arrpopularrcipe[indexPath.row] as TRecipeDetail
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeDetailsVC") as! RecipeDetailsVC
        vc.objectRecipeDetail = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:150, height:195)
    }
}

//MARK: API CALLING
extension OthersRecipeVC {
    func apiGetPopularRecipe() {
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIRecipe
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                self.arrpopularrcipe.removeAll()
                let arrData = responseDict?.object(forKey: "recipes") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                    self.arrpopularrcipe.append(obj)
                }
                if self.arrpopularrcipe.count > 0{
                    self.tableViewOtherRecipe.reloadData()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetAllRecipe() {
        if isSearchRecipe { return }
        self.view.endEditing(true)
        
        var objectFilter = FilterRecipeModel()
        if let model = UserDefaults.standard.getJsonObject(FilterRecipeModel.self, forKey: k_Filter_Recipe)  {
            objectFilter = model
        }
        
        let service = SERVER_RECIPE_URL + APICreateRecipe + "?page=\(intPage)&recipe_type=\(objectFilter.strVegNoVeg)&is_liked=\(objectFilter.isLiked)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIBaseRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                let dictpagination: NSDictionary = responseDict?.object(forKey: "pagination") as! NSDictionary
                if let pages = dictpagination.object(forKey: "total_pages") as? String {
                    self.totalPage = Int(pages)!
                }
                let arrData = responseDict?.object(forKey: "recipes") as! NSArray
                if self.intPage == 1 {
                    self.arrayRecipe.removeAll()
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                        self.arrayRecipe.append(obj)
                    }
                    if self.arrayRecipe.count > 0 {
                        self.tableViewOtherRecipe.showsInfiniteScrolling = true
                    }
                } else {
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                        self.arrayRecipe.append(obj)
                    }
                    self.tableViewOtherRecipe.infiniteScrollingView.stopAnimating()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
                if self.intPage != 1 {
                    self.tableViewOtherRecipe.infiniteScrollingView.stopAnimating()
                }
            }
            
            if self.totalPage == self.intPage {
                self.tableViewOtherRecipe.showsInfiniteScrolling = false
            }
            
            if self.arrayRecipe.count <= 0 { self.tableViewOtherRecipe.showsInfiniteScrolling = false }
            self.isSend = false
            DispatchQueue.main.async {
                self.tableViewOtherRecipe.reloadData()
            }
        }
    }
    
}

