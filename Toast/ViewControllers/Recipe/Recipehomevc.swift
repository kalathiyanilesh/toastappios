//
//  Recipehomevc.swift
//  Toast
//
//  Created by iMac on 30/04/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SwiftyJSON

class CellRecipehome: UITableViewCell {
    @IBOutlet var btnviewall: UIButton!
    @IBOutlet var collrecipes: UICollectionView!
}

extension CellRecipehome {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        collrecipes.delegate = dataSourceDelegate
        collrecipes.dataSource = dataSourceDelegate
        collrecipes.tag = row
        collrecipes.setContentOffset(collrecipes.contentOffset, animated:false)
        collrecipes.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collrecipes.contentOffset.x = newValue }
        get { return collrecipes.contentOffset.x }
    }
}

class CellCollRecipe: UICollectionViewCell {
    @IBOutlet var imgitem: UIImageView!
    @IBOutlet var imglikedislike: UIImageView!
    @IBOutlet var btnlike: UIButton!
    @IBOutlet var lbltotallike: UILabel!
    @IBOutlet var lblitemname: UILabel!
    @IBOutlet var imgowner: UIImageView!
    @IBOutlet var lblnameowner: UILabel!
    @IBOutlet var imgvegnonveg: UIImageView!
    @IBOutlet var imgVerified: UIImageView!
}


class Recipehomevc: UIViewController {

    @IBOutlet var tblrecipe: UITableView!
    @IBOutlet weak var buttonIntroduce: UIButton!
    @IBOutlet var imguser: UIImageView!
    var storedOffsets = [Int: CGFloat]()
    var recipeHeight: CGFloat = 195
    var arrpopularrcipe = [TRecipeDetail]()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblrecipe.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        apiGetUserInfo()
        apiGetPopularRecipe()
    }

    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        apiGetPopularRecipe()
    }

    //MARK:- Button Action
    @IBAction func buttonAddRecipeTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "AddRecipeVC") as! AddRecipeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonIntroduceTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnuser(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "ReviewOrderVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnnotification(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "NotificationVC") as! NotificationVC
        vc.isFromRecipe = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnscan(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "TabVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

//MARK:- UITableViewDelegate METHOD
extension Recipehomevc: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellRecipehome = tableView.dequeueReusableCell(withIdentifier: "CellRecipehome", for: indexPath) as! CellRecipehome
        cell.btnviewall.addTarget(self, action: #selector(btnviewall(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellRecipehome else { return }
            promoCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            promoCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let nooflines: Int = Int((CGFloat(self.arrpopularrcipe.count)/2.0).rounded(.up))
        let space: CGFloat = 20
        var Height = (recipeHeight*CGFloat(nooflines))+(space*(CGFloat(nooflines)-1))+50
        if Height < 50 {
            Height = 50
        }
        return Height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellRecipehome else { return }
            storedOffsets[indexPath.row] = promoCell.collectionViewOffset
        default:
            break
        }
    }

    @objc func btnviewall(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeVC") as! RecipeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension Recipehomevc: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrpopularrcipe.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellCollRecipe = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollRecipe", for: indexPath) as! CellCollRecipe
        
        let model = arrpopularrcipe[indexPath.row] as TRecipeDetail
        
        let arrimages = model.imagesUrls
        if arrimages?.count ?? 0 > 0 {
            cell.imgitem.sd_setImage(with: URL(string: arrimages?[0].RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        }
        
        if model.isLiked ?? "0" == "1" {
            cell.imglikedislike.image = #imageLiteral(resourceName: "like")
        } else {
            cell.imglikedislike.image = #imageLiteral(resourceName: "dislike")
        }
        
        cell.lbltotallike.text = model.likesCount ?? "0"
        
        if model.recipeType ?? "" == "non_veg" {
            cell.imgvegnonveg.image = #imageLiteral(resourceName: "ic_nonveg")
        } else {
            cell.imgvegnonveg.image = #imageLiteral(resourceName: "ic_veg")
        }
        
        cell.lblitemname.text = model.name ?? ""
        
        cell.imgVerified.isHidden = true
        if model.publisher?.isverified == "1" {
            cell.imgVerified.isHidden = false
        }
 
        cell.imgowner.sd_setImage(with: URL(string: model.publisher?.avatarUrl?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
        cell.lblnameowner.text = model.publisher?.name ?? ""
        cell.btnlike.tag = indexPath.row+555
        cell.btnlike.addTarget(self, action: #selector(btnlike(sender:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = arrpopularrcipe[indexPath.row] as TRecipeDetail
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeDetailsVC") as! RecipeDetailsVC
        vc.objectRecipeDetail = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (SCREENWIDTH()-60)/2
        return CGSize(width:width, height:recipeHeight)
    }
    
    @objc func btnlike(sender: UIButton!) {
        let indexPath = IndexPath(item: sender.tag-555, section: 0)
        let obj: TRecipeDetail = arrpopularrcipe[indexPath.row]
        let totalcount = Int(obj.likesCount ?? "0")!
        if obj.isLiked == "1"  {
            unLikeRecipeAPICall(obj: obj)
            obj.isLiked = "0"
            obj.likeId = ""
            obj.likesCount = String(totalcount-1)
        } else {
            likeRecipeAPICall(obj: obj)
            obj.isLiked = "1"
            obj.likesCount = String(totalcount+1)
        }
        tblrecipe.reloadData()
    }
}

//MARK: API CALLING
extension Recipehomevc {
    func apiGetPopularRecipe() {
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIRecipe
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.arrpopularrcipe.removeAll()
                let arrData = responseDict?.object(forKey: "recipes") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                    self.arrpopularrcipe.append(obj)
                }
                self.tblrecipe.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    func apiGetUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let objUser = TUser.init(object: dictData)
                    self.imguser.sd_setImage(with: URL(string: objUser.profilePicture?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}
