//
//  AddRecipeVC.swift
//  toastdemo
//
//  Created by Nikul on 05/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Photos
import OpalImagePicker
import FirebaseStorage
import IQKeyboardManagerSwift

class AddRecipeVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableViewAddRecipe: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var buttonCancelTool: UIBarButtonItem!
    @IBOutlet weak var buttonDoneTool: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var buttonPublishYourRecipe: UIButton!
    
   // var isImae
    
    //MARK:- Enum
    enum AddRecipeSection: Int,CaseIterable {
        case Details, Ingredients, Methods
        func title() -> String {
            switch self {
            case .Details:
                return "Details"
            case .Ingredients:
                return "Ingredients"
            case .Methods:
                return "Instructions"
            }
        }
    }
    
    enum DetailsSection: Int,CaseIterable {
        case RecipeDetail, RecipeTime
    }
    
    struct AddRecipeModel {
        var recipeName: String?
        var recipeDesc: String?
        var isVegRecipe: Bool = true
        var PTHours: String?
        var PTMintues: String?
        var serves: String?
        var CTHours: String?
        var CTMintues: String?
        var rateRecipe: String = "Easy"
        var ingredients: [AddIngredientModel]?
        var methods: [AddMethodModel]?
    }
    
    struct AddIngredientModel {
        var qty: String?
        var unit: String?
        var itemName: String?
    }
    
    struct AddMethodModel {
        var addStep: String?
    }
    
    //MARK:- Variables
    var objectAddRecipe = AddRecipeModel()
    var arrayIngredients = [AddIngredientModel]()
    var arrayMethods = [AddMethodModel]()
    var arrayRate = ["Easy","Moderate","Hard"]
    var arrayRecipeImages = [String]()
    
    var isRecipeNameShadow: Bool = false
    var isServesShadow: Bool = false
    var isPTHHShadow: Bool = false
    var isPTMMShadow: Bool = false
    var isCTHHShadow: Bool = false
    var isCTMMShadow: Bool = false
    var isQTY: Bool = false
    var isUnit: Bool = false
    var isItemName: Bool = false
    var isMethodShadow: Bool = false
    
    var isAlreadyShowQtyShadow: Bool = false
    var isAlreadyShowUnitShadow: Bool = false
    var isAlreadyShowItemNameShadow: Bool = false
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK:- Setup UI
    private func setupUI() {
        
        pickerView.alpha = 0
        toolbar.alpha = 0
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        arrayIngredients.append(AddIngredientModel())
        arrayMethods.append(AddMethodModel())
        
        tableViewAddRecipe.delegate = self
        tableViewAddRecipe.dataSource = self
        tableViewAddRecipe.showsVerticalScrollIndicator = false
        tableViewAddRecipe.separatorStyle = .none
        tableViewAddRecipe.contentInset = UIEdgeInsets(top: -20.0, left: 0.0, bottom: 92.0, right: 0.0)
        tableViewAddRecipe.register(UINib(nibName: "TableViewCellRecipeDetail", bundle: nil), forCellReuseIdentifier: "TableViewCellRecipeDetail")
        tableViewAddRecipe.register(UINib(nibName: "TableViewCellRecipeTime", bundle: nil), forCellReuseIdentifier: "TableViewCellRecipeTime")
        tableViewAddRecipe.register(UINib(nibName: "TableViewCellIngredients", bundle: nil), forCellReuseIdentifier: "TableViewCellIngredients")
        tableViewAddRecipe.register(UINib(nibName: "TableViewCellMethods", bundle: nil), forCellReuseIdentifier: "TableViewCellMethods")
        tableViewAddRecipe.register(UINib(nibName: "TableViewHeaderAddRecipe", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderAddRecipe")
        tableViewAddRecipe.register(UINib(nibName: "TableViewHeaderTitle", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderTitle")
    }
    
    //MARK:- Button Action
    @IBAction func buttonCancelToolTouchUpInside(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.pickerView.alpha = 0
            self.toolbar.alpha = 0
        }
    }
    
    @IBAction func buttonPublishYourRecipeTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)
        if validation() {
            setupAddRecipeData()
        } else {
            tableViewAddRecipe.reloadData()
        }
    }
    
    @IBAction func buttonDoneToolTouchUpInside(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.pickerView.alpha = 0
            self.toolbar.alpha = 0
        }
    }
    
    @objc func buttonCloseTouchUpInside(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonAddImageTouchUpInside(_ button: UIButton) {
        if arrayRecipeImages.count == 3{
            return
        }
        let imagePicker = OpalImagePickerController()
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.maximumSelectionsAllowed = 3-arrayRecipeImages.count
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = "You cannot select that many images!"
        imagePicker.configuration = configuration
        presentOpalImagePickerController(imagePicker, animated: true,
        select: { (assets) in
            let arrimg = NSMutableArray()
            for assest in assets {
                let img = getAssetThumbnail(asset: assest, PHImageManagerMaximumSize)
                arrimg.add(img)
            }
            self.uploadimages(arrimg, forIndex: 0)
            self.dismiss(animated: true, completion: nil)
        }, cancel: {
            //Cancel
        })
    }
    
    func uploadimages(_ arrimg: NSMutableArray, forIndex index:Int) {
        if index < arrimg.count {
            showLoaderHUD(strMessage: "")
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let data = compressImage(image: arrimg[index] as! UIImage)
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "ddMMyyyyHH:mm:ss.SSSS"
            let sImageName = "IMG-" + "\(dateFormatter.string(from: Date()))" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())" + "\(self.GetRandomIntValue())"
            print("imageName: ",sImageName)
            
            let riversRef = storageRef.child("\(IMAGE_FOLDER)/\(sImageName).jpg")
            riversRef.putData(data as Data, metadata: metaData) { (metadata, error) in
                if let error = error {
                    print(error.localizedDescription)
                    self.uploadimages(arrimg, forIndex: index + 1)
                } else {
                    riversRef.downloadURL { (url, error) in
                        self.arrayRecipeImages.append("\(url!)")
                        print(self.arrayRecipeImages)
                        self.tableViewAddRecipe.reloadData()
                        self.uploadimages(arrimg, forIndex: index + 1)
                    }
                }
            }
        } else {
            hideLoaderHUD()
        }
    }
    
    func GetRandomIntValue() -> Int {
        return Int(arc4random_uniform(UInt32(10)))
    }
    
    
    
    @objc func buttonVegTouchUpInside(_ button: UIButton) {
        self.view.endEditing(true)
        objectAddRecipe.isVegRecipe = true
        let indexPath = IndexPath(item: DetailsSection.RecipeDetail.rawValue, section: AddRecipeSection.Details.rawValue)
        tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
    }
    
    @objc func buttonNonVegTouchUpInside(_ button: UIButton) {
        self.view.endEditing(true)
        objectAddRecipe.isVegRecipe = false
        let indexPath = IndexPath(item: DetailsSection.RecipeDetail.rawValue, section: AddRecipeSection.Details.rawValue)
        tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
    }
    
    @objc func buttonAddIngredientsTouchUpInside(_ button: UIButton) {
        self.view.endEditing(true)
        let viewPosition:CGPoint = button.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            if arrayIngredients.count - 1 > indexPath.row {
                arrayIngredients.remove(at: indexPath.row)
                self.tableViewAddRecipe.reloadData()
            } else {
                arrayIngredients.append(AddIngredientModel())
                self.tableViewAddRecipe.reloadData()
            }
        }
    }
    
    @objc func buttonAddMethodsTouchUpInside(_ button: UIButton) {
        self.view.endEditing(true)
        let viewPosition:CGPoint = button.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            if arrayMethods.count - 1 > indexPath.row {
                arrayMethods.remove(at: indexPath.row)
                self.tableViewAddRecipe.reloadData()
            } else {
                arrayMethods.append(AddMethodModel())
                self.tableViewAddRecipe.reloadData()
            }
        }
    }
    
    @objc func buttonRateTouchUpInside(_ button: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.3) {
            self.pickerView.alpha = 1
            self.toolbar.alpha = 1
        }
    }
    
    func makeallfasle() {
        isRecipeNameShadow = false
        isServesShadow = false
        isPTHHShadow = false
        isPTMMShadow = false
        isCTHHShadow = false
        isCTMMShadow = false
        isMethodShadow = false
        isQTY = false
        isUnit = false
        isItemName = false
        isAlreadyShowQtyShadow = false
        isAlreadyShowUnitShadow = false
        isAlreadyShowItemNameShadow = false
    }
    
    //MARK:- Validation
    func validation() -> Bool {
        makeallfasle()
        if arrayRecipeImages.count <= 0 {
            showMessage("Please choose recipe picture")
            return false
        } else if objectAddRecipe.recipeName == nil || objectAddRecipe.recipeName == "" {
            showMessage("Please enter recipe name")
            isRecipeNameShadow = true
            return false
        } else if (objectAddRecipe.PTHours ?? "").count <= 0 {
            showMessage("Please enter recipe preparation hours")
            isPTHHShadow = true
            return false
        } else if (objectAddRecipe.PTHours ?? "").count >= 3 {
            showMessage("Please enter proper preparation hours")
            isPTHHShadow = true
            return false
        } else if (objectAddRecipe.PTMintues ?? "").count <= 0 {
            showMessage("Please enter recipe preparation mintues")
            isPTMMShadow = true
            return false
        } else if (objectAddRecipe.PTMintues ?? "").count >= 3 || (Int(TRIM(string: objectAddRecipe.PTMintues ?? "")) ?? 0) > 59 {
            showMessage("Please enter proper preparation mintues")
            isPTMMShadow = true
            return false
        } else if (objectAddRecipe.serves ?? "").count <= 0 {
            showMessage("Please enter recipe serves")
            isServesShadow = true
            return false
        } else if (objectAddRecipe.serves ?? "").count >= 3 {
            showMessage("Please enter proper serves")
            isServesShadow = true
            return false
        } else if (objectAddRecipe.CTHours ?? "").count <= 0 {
            showMessage("Please enter recipe cooking hours")
            isCTHHShadow = true
            return false
        } else if (objectAddRecipe.CTHours ?? "").count >= 3 {
            showMessage("Please enter proper cooking hours")
            isCTHHShadow = true
            return false
        } else if (objectAddRecipe.CTMintues ?? "").count <= 0 {
            showMessage("Please enter recipe cooking mintues")
            isCTMMShadow = true
            return false
        } else if (objectAddRecipe.CTMintues ?? "").count >= 3 || (Int(TRIM(string: objectAddRecipe.CTMintues ?? "")) ?? 0) > 59 {
            showMessage("Please enter proper cooking mintues")
            isCTMMShadow = true
            return false
        }
        return true
        
    }
    
    func setupAddRecipeData() {
        
        var ingredientArray = [NSDictionary]()
        for model in arrayIngredients {
            if model.qty != nil && model.qty != "" && model.unit != nil && model.unit != "" && model.itemName != nil && model.itemName != "" {
                let dic = ["name":model.itemName ?? "",
                           "qty":model.qty ?? "",
                           "unit":model.unit ?? ""]
                
                ingredientArray.append(dic as NSDictionary)
            }
        }
        
        if ingredientArray.count <= 0 {
            showMessage("Please enter recipe ingredients" )
            for model in arrayIngredients {
                if model.qty == nil || model.qty == "" {
                    isQTY = true
                    break
                } else if model.unit == nil || model.unit == "" {
                    isUnit = true
                    break
                } else if model.itemName == nil || model.itemName == "" {
                    isItemName = true
                    break
                }
            }
            tableViewAddRecipe.reloadData()
            return
        }
        
        var instructionsArray = [String]()
        for model in arrayMethods {
            if model.addStep != nil && model.addStep != "" {
                let str = model.addStep ?? ""
                instructionsArray.append(str)
            }
        }
        
        if instructionsArray.count <= 0 {
            showMessage("Please enter recipe method" )
            isMethodShadow = true
            tableViewAddRecipe.reloadData()
            return
        }
        
        let strRating = objectAddRecipe.rateRecipe.lowercased()
        
        var recipeType = String()
        if objectAddRecipe.isVegRecipe {
            recipeType = "veg"
        } else {
            recipeType = "non_veg"
        }

        if objectAddRecipe.PTHours == nil || objectAddRecipe.PTHours == ""{
            objectAddRecipe.PTHours = "00"
        }
        if objectAddRecipe.PTMintues == nil || objectAddRecipe.PTMintues == "" {
            objectAddRecipe.PTMintues = "00"
        }
        if objectAddRecipe.CTHours == nil || objectAddRecipe.CTHours == ""{
            objectAddRecipe.CTHours = "00"
        }
        if objectAddRecipe.CTMintues == nil || objectAddRecipe.CTMintues == ""{
            objectAddRecipe.CTMintues = "00"
        }
        
        
        let reciperDic = ["name":TRIM(string: objectAddRecipe.recipeName ?? ""),
                          "description":TRIM(string: objectAddRecipe.recipeDesc ?? ""),
                          "preparation_hours":objectAddRecipe.PTHours ?? "00",
                          "preparation_mins":objectAddRecipe.PTMintues ?? "00",
                          "cooking_hours":objectAddRecipe.CTHours ?? "00",
                          "cooking_mins":objectAddRecipe.CTMintues ?? "00",
                          "recipe_yield":objectAddRecipe.serves ?? "",
                          "rating":strRating,
                          "recipe_type":recipeType,
                          "instructions":instructionsArray,
                          "images_urls": arrayRecipeImages,
                          "ingredients_attributes":ingredientArray] as [String : Any]
        
        let mainDic = ["recipe":reciperDic]
        print(mainDic)
        createNewReciperAPICall(main: mainDic as NSDictionary)
        
    }
    
    func createNewReciperAPICall(main: NSDictionary) {
        self.view.endEditing(true)
        UIApplication.shared.beginIgnoringInteractionEvents()
        let service = SERVER_RECIPE_URL + APICreateRecipe
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICreateRecipe,
            parameters: main,
            method: .post,
            getReturn: true,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                if responseDict != nil {
                    showMessage("Receipe created successfully")
                    NotificationCenter.default.post(name:NSNotification.Name(k_Recipe_Create),object: nil)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            } else {
                UIApplication.shared.endIgnoringInteractionEvents()
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }

}

extension AddRecipeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AddRecipeSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch AddRecipeSection (rawValue: section)! {
        case .Details:
            return 224.0
        case .Ingredients:
            return 40.0
        case .Methods:
            return 40.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch AddRecipeSection (rawValue: section)! {
        case .Details:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderAddRecipe") as! TableViewHeaderAddRecipe
            
            headerView.buttonClose.addTarget(self, action: #selector(buttonCloseTouchUpInside(_:)), for: .touchUpInside)
            headerView.buttonAddImage.addTarget(self, action: #selector(buttonAddImageTouchUpInside(_:)), for: .touchUpInside)
            
            headerView.collectionView.delegate = self
            headerView.collectionView.dataSource = self
            headerView.collectionView.register(UINib(nibName: "CollectionViewCellRecipeImage", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellRecipeImage")
            headerView.collectionView.reloadData()
            
            if arrayRecipeImages.count >= 3 {
                headerView.buttonAddImage.isHidden = true
                headerView.imageViewAddPhoto.isHidden = true
                headerView.labelAddPicture.isHidden = true
            } else {
                headerView.buttonAddImage.isHidden = false
                headerView.imageViewAddPhoto.isHidden = false
                headerView.labelAddPicture.isHidden = false
            }
            
            
            return headerView
        case .Ingredients:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderTitle") as! TableViewHeaderTitle
            headerView.labelTitle.text = AddRecipeSection(rawValue: section)?.title()
            return headerView
        case .Methods:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderTitle") as! TableViewHeaderTitle
            headerView.labelTitle.text = AddRecipeSection(rawValue: section)?.title()
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView (frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch AddRecipeSection (rawValue: section)! {
        case .Details:
            return DetailsSection.allCases.count
        case .Ingredients:
            return arrayIngredients.count
        case .Methods:
            return arrayMethods.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch AddRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                return 245.0
            case .RecipeTime:
                return 165.0
            }
        case .Ingredients:
            return UITableView.automaticDimension
        case .Methods:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch AddRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                return 245.0
            case .RecipeTime:
                return 165.0
            }
        case .Ingredients:
            return 150.0
        case .Methods:
            return 50.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch AddRecipeSection (rawValue: indexPath.section)! {
        case .Details:
            switch DetailsSection (rawValue: indexPath.row)! {
            case .RecipeDetail:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellRecipeDetail", for: indexPath) as! TableViewCellRecipeDetail
                cell.selectionStyle = .none
                
                cell.textFieldRecipeName.delegate = self
                cell.textFieldRecipeName.keyboardType = .default
                cell.textFieldRecipeName.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldRecipeName.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldRecipeName.text = objectAddRecipe.recipeName ?? ""
                
                if isRecipeNameShadow {
                    cell.textFieldRecipeName.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldRecipeName.superview?.removeBorder()
                }
                cell.textFieldRecipeName.maskToBounds = false
                
                cell.textViewRecipeDes.delegate = self
                cell.textViewRecipeDes.keyboardType = .default
                if objectAddRecipe.recipeDesc != nil && objectAddRecipe.recipeDesc != "" && objectAddRecipe.recipeDesc != "Tell us something about your recipe" {
                    cell.textViewRecipeDes.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                } else {
                    cell.textViewRecipeDes.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                }
                cell.textViewRecipeDes.tag = 555
                cell.textViewRecipeDes.text = objectAddRecipe.recipeDesc ?? "Tell us something about your recipe"
                
                if objectAddRecipe.isVegRecipe {
                    cell.buttonVeg.setImage(#imageLiteral(resourceName: "ic_round_selected"), for: .normal)
                    cell.buttonNonVeg.setImage(#imageLiteral(resourceName: "ic_round_unselect"), for: .normal)
                } else {
                    cell.buttonVeg.setImage(#imageLiteral(resourceName: "ic_round_unselect"), for: .normal)
                    cell.buttonNonVeg.setImage(#imageLiteral(resourceName: "ic_round_selected"), for: .normal)
                }
                
                cell.buttonVeg.addTarget(self, action: #selector(buttonVegTouchUpInside(_:)), for: .touchUpInside)
                cell.buttonNonVeg.addTarget(self, action: #selector(buttonNonVegTouchUpInside(_:)), for: .touchUpInside)
                
                return cell
            case .RecipeTime:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellRecipeTime", for: indexPath) as! TableViewCellRecipeTime
                cell.selectionStyle = .none
                
                cell.textFieldPTHours.tag = 100
                cell.textFieldPTHours.keyboardType = .numberPad
                cell.textFieldPTHours.delegate = self
                cell.textFieldPTHours.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldPTHours.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldPTHours.text = objectAddRecipe.PTHours ?? ""
                if isPTHHShadow {
                    cell.textFieldPTHours.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldPTHours.superview?.removeBorder()
                }
                
                cell.textFieldPTMintues.tag = 101
                cell.textFieldPTMintues.keyboardType = .numberPad
                cell.textFieldPTMintues.delegate = self
                cell.textFieldPTMintues.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldPTMintues.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldPTMintues.text = objectAddRecipe.PTMintues ?? ""
                if isPTMMShadow {
                    cell.textFieldPTMintues.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldPTMintues.superview?.removeBorder()
                }
                
                cell.textFieldCTHours.tag = 102
                cell.textFieldCTHours.keyboardType = .numberPad
                cell.textFieldCTHours.delegate = self
                cell.textFieldCTHours.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldCTHours.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldCTHours.text = objectAddRecipe.CTHours ?? ""
                if isCTHHShadow {
                    cell.textFieldCTHours.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldCTHours.superview?.removeBorder()
                }
                
                cell.textFieldCTMintues.tag = 103
                cell.textFieldCTMintues.keyboardType = .numberPad
                cell.textFieldCTMintues.delegate = self
                cell.textFieldCTMintues.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldCTMintues.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldCTMintues.text = objectAddRecipe.CTMintues ?? ""
                if isCTMMShadow {
                    cell.textFieldCTMintues.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldCTMintues.superview?.removeBorder()
                }
                
                cell.textFieldServes.tag = 104
                cell.textFieldServes.keyboardType = .numberPad
                cell.textFieldServes.delegate = self
                cell.textFieldServes.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                cell.textFieldServes.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                cell.textFieldServes.text = objectAddRecipe.serves ?? ""
                if isServesShadow {
                    cell.textFieldServes.superview?.dropBorder(color: .red)
                } else {
                    cell.textFieldServes.superview?.removeBorder()
                }
                
                cell.labelrate.text = objectAddRecipe.rateRecipe
                cell.buttonRate.addTarget(self, action: #selector(buttonRateTouchUpInside(_:)), for: .touchUpInside)
                                
                return cell
            }
        case .Ingredients:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellIngredients", for: indexPath) as! TableViewCellIngredients
            cell.selectionStyle = .none
            
            let model = arrayIngredients[indexPath.row] as AddIngredientModel
            
            cell.textFieldQty.tag = 201
            cell.textFieldQty.keyboardType = .numbersAndPunctuation
            cell.textFieldQty.delegate = self
            cell.textFieldQty.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            cell.textFieldQty.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            cell.textFieldQty.text = model.qty ?? ""
            if isQTY && (model.qty?.count ?? 0 <= 0 || model.qty == nil) && !isAlreadyShowQtyShadow {
                isAlreadyShowQtyShadow = true
                cell.textFieldQty.superview?.dropBorder(color: .red)
            } else {
                cell.textFieldQty.superview?.removeBorder()
            }
            
            cell.textFieldUnit.tag = 202
            cell.textFieldUnit.keyboardType = .default
            cell.textFieldUnit.delegate = self
            cell.textFieldUnit.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            cell.textFieldUnit.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            cell.textFieldUnit.text = model.unit ?? ""
            if isUnit && (model.unit?.count ?? 0 <= 0 || model.unit == nil) && !isAlreadyShowUnitShadow {
                isAlreadyShowUnitShadow = true
                cell.textFieldUnit.superview?.dropBorder(color: .red)
            } else {
                cell.textFieldUnit.superview?.removeBorder()
            }
            
            cell.textViewItemName.keyboardType = .default
            cell.textViewItemName.delegate = self
            cell.textViewItemName.tag = indexPath.row+10000
            if model.itemName != nil && model.itemName != "" && model.itemName != "Item name" {
                cell.textViewItemName.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            } else {
                cell.textViewItemName.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            }
            cell.textViewItemName.text = model.itemName ?? "Item name"
            if isItemName && (model.itemName?.count ?? 0 <= 0 || model.itemName == nil) && !isAlreadyShowItemNameShadow {
                isAlreadyShowItemNameShadow = true
                cell.viewItemBack.dropBorder(color: .red)
            } else {
                cell.viewItemBack.removeBorder()
            }
            
            if arrayIngredients.count - 1 > indexPath.row {
                cell.buttonAddIngredients.setImage(#imageLiteral(resourceName: "minus_red"), for: .normal)
            } else {
                cell.buttonAddIngredients.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
            }
            cell.buttonAddIngredients.addTarget(self, action: #selector(buttonAddIngredientsTouchUpInside(_:)), for: .touchUpInside)
            
            return cell
        case .Methods:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellMethods", for: indexPath) as! TableViewCellMethods
            cell.selectionStyle = .none
            
            let model = arrayMethods[indexPath.row] as AddMethodModel
            
            cell.textViewAddStep.delegate = self
            cell.textViewAddStep.keyboardType = .default
            cell.textViewAddStep.tag = indexPath.row+100000
            if model.addStep != nil && model.addStep != "" && model.addStep != "Add step" {
                cell.textViewAddStep.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            } else {
                cell.textViewAddStep.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            }
            cell.textViewAddStep.text = model.addStep ?? "Add step"
            
            if arrayMethods.count - 1 > indexPath.row {
                cell.buttonAddMethods.setImage(#imageLiteral(resourceName: "minus_red"), for: .normal)
            } else {
                cell.buttonAddMethods.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
            }
            cell.buttonAddMethods.addTarget(self, action: #selector(buttonAddMethodsTouchUpInside(_:)), for: .touchUpInside)
            
            if isMethodShadow && (model.addStep?.count ?? 0 <= 0 || model.addStep == nil) {
                cell.viewback.dropBorder(color: .red)
            } else {
                cell.viewback.superview?.removeBorder()
            }
            
            return cell
        }
    }
    
}

extension AddRecipeVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let viewPosition:CGPoint = textField.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            switch AddRecipeSection (rawValue: indexPath.section)! {
            case .Details:
                switch DetailsSection (rawValue: indexPath.row)! {
                case .RecipeDetail:
                    break
                case .RecipeTime:
                    break
                }
            case .Ingredients:
                break
            case .Methods:
                break
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let viewPosition:CGPoint = textField.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            switch AddRecipeSection (rawValue: indexPath.section)! {
            case .Details:
                switch DetailsSection (rawValue: indexPath.row)! {
                case .RecipeDetail:
                    objectAddRecipe.recipeName = TRIM(string: textField.text ?? "")
                case .RecipeTime:
                    if textField.tag == 100 {
                        objectAddRecipe.PTHours = String(format: "%02d", Int(TRIM(string: textField.text ?? "")) ?? 0)
                        textField.text = objectAddRecipe.PTHours
                    } else if textField.tag == 101 {
                        objectAddRecipe.PTMintues = String(format: "%02d", Int(TRIM(string: textField.text ?? "")) ?? 0)
                        textField.text = objectAddRecipe.PTMintues
                    } else if textField.tag == 102 {
                        objectAddRecipe.CTHours = String(format: "%02d", Int(TRIM(string: textField.text ?? "")) ?? 0)
                        textField.text = objectAddRecipe.CTHours
                    } else if textField.tag == 103 {
                        objectAddRecipe.CTMintues = String(format: "%02d", Int(TRIM(string: textField.text ?? "")) ?? 0)
                        textField.text = objectAddRecipe.CTMintues
                    } else if textField.tag == 104 {
                        objectAddRecipe.serves = String(format: "%02d", Int(TRIM(string: textField.text ?? "")) ?? 0)
                        textField.text = objectAddRecipe.serves
                    }
                }
            case .Ingredients:
                if textField.tag == 201 {
                    arrayIngredients[indexPath.row].qty = TRIM(string: textField.text ?? "")
                } else if textField.tag == 202 {
                    arrayIngredients[indexPath.row].unit = TRIM(string: textField.text ?? "")
                }
            case .Methods:
                break
            }
        }
    }
    
}

extension AddRecipeVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let viewPosition:CGPoint = textView.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            switch AddRecipeSection (rawValue: indexPath.section)! {
            case .Details:
                switch DetailsSection (rawValue: indexPath.row)! {
                case .RecipeDetail:
                    if textView.textColor == #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5) {
                        textView.text = nil
                        textView.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                    }
                case .RecipeTime:
                    break
                }
            case .Ingredients:
                textView.isScrollEnabled = true
                if textView.textColor == #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5) {
                    textView.text = nil
                    textView.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                }
            case .Methods:
                textView.isScrollEnabled = true
                if textView.textColor == #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5) {
                    textView.text = nil
                    textView.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
                }
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag >= 10000 && textView.tag < 100000 {
            let index = textView.tag-10000
            textView.isScrollEnabled = false
            if textView.text.isEmpty {
                arrayIngredients[index].itemName = nil
                textView.text = "Item name"
                textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            } else {
                arrayIngredients[index].itemName = TRIM(string: textView.text ?? "")
                tableViewAddRecipe.reloadRows(at: [IndexPath(item: index, section: 1)], with: .none)
            }
            return
        } else if textView.tag >= 100000 {
            let index = textView.tag-100000
            textView.isScrollEnabled = false
            if textView.text.isEmpty {
                arrayMethods[index].addStep = nil
                textView.text = "Add step"
                textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
            } else {
                arrayMethods[index].addStep = TRIM(string: textView.text ?? "")
                tableViewAddRecipe.reloadRows(at: [IndexPath(item: index, section: 2)], with: .none)
            }
            return
        }
        let viewPosition:CGPoint = textView.convert(CGPoint.zero, to:tableViewAddRecipe)
        if let indexPath = tableViewAddRecipe.indexPathForRow(at: viewPosition) {
            switch AddRecipeSection (rawValue: indexPath.section)! {
            case .Details:
                switch DetailsSection (rawValue: indexPath.row)! {
                case .RecipeDetail:
                    if textView.text.isEmpty {
                        objectAddRecipe.recipeDesc = nil
                        textView.text = "Tell us something about your recipe"
                        textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                    } else {
                        objectAddRecipe.recipeDesc = TRIM(string: textView.text ?? "")
                    }
                case .RecipeTime:
                    break
                }
            case .Ingredients:
                textView.isScrollEnabled = false
                if textView.text.isEmpty {
                    arrayIngredients[indexPath.row].itemName = nil
                    textView.text = "Item name"
                    textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                } else {
                    arrayIngredients[indexPath.row].itemName = TRIM(string: textView.text ?? "")
                    tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
                }
            case .Methods:
                textView.isScrollEnabled = false
                if textView.text.isEmpty {
                    arrayMethods[indexPath.row].addStep = nil
                    textView.text = "Add step"
                    textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                } else {
                    arrayMethods[indexPath.row].addStep = TRIM(string: textView.text ?? "")
                    tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
                }
            }
        } else {
            if textView.tag == 555 {
                if textView.text.isEmpty {
                    objectAddRecipe.recipeDesc = nil
                    textView.text = "Tell us something about your recipe"
                    textView.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.5)
                } else {
                    objectAddRecipe.recipeDesc = TRIM(string: textView.text ?? "")
                }
            }
        }
    }
    
}

extension AddRecipeVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayRate.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        objectAddRecipe.rateRecipe = arrayRate[row]
        let indexPath = IndexPath(item: DetailsSection.RecipeTime.rawValue, section: AddRecipeSection.Details.rawValue)
        tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
        return arrayRate[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        objectAddRecipe.rateRecipe = arrayRate[row]
        let indexPath = IndexPath(item: DetailsSection.RecipeTime.rawValue, section: AddRecipeSection.Details.rawValue)
        tableViewAddRecipe.reloadRows(at: [indexPath], with: .none)
    }
}

extension AddRecipeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayRecipeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellRecipeImage", for: indexPath) as! CollectionViewCellRecipeImage
        
        let url = arrayRecipeImages[indexPath.row]
        
        cell.imageViewRecipe.sd_setImage(with: URL(string: url), placeholderImage: FOOD_PLACE_HOLDER)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}
