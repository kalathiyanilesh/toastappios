//
//  CheckRecipeVC.swift
//  Toast
//
//  Created by iMac on 01/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CheckRecipeVC: UIViewController {
    
    @IBOutlet var lblitemname: UILabel!
    @IBOutlet var viewPage: UIView!
    var pageMenu : CAPSPageMenu?
    var recipeModel: TRecipeDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblitemname.text = recipeModel.name ?? ""
        SetPageMenu()
    }
    
    @IBAction func btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOption(_ sender: UIButton) {
        SetButtonColors(sender)
        pageMenu?.moveToPage(sender.tag-1)
    }
    
    func SetButtonColors(_ sender: UIButton) {
        for i in 1..<3 {
            let btn = self.view.viewWithTag(i) as! UIButton
            btn.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        }
        sender.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
    }
}

//MARK:- SET PAGEMENU
extension CheckRecipeVC {
    func SetPageMenu() {
        pageMenu?.view.removeFromSuperview()
        pageMenu?.removeFromParent()
        
        var arrController : [UIViewController] = []
    
        let other = loadVC(strStoryboardId: SB_RECIPE, strVCId: "IngredientsVC") as! IngredientsVC
        other.recipeModel = recipeModel
        arrController.append(other)
        
        let your = loadVC(strStoryboardId: SB_RECIPE, strVCId: "MethodVC") as! MethodVC
        your.recipeModel = recipeModel
        arrController.append(your)
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
}
