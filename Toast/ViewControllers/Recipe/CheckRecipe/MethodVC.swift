//
//  MethodVC.swift
//  Toast
//
//  Created by iMac on 01/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellMethod: UITableViewCell {
    @IBOutlet var lblNo: UILabel!
    @IBOutlet var lblMethod: UILabel!
}

class MethodVC: UIViewController {

    var recipeModel: TRecipeDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

//MARK:- UITableViewDelegate METHOD
extension MethodVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeModel.instructions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellMethod = tableView.dequeueReusableCell(withIdentifier: "CellMethod", for: indexPath) as! CellMethod
        
        cell.lblNo.text = "\(indexPath.row+1)."
        cell.lblMethod.text = recipeModel.instructions?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
