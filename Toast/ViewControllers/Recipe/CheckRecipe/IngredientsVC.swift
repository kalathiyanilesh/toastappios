//
//  IngredientsVC.swift
//  Toast
//
//  Created by iMac on 01/05/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellIngredients: UITableViewCell {
    @IBOutlet var lblIngredients: UILabel!
}

class IngredientsVC: UIViewController {
    var recipeModel: TRecipeDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

//MARK:- UITableViewDelegate METHOD
extension IngredientsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeModel.ingredients?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellIngredients = tableView.dequeueReusableCell(withIdentifier: "CellIngredients", for: indexPath) as! CellIngredients
        
        let model = recipeModel.ingredients?[indexPath.row]
        
        if model?.qty != nil && model?.qty != "" && model?.unit != nil && model?.unit != "" && model?.name != nil && model?.name != "" {
            cell.lblIngredients.text = "\(model?.qty ?? "") \(model?.unit ?? "") \(model?.name ?? "")"
        } else if model?.qty != nil && model?.qty != nil && model?.name != nil && model?.name != nil {
            cell.lblIngredients.text = "\(model?.qty ?? "") \(model?.name ?? "")"
        } else {
            cell.lblIngredients.text = "\(model?.name ?? "")"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
