//
//  OnboardingVC.swift
//  Toast
//
//  Created by iMac on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SCPageControl

class CellOnboarding: UICollectionViewCell {
    @IBOutlet var imgOnboarding: UIImageView!
}

class OnboardingVC: UIViewController {
    
    @IBOutlet var collOnboarding: UICollectionView!
    @IBOutlet var pageControl: SCPageControlView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnLetsStart: UIButton!
    @IBOutlet var viewEnterName: UIView!
    @IBOutlet var txtUserName: UITextField!
    
    var indexNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewEnterName.alpha = 0
        btnLetsStart.alpha = 0
        pageControl.scp_style = .SCNormal
        pageControl.set_view(4, current: 0, current_color: Color_Hex(hex: "3CBCB4"), disable_color: Color_Hex(hex: "#656565").withAlphaComponent(0.30))
        SetUpText()
    }
    
    @IBAction func btnNext(_ sender: Any) {
        indexNumber += 1
        collOnboarding.scrollToItem(at:IndexPath(item: indexNumber, section: 0), at: .right, animated: true)
        SetUpText()
    }
    
    @IBAction func btnLetsStart(_ sender: Any) {
        if validateTxtFieldLength(txtUserName, withMessage: EnterName) {
            APP_DELEGATE.strDefualtUserName = TRIM(string: txtUserName.text!)
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "LoginVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension OnboardingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellOnboarding = collectionView.dequeueReusableCell(withReuseIdentifier: "CellOnboarding", for: indexPath) as! CellOnboarding
        cell.imgOnboarding.image = UIImage(named: "onboard\(indexPath.row)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        indexNumber = indexPath.row
    }
    
}

//MARK: ScrollView Delegate
extension OnboardingVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.scroll_did(scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        indexNumber = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        SetUpText()
    }
}

//MARK: CLASS METHODS
extension OnboardingVC {
    func SetUpText() {
        UIView.transition(with: lblTitle,
             duration: 0.25,
              options: .transitionCrossDissolve,
           animations: { [weak self] in
            if self?.indexNumber == 0  {
                self?.lblTitle.text = "Browse, order, pay online\neverything directly from\nToast App"
            } else if self?.indexNumber == 1  {
                self?.lblTitle.text = "Pickup your favorite food\nfrom your favorite\nrestaurant"
            } else if self?.indexNumber == 1  {
                self?.lblTitle.text = "Relax at your home and\norder your favorite food\nonline"
            }
            self?.lblDesc.text = ""
        }, completion: nil)
        
        UIView.animate(withDuration: 0.35) {
            if self.indexNumber == 3 {
                self.viewEnterName.alpha = 1
                self.btnLetsStart.alpha = 1
                self.btnNext.alpha = 0
            } else {
                self.viewEnterName.alpha = 0
                self.btnLetsStart.alpha = 0
                self.btnNext.alpha = 1
            }
        }
        self.view.endEditing(true)
    }
}
