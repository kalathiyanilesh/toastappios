//
//  LoginVC.swift
//  Toast
//
//  Created by iMac on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SKCountryPicker

class LoginVC: UIViewController {

    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var lblphonecode: UILabel!
    @IBOutlet var txtUserName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setcurrentcountrycode()
        txtUserName.text = APP_DELEGATE.strDefualtUserName
    }
    
    @IBAction func btnPhoneCode(_ sender: Any) {
        /*
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
          guard let self = self else { return }
            self.lblphonecode.text = country.dialingCode
        }
        countryController.detailColor = Color_Hex(hex: "3CBCB4")*/
    }
    
    
    @IBAction func btnVerify(_ sender: Any) {
        if validateTxtFieldLength(txtUserName, withMessage: EnterName) && validatePhoneNo(txtMobileNo, withMessage: EnterValidPhoneNo) && validateMinTxtFieldLength(txtMobileNo, withMessage: EnterValidPhoneNo) {
            apiUserLogin()
        }
    }
    
    func setcurrentcountrycode() {
        guard let country = CountryManager.shared.currentCountry else {
            return
        }
        lblphonecode.text = country.dialingCode
    }
}

//MARK: API CALLING
extension LoginVC {
    func apiUserLogin() {
        self.view.endEditing(true)
        //let scode = lblphonecode.text?.replacingOccurrences(of: "+", with: "")
        let mobileNo = "91\(txtMobileNo.text!)"
        /*
        if APP_DELEGATE.strDefualtUserName.count <= 0 && mobileNo == "919597967762" {
             APP_DELEGATE.strDefualtUserName = "Arun"
        }*/
        APP_DELEGATE.strDefualtUserName = TRIM(string: txtUserName.text!)
        let service = SERVER_URL + APILogin + mobileNo
        print(service)
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APILogin,
            parameters: NSDictionary(),
            method: .get,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "VerifyOTPVC") as! VerifyOTPVC
                vc.strMobileNo = mobileNo
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}
