//
//  VerifyOTPVC.swift
//  Toast
//
//  Created by iMac on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
}

class VerifyOTPVC: UIViewController {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    @IBOutlet var fifth: MyTextField!
    @IBOutlet var six: MyTextField!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var viewResendOTP: UIView!
    @IBOutlet var heightViewWelcome: NSLayoutConstraint!
    
    var totalTimeToResend = 30
    var timerOTP: Timer?
    var strMobileNo: String = ""
    var currentTextFiled:MyTextField! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() < 737 {
            heightViewWelcome.constant = 238
        }
        AddRightSwipeGesture()
        StartTimer()
        setUpTextField()
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLetsGo(_ sender: Any) {

        first.text = TRIM(string: first.text ?? "")
        second.text = TRIM(string: second.text ?? "")
        third.text = TRIM(string: third.text ?? "")
        fourth.text = TRIM(string: fourth.text ?? "")
        fifth.text = TRIM(string: fifth.text ?? "")
        six.text = TRIM(string: six.text ?? "")
        let strOTP = "\(first.text ?? "")\(second.text ?? "")\(third.text ?? "")\(fourth.text ?? "")\(fifth.text ?? "")\(six.text ?? "")"
        if strOTP.count == 6 {
            apiUserLogin(strOTP)
        }
    }
    
    @IBAction func btnResend(_ sender: Any) {
        apiResendOTP()
    }
}

//MARK: API CALLING
extension VerifyOTPVC {
    func apiUserLogin(_ strOTP: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APILogin + "\(strMobileNo)"
        print(service)
        let dictParam: NSDictionary = ["otp_value":strOTP, "app_id":getDeviceToken()]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APILogin,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if ((responseDict?.object(forKey: kDATA)) != nil) {
                    let dictData = responseDict?.object(forKey: kDATA) as! NSDictionary
                    if dictData.object(forKey: "status") as? String ?? "0" == "1" {
                        
                        let strAuthToken = dictData.object(forKey: "auth_token") as? String ?? ""
                        if strAuthToken.count > 0 {
                            UserDefaults.standard.set(strAuthToken, forKey: UD_AUTHTOKEN)
                            UserDefaults.standard.synchronize()
                        }
                        
                        CUSTOMER_ID = dictData.object(forKey: "customer_id") as? String ?? ""
                        UserDefaults.standard.set("1", forKey: IS_LOGIN)
                        UserDefaults.standard.set(CUSTOMER_ID, forKey: UD_CUSTOMERID)
                        UserDefaults.standard.synchronize()
                        
                        self.apiUpdateUserInfo()
                        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
                        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                            APP_DELEGATE.window?.rootViewController = vc
                        }, completion: nil)
                    } else {
                        showMessage("You enter wrong code!")
                    }
                } else {
                    showMessage("You enter wrong code!")
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiResendOTP() {
        self.view.endEditing(true)
        let service = SERVER_URL + APILogin + "\(strMobileNo)/resend-otp"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APILogin,
            parameters: NSDictionary(),
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.StartTimer()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiUpdateUserInfo() {
        if APP_DELEGATE.strDefualtUserName.count <= 0 { return }
        let service = SERVER_URL + APICustomerProfile
        let dictParam: NSDictionary = ["customer_name":APP_DELEGATE.strDefualtUserName, "email":"", "profile_picture": ""]
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: service,
            showLoader: false,
            parameters: dictParam) { (error, responseDict) in
                print(responseDict ?? "")
                if error == nil {
                    if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                        if let strStatus = dictData.object(forKey: "status") as? String {
                            if strStatus.lowercased() == "success" {
                                APP_DELEGATE.strDefualtUserName = ""
                            }
                        }
                    }
                }
        }
    }
}

//MARK:- MyTextFieldDelegate Delegate
extension VerifyOTPVC: UITextFieldDelegate, MyTextFieldDelegate {
    func setUpTextField() {
        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        fifth.myDelegate = self
        six.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fifth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        six.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fifth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        six.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
    
    func textFieldDidDelete() {
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0)
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0)
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0)
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0)
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fifth)
        {
            if(fifth.text!.count > 0)
            {
                fifth.text = ""
            }
            else
            {
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == six)
        {
            if(six.text!.count > 0)
            {
                six.text = ""
            }
            else
            {
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
        else if(textField == fifth)
        {
            currentTextFiled = fifth
            fifth.text = ""
        }
        else if(textField == six)
        {
            currentTextFiled = six
            six.text = ""
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            case fifth:
                currentTextFiled = six
                six.text = ""
                six.becomeFirstResponder()
            case six:
                self.view.endEditing(true)
            default:
                break
            }
        }
    }
    
    
    //MARK:- TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFiled = textField as? MyTextField
        textField.text = ""
        clearBorder(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        clearBorder(textField)
        if let viewNo = self.view.viewWithTag(textField.tag/10) {
            viewNo.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
    
    func clearBorder(_ textField: UITextField) {
        for i in 1..<7 {
            if let viewNo = self.view.viewWithTag(i) {
                viewNo.layer.borderColor = UIColor.lightGray.cgColor
            }
        }
        if let viewNo = self.view.viewWithTag(textField.tag/10) {
            viewNo.layer.borderColor = Color_Hex(hex: "3CBCB4").cgColor
            viewNo.clipsToBounds = true
        }
    }
}

//MARK:-  TIMER
extension VerifyOTPVC {
    func StartTimer() {
        UIView.animate(withDuration: 0.35) {
            self.viewResendOTP.alpha = 0
        }
        totalTimeToResend = 30
        lblTime.text = String(format: "%02d", totalTimeToResend)
        timerOTP = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(eventWith(timer:)), userInfo: nil, repeats: true)
    }
    
    @objc func eventWith(timer: Timer!) {
        totalTimeToResend -= 1
        lblTime.text = String(format: "%02d", totalTimeToResend)
        if totalTimeToResend <= 0 {
            if (timerOTP != nil) {
                timerOTP!.invalidate()
            }
            UIView.animate(withDuration: 0.35) {
                self.viewResendOTP.alpha = 1
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension VerifyOTPVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
