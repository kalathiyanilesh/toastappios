//
//  OffersVC.swift
//  Toast
//
//  Created by iMac on 16/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellPromocode: UITableViewCell {
    @IBOutlet weak var CollPromo: UICollectionView!
    @IBOutlet var lblPromoCode: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblValidDate: UILabel!
    @IBOutlet var imgSelected: UIImageView! //for coupon
    @IBOutlet weak var lblResName: UILabel!
}

extension CellPromocode {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        CollPromo.delegate = dataSourceDelegate
        CollPromo.dataSource = dataSourceDelegate
        CollPromo.tag = row
        CollPromo.setContentOffset(CollPromo.contentOffset, animated:false)
        CollPromo.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { CollPromo.contentOffset.x = newValue }
        get { return CollPromo.contentOffset.x }
    }
}

class CollCell: UICollectionViewCell {
    
}

class OffersVC: UIViewController {

    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var tblOffers: UITableView!
    @IBOutlet var bottom: NSLayoutConstraint!
    @IBOutlet var lblNoOffers: UILabel!
    
    var storedOffsets = [Int: CGFloat]()
    var arrOffers = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() <= 736 {
            bottom.constant = 34
        }
        if !isConnectedToNetwork() {
            activity.stopAnimating()
        }
        apiGetOffers()
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshOffers), name: NSNotification.Name(rawValue: OFFERS_PAGE_REFRESH), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnOffers = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnOffers = false
    }
    @objc func RefreshOffers() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetOffers()
        }
    }
}

//MARK: API CALLING
extension OffersVC {
    func apiGetOffers() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIOffers
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIEvents,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            self.activity.stopAnimating()
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.arrOffers = NSMutableArray()
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        print(convertDictionaryToJSONString(dic: dictData) ?? "")
                        let obj: TOffers = TOffers.init(object: dictData)
                        self.arrOffers.add(obj)
                    }
                    self.tblOffers.reloadData()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension OffersVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            lblNoOffers.alpha = arrOffers.count > 0 ? 0 : 1
            return arrOffers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell : CellPromocode = tableView.dequeueReusableCell(withIdentifier: "collpromo", for: indexPath) as! CellPromocode
            return cell
        default:
            let cell : CellPromocode = tableView.dequeueReusableCell(withIdentifier: "CellPromocode", for: indexPath) as! CellPromocode
            let objOffers = arrOffers[indexPath.row] as! TOffers
            cell.lblPromoCode.text = objOffers.couponCode
            cell.lblTitle.text = objOffers.couponTitle
            cell.lblDesc.text = objOffers.couponDescription
            cell.lblValidDate.text = ""
            if let timeResult = Double(objOffers.couponValidTill ?? "0") {
                let date = Date(timeIntervalSince1970: timeResult)
                let sDate = getDateString(date, format: "dd MMM, yyyy")
                cell.lblValidDate.text = "Valid till \(sDate)"
            }
            cell.lblResName.text = objOffers.restrauntName
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPromocode else { return }
            promoCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            promoCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 0 //198
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPromocode else { return }
            storedOffsets[indexPath.row] = promoCell.collectionViewOffset
        default:
            break
        }
    }

}

//MARK:- UICollectionViewDelegate METHOD
extension OffersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollCell", for: indexPath) as! CollCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:163, height:147)
    }
}
