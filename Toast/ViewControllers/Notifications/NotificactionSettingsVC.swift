//
//  NotificactionSettingsVC.swift
//  Toast
//
//  Created by iMac on 17/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class NotificactionSettingsVC: UIViewController {

    @IBOutlet var bottomView: NSLayoutConstraint!
    var objSettings: TNotificationSettings!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.constant = -500
        AllEnableDisable(false)
        apiGetNotificationSettings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        bottomView.constant = 8
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        apiSaveNotificationSettings()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        bottomView.constant = -500
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        }
    }
    

    @IBAction func switchSettings(_ sender: UISwitch) {
        if objSettings == nil { return }
        if sender.tag != 1 && objSettings.all ?? true {
            return
        }
        //sender.isOn = !sender.isOn
        
        if sender.tag == 1 {
            AllEnableDisable(sender.isOn)
        } else {
            let lblText = self.view.viewWithTag(sender.tag*10) as! UILabel
            lblText.font = FontWithSize("Montserrat-Regular", 13)
            lblText.textColor = UIColor.white
            if sender.isOn {
                lblText.textColor = Color_Hex(hex: "#3CBCB4")
                lblText.font = FontWithSize("Montserrat-SemiBold", 13)
            }
            
            var isAllSelected = false
            for i in 2..<5 {
                let btnS = self.view.viewWithTag(i) as! UISwitch
                if !btnS.isOn {
                    isAllSelected = false
                    break
                }
                isAllSelected = true
            }
            if isAllSelected {
                AllEnableDisable(true)
            } else {
                let btnS = self.view.viewWithTag(1) as! UISwitch
                btnS.isOn = false
                let lblS = self.view.viewWithTag(10) as! UILabel
                lblS.textColor = UIColor.white
                lblS.font = FontWithSize("Montserrat-Regular", 13)
            }
        }
        
        SetObject()
    }
}

//MARK: API CALLING
extension NotificactionSettingsVC {
    func apiGetNotificationSettings() {
        self.view.endEditing(true)
        let service = SERVER_URL + APINotificationSettings
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APINotificationSettings,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    self.objSettings = TNotificationSettings.init(object: dictData)
                    self.objSettings.all = true
                    self.objSettings.events = true
                    self.objSettings.offers = true
                    self.objSettings.toast = true
                    
                    if dictData.object(forKey: "all") as! String == "0" {
                        self.objSettings.all = false
                    }
                    if dictData.object(forKey: "events") as! String == "0" {
                        self.objSettings.events = false
                    }
                    if dictData.object(forKey: "offers") as! String == "0" {
                        self.objSettings.offers = false
                    }
                    if dictData.object(forKey: "toast") as! String == "0" {
                        self.objSettings.toast = false
                    }
         
                    if self.objSettings.all ?? true {
                        self.AllEnableDisable(true)
                    } else {
                        self.SetUI(1, self.objSettings.all ?? true)
                        self.SetUI(2, self.objSettings.events ?? true)
                        self.SetUI(3, self.objSettings.offers ?? true)
                        self.SetUI(4, self.objSettings.toast ?? true)
                    }
                } else {
                    
                    let dictData: NSDictionary = ["events":true,"toast":true,"offers":true,"all":true,"customer_id":"\(CUSTOMER_ID)",]
                    self.objSettings = TNotificationSettings.init(object: dictData)
                    self.AllEnableDisable(true)
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiSaveNotificationSettings() {
        if !isConnectedToNetwork() || objSettings == nil { return }
        self.view.endEditing(true)
        
        let dictParam: NSDictionary = ["all":objSettings.all ?? true,"events":objSettings.events ?? true,"toast":objSettings.toast ?? true,"offers":objSettings.offers ?? true]
        print(dictParam)
        let service = SERVER_URL + APINotificationSettings
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APINotificationSettings,
            parameters: dictParam,
            method: .patch,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
            }
        }
    }
}

//MARK:- CLASS METHOS
extension NotificactionSettingsVC {
    func AllEnableDisable(_ isEnable: Bool) {
        for i in 1..<5 {
            SetUI(i, isEnable)
        }
        
        if isEnable {
            for i in 2..<5 {
                let btnS = self.view.viewWithTag(i) as! UISwitch
                btnS.isEnabled = false
            }
        } else {
            for i in 2..<5 {
                SetUI(i, true)
            }
            for i in 2..<5 {
                let btnS = self.view.viewWithTag(i) as! UISwitch
                btnS.isEnabled = true
            }
        }
    }
    
    func SetUI(_ tag: Int, _ isEnable: Bool) {
        let btnS = self.view.viewWithTag(tag) as! UISwitch
        let lblS = self.view.viewWithTag(tag*10) as! UILabel
        btnS.isOn = isEnable
        if isEnable {
            lblS.textColor = Color_Hex(hex: "#3CBCB4")
            lblS.font = FontWithSize("Montserrat-SemiBold", 13)
        } else {
            lblS.textColor = UIColor.white
            lblS.font = FontWithSize("Montserrat-Regular", 13)
        }
    }
    
    func SetObject() {
        for i in 1..<5 {
            let btnS = self.view.viewWithTag(i) as! UISwitch
            switch i {
            case 1:
                objSettings.all = btnS.isOn
            case 2:
                objSettings.events = btnS.isOn
            case 3:
                objSettings.offers = btnS.isOn
            case 4:
                objSettings.toast = btnS.isOn
            default:
                break
            }
        }
    }
}
