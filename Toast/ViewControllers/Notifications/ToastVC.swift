//
//  ToastVC.swift
//  Toast
//
//  Created by iMac on 16/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellToast: UITableViewCell  {
    @IBOutlet var lblToastTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblDate: UILabel!
}

class ToastVC: UIViewController {

    @IBOutlet var tblToast: UITableView!
    @IBOutlet var bottom: NSLayoutConstraint!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var lblNoToast: UILabel!
    var arrToast = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() <= 736 {
            bottom.constant = 34
        }
        apiGetToast()
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshToast), name: NSNotification.Name(rawValue: TOAST_PAGE_REFRESH), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnToast = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnToast = false
    }
    
    @objc func RefreshToast() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetToast()
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension ToastVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        lblNoToast.alpha = arrToast.count > 0 ? 0 : 1
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrToast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellToast = tableView.dequeueReusableCell(withIdentifier: "CellToast", for: indexPath) as! CellToast
        let dictToast = arrToast[indexPath.row] as! NSDictionary
        let dictNotiObj = dictToast.object(forKey: "notification_obj") as! NSDictionary
        cell.lblToastTitle.text = "Hello From \(dictToast.object(forKey: "restraunt_name") as! String)"
        cell.lblDesc.text = dictNotiObj.object(forKey: "text") as? String
        
        let milisecond = TimeInterval(dictToast["created_on"] as! String)
        let dateVar = Date.init(timeIntervalSinceNow: (milisecond ?? 0)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        cell.lblDate.text = dateFormatter.string(from: dateVar)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: API CALLING
extension ToastVC {
    func apiGetToast() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIToast
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIToast,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            self.activity.stopAnimating()
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.arrToast = NSMutableArray()
                    //self.arrToast = arrData.mutableCopy() as! NSMutableArray
                    self.arrToast = (arrData.reverseObjectEnumerator().allObjects as NSArray).mutableCopy() as! NSMutableArray
                    self.tblToast.reloadData()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}
