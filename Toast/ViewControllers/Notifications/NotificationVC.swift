//
//  NotificationVC.swift
//  Toast
//
//  Created by iMac on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet var viewPage: UIView!
    var isFromPush: Bool = false
    var isFromRecipe: Bool = false
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetButtonColors(self.view.viewWithTag(1) as! UIButton)
        AddRightSwipeGesture()
        SetPageMenu()
        NotificationCenter.default.addObserver(self, selector: #selector(self.MoveToPage), name: NSNotification.Name(rawValue: NOTIFICATION_PAGE_REFRESH), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnNotificationTab = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnNotificationTab = false
    }
    
    @objc func MoveToPage() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            //isFromPush = true
            if APP_DELEGATE.NotiPageIndex == pageMenu?.currentPageIndex {
                APP_DELEGATE.isPostNoti = true
                refreshpage()
            } else {
                self.pageMenu?.moveToPage(APP_DELEGATE.NotiPageIndex)
                runAfterTime(time: 0.6) {
                    self.refreshpage()
                }
                //SetPageMenu()
            }
        }
    }
    
    func refreshpage() {
        if APP_DELEGATE.NotiPageIndex == 0 {
            NotificationCenter.default.post(name: Notification.Name(EVENTS_PAGE_REFRESH), object: nil)
        } else if APP_DELEGATE.NotiPageIndex == 1 {
            NotificationCenter.default.post(name: Notification.Name(OFFERS_PAGE_REFRESH), object: nil)
        } else if APP_DELEGATE.NotiPageIndex == 2 {
            NotificationCenter.default.post(name: Notification.Name(TOAST_PAGE_REFRESH), object: nil)
        }
        let btn = self.view.viewWithTag(APP_DELEGATE.NotiPageIndex+1) as! UIButton
        SetButtonColors(btn)
        APP_DELEGATE.NotiPageIndex = -1
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMenus(_ sender: UIButton) {
        SetButtonColors(sender)
        pageMenu?.moveToPage(sender.tag-1)
    }
    
    @IBAction func btnSettings(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "NotificactionSettingsVC")
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func SetButtonColors(_ sender: UIButton) {
        for i in 1..<4 {
            let btn = self.view.viewWithTag(i) as! UIButton
            btn.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        }
        sender.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
    }
}

//MARK:- SET PAGEMENU
extension NotificationVC {
    func SetPageMenu() {
        pageMenu?.view.removeFromSuperview()
        pageMenu?.removeFromParent()
        
        var arrController : [UIViewController] = []
    
        let events = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "EventsVC")
        arrController.append(events)
        
        let offers = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "OffersVC")
        arrController.append(offers)
        
        let toast = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "ToastVC")
        arrController.append(toast)
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
        if isFromPush {
            isFromPush = false
            runAfterTime(time: 0.5) {
                let btn = self.view.viewWithTag(APP_DELEGATE.NotiPageIndex+1) as! UIButton
                self.SetButtonColors(btn)
                self.pageMenu?.moveToPage(APP_DELEGATE.NotiPageIndex)
                APP_DELEGATE.NotiPageIndex = -1
            }
        }
        if isFromRecipe {
            runAfterTime(time: 0.5) {
                let btn = self.view.viewWithTag(3) as! UIButton
                self.SetButtonColors(btn)
                self.pageMenu?.moveToPage(2)
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension NotificationVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
