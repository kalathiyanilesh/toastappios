//
//  EventsVC.swift
//  Toast
//
//  Created by iMac on 16/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellEvents: UITableViewCell {
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var lblEventName: UILabel!
    @IBOutlet var imgEvent: UIImageView!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnAttend: UIButton!
    @IBOutlet var lblTotalPeopleAttend: UILabel!
    @IBOutlet var lblTimeAdd: UILabel!
    @IBOutlet var widthAttendbtn: NSLayoutConstraint!
}

class EventsVC: UIViewController {

    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var tblEvents: UITableView!
    @IBOutlet var bottom: NSLayoutConstraint!
    @IBOutlet var lblNoEvents: UILabel!
    
    var dictEvents = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() <= 736 {
            bottom.constant = 34
        }
        if !isConnectedToNetwork() {
            activity.stopAnimating()
        }
        apiGetEvents()
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshEvent), name: NSNotification.Name(rawValue: EVENTS_PAGE_REFRESH), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnEvents = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnEvents = false
    }
    
    @objc func RefreshEvent() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetEvents()
        }
    }
    
    func getDictAllKeys() -> NSArray {
        return (dictEvents.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
    }
}

//MARK: API CALLING
extension EventsVC {
    func apiGetEvents() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIEvents
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIEvents,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            self.activity.stopAnimating()
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.dictEvents = NSMutableDictionary()
                    for i in 0..<arrData.count {
                        var arrEvents = NSMutableArray()
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TEvents = TEvents.init(object: dictData)
                        if let timeResult = Double(obj.eventOn ?? "0") {
                            let date = Date(timeIntervalSince1970: timeResult)
                            let sDate = getDateString(date, format: "dd MMM yyyy")
                            if (self.dictEvents.object(forKey: sDate) != nil) {
                                arrEvents = (self.dictEvents.object(forKey: sDate) as! NSArray).mutableCopy() as! NSMutableArray
                                arrEvents.add(obj)
                            } else {
                                arrEvents.add(obj)
                            }
                            self.dictEvents.setObject(arrEvents.copy(), forKey: sDate as NSCopying)
                        }
                    }
                    self.tblEvents.reloadData()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiEventsAttendUnattend(_ eventID: String, sAttending: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIEvents + "/\(eventID)"
        let dictParam: NSDictionary = ["attending":sAttending == "1" ? true : false]
        print(dictParam)
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIAttendUnAttend,
            parameters: dictParam,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension EventsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let arrSection = getDictAllKeys()
        lblNoEvents.alpha = arrSection.count > 0 ? 0 : 1
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = self.view.backgroundColor
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 15, width: headerView.frame.width-20, height: 0.5)
        label.backgroundColor = Color_Hex(hex: "#707070")
        
        let labelDate = UILabel()
        labelDate.frame = CGRect.init(x: (headerView.frame.width-95)/2, y: 8, width: 95, height: 15)
        labelDate.text = getDictAllKeys()[section] as? String
        labelDate.textAlignment = .center
        labelDate.font = FontWithSize("Montserrat-Regular", 12)
        labelDate.textColor = UIColor.black
        labelDate.backgroundColor = self.view.backgroundColor
        
        headerView.addSubview(label)
        headerView.addSubview(labelDate)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrEvents: NSArray = dictEvents.object(forKey: getDictAllKeys()[section]) as! NSArray
        return arrEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellEvents = tableView.dequeueReusableCell(withIdentifier: "CellEvents", for: indexPath) as! CellEvents
        let arrEvents: NSArray = dictEvents.object(forKey: getDictAllKeys()[indexPath.section]) as! NSArray
        let objEvent = arrEvents[indexPath.row] as! TEvents
        cell.imgEvent.sd_setImage(with: URL(string: objEvent.eventImageUrl?.RemoveSpace() ?? ""), placeholderImage: EVENT_PLACE_HOLDER)
        cell.lblEventName.text = objEvent.eventName
        cell.lblTotalPeopleAttend.text = "\(objEvent.attending ?? "0") people attending"
        cell.lblTimeAdd.text = ""
        if let timeResult = Double(objEvent.eventOn ?? "0") {
            let date = Date(timeIntervalSince1970: timeResult)
            let sDate = getDateString(date, format: "hh:mm a")
            cell.lblTimeAdd.text = "\(sDate) | \(objEvent.eventLocation ?? "")"
        }
        
        cell.widthAttendbtn.constant = 53
        cell.btnAttend.setImage(UIImage(named: "ic_attend"), for: .normal)
        if objEvent.attending ?? "0" == "1" {
            cell.widthAttendbtn.constant = 76
            cell.btnAttend.setImage(UIImage(named: "ic_attending"), for: .normal)
        }
        cell.btnAttend.addTarget(self, action: #selector(btnAttendEvent(_ :)), for: .touchUpInside)
        cell.btnShare.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arrEvents: NSArray = dictEvents.object(forKey: getDictAllKeys()[indexPath.section]) as! NSArray
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "EventDetailVC") as! EventDetailVC
        vc.delegateEventDetails = self
        vc.objEvent = arrEvents[indexPath.row] as? TEvents
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    @objc func btnAttendEvent(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblEvents)
        let indexPath = tblEvents.indexPathForRow(at: buttonPosition)
        let arrEvents: NSArray = dictEvents.object(forKey: getDictAllKeys()[indexPath!.section]) as! NSArray
        let objEvent = arrEvents[indexPath!.row] as! TEvents
        if objEvent.attending ?? "0" == "1" {
            objEvent.attending = "0"
        } else {
            objEvent.attending = "1"
        }
        tblEvents.reloadData()
        apiEventsAttendUnattend(objEvent.eventId ?? "0", sAttending: objEvent.attending ?? "0")
    }
    
}

extension EventsVC: delegateEventDetails {
    func UpdateAttendUnattendEvent(_ objEvent: TEvents!) {
        tblEvents.reloadData()
        apiEventsAttendUnattend(objEvent.eventId ?? "0", sAttending: objEvent.attending ?? "0")
    }
}
