//
//  EventDetailVC.swift
//  Toast
//
//  Created by iMac on 17/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import CHIPageControl

protocol delegateEventDetails {
    func UpdateAttendUnattendEvent(_ objEvent: TEvents!)
}

class CellEventImages: UICollectionViewCell {
    @IBOutlet var imgEvent: UIImageView!
    @IBOutlet var btnPlay: UIButton!
}

class EventDetailVC: UIViewController {

    @IBOutlet var collImages: UICollectionView!
    @IBOutlet var pagecontrol: CHIPageControlChimayo!
    @IBOutlet var viewBlur: UIView!
    @IBOutlet var lblEventName: UILabel!
    @IBOutlet var lblTotalAttending: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnAttending: UIButton!
    @IBOutlet var widthbtnAttend: NSLayoutConstraint!
    @IBOutlet var txtDesc: UITextView!
    @IBOutlet var viewDetails: UIView!
    
    var delegateEventDetails: delegateEventDetails?
    var objEvent: TEvents!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetails.alpha = 0
        AddBlurEffect(viewBlur)
        AddRightSwipeGesture()
        pagecontrol.numberOfPages = 1
        pagecontrol.radius = 3
        pagecontrol.tintColor = UIColor.white
        pagecontrol.currentPageTintColor = UIColor.white
        pagecontrol.padding = 2
        
        SetUpEventData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.viewDetails.alpha = 1
        }
    }
    
    func SetUpEventData() {
        
        lblEventName.text = objEvent.eventName
        lblTotalAttending.text = "\(objEvent.attending ?? "0") attending"
        lblDateTime.text = ""
        if let timeResult = Double(objEvent.eventOn ?? "0") {
            let date = Date(timeIntervalSince1970: timeResult)
            lblDateTime.text = getDateString(date, format: "dd MMM yyyy | hh:mm a")
        }
        
        lblAddress.text = "\(objEvent.restrauntName ?? ""), \(objEvent.eventLocation ?? "")"
        txtDesc.text = objEvent.eventDescription
        
        UpdateEventWidth()
    }
    
    func UpdateEventWidth() {
        widthbtnAttend.constant = 53
        btnAttending.setImage(UIImage(named: "ic_attend"), for: .normal)
        if objEvent.attending ?? "0" == "1" {
            widthbtnAttend.constant = 76
            btnAttending.setImage(UIImage(named: "ic_attending"), for: .normal)
        }
    }
    

    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnAttend(_ sender: Any) {
        if objEvent.attending ?? "0" == "1" {
            objEvent.attending = "0"
        } else {
            objEvent.attending = "1"
        }
        UpdateEventWidth()
        self.delegateEventDetails?.UpdateAttendUnattendEvent(objEvent)
    }
    
    @IBAction func btnShare(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension EventDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellEventImages = collectionView.dequeueReusableCell(withReuseIdentifier: "CellEventImages", for: indexPath) as! CellEventImages
        cell.imgEvent.sd_setImage(with: URL(string: objEvent.eventImageUrl?.RemoveSpace() ?? ""), placeholderImage: EVENT_PLACE_HOLDER)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}

//MARK: ScrollView Delegate
extension EventDetailVC {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        pagecontrol.set(progress: Int(offSet + horizontalCenter) / Int(width), animated: true)
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension EventDetailVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
