//
//  HomeVC.swift
//  Toast
//
//  Created by iMac on 03/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellPopularRestaurant: UITableViewCell {
    @IBOutlet var collRestuarants: UICollectionView!
    @IBOutlet var btnViewAll: UIButton!
}

extension CellPopularRestaurant {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        collRestuarants.delegate = dataSourceDelegate
        collRestuarants.dataSource = dataSourceDelegate
        collRestuarants.tag = row
        collRestuarants.setContentOffset(collRestuarants.contentOffset, animated:false)
        collRestuarants.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collRestuarants.contentOffset.x = newValue }
        get { return collRestuarants.contentOffset.x }
    }
}

class CellCollRestaurant: UICollectionViewCell {
    @IBOutlet var imgRes: UIImageView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblRate: UILabel!
}

class CellCovid: UITableViewCell {
    
    @IBOutlet var btnViewAll: UIButton!
}

class CellPopularRecipe: UITableViewCell {
    @IBOutlet weak var labelOwnerName: UILabel!
    @IBOutlet weak var imageViewOwner: UIImageView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var labelLikeCount: UILabel!
    @IBOutlet weak var labelRecipeName: UILabel!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewRecipe: UIImageView!
    @IBOutlet var btlike: UIButton!
    @IBOutlet var imgVerified: UIImageView!
    @IBOutlet var lblCookingTime: UILabel!
}

class HomeVC: UIViewController {

    @IBOutlet var tblPopular: UITableView!
    @IBOutlet var imguser: UIImageView!
    @IBOutlet var heightColorView: NSLayoutConstraint!
    @IBOutlet var viewDTD: UIView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewAllOrder: UIView!
    
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    @IBOutlet var lblType: UILabel!
    
    @IBOutlet var bottomViewAllOrder: NSLayoutConstraint!
    var storedOffsets = [Int: CGFloat]()
    var arrPopularRecipe = [TRecipeDetail]()
    var arrPopularRestaurantList = [TRestrauntList]()
    var refreshControl = UIRefreshControl()
    
    var arrAllOrder = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPopular.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        apiGetUserInfo()
        apiGetPopularRecipe()
        bottomViewAllOrder.constant = -200
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshHomePage), name: NSNotification.Name(rawValue: HOME_PAGE_REFRESH), object: nil)
        
        if (UserDefaults.standard.object(forKey: IS_ENTER_TABLE) != nil) {
            let strCode = getUserDefautSavedString(TEMP_MY_JOIN_CODE)
            APP_DELEGATE.strScannedQR = getUserDefautSavedString(TEMP_MY_QR_CODE)
            if strCode.count > 0 {
                self.apiJoinWithCode(strCode)
            } else {
                CheckTableOngoing(responseData: { (isGoToScan) in })
            }
        } else {
            CheckTableOngoing(responseData: { (isGoToScan) in })
        }
        
        getCurrentCity { (sCity) in
            if sCity?.count ?? 0 <= 0 {
                let selectedCity = GetUserSelectedCity()
                if selectedCity.count <= 0 {
                    let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "SelectCityVC") as! SelectCityVC
                    vc.delegateSelectedCity = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isInHomeScreen = true
        apiGetPopularRestaurant()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isInHomeScreen = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (UIScreen.main.nativeBounds.height > 2209) || UIScreen.main.nativeBounds.height == 1792 {
            heightColorView.constant = viewDTD.frame.origin.y+45+22
        } else {
            heightColorView.constant = viewDTD.frame.origin.y+45
        }
        self.view.layoutIfNeeded()
        apiGetActiveOrders()
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        apiGetPopularRecipe()
        apiGetPopularRestaurant()
    }
    
    @objc func RefreshHomePage() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetActiveOrders()
        }
    }

    //MARK:- Button Action
    @IBAction func btnUser(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "ReviewOrderVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonAddRecipeTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "AddRecipeVC") as! AddRecipeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func btnNotification(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "NotificationVC") as! NotificationVC
        vc.isFromRecipe = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnViewAllOrder(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "ActiveOrdersVC") as! ActiveOrdersVC
        vc.arrAllOrder = arrAllOrder
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonDineInTouchUpInside(_ sender: Any) {
        /*
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "RestaurantVC") as! RestaurantVC
        self.navigationController?.pushViewController(vc, animated: true)*/
        CheckTableOngoing(responseData: { (isGoToScan) in
            if isGoToScan ?? false {
                let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "TabVC")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func buttonTakeAwayTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "RestaurantListVC") as! RestaurantListVC
        vc.isTakeAway = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonDeliveryTouchUpInside(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "RestaurantListVC") as! RestaurantListVC
        vc.isDelivery = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + arrPopularRecipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: CellPopularRestaurant = tableView.dequeueReusableCell(withIdentifier: "CellPopularRestaurant", for: indexPath) as! CellPopularRestaurant
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAllRestaurant(_ :)), for: .touchUpInside)
            return cell
        case 1:
            let cell: CellCovid = tableView.dequeueReusableCell(withIdentifier: "CellCovid", for: indexPath) as! CellCovid
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAllRecipe(_ :)), for: .touchUpInside)
            return cell
        default:
            let cell: CellPopularRecipe = tableView.dequeueReusableCell(withIdentifier: "CellPopularRecipe", for: indexPath) as! CellPopularRecipe
            let obj: TRecipeDetail = arrPopularRecipe[indexPath.row-2]
            let arrimages = obj.imagesUrls
            if arrimages?.count ?? 0 > 0 {
                cell.imageViewRecipe.sd_setImage(with: URL(string: TRIM(string: (arrimages?[0]) ?? "")), placeholderImage: FOOD_PLACE_HOLDER)
            }
            cell.labelRecipeName.text = obj.name
            cell.labelLikeCount.text = obj.likesCount
            
            let objpub: TPublisher = obj.publisher!
            cell.imgVerified.isHidden = true
            if objpub.isverified == "1" {
                cell.imgVerified.isHidden = false
            }
            cell.imageViewOwner.sd_setImage(with: URL(string: objpub.avatarUrl?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
            cell.labelOwnerName.text = objpub.name
            if obj.recipeType == "non_veg" {
                cell.imageViewVeg.image = UIImage(named: "ic_nonveg")
            } else {
                cell.imageViewVeg.image = UIImage(named: "ic_veg")
            }
            if obj.isLiked == "1"  {
                cell.imageLike.image = UIImage(named: "like")
            } else {
                cell.imageLike.image = UIImage(named: "dislike")
            }
            
            if obj.cookingHours != nil && obj.cookingHours != "" && Int(obj.cookingHours ?? "0") != 0 {
                if obj.cookingHours != nil || obj.cookingMins == "" || Int(obj.cookingHours ?? "0") != 0 {
                    cell.lblCookingTime.text = "\(String(format: "%02d", Int(TRIM(string: obj.cookingHours ?? "")) ?? 0)) hours   "
                } else {
                    cell.lblCookingTime.text = "\(String(format: "%02d", Int(TRIM(string: obj.cookingHours ?? "")) ?? 0)) hours \(String(format: "%02d", Int(TRIM(string: obj.cookingMins ?? "")) ?? 0)) mins   "
                }
            } else {
                cell.lblCookingTime.text = "\(String(format: "%02d", Int(TRIM(string: obj.cookingMins ?? "")) ?? 0)) mins   "
            }
            
            cell.btlike.addTarget(self, action: #selector(btlike(sender:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeVC")
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            let model = arrPopularRecipe[indexPath.row-2] as TRecipeDetail
            let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeDetailsVC") as! RecipeDetailsVC
            vc.objectRecipeDetail = model
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 248
        case 1:
            return 220
        default:
            return 85
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPopularRestaurant else { return }
            promoCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            promoCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPopularRestaurant else { return }
            storedOffsets[indexPath.row] = promoCell.collectionViewOffset
        default:
            break
        }
    }
    
    
    //MARK:- UITABLEVIEW CELL BUTTON
    @objc func btlike(sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblPopular)
        let indexPath = tblPopular.indexPathForRow(at: buttonPosition)
        let obj: TRecipeDetail = arrPopularRecipe[indexPath!.row]
        let totalcount = Int(obj.likesCount ?? "0")!
        if obj.isLiked == "1"  {
            unLikeRecipeAPICall(obj: obj)
            obj.isLiked = "0"
            obj.likeId = ""
            obj.likesCount = String(totalcount-1)
        } else {
            likeRecipeAPICall(obj: obj)
            obj.isLiked = "1"
            obj.likesCount = String(totalcount+1)
        }
        tblPopular.reloadData()
    }
    
    @objc func btnViewAllRecipe(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "RecipeVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func btnViewAllRestaurant(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "RestaurantListVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPopularRestaurantList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellCollRestaurant = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollRestaurant", for: indexPath) as! CellCollRestaurant
        
        let model = arrPopularRestaurantList[indexPath.row]
        
        cell.imgUser.image = UIImage(named: "")
        cell.imgRes.image = UIImage(named: "")
        cell.imgUser.sd_setImage(with: URL(string: model.restrauntImage?.RemoveSpace() ?? ""), placeholderImage: RESTAURANT_PLACE_HOLDER)
        cell.imgRes.sd_setImage(with: URL(string: model.featureImage?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        cell.lblName.text = model.restrauntName ?? ""
        cell.lblType.text = model.restrauntStoreDetails?.area ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = arrPopularRestaurantList[indexPath.row]
        OpenPriceLessMenu(model.restrauntStoreDetails?.restrauntId ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:150, height:200)
    }
}

//MARK: API CALLING
extension HomeVC {
    func apiGetPopularRecipe() {
        self.view.endEditing(true)
        let service = SERVER_RECIPE_URL + APIRecipe
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIRecipe,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                self.arrPopularRecipe.removeAll()
                if let arrData = responseDict?.object(forKey: "recipes") as? NSArray {
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRecipeDetail = TRecipeDetail.init(object: dictData)
                        self.arrPopularRecipe.append(obj)
                    }
                }
                self.tblPopular.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    func apiGetPopularRestaurant() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIPopularRestaurant
        var dictParam: NSDictionary = NSDictionary()
        let sCity = GetUserSelectedCity()
        if sCity.count > 0 {
            dictParam = ["city":sCity]
        } else {
            dictParam = ["city":APP_DELEGATE.strCurrentCity, "lat":"\(lat_currnt)", "lon":"\(long_currnt)"]
        }
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIPopularRestaurant,
            parameters: dictParam,
            method: .post,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                self.arrPopularRestaurantList.removeAll()
                let arrData = responseDict?.object(forKey: "data") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRestrauntList = TRestrauntList.init(object: dictData)
                    self.arrPopularRestaurantList.append(obj)
                }
                self.tblPopular.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    func apiGetUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let objUser = TUser.init(object: dictData)
                    self.imguser.sd_setImage(with: URL(string: objUser.profilePicture?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
                    self.lblUserName.text = "Hey \(objUser.customerName ?? "")!"
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetActiveOrders() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIActiveOrder
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIActiveOrder,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.arrAllOrder = NSMutableArray()
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TAllOrder = TAllOrder.init(object: dictData)
                        if i == 0 {
                            self.lblOrderID.text = "Order ID #\(obj.orderId ?? "")"
                            self.lblRestaurantName.text = obj.restrauntDetails?.restrauntName ?? ""
                            self.lblType.text = (obj.orderType)?.replacingOccurrences(of: "_", with: " ").uppercased()
                            self.lblOrderStatus.text = obj.orderStatus?.uppercased()
                        }
                        self.arrAllOrder.add(obj)
                    }
                    if self.arrAllOrder.count > 0 {
                        self.bottomViewAllOrder.constant = 0
                        self.tblPopular.tableFooterView = self.viewHeader
                    } else {
                        self.bottomViewAllOrder.constant = -200
                        self.tblPopular.tableFooterView = nil
                    }
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            } else {
                self.bottomViewAllOrder.constant = -200
                self.tblPopular.tableFooterView = nil
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func OpenPriceLessMenu(_ restrauntId: String) {
        apiOpenPricelessMenu(restrauntId: restrauntId) { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    
                    let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                    if let dictMenu = dictData.object(forKey: "menu") as? NSDictionary {
                        vc.dictMenu = dictMenu
                    }
                    if let dictRestaurantDetails = dictData.object(forKey: "restraunt_details") as? NSDictionary {
                        APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                    }
                    vc.PrcelessRestaurantID = restrauntId
                    vc.isFromPriceLess = true
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func apiJoinWithCode(_ strCode: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIJoinWithCode
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "table_joining_code":strCode]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIJoinWithCode,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "failure" {
                            //showMessage(dictData.object(forKey: "reason") as? String ?? "Try Again!")
                            ClearTableCodes()
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    } else if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        SaveMyJoinCode(strCode)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

extension HomeVC: delegateSelectedCity {
    func getSelectedCity(_ sCity: String) {
        print(sCity)
        APP_DELEGATE.strCurrentCity = sCity
        UserDefaults.standard.set(sCity, forKey: "userselectedcity")
        self.apiGetPopularRestaurant()
    }
}
