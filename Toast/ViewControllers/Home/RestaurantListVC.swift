//
//  RestaurantListVC.swift
//  Toast
//
//  Created by iMac on 03/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellNearByRestaurant: UITableViewCell {
    @IBOutlet var imgRestaurant: UIImageView!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblRestaurantDesc: UILabel!
    @IBOutlet var lblRate: UILabel!
    @IBOutlet var lblTotalDistance: UILabel!
}

class RestaurantListVC: UIViewController {

    @IBOutlet var tblRestaurant: UITableView!
    @IBOutlet weak var buttonDineIn: UIButton!
    @IBOutlet weak var buttonTakeAway: UIButton!
    @IBOutlet weak var buttonDelivery: UIButton!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var viewTAScan: UIView!
    
    var storedOffsets = [Int: CGFloat]()
    var isDineIn = Bool()
    var isTakeAway = Bool()
    var isDelivery = Bool()
    var isSearch = false
    var arrPopularRestaurantList = [TRestrauntList]()
    var arrNearRestaurantList = [TRestrauntList]()
    var arrSearchRestaurantList = [TRestrauntList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        
        /*
        if isTakeAway {
            buttonTakeAwayTouchUpInside(UIButton())
        } else if isDelivery {
            buttonDeliveryTouchUpInside(UIButton())
        } else if isDineIn {
            buttonDineInTouchUpInside(UIButton())
        }*/
        
        textFieldSearch.isEnabled = false
        textFieldSearch.text = ""
        textFieldSearch.delegate = self
        
        let sCity = GetUserSelectedCity()
        if sCity.count > 0 {
            self.textFieldSearch.text = sCity
        } else {
            getCurrentCity { (sCity) in
                self.textFieldSearch.text = sCity
            }
        }
        
        
        apiGetPopularRestaurant()
        setupManageType()
    }
    
    private func setupManageType() {
        viewTAScan.alpha = 0
        //textFieldSearch.text = ""
        for i in 1..<4 {
            let btn = self.view.viewWithTag(i) as! UIButton
            btn.backgroundColor = #colorLiteral(red: 0.6594591737, green: 0.8258991838, blue: 0.8606379628, alpha: 1)
        }
        if isDineIn {
            buttonDineIn.backgroundColor = #colorLiteral(red: 0.2743178606, green: 0.7775933146, blue: 0.7579391599, alpha: 1)
        } else if isTakeAway {
            buttonTakeAway.backgroundColor = #colorLiteral(red: 0.2743178606, green: 0.7775933146, blue: 0.7579391599, alpha: 1)
            viewTAScan.alpha = 1
        } else if isDelivery {
            buttonDelivery.backgroundColor = #colorLiteral(red: 0.2743178606, green: 0.7775933146, blue: 0.7579391599, alpha: 1)
        }
        if isDineIn || isTakeAway || isDelivery {
            apiGetPopularRestaurant()
            apiGetNearRestaurant()
        }
    }
    
    @IBAction func btnCity(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "SelectCityVC") as! SelectCityVC
        vc.delegateSelectedCity = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTakeAwayScan(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "TabVC") as! TabVC
        vc.modalPresentationStyle = .fullScreen
        vc.isFromTakeAway = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonDineInTouchUpInside(_ sender: Any) {
        viewTAScan.alpha = 0
        isDineIn = true
        isTakeAway = false
        isDelivery = false
        setupManageType()
    }
    
    @IBAction func buttonTakeAwayTouchUpInside(_ sender: Any) {
        viewTAScan.alpha = 1
        isDineIn = false
        isTakeAway = true
        isDelivery = false
        setupManageType()
    }
    
    @IBAction func buttonDeliveryTouchUpInside(_ sender: Any) {
        viewTAScan.alpha = 0
        isDineIn = false
        isTakeAway = false
        isDelivery = true
        setupManageType()
    }
    
    @IBAction func buttonSearchTouchUpInside(_ sender: Any) {
        self.view.endEditing(true)
    }
    
}

extension RestaurantListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + self.arrNearRestaurantList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: CellPopularRestaurant = tableView.dequeueReusableCell(withIdentifier: "CellPopularRestaurant", for: indexPath) as! CellPopularRestaurant
            return cell
        case 1:
            let cell: CellCovid = tableView.dequeueReusableCell(withIdentifier: "CellCovid", for: indexPath) as! CellCovid
            return cell
        default:
            let cell: CellNearByRestaurant = tableView.dequeueReusableCell(withIdentifier: "CellNearByRestaurant", for: indexPath) as! CellNearByRestaurant
            let model = self.arrNearRestaurantList[indexPath.row - 2]
            cell.lblRestaurantName.text = model.restrauntName ?? ""
            cell.lblRestaurantDesc.text = model.restrauntStoreDetails?.area ?? ""
            cell.imgRestaurant.sd_setImage(with: URL(string: model.restrauntImage?.RemoveSpace() ?? ""), placeholderImage: RESTAURANT_PLACE_HOLDER)
            
            let distance = CGFloat(((model.distanceKm ?? "0") as NSString).floatValue)
            let sDistance = String(format:"%0.2f",distance)
            cell.lblTotalDistance.text = "\(sDistance) km"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        default:
            if isTakeAway {
                let model = self.arrNearRestaurantList[indexPath.row - 2]
                self.apiJoinViaDelivery(restrauntId: model.restrauntStoreDetails?.restrauntId ?? "")
            } else if isDineIn {
                GoForScan()
            } else if isDelivery {
                let model = self.arrNearRestaurantList[indexPath.row - 2]
                self.apiJoinViaDelivery(restrauntId: model.restrauntStoreDetails?.restrauntId ?? "")
            } else {
                let model = self.arrNearRestaurantList[indexPath.row - 2]
                OpenPriceLessMenu(model.restrauntStoreDetails?.restrauntId ?? "")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 0//248
        case 1:
            return 40//65
        default:
            return 98
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPopularRestaurant else { return }
            promoCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            promoCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let promoCell = cell as? CellPopularRestaurant else { return }
            storedOffsets[indexPath.row] = promoCell.collectionViewOffset
        default:
            break
        }
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension RestaurantListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPopularRestaurantList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellCollRestaurant = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollRestaurant", for: indexPath) as! CellCollRestaurant
        
        let model = arrPopularRestaurantList[indexPath.row]
        
        cell.imgUser.sd_setImage(with: URL(string: model.restrauntImage?.RemoveSpace() ?? ""), placeholderImage: RESTAURANT_PLACE_HOLDER)
        cell.imgRes.sd_setImage(with: URL(string: model.featureImage?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        cell.lblName.text = model.restrauntName ?? ""
        cell.lblType.text = model.restrauntStoreDetails?.area ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isTakeAway {
            let model = self.arrPopularRestaurantList[indexPath.row]
            self.apiJoinViaDelivery(restrauntId: model.restrauntStoreDetails?.restrauntId ?? "")
        } else if isDineIn {
            GoForScan()
        } else if isDelivery {
            let model = arrPopularRestaurantList[indexPath.row]
            apiJoinViaDelivery(restrauntId: model.restrauntStoreDetails?.restrauntId ?? "")
        } else {
            let model = arrPopularRestaurantList[indexPath.row]
            OpenPriceLessMenu(model.restrauntStoreDetails?.restrauntId ?? "")
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:150, height:200)
    }
}

extension RestaurantListVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldSearch {
            if textField.text != "" {
                apiSearchRestaurant(str: textField.text!)
            } else {
                setupManageType()
            }
        }
    }
    
}

//MARK:- API CALLING
extension RestaurantListVC {
    func apiJoinViaDelivery(restrauntId: String) {
        self.view.endEditing(true)
        var strType = ""
        if isDelivery {
            strType = "delivery"
        } else {
            strType = "pick_up"
        }
        let service = SERVER_URL + APIMenuDelivery + strType
        APP_DELEGATE.restaurantID = restrauntId
        let dictParam: NSDictionary = ["restraunt_id":restrauntId]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuDelivery,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        //print(convertDictionaryToJSONString(dic: dictTemp) ?? "")
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        
                        if self.isDelivery {
                            vc.isFromDelivery = true
                        } else if self.isTakeAway {
                            vc.isFromTakeWay = true
                        }
                        
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func apiGetPopularRestaurant() {
        self.apiGetNearRestaurant()
        
        /*
         self.view.endEditing(true)
        let service = SERVER_URL + APIPopularRestaurant
        
        var dictParam: NSDictionary = NSDictionary()
        
        if isDineIn || isTakeAway || isDelivery {
            let sCity = GetUserSelectedCity()
            if sCity.count > 0 {
                dictParam = ["city":sCity]
            } else {
                dictParam = ["city":APP_DELEGATE.strCurrentCity, "lat":"\(lat_currnt)", "lon":"\(long_currnt)"]
            }
        } else {
            let sCity = GetUserSelectedCity()
            if sCity.count > 0 {
                dictParam = ["city":sCity]
            } else {
                dictParam = ["city":APP_DELEGATE.strCurrentCity, "lat":"\(lat_currnt)", "lon":"\(long_currnt)"]
            }
        }

        print("popular Res: ", dictParam)
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIPopularRestaurant,
            parameters: dictParam,
            method: .post,
            getReturn:true,
            showLoader: true)
        { (error, responseDict) in
            self.apiGetNearRestaurant()
            if error == nil {
                self.arrPopularRestaurantList.removeAll()
                if responseDict == nil {
                    return
                }
                let arrData = responseDict?.object(forKey: "data") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRestrauntList = TRestrauntList.init(object: dictData)
                    self.arrPopularRestaurantList.append(obj)
                }
                self.tblRestaurant.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }*/
    }
    
    func apiGetNearRestaurant() {
        
        self.view.endEditing(true)
        var strType = ""
        if isDineIn {
            strType = "/menu/dine_in"
        } else if isTakeAway {
            strType = "/menu/pick_up"
        } else if isDelivery {
            strType = "/menu/delivery"
        }
        var sCity = GetUserSelectedCity()
        if sCity.count <= 0 {
            sCity = APP_DELEGATE.strCurrentCity
        }
        var service = SERVER_URL + APISearchNearRestaurant + "/city/\(sCity)\(strType)?lat=\(lat_currnt)&lon=\(long_currnt)"
        service = service.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? service
        print(service)
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APISearchNearRestaurant,
            parameters: NSDictionary(),
            method: .get,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.arrNearRestaurantList.removeAll()
                let arrData = responseDict?.object(forKey: "data") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRestrauntList = TRestrauntList.init(object: dictData)
                    self.arrNearRestaurantList.append(obj)
                }
                self.tblRestaurant.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiSearchRestaurant(str: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APISearchRestaurant + "/\(str)"
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APISearchRestaurant,
            parameters: NSDictionary(),
            method: .get,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.arrNearRestaurantList.removeAll()
                let arrData = responseDict?.object(forKey: "data") as! NSArray
                for i in 0..<arrData.count {
                    let dictData = arrData[i] as! NSDictionary
                    let obj: TRestrauntList = TRestrauntList.init(object: dictData)
                    self.arrNearRestaurantList.append(obj)
                }
                
                self.tblRestaurant.reloadData()
                
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func OpenPriceLessMenu(_ restrauntId: String) {
        apiOpenPricelessMenu(restrauntId: restrauntId) { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    
                    let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                    if let dictMenu = dictData.object(forKey: "menu") as? NSDictionary {
                        vc.dictMenu = dictMenu
                    }
                    if let dictRestaurantDetails = dictData.object(forKey: "restraunt_details") as? NSDictionary {
                        APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                    }
                    vc.PrcelessRestaurantID = restrauntId
                    vc.isFromPriceLess = true
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func GoForScan() {
        CheckTableOngoing(responseData: { (isGoToScan) in
            if isGoToScan ?? false {
                let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "TabVC")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
}

extension RestaurantListVC: delegateSelectedCity {
    func getSelectedCity(_ sCity: String) {
        print(sCity)
        textFieldSearch.text = sCity
        UserDefaults.standard.set(sCity, forKey: "userselectedcity")
        self.apiGetPopularRestaurant()
    }
}
