//
//  SelectCityVC.swift
//  Toast
//
//  Created by macOS on 03/07/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellCity: UITableViewCell {
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
}

protocol delegateSelectedCity {
    func getSelectedCity(_ sCity: String)
}

class SelectCityVC: UIViewController {

    @IBOutlet weak var tblCity: UITableView!
    var delegateSelectedCity: delegateSelectedCity?
    var arrCity = NSMutableArray()
    var selectedIndex: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        apiGetCityList()
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectCityVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellCity = tableView.dequeueReusableCell(withIdentifier: "CellCity", for: indexPath) as! CellCity
        cell.lblCity.text = (arrCity.object(at: indexPath.row) as? String)?.capitalized
        cell.imgSelection.isHidden = (selectedIndex == indexPath.row) ? false : true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblCity.reloadData()
        
        runAfterTime(time: 0.5) {
            self.delegateSelectedCity?.getSelectedCity(self.arrCity.object(at: indexPath.row) as! String)
            self.btnBack(UIButton())
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension SelectCityVC {
    func apiGetCityList() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIGetCity
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetCity,
            parameters: NSDictionary(),
            method: .get,
            isrecipeapi: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.arrCity = arrData.mutableCopy() as! NSMutableArray
                }
                self.tblCity.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
            
        }
    }
}
