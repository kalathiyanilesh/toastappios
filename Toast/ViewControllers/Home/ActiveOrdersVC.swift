//
//  ActiveOrdersVC.swift
//  Toast
//
//  Created by iMac on 24/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellActiveOrder: UITableViewCell {
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblPaidTypePrice: UILabel!
}

class ActiveOrdersVC: UIViewController {

    @IBOutlet var tblOrders: UITableView!
    var arrAllOrder = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshActiveOrderPage), name: NSNotification.Name(rawValue: ACTIVE_ORDER_PAGE_REFRESH), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isInActiveOrder = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isInActiveOrder = false
    }
    
    @objc func RefreshActiveOrderPage() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetActiveOrders()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITableViewDelegate METHOD
extension ActiveOrdersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellActiveOrder = tableView.dequeueReusableCell(withIdentifier: "CellActiveOrder", for: indexPath) as! CellActiveOrder
        let obj: TAllOrder = arrAllOrder.object(at: indexPath.row) as! TAllOrder
        cell.lblOrderID.text = "Order ID #\(obj.orderId ?? "")"
        cell.lblRestaurantName.text = obj.restrauntDetails?.restrauntName ?? ""
        cell.lblPaidTypePrice.text = "Paid \(obj.paymentDetails?.paymentType?.capitalized ?? "Online") | ₹\(obj.paymentDetails?.paymentAmount ?? "0")"
        cell.lblOrderStatus.text = obj.orderStatus?.uppercased()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj: TAllOrder = arrAllOrder.object(at: indexPath.row) as! TAllOrder
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "OrderSuccessVC") as! OrderSuccessVC
        vc.total = CGFloat(((obj.paymentDetails?.paymentAmount ?? "0") as NSString).floatValue)
        vc.isFromTakeAway = obj.orderType == "pick_up" ? true : false
        vc.isFromActiveOrder = true
        vc.objOrder = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
}

//MARK:- API CALLING
extension ActiveOrdersVC {
    func apiGetActiveOrders() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIActiveOrder
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIActiveOrder,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    self.arrAllOrder = NSMutableArray()
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TAllOrder = TAllOrder.init(object: dictData)
                        self.arrAllOrder.add(obj)
                    }
                    self.tblOrders.reloadData()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}
