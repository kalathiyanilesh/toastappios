//
//  TabVC.swift
//  Toast
//
//  Created by iMac on 16/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GLNotificationBar

class TabVC: UIViewController {

    var scanner: MTBBarcodeScanner?
    var isApiCalling = false
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false
    
    @IBOutlet var viewBlack: UIView!
    @IBOutlet var viewButtons: UIView!
    @IBOutlet var viewScan: UIView!
    @IBOutlet var imgBorder: UIImageView!
    @IBOutlet var viewRectScan: UIView!
    @IBOutlet var bottomViewButtons: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() < 736 { bottomViewButtons.constant = 12 }
        if isFromTakeAway {
            lblTitle.text = "Scan the QR code at the\nrestaurant for takeaway"
        }
        imgBorder.image = imgBorder.image?.withRenderingMode(.alwaysTemplate)
        imgBorder.tintColor = UIColor.white
        UserDefaults.standard.removeObject(forKey: MENU_DICT)
        AddGradient(viewButtons)
        scanner = MTBBarcodeScanner(previewView: viewScan)
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    // Start scanning with the front camera
                    try self.scanner?.startScanning(with: .back,
                                                    resultBlock: { codes in
                                                        self.scanner?.scanRect = self.viewRectScan.frame
                                                        if let codes = codes {
                                                            for code in codes {
                                                                let stringValue = code.stringValue ?? ""
                                                                print("Found code: \(stringValue)")
                                                                if APP_DELEGATE.isOnScanPage {
                                                                    if stringValue.validateUrl() {
                                                                        let url = URL(string: stringValue)
                                                                        APP_DELEGATE.strScannedQR = url?.valueOf("qr_code") ?? ""
                                                                    } else {
                                                                        APP_DELEGATE.strScannedQR = stringValue
                                                                    }
                                                                    if APP_DELEGATE.strScannedQR.count <= 0 { return }
                                                                    if self.isFromTakeAway {
                                                                        self.apiJoinViaTakeAway()
                                                                    } else if self.isFromDelivery {
                                                                        self.apiJoinViaDelivery()
                                                                    } else {
                                                                        self.apiJoin()
                                                                    }
                                                                }
                                                            }
                                                        }
                    })
                } catch {
                    NSLog("Unable to start scanning")
                }
            } else {
                showMessage("This app does not have permission to access the camera")
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnMenu = false
        APP_DELEGATE.isOnScanPage = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnScanPage = false
        /*
         DispatchQueue.main.async {
         self.scanner?.stopScanning()
         }*/
    }
    
    override func viewDidLayoutSubviews() {
        let rect = CGRect(x: (SCREENWIDTH()-viewRectScan.frame.width)/2, y: (viewBlack.frame.height-viewRectScan.frame.height)/2, width:  viewRectScan.frame.width, height:  viewRectScan.frame.height)
        viewBlack.mask(withRect: rect, inverse: true)
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnNotification(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "NotificationVC") as! NotificationVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnScan(_ sender: Any) {
        
    }
    
    @IBAction func btnUser(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "ReviewOrderVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func GoForJoinTable() {
        let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "JoinTableVC") as! JoinTableVC
        vc.modalPresentationStyle = .fullScreen
        vc.isFromTakeAway = self.isFromTakeAway
        vc.isFromDelivery = self.isFromDelivery
        self.present(vc, animated: true, completion: nil)
    }
    
    func GoToMenuScreen() {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
        vc.modalPresentationStyle = .fullScreen
        vc.isFromTakeWay = self.isFromTakeAway
        vc.isFromDelivery = self.isFromDelivery
        self.present(vc, animated: true, completion: nil)
    }
}

//MARK: API CALLING
extension TabVC {
    func apiJoin() {
        if  isApiCalling { return }
        isApiCalling = true
        self.view.endEditing(true)
        let service = SERVER_URL + APIJoin
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIJoin,
            parameters: dictParam,
            method: .post,
            getReturn: true,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "occupied" {
                            if (UserDefaults.standard.object(forKey: IS_ENTER_TABLE) != nil) {
                                let strCode = getUserDefautSavedString(TEMP_MY_JOIN_CODE)
                                if strCode.count > 0 {
                                    if APP_DELEGATE.strScannedQR == getUserDefautSavedString(TEMP_MY_QR_CODE) {
                                        self.apiJoinWithCode(strCode)
                                    } else {
                                        self.GoForJoinTable()
                                        showMessage(dictData.object(forKey: "message") as? String ?? "")
                                    }
                                } else {
                                    self.GoForJoinTable()
                                    showMessage(dictData.object(forKey: "message") as? String ?? "")
                                }
                            } else {
                                self.GoForJoinTable()
                                showMessage(dictData.object(forKey: "message") as? String ?? "")
                            }
                        }
                    } else if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        RemoveAllTempData()
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            self.isApiCalling = false
        }
    }
    
    func apiJoinViaTakeAway() {
        if  isApiCalling { return }
        isApiCalling = true
        self.view.endEditing(true)
        let service = SERVER_URL + APIJoinViaTakeAway
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIJoinViaTakeAway,
            parameters: dictParam,
            method: .post,
            getReturn: true,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
//                    if let strStatus = dictData.object(forKey: "status") as? String {
//                        if strStatus == "occupied" {
//                            self.GoForJoinTable()
//                            showMessage(dictData.object(forKey: "message") as? String ?? "")
//                        } else {
//                            showMessage("Work in progress!")
//                        }
//                    } else
                    if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.isFromTakeWay = self.isFromTakeAway
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            self.isApiCalling = false
        }
    }
    
    func apiJoinViaDelivery() {
        if  isApiCalling { return }
        isApiCalling = true
        self.view.endEditing(true)
        let service = SERVER_URL + APIMenuDelivery
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuDelivery,
            parameters: dictParam,
            method: .post,
            getReturn: true,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    //                    if let strStatus = dictData.object(forKey: "status") as? String {
                    //                        if strStatus == "occupied" {
                    //                            self.GoForJoinTable()
                    //                            showMessage(dictData.object(forKey: "message") as? String ?? "")
                    //                        } else {
                    //                            showMessage("Work in progress!")
                    //                        }
                    //                    } else
                    if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.isFromDelivery = self.isFromDelivery
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            self.isApiCalling = false
        }
    }
    
    func apiJoinWithCode(_ strCode: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIJoinWithCode
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "table_joining_code":strCode]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIJoinWithCode,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "failure" {
                            //showMessage(dictData.object(forKey: "reason") as? String ?? "Try Again!")
                            ClearTableCodes()
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    } else if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        SaveMyJoinCode(strCode)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func GetPastOrder() {
        if APP_DELEGATE.sOrderID.count > 0 {
            apiGetPastOrder(true) { (dictOrder) in
                if dictOrder != nil {
                    if isTableClose(dictOrder!) {
                        ClearTableCodes()
                    } else {
                        self.apiJoinWithCode(getMyJoinCode())
                    }
                }
            }
        }
    }
}

extension UIView {

    func mask(withRect rect: CGRect, inverse: Bool = false) {
        let path = UIBezierPath(rect: rect)
        let maskLayer = CAShapeLayer()

        if inverse {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }

        maskLayer.path = path.cgPath

        self.layer.mask = maskLayer
    }

    func mask(withPath path: UIBezierPath, inverse: Bool = false) {
        let path = path
        let maskLayer = CAShapeLayer()

        if inverse {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }

        maskLayer.path = path.cgPath

        self.layer.mask = maskLayer
    }
}

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}

extension String {
    func validateUrl () -> Bool {
        if self.contains("qr_code") {
            return true
        } else {
            return false
        }
    }
}
