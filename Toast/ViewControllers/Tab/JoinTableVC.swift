//
//  JoinTableVC.swift
//  Toast
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class JoinTableVC: UIViewController {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    
    var currentTextFiled:MyTextField! = nil
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        setUpTextField()
        
        /*
        first.text = "M"
        second.text = "A"
        third.text = "O"
        fourth.text = "N"*/
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnJoinTable(_ sender: Any) {
        first.text = TRIM(string: first.text ?? "")
        second.text = TRIM(string: second.text ?? "")
        third.text = TRIM(string: third.text ?? "")
        fourth.text = TRIM(string: fourth.text ?? "")
        let strCode = "\(first.text ?? "")\(second.text ?? "")\(third.text ?? "")\(fourth.text ?? "")"
        if strCode.count == 4 {
            apiJoinWithCode(strCode)
        }
    }
}

//MARK: API CALLING
extension JoinTableVC {
    func apiJoinWithCode(_ strCode: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIJoinWithCode
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "table_joining_code":strCode]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIJoinWithCode,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "failure" {
                            showMessage(dictData.object(forKey: "reason") as? String ?? "Try Again!")
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    } else if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        SaveMyJoinCode(strCode)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        vc.isFromTakeWay = self.isFromTakeAway
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

//MARK:- MyTextFieldDelegate Delegate
extension JoinTableVC: UITextFieldDelegate, MyTextFieldDelegate {
    func setUpTextField() {
        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
    
    func textFieldDidDelete() {
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0)
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0)
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0)
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0)
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                self.view.endEditing(true)
            default:
                break
            }
        }
    }
    
    
    //MARK:- TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFiled = textField as? MyTextField
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension JoinTableVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
