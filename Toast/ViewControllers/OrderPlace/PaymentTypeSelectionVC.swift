//
//  PaymentTypeSelectionVC.swift
//  Toast
//
//  Created by iMac on 24/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegatePaymentType {
    func SelectedType(_ sType: String)
}

class CellPayment: UITableViewCell {
    @IBOutlet weak var imgRound: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}

class PaymentTypeSelectionVC: UIViewController {

    var delegatePaymentType: delegatePaymentType?
    var GrandTotal: CGFloat = 0.0
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false
    var sSelectionType: String = ""
    
    var arrPaymentOptions = NSMutableArray()
    
    @IBOutlet weak var tblSelection: UITableView!
    @IBOutlet var lblTotalAmount: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromTakeAway || isFromDelivery {
            arrPaymentOptions = ["online"]
        } else {
            arrPaymentOptions = ["online", "cash"]
        }
        if arrPaymentOptions.count > 0 {
            sSelectionType = arrPaymentOptions.object(at: 0) as! String
        }
        AddRightSwipeGesture()
        lblTotalAmount.text = String(format:"₹%.2f",GrandTotal)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnProceed(_ sender: Any) {
        print("Payment Method: ", sSelectionType)
        if sSelectionType.count <= 0 {
            showMessage("Please select payment method.")
        }
        if isFromTakeAway {
            apiInitiatePayment()
        } else if isFromDelivery {
            apiDeliveryInitiatePayment()
        } else {
            self.delegatePaymentType?.SelectedType(sSelectionType)
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        }
    }
    
    private func apiInitiatePayment() {
        self.view.endEditing(true)
        let service = SERVER_URL + APITAInitiataPay
        let dictParam: NSDictionary = ["payment_type":sSelectionType]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APITAInitiataPay,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            self.delegatePaymentType?.SelectedType(self.sSelectionType)
                            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                self.dismiss(animated: false, completion: nil)
                            }, completion: nil)
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    private func apiDeliveryInitiatePayment() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIMenuDeliveryInitiataPay
        let dictParam: NSDictionary = ["payment_type":sSelectionType]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuDeliveryInitiataPay,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            self.delegatePaymentType?.SelectedType(self.sSelectionType)
                            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                self.dismiss(animated: false, completion: nil)
                            }, completion: nil)
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension PaymentTypeSelectionVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}

//MARK:- UITABLEVIEW DELEGATE METHOD
extension PaymentTypeSelectionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellPayment = tableView.dequeueReusableCell(withIdentifier: "CellPayment", for: indexPath) as! CellPayment
        cell.imgRound.image = UIImage(named: "ic_round_unselect")
        cell.lblTitle.textColor = Color_Hex(hex: "#404040")
        let sType = arrPaymentOptions[indexPath.row] as! String
        switch sType {
        case "cash":
            cell.lblTitle.text = "Pay by Cash"
        case "online":
            cell.lblTitle.text = "Pay Online\n( Wallets, Card, Net Banking )"
        default:
            break
        }
        if sType == sSelectionType {
            cell.imgRound.image = UIImage(named: "ic_round_selected")
            cell.lblTitle.textColor = Color_Hex(hex: "#3CBCB4")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sSelectionType = arrPaymentOptions[indexPath.row] as! String
        tblSelection.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
