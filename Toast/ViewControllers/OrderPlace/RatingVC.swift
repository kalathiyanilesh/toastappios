//
//  RatingVC.swift
//  Toast
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Cosmos
import Photos
import OpalImagePicker

protocol CellReviewDelegate: class {
    func didEndEditing(_ index: Int, _ text: String)
}

class CellRating: UITableViewCell, UITextFieldDelegate {
    weak var delegate: CellReviewDelegate?
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var txtItemReview: UITextField!
    @IBOutlet var btnRateArray:[UIButton]?
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var viewImages: UIView!
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var img3: UIImageView!
    @IBOutlet var heightImagesView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtItemReview.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let index = textField.tag - 555
        self.delegate?.didEndEditing(index, TRIM(string: textField.text ?? ""))
    }
}

class RatingVC: UIViewController {

    @IBOutlet var txtRestaurantReviewText: UITextField!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblRReviewType: UILabel!
    @IBOutlet var viewRRate: CosmosView!
    @IBOutlet var tblRate: UITableView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var activity: UIActivityIndicatorView!
    var isFromPush: Bool = false
    var arrReview = NSMutableArray()
    var arrImages = NSMutableArray()
    var arrPastOrders = NSMutableArray()
    var dictReview: NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblRReviewType.text = ""
        AddGradient(viewGradient)
        viewRRate.settings.fillMode = .half
        viewRRate.didTouchCosmos = didTouchCosmos
        
        if isFromPush {
            GetPastOrder()
        } else {
            SetUpRatingData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnRatingPage = true
        btnSubmit.setTitle("Submit", for: .normal)
        activity.stopAnimating()
    }
    
    func didTouchCosmos(_ rating: Double) {
        lblRReviewType.text = GetRateText(rating)
    }
    
    func SetUpRatingData() {
        print(arrPastOrders)
        lblRestaurantName.text = APP_DELEGATE.objRestaurantDetails?.restrauntName
        for i in 0..<arrPastOrders.count {
            let dictItems: NSMutableDictionary = (arrPastOrders.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            dictItems.setObject("\(i)", forKey: "itemIndex" as NSCopying)
            arrPastOrders.replaceObject(at: i, with: dictItems.copy() as! NSDictionary)
        }
        DispatchQueue.main.async {
            self.tblRate.reloadData()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        if isFromPush { return }
        GoToHomePage()
    }
    
    @IBAction func btnInfo(_ sender: Any) {
        
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if viewRRate.rating > 0 {
            apiOrderReview()
        } else {
            showMessage("Please give ratings to this restaurant!")
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension RatingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPastOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellRating = tableView.dequeueReusableCell(withIdentifier: "CellRating", for: indexPath) as! CellRating
        let objData: TPastItem = TPastItem.init(object: arrPastOrders.object(at: indexPath.row) as! NSDictionary)
        cell.delegate = self
        cell.lblItemName.text = objData.itemName
        if objData.itemType == "nonveg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objData.itemType == "veg" && objData.containsEgg == "true" {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        
        for btnRate in cell.btnRateArray! {
            btnRate.addTarget(self, action: #selector(btnRateTap(_:)), for: .touchUpInside)
            btnRate.alpha = 0.5
        }
        
        cell.txtItemReview.tag = indexPath.row+555
        let predicate = NSPredicate(format: "itemIndex == %@", objData.itemIndex!)
        let filteredArray = (arrReview as NSArray).filtered(using: predicate) as NSArray
        if filteredArray.count > 0 {
            let dictData: NSDictionary = filteredArray[0] as! NSDictionary
            cell.txtItemReview.text = dictData["item_review"] as? String
            let itemRating = Int(dictData["item_rating"] as! String)!
            if itemRating > 0 {
                cell.btnRateArray?[itemRating-1].alpha = 1
            }
        }
        
        //SETUP IMAGES
        cell.viewImages.alpha = 0
        cell.heightImagesView.constant = 0
        cell.img1.image = nil
        cell.img2.image = nil
        cell.img3.image = nil
        var arrItemReviewImg = NSMutableArray()
        let predicate1 = NSPredicate(format: "imageindex == %@", "\(indexPath.row)")
        let filteredArray1 = (self.arrImages as NSArray).filtered(using: predicate1) as NSArray
        if filteredArray1.count > 0 {
            let dictData: NSMutableDictionary = (filteredArray1[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            arrItemReviewImg = (dictData.object(forKey: "images") as! NSArray).mutableCopy() as! NSMutableArray
        }
        if arrItemReviewImg.count > 0 {
            cell.heightImagesView.constant = 65
            cell.viewImages.alpha = 1
        }
        for i in 0..<arrItemReviewImg.count {
            switch i {
            case 0:
                cell.img1.image = arrItemReviewImg[i] as? UIImage
            case 1:
                cell.img2.image = arrItemReviewImg[i] as? UIImage
            default:
                cell.img3.image = arrItemReviewImg[i] as? UIImage
            }
        }
        if arrItemReviewImg.count < 3 {
            cell.btnAddImage.addTarget(self, action: #selector(btnAddImage(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var arrItemReviewImg = NSMutableArray()
        let predicate1 = NSPredicate(format: "imageindex == %@", "\(indexPath.row)")
        let filteredArray1 = (self.arrImages as NSArray).filtered(using: predicate1) as NSArray
        if filteredArray1.count > 0 {
            let dictData: NSMutableDictionary = (filteredArray1[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            arrItemReviewImg = (dictData.object(forKey: "images") as! NSArray).mutableCopy() as! NSMutableArray
        }
        if arrItemReviewImg.count > 0 {
            return 163
        }
        return 98
    }
    
    
    @objc func btnAddImage(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblRate)
        let indexPath = tblRate.indexPathForRow(at: buttonPosition)
        let objData: TPastItem = TPastItem.init(object: arrPastOrders.object(at: indexPath!.row) as! NSDictionary)
        var arrItemReviewImg = NSMutableArray()
        let predicate1 = NSPredicate(format: "imageindex == %@", "\(indexPath!.row)")
        let filteredArray1 = (self.arrImages as NSArray).filtered(using: predicate1) as NSArray
        if filteredArray1.count > 0 {
            let dictData: NSMutableDictionary = (filteredArray1[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            arrItemReviewImg = (dictData.object(forKey: "images") as! NSArray).mutableCopy() as! NSMutableArray
        }
        if arrItemReviewImg.count == 3 {
            return
        }
        let imagePicker = OpalImagePickerController()
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.selectionImageTintColor = UIColor.black
        //imagePicker.selectionImage = UIImage(named: "x_image")
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.maximumSelectionsAllowed = 3-arrItemReviewImg.count
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = "You cannot select that many images!"
        imagePicker.configuration = configuration
        presentOpalImagePickerController(imagePicker, animated: true,
        select: { (assets) in
            for assest in assets {
                let img = getAssetThumbnail(asset: assest, CGSize(width: 100, height: 100))
                var arrSelectedImages = NSMutableArray()
                var dictData = NSMutableDictionary()
                let predicate = NSPredicate(format: "imageindex == %@", "\(indexPath!.row)")
                let filteredArray = (self.arrImages as NSArray).filtered(using: predicate) as NSArray
                if filteredArray.count > 0 {
                    dictData = (filteredArray[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    arrSelectedImages = (dictData.object(forKey: "images") as! NSArray).mutableCopy() as! NSMutableArray
                    arrSelectedImages.add(img)
                    self.arrImages.remove(dictData)
                    dictData.setObject(arrSelectedImages, forKey: "images" as NSCopying)
                    self.arrImages.add(dictData.copy())
                } else {
                    arrSelectedImages.add(img)
                    dictData.setObject(arrSelectedImages, forKey: "images" as NSCopying)
                    dictData.setObject("\(indexPath!.row)", forKey: "imageindex" as NSCopying)
                    dictData.setObject(objData.itemOrderId ?? "", forKey: "item_order_id" as NSCopying)
                    self.arrImages.add(dictData.copy())
                }
            }
            print(self.arrImages)
            self.tblRate.reloadData()
            self.dismiss(animated: true, completion: nil)
        }, cancel: {
            //Cancel
        })
    }
    
    @objc func btnRateTap(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblRate)
        let indexPath = tblRate.indexPathForRow(at: buttonPosition)
        let objData: TPastItem = TPastItem.init(object: arrPastOrders.object(at: indexPath!.row) as! NSDictionary)
        let predicate = NSPredicate(format: "itemIndex == %@", objData.itemIndex!)
        let filteredArray = (arrReview as NSArray).filtered(using: predicate) as NSArray
        if filteredArray.count > 0 {
            let dictData: NSMutableDictionary = (filteredArray[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            arrReview.remove(dictData)
            dictData.setObject("\(sender.tag)", forKey: "item_rating" as NSCopying)
            arrReview.add(dictData)
        } else {
            let dictReview: NSDictionary = ["itemIndex":objData.itemIndex!, "item_review":"", "item_rating":"\(sender.tag)"]
            arrReview.add(dictReview)
        }
        tblRate.reloadData()
    }
}

//MARK:- API CALLING
extension RatingVC {
    func apiOrderReview() {
        print(viewRRate.rating)
        self.view.isUserInteractionEnabled = false
        self.view.endEditing(true)
        btnSubmit.setTitle("", for: .normal)
        activity.startAnimating()
        dictReview.setObject(APP_DELEGATE.objRestaurantDetails?.restrauntId ?? "", forKey: "restraunt_id" as NSCopying)
        dictReview.setObject(viewRRate.rating, forKey: "overall_ratings" as NSCopying)
        dictReview.setObject(TRIM(string: txtRestaurantReviewText.text ?? ""), forKey: "overall_review" as NSCopying)
        
        let arrItemsReview = NSMutableArray()
        for i in 0..<arrPastOrders.count {
            let dictR = NSMutableDictionary()
            let objData: TPastItem = TPastItem.init(object: arrPastOrders.object(at: i) as! NSDictionary)
            dictR.setObject(objData.itemId ?? "", forKey: "item_id" as NSCopying)
            dictR.setObject("", forKey: "item_rating" as NSCopying)
            dictR.setObject("", forKey: "item_review" as NSCopying)
            dictR.setObject(objData.itemName ?? "", forKey: "item_name" as NSCopying)
            dictR.setObject(objData.itemType ?? "veg", forKey: "item_type" as NSCopying)
            dictR.setObject(objData.containsEgg ?? "false", forKey: "contains_egg" as NSCopying)
            dictR.setObject(objData.itemOrderId ?? "0", forKey: "item_order_id" as NSCopying)
            var itemRating = 0
            let predicate = NSPredicate(format: "itemIndex == %@", objData.itemIndex!)
            let filteredArray = (arrReview as NSArray).filtered(using: predicate) as NSArray
            if filteredArray.count > 0 {
                let dictData: NSDictionary = filteredArray[0] as! NSDictionary
                let sReview = dictData["item_review"] as? String ?? ""
                dictR.setObject(sReview, forKey: "item_review" as NSCopying)
                itemRating = Int(dictData["item_rating"] as! String)!
                if itemRating > 0 {
                    dictR.setObject(itemRating, forKey: "item_rating" as NSCopying)
                }
            }
        
            if itemRating > 0 ||  dictR.object(forKey: "item_review") as! String != ""{
                arrItemsReview.add(dictR.copy() as! NSDictionary)
            }
        }
        print(arrItemsReview)
        dictReview.setObject(arrItemsReview, forKey: "items_reviews" as NSCopying)

        let sReview = convertDictionaryToJSONString(dic: dictReview.copy() as! NSDictionary)

        let service = SERVER_URL + APIOrderReview + "\(APP_DELEGATE.sOrderID)"
        let dictParam: NSDictionary = ["review":sReview ?? "", "review_item_images":self.arrImages]
        print("Order Review: ",dictParam)
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: service,
            showLoader: false,
            getReturn: true,
            parameters: dictParam) { (error, responseDict) in
                print(responseDict ?? "")
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                if error == nil {
                    if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                        if let strStatus = dictData.object(forKey: "status") as? String {
                            if strStatus.lowercased() == "success" {
                                let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "RatingFeedbackVC")
                                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }, completion: nil)
                            }
                        }
                    }
                } else {
                    showMessage(error?.localizedDescription ?? "Try again!" )
                }
        }
    }
    func GetPastOrder() {
        apiGetPastOrder(true) { (dictOrder) in
            if dictOrder != nil {
                let dictData = dictOrder?.object(forKey: "order_details") as! NSDictionary
                let arrOrderDetailsMap = dictOrder?.object(forKey: "order_details_id_name_map") as! NSArray
                for i in 0..<arrOrderDetailsMap.count {
                    let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                    let Name = dictOrd.object(forKey: "id_name") as! String
                    let sK = dictOrd.object(forKey: "id") as! String
                    let arrTmp = dictData.object(forKey: sK) as! NSArray
                    for k in 0..<arrTmp.count {
                        let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if sK == CUSTOMER_ID {
                            dictTmp.setObject("Ordered by You", forKey: "orderby" as NSCopying)
                        } else {
                            dictTmp.setObject("Ordered by \(Name)", forKey: "orderby" as NSCopying)
                        }
                        self.arrPastOrders.add(dictTmp.copy() as! NSDictionary)
                    }
                }
                self.SetUpRatingData()
            }
        }
    }
}

extension RatingVC: CellReviewDelegate {
    func didEndEditing(_ index: Int, _ text: String) {
        let objData: TPastItem = TPastItem.init(object: arrPastOrders.object(at: index) as! NSDictionary)
        
        let predicate = NSPredicate(format: "itemIndex == %@", objData.itemIndex!)
        let filteredArray = (arrReview as NSArray).filtered(using: predicate) as NSArray
        if filteredArray.count > 0 {
            let dictData: NSMutableDictionary = (filteredArray[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            arrReview.remove(dictData)
            dictData.setObject(text, forKey: "item_review" as NSCopying)
            arrReview.add(dictData.copy() as! NSDictionary)
        } else {
            let dictReview: NSDictionary = ["itemIndex":objData.itemIndex!, "item_review":text, "item_rating":"0"]
            arrReview.add(dictReview)
        }
        UIView.performWithoutAnimation {
            tblRate.reloadData()
        }
    }
}
