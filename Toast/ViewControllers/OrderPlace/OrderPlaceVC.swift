
//
//  OrderPlaceVC.swift
//  Toast
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class OrderPlaceVC: UIViewController {
    
    @IBOutlet weak var buttonComplaint: UIButton!
    @IBOutlet var btnOrderMoreFood: UIButton!
    @IBOutlet var btnMakePayment: UIButton!
    @IBOutlet var lblTotalAmountDesc: UILabel!
    var arrPastOrders = NSMutableArray()
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false

    var sTotalItems: String = ""
    var sTotalAmount: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AddGradient(btnOrderMoreFood)
        AddGradient(btnMakePayment, Color_Hex(hex: "656565"), Color_Hex(hex: "007991"))
        btnOrderMoreFood.clipsToBounds = true
        btnMakePayment.clipsToBounds = true
        
        if UserDefaults.standard.object(forKey: "totalitems") != nil {
            sTotalItems = UserDefaults.standard.object(forKey: "totalitems") as? String ?? ""
            UserDefaults.standard.removeObject(forKey: "totalitems")
        }
        if UserDefaults.standard.object(forKey: "totalamount") != nil {
            sTotalAmount = UserDefaults.standard.object(forKey: "totalamount") as? String ?? ""
            UserDefaults.standard.removeObject(forKey: "totalamount")
        }
        
        lblTotalAmountDesc.text = "Your total bill amount is now ₹\(sTotalItems) for \(sTotalAmount) items"
        
        let s1 = NSMutableAttributedString(string: "Your total bill amount is now", attributes: [.font: UIFont(name: "Manrope-Medium", size: 12)!, .foregroundColor: Color_Hex(hex: "999999")])
        let s2 = NSAttributedString(string: " ₹\(sTotalAmount) ", attributes: [.font: UIFont(name: "Manrope-Medium", size: 12)!, .foregroundColor: Color_Hex(hex: "3CBCB4")])
        s1.append(s2)
        let s3 = NSMutableAttributedString(string: "\nfor \(sTotalItems) items", attributes: [.font: UIFont(name: "Manrope-Medium", size: 12)!, .foregroundColor: Color_Hex(hex: "999999")])
        s1.append(s3)
        lblTotalAmountDesc.attributedText = s1
        
        isFromTakeAway = APP_DELEGATE.isFromTakeAway
        isFromDelivery = APP_DELEGATE.isFromDelivery
        if isFromTakeAway || isFromDelivery {
            APP_DELEGATE.isFromTakeAway = false
            APP_DELEGATE.isFromDelivery = false
            buttonComplaint.isHidden = true
        }
        
        AddRightSwipeGesture()
        if !isFromTakeAway && !isFromDelivery {
            apiGetPastOrder()
        }
        
    }
    
    @IBAction func btnHaveAnyComplaint(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "ComplaintsVC") as! ComplaintsVC
        vc.arrItems = arrPastOrders
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnBackToMenu(_ sender: Any) {
        if isFromTakeAway || isFromDelivery {
            let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                APP_DELEGATE.window?.rootViewController = vc
            }, completion: nil)
        } else {
            let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
            vc.isFromTakeWay = self.isFromTakeAway
            vc.isFromDelivery = self.isFromDelivery
            if (UserDefaults.standard.object(forKey: MENU_DICT) != nil) {
                vc.dictMenu = UserDefaults.standard.object(forKey: MENU_DICT) as! NSDictionary
            }
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                APP_DELEGATE.window?.rootViewController = vc
            }, completion: nil)
        }

        
//        
//        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
//            self.dismiss(animated: false, completion: nil)
//        }, completion: nil)
    }
    
    @IBAction func btnCheckOut(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "BillingDetailsVC") as! BillingDetailsVC
        vc.isFromTakeAway = self.isFromTakeAway
        vc.isFromDelivery = self.isFromDelivery
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnViewAll(_ sender: Any) {
        self.btnCheckOut(UIButton())
    }
}

//MARK: API CALLING
extension OrderPlaceVC {
    
    func apiGetPastOrder() {
        self.view.endEditing(true)
        self.arrPastOrders = NSMutableArray()
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictOrder = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let dictOrderDetails = dictOrder.object(forKey: "order_details") as! NSDictionary
                    let arrOrderDetailsMap = dictOrder.object(forKey: "order_details_id_name_map") as! NSArray
                    for i in 0..<arrOrderDetailsMap.count {
                        let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                        let sK = dictOrd.object(forKey: "id") as! String
                        let arrTmp = dictOrderDetails.object(forKey: sK) as! NSArray
                        for k in 0..<arrTmp.count {
                            let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            if sK == CUSTOMER_ID {
                                dictTmp.setObject("Ordered by You", forKey: "orderby" as NSCopying)
                            } else {
                                dictTmp.setObject("Ordered by \(dictOrd.object(forKey: "id_name") as! String)", forKey: "orderby" as NSCopying)
                            }
                            self.arrPastOrders.add(dictTmp.copy() as! NSDictionary)
                        }
                    }
                }
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension OrderPlaceVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBackToMenu(UIButton())
    }
}
