//
//  RatingFeedbackVC.swift
//  Toast
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RatingFeedbackVC: UIViewController {

    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lbldesc: UILabel!
    @IBOutlet var lblback: UILabel!
    var isfrompayment: Bool  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        if isfrompayment {
            lblback.text = "Go to Back"
            lbltitle.text = "Thank you for\nvisiting!"
            lbldesc.text = "Your server will assist you shortly to collect cash/card from you. Don't forget to rate your experience and items too. Hope you had a great experience! :)"
        }
    }
    
    
    @IBAction func btnGoToHome(_ sender: Any) {
        if isfrompayment {
            self.navigationController?.popViewController(animated: true)
        } else {
            GoToHomePage()
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension RatingFeedbackVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnGoToHome(UIButton())
    }
}
