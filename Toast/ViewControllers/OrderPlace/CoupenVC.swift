//
//  CoupenVC.swift
//  Toast
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateApplyCoupon {
    func ApplyCoupon(_ objSelectedCoupon: TCoupon)
}

class CoupenVC: UIViewController {

    var delegateApplyCoupon: delegateApplyCoupon?
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var tblCoupon: UITableView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var heightApplyCoupon: NSLayoutConstraint!
    
    var arrCoupons = NSMutableArray()
    
    var objSelectedCoupon: TCoupon?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        AddGradient(viewGradient)
        apiGetCoupons()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyCoupen(_ sender: Any) {
        if objSelectedCoupon != nil {
            apiApplyCoupons()
        }
    }

}

//MARK:- UITableViewDelegate METHOD
extension CoupenVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if objSelectedCoupon == nil {
            viewGradient.alpha = 0
            heightApplyCoupon.constant = 0
        } else {
            viewGradient.alpha = 1
            heightApplyCoupon.constant = 50
        }
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
        return setNoDataLabel(tableView: tblCoupon, array: arrCoupons, text: "No coupons available")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCoupons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellPromocode = tableView.dequeueReusableCell(withIdentifier: "CellPromocode", for: indexPath) as! CellPromocode
        let objCoupon: TCoupon = arrCoupons[indexPath.row] as! TCoupon
        cell.lblPromoCode.text = objCoupon.couponCode
        cell.lblTitle.text = objCoupon.couponTitle
        cell.lblDesc.text = objCoupon.couponDescription
        cell.imgSelected.image = UIImage(named:"ic_round_unselect")
        if objSelectedCoupon?.couponId == objCoupon.couponId {
            cell.imgSelected.image = UIImage(named:"ic_round_selected")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        objSelectedCoupon = arrCoupons[indexPath.row] as? TCoupon
        tblCoupon.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK:- API CALLING
extension CoupenVC {
    func apiGetCoupons() {
        self.view.endEditing(true)
        activity.startAnimating()
        let service = SERVER_URL + APIGetCoupons
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetCoupons,
            parameters: dictParam,
            method: .post,
            showLoader: false) { (error, responseDict) in
                self.activity.stopAnimating()
                print(responseDict ?? "")
                if error == nil {
                    if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                        self.arrCoupons = NSMutableArray()
                        for i in 0..<arrData.count {
                            let dictData = arrData[i] as! NSDictionary
                            let obj: TCoupon = TCoupon.init(object: dictData)
                            self.arrCoupons.add(obj)
                        }
                        self.tblCoupon.reloadData()
                    }
                } else {
                    showMessage(error?.localizedDescription ?? "Try again!" )
                }
        }
    }
    
    func apiApplyCoupons() {
        self.view.endEditing(true)
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/" + APIApplyCoupon
        let dictParam: NSDictionary = ["coupon_id":objSelectedCoupon?.couponId ?? "0", "qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIApplyCoupon,
            parameters: dictParam,
            method: .post,
            showLoader: false) { (error, responseDict) in
                self.activity.stopAnimating()
                print(responseDict ?? "")
                if error == nil {
                    if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                        if let strStatus = dictData.object(forKey: "status") as? String {
                            if strStatus.lowercased() == "success" {
                                showMessage("Successfully apply coupon!")
                                self.delegateApplyCoupon?.ApplyCoupon(self.objSelectedCoupon!)
                                self.btnBack(self)
                            } else {
                                if let strMsg = dictData.object(forKey: "message") as? String {
                                    showMessage(strMsg)
                                }
                            }
                        }
                    }
                } else {
                    showMessage(error?.localizedDescription ?? "Try again!" )
                }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension CoupenVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
