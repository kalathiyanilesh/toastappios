//
//  OrderSuccessVC.swift
//  Toast
//
//  Created by Nikul on 17/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class OrderSuccessVC: UIViewController {

    @IBOutlet weak var labelOrderID: UILabel!
    @IBOutlet weak var labelPaymentType: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPickUpAddress: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var tableViewOrderSuccess: UITableView!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblOrderStaus: UILabel!
    var isFromTakeAway: Bool = false
    var isFromActiveOrder: Bool = false
    var total = CGFloat()
    var dictPastOrders = NSMutableDictionary()
    var arrPastOrders = NSMutableArray()
    var sContactNo = ""
    
    var objOrder: TAllOrder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromActiveOrder {
            APP_DELEGATE.sOrderID = objOrder?.orderId ?? ""
        }
        setupUI()
        lblAddressTitle.text = "Delivery Address:"
        lblDescription.text = "Your order has been received by the restaurant and will be delivered to soon. Enjoy you meal!"
        if isFromTakeAway {
            lblAddressTitle.text = "Pickup Address:"
            lblDescription.text = "Your order has been received by the restaurant and will be ready soon. Enjoy you meal!"
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshOrderPage), name: NSNotification.Name(rawValue: ORDER_SUCCESS_PAGE_REFRESH), object: nil)
        labelPickUpAddress.text = ""
        if isFromActiveOrder {
            apiGetPastOrder()
        } else {
            apiGetRestaurantDetails(APP_DELEGATE.objRestaurantDetails?.restrauntId ?? "")
        }
        apiGetActiveOrders()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isInOrderSuccess = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isInOrderSuccess = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableViewOrderSuccess.reloadData()
    }
    
    @objc func RefreshOrderPage() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetActiveOrders()
        }
    }
    
    private func setupUI() {
        labelOrderID.text = APP_DELEGATE.sOrderID
        labelTotal.text = String(format:"₹%.2f",total)
        labelPaymentType.text = "Paid, Online"
        if isFromActiveOrder {
            labelPaymentType.text = "Paid \(objOrder?.paymentDetails?.paymentType?.capitalized ?? "Online")"
            guard let timeStamp = Int(objOrder?.createdOn ?? "0") else { return labelDate.text = Date().toString(format: "MMM dd,yyyy hh:mm a") }
            let dateVar = Date.init(timeIntervalSince1970: TimeInterval(timeStamp))
            labelDate.text = dateVar.toString(format: "MMM dd,yyyy hh:mm a")
        } else {
            labelDate.text = Date().toString(format: "MMM dd,yyyy hh:mm a")
        }
    }

    @IBAction func buttonBackTouchUpInside(_ sender: Any) {
        if isFromActiveOrder {
            self.navigationController?.popViewController(animated: true)
        } else {
            let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                APP_DELEGATE.window?.rootViewController = vc
            }, completion: nil)
        }
    }
    
    @IBAction func btnSupport(_ sender: Any) {
        if sContactNo.count > 0 {
            if let url = NSURL(string: "tel://\(sContactNo)"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func GetAllKeys() -> NSArray {
        let arrAllKeys = (dictPastOrders.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
        return arrAllKeys
    }
    
}

//MARK:- UITableViewDelegate
extension OrderSuccessVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let arrAllKeys = GetAllKeys()
        return arrAllKeys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let arrAllKeys = GetAllKeys()
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 31))
        headerView.backgroundColor = self.view.backgroundColor
        let labelType = UILabel()
        labelType.frame = CGRect.init(x: 14, y: 0, width: SCREENWIDTH()-28, height: 31)
        labelType.text = (arrAllKeys[section] as? String)?.uppercased()
        labelType.font = FontWithSize("Manrope-SemiBold", 10)
        labelType.textColor = Color_Hex(hex: "#404040")
        headerView.addSubview(labelType)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 31
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrAllKeys = GetAllKeys()
        let arrOrder = self.dictPastOrders.object(forKey: arrAllKeys[section]) as! NSArray
        return arrOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arrAllKeys = GetAllKeys()
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFoodBillingDetails", for: indexPath) as! CellFoodBillingDetails
        let arrOrder = self.dictPastOrders.object(forKey: arrAllKeys[indexPath.section]) as! NSArray
        if #available(iOS 11.0, *) {
            cell.setupContainerAndSeparatorFor(indexPath: indexPath, itemsCount: arrOrder.count)
        } else {
            if arrOrder.count == 1 && indexPath.row == 0 {
                cell.viewContainer.roundCorners([.topLeft, .topRight,.bottomLeft, .bottomRight], radius: 10)
            } else if indexPath.row == 0 {
                cell.viewContainer.roundCorners([.topLeft, .topRight], radius: 10)
            } else if indexPath.row == arrOrder.count-1 {
                cell.viewContainer.roundCorners([.bottomLeft, .bottomRight], radius: 10)
            }
        }
        let objData: TPastItem = TPastItem.init(object: arrOrder.object(at: indexPath.row) as! NSDictionary)
        cell.lblItemName.text = objData.itemName
        let total = Double(objData.itemTotalPrice ?? "0")!
        cell.lblPrice.text = String(format:"₹%.f",total)
        cell.lblQty.text = objData.qty
        if objData.itemType == "non-veg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objData.itemType == "veg" && (objData.containsEgg == "1" || objData.containsEgg == "true") {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
}

//MARK: API CALLING
extension OrderSuccessVC {
    func apiGetRestaurantDetails(_ resID: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + "restraunt/\(resID)/details/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIRestaurantDetails,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
               print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let dictRes = dictData.object(forKey: "restraunt_store_details") as? NSDictionary {
                        let arrAddress = NSMutableArray()
                        let add1 = dictRes.object(forKey: "add1") as? String ?? ""
                        let add2 = dictRes.object(forKey: "add2") as? String ?? ""
                        let area = dictRes.object(forKey: "area") as? String ?? ""
                        let city = dictRes.object(forKey: "city") as? String ?? ""
                        let pincode = dictRes.object(forKey: "pincode") as? String ?? ""
                        
                        if TRIM(string: add1).count > 0 {
                            arrAddress.add(add1)
                        }
                        if TRIM(string: add2).count > 0 {
                            arrAddress.add(add2)
                        }
                        if TRIM(string: area).count > 0 {
                            arrAddress.add(area)
                        }
                        if TRIM(string: city).count > 0 {
                            arrAddress.add(city)
                        }
                        if TRIM(string: pincode).count > 0 {
                            arrAddress.add(pincode)
                        }
                        self.labelPickUpAddress.text = ""
                        if arrAddress.count > 0 {
                            self.labelPickUpAddress.text = arrAddress.componentsJoined(by: ", ")
                        }
                        self.sContactNo = dictRes.object(forKey: "contact_no") as! String
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetActiveOrders() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIActiveOrder
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIActiveOrder,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TAllOrder = TAllOrder.init(object: dictData)
                        if obj.orderId == APP_DELEGATE.sOrderID {
                            self.lblOrderStaus.text = "      \(obj.orderStatus?.uppercased() ?? "")      "
                            break
                        }
                    }
                }
            }
        }
    }
    
    func apiGetPastOrder() {
        self.view.endEditing(true)
        self.dictPastOrders = NSMutableDictionary()
        arrPastOrders = NSMutableArray()
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: false,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictOrder = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let dictData = dictOrder.object(forKey: "order_details") as! NSDictionary
                    let arrOrderDetailsMap = dictOrder.object(forKey: "order_details_id_name_map") as! NSArray
                    for i in 0..<arrOrderDetailsMap.count {
                        let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                        let Name = dictOrd.object(forKey: "id_name") as! String
                        let sK = dictOrd.object(forKey: "id") as! String
                        let arrTmp = dictData.object(forKey: sK) as! NSArray
                        for k in 0..<arrTmp.count {
                            let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            if sK == CUSTOMER_ID {
                                dictTmp.setObject("Ordered by You", forKey: "orderby" as NSCopying)
                            } else {
                                dictTmp.setObject("Ordered by \(Name)", forKey: "orderby" as NSCopying)
                            }
                            self.arrPastOrders.add(dictTmp.copy() as! NSDictionary)
                        }
                        
                        if sK == CUSTOMER_ID {
                            self.dictPastOrders.setObject(arrTmp, forKey: "YOUR ORDER" as NSCopying)
                        } else {
                            self.dictPastOrders.setObject(arrTmp, forKey: Name as NSCopying)
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableViewOrderSuccess.reloadData()
                    }
                    
                    self.apiGetRestaurantDetails(dictOrder.object(forKey: "restraunt_id") as? String ?? "")
                }
            }
        }
    }
}
