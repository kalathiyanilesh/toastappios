//
//  ComplaintsVC.swift
//  Toast
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol CellComplaintsDelegate: class {
    func didEndEditing(_ index: Int, _ text: String)
}

class CellComplaints: UITableViewCell, UITextFieldDelegate {
    weak var delegate: CellComplaintsDelegate?
    @IBOutlet var viewDesc: UIView!
    @IBOutlet var viewComplaints: UIView!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var imgRightZero: UIImageView!
    @IBOutlet var imgRightOne: UIImageView!
    @IBOutlet var imgRightTwo: UIImageView!
    @IBOutlet var btnZero: UIButton!
    @IBOutlet var btnOne: UIButton!
    @IBOutlet var btnTwo: UIButton!
    @IBOutlet var txtComplaints: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        txtComplaints.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let index = textField.tag - 555
        self.delegate?.didEndEditing(index, TRIM(string: textField.text ?? ""))
    }
}

class ComplaintsVC: UIViewController {
    
    @IBOutlet var tblComplaints: UITableView!
    @IBOutlet var viewSubmit: UIView!
    @IBOutlet var heightSubmit: NSLayoutConstraint!
    
    var arrItems = NSMutableArray()
    var dictComplaints = NSMutableDictionary()
    var arrOpenItems = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        
        for i in 0..<arrItems.count {
            let dictItems: NSMutableDictionary = (arrItems.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            dictItems.setObject("\(i)", forKey: "itemIndex" as NSCopying)
            arrItems.replaceObject(at: i, with: dictItems.copy() as! NSDictionary)
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: true, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        let arrFinalComplaints = NSMutableArray()
        if dictComplaints.allKeys.count > 0 {
            for i in 0..<arrItems.count {
                let objData: TPastItem = TPastItem.init(object: arrItems.object(at: i) as! NSDictionary)
                var strComplaints = ""
                if (dictComplaints.object(forKey: objData.itemIndex!) != nil) {
                    let dictData: NSDictionary = dictComplaints.object(forKey: objData.itemIndex!) as! NSDictionary
                    strComplaints = dictData.object(forKey: "complaintstext") as! String
                    if strComplaints.count <= 0 {
                        strComplaints = dictData.object(forKey: "complaintsdefaulttext") as! String
                    }
                }
                if strComplaints.count > 0 {
                    var sContainsEgg = "false"
                    if objData.containsEgg == "true" {
                        sContainsEgg = "true"
                    }
                    let dictData: NSDictionary = ["item_name":objData.itemName ?? "", "item_id":objData.itemId!, "item_image_url": "", "item_type": objData.itemType ?? "", "contains_egg":sContainsEgg, "complaint": strComplaints, "item_order_id":objData.itemOrderId ?? ""]
                    arrFinalComplaints.add(dictData)
                }
            }
            apiAddComplaints(arrFinalComplaints)
        }
    }
    
    func CheckComplaintsAdd() {
        var isAdded:Bool = false
        if dictComplaints.allKeys.count > 0 {
            for i in 0..<arrItems.count {
                let objData: TPastItem = TPastItem.init(object: arrItems.object(at: i) as! NSDictionary)
                if (dictComplaints.object(forKey: objData.itemIndex!) != nil) {
                    print(dictComplaints)
                    //["selectedcomplaints":-1,"complaintstext":"","complaintsdefaulttext":""]
                    let dictData = dictComplaints.object(forKey: objData.itemIndex!) as! NSDictionary
                    let sComplainText = dictData.object(forKey: "complaintstext") as? String
                    let selectedComplaint = dictData.object(forKey: "selectedcomplaints") as! Int
                    let sComplainDefaultText = dictData.object(forKey: "complaintsdefaulttext") as! String
                    if selectedComplaint == -1 && TRIM(string: sComplainText ?? "") == "" && TRIM(string: sComplainDefaultText) == "" {
                        isAdded = false
                    } else {
                        isAdded = true
                        break
                    }
                }
            }
        }
        if isAdded {
            viewSubmit.alpha = 1
            heightSubmit.constant = 50
        } else {
            viewSubmit.alpha = 0
            heightSubmit.constant = 0
        }
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: API CALLING
extension ComplaintsVC {
    func apiAddComplaints(_ arrFinalComplaints: NSMutableArray) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIAddComplaints
        let dictParam: NSDictionary = ["complaints":arrFinalComplaints, "qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIAddComplaints,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            showMessage("Successfully added your complaints!")
                            self.btnBack(self)
                        }
                    }
                }
            }
        }
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension ComplaintsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        CheckComplaintsAdd()
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellComplaints = tableView.dequeueReusableCell(withIdentifier: "CellComplaints", for: indexPath) as! CellComplaints
        cell.delegate = self
        let objData: TPastItem = TPastItem.init(object: arrItems.object(at: indexPath.row) as! NSDictionary)
        //cell.imgItem.sd_setImage(with: URL(string: objData.itemImageUrl ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        cell.imgItem.sd_setImage(with: URL(string: ""), placeholderImage: FOOD_PLACE_HOLDER)
        cell.lblItemName.text = objData.itemName
        cell.lblPrice.text = "₹\(objData.itemPrice ?? "0")"
        if objData.itemType == "nonveg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objData.itemType == "veg" && objData.containsEgg == "true" {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        cell.lblDesc.text = objData.itemDescription
        cell.txtComplaints.text = ""
        cell.txtComplaints.tag = indexPath.row+555
        
        cell.imgRightZero.isHidden = true
        cell.imgRightOne.isHidden = true
        cell.imgRightTwo.isHidden = true
        
        let arrKeys: NSMutableArray = (dictComplaints.allKeys as NSArray).mutableCopy() as! NSMutableArray
        if arrKeys.contains(objData.itemIndex!) {
            let dictData: NSDictionary = dictComplaints.object(forKey: objData.itemIndex!) as! NSDictionary
            cell.txtComplaints.text = dictData.object(forKey: "complaintstext") as? String
            let selectedComplaint = dictData.object(forKey: "selectedcomplaints") as! Int
            switch selectedComplaint {
            case 0:
                cell.imgRightZero.isHidden = false
            case 1:
                cell.imgRightOne.isHidden = false
            case 2:
                cell.imgRightTwo.isHidden = false
            default:
                break
            }
        }
        
        cell.btnZero.addTarget(self, action: #selector(btnZero(_:)), for: .touchUpInside)
        cell.btnOne.addTarget(self, action: #selector(btnOne(_:)), for: .touchUpInside)
        cell.btnTwo.addTarget(self, action: #selector(btnTwo(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objData: TPastItem = TPastItem.init(object: arrItems.object(at: indexPath.row) as! NSDictionary)
        let arrKeys: NSMutableArray = (dictComplaints.allKeys as NSArray).mutableCopy() as! NSMutableArray
        if !arrKeys.contains(objData.itemIndex!) {
            let dictData: NSDictionary = ["selectedcomplaints":-1,"complaintstext":"","complaintsdefaulttext":""]
            dictComplaints.setObject(dictData, forKey: objData.itemIndex! as NSCopying)
        }
        if arrOpenItems.contains(objData.itemIndex!) {
            arrOpenItems.remove(objData.itemIndex!)
        } else {
            arrOpenItems.add(objData.itemIndex!)
        }
        tblComplaints.beginUpdates()
        tblComplaints.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let objData: TPastItem = TPastItem.init(object: arrItems.object(at: indexPath.row) as! NSDictionary)
        if arrOpenItems.contains(objData.itemIndex!) {
            return 269
        }
        return 93
    }
    
    @objc func btnZero(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblComplaints)
        let indexPath = tblComplaints.indexPathForRow(at: buttonPosition)
        UpdateComplaintIndex(indexPath!, 0, "Order taking too long")
    }
    
    @objc func btnOne(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblComplaints)
        let indexPath = tblComplaints.indexPathForRow(at: buttonPosition)
        UpdateComplaintIndex(indexPath!, 1, "Order not according to specification")
    }
    
    @objc func btnTwo(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblComplaints)
        let indexPath = tblComplaints.indexPathForRow(at: buttonPosition)
        UpdateComplaintIndex(indexPath!, 2, "Missing Items")
    }
    
    func UpdateComplaintIndex(_ indexPath: IndexPath, _ option: Int, _ text: String) {
        self.view.endEditing(true)
        let objData: TPastItem = TPastItem.init(object: arrItems.object(at: indexPath.row) as! NSDictionary)
        let dictData: NSMutableDictionary = (dictComplaints.object(forKey: objData.itemIndex!) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        let strWritableText = dictData.object(forKey: "complaintstext") as! String
        if strWritableText.count > 0 {
            let alertController = UIAlertController(title: "", message: "Text would be discarded", preferredStyle: .alert)
                    
            let action1 = UIAlertAction(title: "Proceed", style: .default) { (action:UIAlertAction) in
                dictData.setObject(option, forKey: "selectedcomplaints" as NSCopying)
                dictData.setObject(text, forKey: "complaintsdefaulttext" as NSCopying)
                dictData.setObject("", forKey: "complaintstext" as NSCopying)
                self.dictComplaints.setObject(dictData.copy(), forKey: objData.itemIndex! as NSCopying)
                UIView.performWithoutAnimation {
                    self.tblComplaints.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
                }
            }

            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in

            }
            alertController.addAction(action1)
            alertController.addAction(action2)
            self.present(alertController, animated: true, completion: nil)
        } else {
            dictData.setObject(option, forKey: "selectedcomplaints" as NSCopying)
            dictData.setObject(text, forKey: "complaintsdefaulttext" as NSCopying)
            self.dictComplaints.setObject(dictData.copy(), forKey: objData.itemIndex! as NSCopying)
            UIView.performWithoutAnimation {
                self.tblComplaints.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
            }
        }
    }
}

extension ComplaintsVC: CellComplaintsDelegate {
    func didEndEditing(_ index: Int, _ text: String) {
        let objData: TPastItem = TPastItem.init(object: arrItems.object(at: index) as! NSDictionary)
        let dictData: NSMutableDictionary = (dictComplaints.object(forKey: objData.itemIndex!) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        dictData.setObject(text, forKey: "complaintstext" as NSCopying)
        if text.count > 0 {
            dictData.setObject(-1, forKey: "selectedcomplaints" as NSCopying)
        }
        dictComplaints.setObject(dictData.copy(), forKey: objData.itemIndex! as NSCopying)
        
        UIView.performWithoutAnimation {
            tblComplaints.reloadData()
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension ComplaintsVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
