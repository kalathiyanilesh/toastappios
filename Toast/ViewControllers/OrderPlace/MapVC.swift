//
//  MapVC.swift
//  Yumma
//
//  Created by PC on 25/03/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import GoogleMaps

protocol delegateMapAdd {
    func getAddressLatLong(_ lattitude: Double, _ longitude: Double, _ strAddress: String)
}

protocol delegateMapAddCancel {
    func cancelAddSelection()
}

class MapVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    
    var isSearchAdd: Bool = false
    var lat_add: Double = 0
    var long_add: Double = 0
    var delegateMapAdd: delegateMapAdd?
    var delegateMapAddCancel: delegateMapAddCancel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        runAfterTime(time: 0.2) {
            let camera = GMSCameraPosition.camera(withLatitude: lat_currnt, longitude: long_currnt, zoom: 17.0)
            self.mapView?.animate(to: camera)
        }
    }
}

// MARK: - UIBUTTON ACTIONS
extension MapVC {
    
    @IBAction func btnBack(_ sender: Any) {
        self.delegateMapAddCancel?.cancelAddSelection()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        //openAddressPicker()
    }

    @IBAction func btnSelectLoc(_ sender: Any) {
        if lblAddress.text?.count ?? 0 <= 0 { return }
        self.delegateMapAdd?.getAddressLatLong(lat_add, long_add, lblAddress.text!)
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- GMSMapViewDelegate METHOD
extension MapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if isSearchAdd { return }
        lat_add = mapView.camera.target.latitude
        long_add = mapView.camera.target.longitude
        getAddressFromLatLon()
    }
    
    func getAddressFromLatLon() {
        let geocoder = GMSGeocoder()
        let position: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:lat_add, longitude: long_add)
        geocoder.reverseGeocodeCoordinate(position) { response, error in
            if let location = response?.firstResult() {
                let lines = location.lines! as [String]
                print(lines)
                self.lblAddress.text = lines.joined(separator: "\n")
            } else {
                self.lblAddress.text = ""
                self.lat_add = 0
                self.long_add = 0
                print("reverse geodcode fail: \(error?.localizedDescription ?? "Not Found")")
            }
        }
    }
    
    func getAdd() {
        let geocoder = CLGeocoder()
        let location: CLLocation = CLLocation(latitude:lat_add, longitude: long_add)
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            if error == nil && placemarks?.count ?? 0 > 0 {
                var addressString : String = ""
                let pm = placemarks![0]
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                self.lblAddress.text = addressString
            } else {
                self.lblAddress.text = ""
                self.lat_add = 0
                self.long_add = 0
                print("reverse geodcode fail: \(error?.localizedDescription ?? "Not Found")")
            }
        })
    }
}
