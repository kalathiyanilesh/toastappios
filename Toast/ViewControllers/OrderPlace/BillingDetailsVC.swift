//
//  BillingDetailsVC.swift
//  Toast
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Razorpay
import CoreLocation
import IQKeyboardManagerSwift

protocol CellTipDelegate: class {
    func didEndEditing(_ text: String)
}

class CellGrandTotal: UITableViewCell {
    @IBOutlet var lblItemTotal: UILabel!
    @IBOutlet var lblGSTAmount: UILabel!
    @IBOutlet weak var lblGSTTitle: UILabel!
    @IBOutlet var lblTaxes: UILabel!
    @IBOutlet var lblServiceChargePercent: UILabel!
    @IBOutlet var lblServiceCharge: UILabel!
    @IBOutlet var lblPackingCharge: UILabel!
    @IBOutlet var lblDeliveryCharge: UILabel!
    @IBOutlet var lblResDiscount: UILabel!
    @IBOutlet var lblCoupenDiscount: UILabel!
    @IBOutlet var lblZomatoDiscount: UILabel!
    @IBOutlet var lblTip: UILabel!
    @IBOutlet var lblGrandTotal: UILabel!
    @IBOutlet weak var heightAddressView: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtAddress: IQTextView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet var btnChangeAddress: UIButton!
    
    @IBOutlet weak var viewGST: UIView!
    @IBOutlet weak var heightGST: NSLayoutConstraint!
    
    @IBOutlet weak var viewTAX: UIView!
    @IBOutlet weak var heightTAX: NSLayoutConstraint!
    
    @IBOutlet weak var viewServiceChargePercent: UIView!
    @IBOutlet weak var heightServiceChargePercent: NSLayoutConstraint!
    
    @IBOutlet weak var viewServiceChargeAmount: UIView!
    @IBOutlet weak var heightServiceChargeAmount: NSLayoutConstraint!
    
    @IBOutlet weak var viewPackingCharge: UIView!
    @IBOutlet weak var heightPackingCharge: NSLayoutConstraint!
    
    @IBOutlet weak var viewDeliveryCharge: UIView!
    @IBOutlet weak var heightDeliveryCharge: NSLayoutConstraint!
    
    @IBOutlet weak var viewResDisc: UIView!
    @IBOutlet weak var heightResDisc: NSLayoutConstraint!
    
    @IBOutlet weak var viewCouponDisc: UIView!
    @IBOutlet weak var heightCouponDisc: NSLayoutConstraint!
    
    @IBOutlet weak var viewZomatoDisc: UIView!
    @IBOutlet weak var heightZomatoDisc: NSLayoutConstraint!
    
    @IBOutlet weak var viewTipDisc: UIView!
    @IBOutlet weak var heightTipDisc: NSLayoutConstraint!
}

class BillingDetailsVC: UIViewController {

    @IBOutlet var tblBillingDetails: UITableView!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var heightContPayment: NSLayoutConstraint!
    @IBOutlet var bottomView: NSLayoutConstraint!
    var txtDeliverToAddress: IQTextView?
    var sDeliverAddress = ""
    
    var sSelecetdAddress = ""
    var selectedLat: Double = 0
    var selectedLong: Double = 0
    var arrAllTaxDetails = NSMutableArray()
    
    /*
     Live Keys:
     key_id = "rzp_live_oulTpwmrogb8gw"
     key_secret = "sgH6MS6Z6jTLIhczJS6InLZj"
     
     Test Keys:
     key_id = "rzp_test_m4B1kzV64HjvXf"
     */
    
    var razorpay: RazorpayCheckout!
    var razorpayKey: String = "rzp_live_oulTpwmrogb8gw" //"rzp_test_m4B1kzV64HjvXf"
    var objUser:TUser!
    var OrderAmount: CGFloat = 0.0
    var TotalTip: CGFloat = 0.0
    var GrandTotal: CGFloat = 0.0
    var CouponDiscount: CGFloat = 0.0
    var ZomatoGoldDiscount: CGFloat = 0.0
    var RestrauntDiscountAmount: CGFloat = 0.0
    var GST: CGFloat = 0.0
    var Taxes: CGFloat = 0.0
    var ServiceChargePercent: CGFloat = 0.0
    var ServiceChargeAmount: CGFloat = 0.0
    var PackingCharge: CGFloat = 0.0
    var DeliveryCharge: CGFloat = 0.0
    var dictPastOrders = NSMutableDictionary()
    var arrPastOrders = NSMutableArray()
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false
    var isRemoveServiceCharges: Bool = false
    var isFromPlaceOrder: Bool = false
    var arrZomatoIds = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        /*
        if SCREENHEIGHT() > 736 {
            bottomView.constant = -19
            //heightContPayment.constant = 50
            self.view.layoutIfNeeded()
        }*/
        AddGradient(viewGradient)
        apiGetPastOrder(true)
        apiGetUserInfo()
        razorpay = RazorpayCheckout.initWithKey(razorpayKey, andDelegate: self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshBillingPage), name: NSNotification.Name(rawValue: BILLING_PAGE_REFRESH), object: nil)
        if isFromDelivery {
            getCurrentAddress { (Address) in
                self.sDeliverAddress = Address ?? ""
                self.sSelecetdAddress = Address ?? ""
                self.selectedLat = lat_currnt
                self.selectedLong = long_currnt
                self.tblBillingDetails.reloadData()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isOnBillingDetailsPage = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        APP_DELEGATE.isOnBillingDetailsPage = false
    }
    
    @objc func RefreshBillingPage() {
        if APP_DELEGATE.isPostNoti {
            APP_DELEGATE.isPostNoti = false
            apiGetPastOrder(false)
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        if isFromPlaceOrder {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnContinuePayment(_ sender: Any) {
        self.view.endEditing(true)
        if isFromDelivery {
            if validateTextLength(txtDeliverToAddress?.text ?? "", withMessage: "Please enter deliver address") {
                sDeliverAddress = txtDeliverToAddress?.text ?? ""
                apiCheckDeliverAddress()
            }
        } else {
            ContinuePayment()
        }
        
    }
    
    func ContinuePayment() {
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "PaymentTypeSelectionVC") as! PaymentTypeSelectionVC
        vc.delegatePaymentType = self
        vc.GrandTotal = GrandTotal
        vc.isFromTakeAway = self.isFromTakeAway
        vc.isFromDelivery = self.isFromDelivery
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func GetAllKeys() -> NSArray {
        let arrAllKeys = (dictPastOrders.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
        return arrAllKeys
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tblBillingDetails.reloadData()
    }
}

extension BillingDetailsVC: delegatePaymentType {
    func SelectedType(_ sType: String) {
        //["online", "cash"]
        if sType == "online" {
            apiCheckout("online")
            showPaymentForm()
        } else {
            apiCheckout(sType)
            /*
            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "RatingVC") as! RatingVC
            vc.arrPastOrders = arrPastOrders
            self.navigationController?.pushViewController(vc, animated: true)*/
        }
    }
}

//MARK:- UITableViewDelegate
extension BillingDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let arrAllKeys = GetAllKeys()
        return arrAllKeys.count+2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let arrAllKeys = GetAllKeys()
        switch section {
        case arrAllKeys.count:
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 15))
            headerView.backgroundColor = self.view.backgroundColor
            return headerView
        case arrAllKeys.count+1:
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 10))
            headerView.backgroundColor = self.view.backgroundColor
            return headerView
        default:
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 31))
            headerView.backgroundColor = self.view.backgroundColor
            let labelType = UILabel()
            labelType.frame = CGRect.init(x: 14, y: 0, width: SCREENWIDTH()-28, height: 31)
            labelType.text = (arrAllKeys[section] as? String)?.uppercased()
            labelType.font = FontWithSize("Montserrat-SemiBold", 10)
            labelType.textColor = Color_Hex(hex: "#404040")
            headerView.addSubview(labelType)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let arrAllKeys = GetAllKeys()
        switch section {
        case arrAllKeys.count:
            return 15
        case arrAllKeys.count+1:
            return 10
        default:
            return 31
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrAllKeys = GetAllKeys()
        switch section {
        case arrAllKeys.count:
            return isFromTakeAway || isFromDelivery ? 1 : 2
        case arrAllKeys.count+1:
            return 1
        default:
            let arrOrder = self.dictPastOrders.object(forKey: arrAllKeys[section]) as! NSArray
            return arrOrder.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.section)
        let arrAllKeys = GetAllKeys()
        switch indexPath.section {
        case arrAllKeys.count:
            if isFromTakeAway || isFromDelivery {
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellCoupen", for: indexPath) as! CellCoupen
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellTip", for: indexPath) as! CellTip
                    cell.delegate = self
                    cell.btnFifty.addTarget(self, action: #selector(btnFifty), for: .touchUpInside)
                    cell.btnHundred.addTarget(self, action: #selector(btnHundred), for: .touchUpInside)
                    cell.txtTip.text = ""
                    if TotalTip > 0 {
                        cell.txtTip.text = "\(TotalTip)"
                    }
                    cell.btnFifty.borderColor = UIColor.black
                    cell.btnHundred.borderColor = UIColor.black
                    if TotalTip == 50 {
                        cell.btnFifty.borderColor = Color_Hex(hex: "007436")
                    }
                    if TotalTip == 100 {
                        cell.btnHundred.borderColor = Color_Hex(hex: "007436")
                    }
                    return cell
                }
            } else {
                switch indexPath.row {
//                case 0:
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellZomatoGold", for: indexPath) as! CellZomatoGold
//                    return cell
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellCoupen", for: indexPath) as! CellCoupen
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellTip", for: indexPath) as! CellTip
                    cell.delegate = self
                    cell.btnFifty.addTarget(self, action: #selector(btnFifty), for: .touchUpInside)
                    cell.btnHundred.addTarget(self, action: #selector(btnHundred), for: .touchUpInside)
                    cell.txtTip.text = ""
                    if TotalTip > 0 {
                        cell.txtTip.text = "\(TotalTip)"
                    }
                    cell.btnFifty.borderColor = UIColor.black
                    cell.btnHundred.borderColor = UIColor.black
                    if TotalTip == 50 {
                        cell.btnFifty.borderColor = Color_Hex(hex: "007436")
                    }
                    if TotalTip == 100 {
                        cell.btnHundred.borderColor = Color_Hex(hex: "007436")
                    }
                    return cell
                }
            }
            
        case arrAllKeys.count+1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellGrandTotal", for: indexPath) as! CellGrandTotal
            if !isFromTakeAway && !isFromDelivery {
                cell.heightAddressView.constant = 0
                cell.viewAddress.alpha = 0
                cell.btnChangeAddress.isHidden = true
            } else {
                cell.btnChangeAddress.isHidden = true
                if isFromTakeAway {
                    cell.lblTitle.text = "Pick-up Address"
                    cell.txtAddress.isEditable = false
                    cell.txtAddress.isSelectable = false
                    cell.txtAddress.text = "\(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.add1 ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.add2 ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.area ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.city ?? "")"
                } else {
                    cell.lblTitle.text = "Deliver To:"
                    cell.txtAddress.isEditable = true
                    cell.txtAddress.placeholder = "Add your adress..."
                    cell.txtAddress.text = sDeliverAddress
                    txtDeliverToAddress = cell.txtAddress
                    cell.btnChangeAddress.isHidden = false
                    cell.btnChangeAddress.addTarget(self, action: #selector(btnChangeAddress(_:)), for: .touchUpInside)
                }
            }
            Taxes = 0
            cell.lblItemTotal.text = String(format:"₹%.2f",OrderAmount)
            cell.lblGSTAmount.text = "" //String(format:"₹%.2f",GST)
            cell.lblGSTTitle.text = ""
            for i in 0..<arrAllTaxDetails.count {
                let dictTaxes: NSDictionary = arrAllTaxDetails.object(at: i) as! NSDictionary
                let name = dictTaxes.object(forKey: "name") as? String ?? ""
                let value = dictTaxes.object(forKey: "value") as? CGFloat ?? 0
                let val = String(format:"₹%.2f",value)
                if cell.lblGSTTitle.text?.count ?? 0 <= 0 {
                    cell.lblGSTTitle.text = name
                } else {
                    cell.lblGSTTitle.text = "\(cell.lblGSTTitle.text ?? "")\n\(name)"
                }
                if cell.lblGSTAmount.text?.count ?? 0 <= 0 {
                    cell.lblGSTAmount.text = val
                } else {
                    cell.lblGSTAmount.text = "\(cell.lblGSTAmount.text ?? "")\n\(val)"
                }
            }
            
            cell.lblGSTTitle.setLineSpacing(lineSpacing: 8)
            cell.lblGSTAmount.setLineSpacing(lineSpacing: 8)
            
            //cell.lblTaxes.text = String(format:"₹%.2f",Taxes)
            cell.lblServiceChargePercent.text = String(format:"%.f%%",ServiceChargePercent)
            cell.lblServiceCharge.text = String(format:"(%@) ₹%.2f",cell.lblServiceChargePercent.text ?? "",ServiceChargeAmount)
            cell.lblPackingCharge.text = String(format:"₹%.2f",PackingCharge)
            cell.lblDeliveryCharge.text = String(format:"₹%.2f",DeliveryCharge)
            cell.lblResDiscount.text = String(format:"-₹%.2f",RestrauntDiscountAmount)
            cell.lblCoupenDiscount.text = String(format:"-₹%.2f",CouponDiscount)
            cell.lblZomatoDiscount.text = String(format:"-₹%.2f",ZomatoGoldDiscount)
            cell.lblTip.text = String(format:"₹%.2f",TotalTip)
            cell.lblGrandTotal.text = String(format:"₹%.2f",GrandTotal)
            
            cell.heightGST.constant = 21
            cell.viewGST.isHidden = false
            
            cell.heightTAX.constant = 21
            cell.viewTAX.isHidden = false
            
            cell.heightServiceChargePercent.constant = 21
            cell.viewServiceChargePercent.isHidden = false
            
            cell.heightServiceChargeAmount.constant = 21
            cell.viewServiceChargeAmount.isHidden = false
            
            cell.heightPackingCharge.constant = 21
            cell.viewPackingCharge.isHidden = false
            
            cell.heightDeliveryCharge.constant = 21
            cell.viewDeliveryCharge.isHidden = false
            
            cell.heightResDisc.constant = 21
            cell.viewResDisc.isHidden = false
            
            cell.heightCouponDisc.constant = 21
            cell.viewCouponDisc.isHidden = false
            
            cell.heightZomatoDisc.constant = 21
            cell.viewZomatoDisc.isHidden = false
            
            cell.heightTipDisc.constant = 21
            cell.viewTipDisc.isHidden = false
            
            if GST <= 0 {
                cell.heightGST.constant = 0
                cell.viewGST.isHidden = true
            }
            
            if Taxes <= 0 {
                cell.heightTAX.constant = 0
                cell.viewTAX.isHidden = true
            }
            
            cell.heightGST.constant = CGFloat(arrAllTaxDetails.count * 21)
            cell.viewGST.isHidden = false

            //if ServiceChargePercent <= 0 {
                cell.heightServiceChargePercent.constant = 0
                cell.viewServiceChargePercent.isHidden = true
            //}
            
            if ServiceChargeAmount <= 0 {
                cell.heightServiceChargeAmount.constant = 0
                cell.viewServiceChargeAmount.isHidden = true
            }
            
            if isRemoveServiceCharges {
                cell.heightServiceChargePercent.constant = 0
                cell.viewServiceChargePercent.isHidden = true
                cell.heightServiceChargeAmount.constant = 0
                cell.viewServiceChargeAmount.isHidden = true
            }
            
            if PackingCharge <= 0 {
                cell.heightPackingCharge.constant = 0
                cell.viewPackingCharge.isHidden = true
            }
            if DeliveryCharge <= 0 {
                cell.heightDeliveryCharge.constant = 0
                cell.viewDeliveryCharge.isHidden = true
            }

            if RestrauntDiscountAmount <= 0 {
                cell.heightResDisc.constant = 0
                cell.viewResDisc.isHidden = true
            }
            if CouponDiscount <= 0 {
                cell.heightCouponDisc.constant = 0
                cell.viewCouponDisc.isHidden = true
            }
            if ZomatoGoldDiscount <= 0 {
                cell.heightZomatoDisc.constant = 0
                cell.viewZomatoDisc.isHidden = true
            }
            if TotalTip <= 0 {
                cell.heightTipDisc.constant = 0
                cell.viewTipDisc.isHidden = true
            }
        
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellFoodBillingDetails", for: indexPath) as! CellFoodBillingDetails
            let arrOrder = self.dictPastOrders.object(forKey: arrAllKeys[indexPath.section]) as! NSArray
            if #available(iOS 11.0, *) {
                cell.setupContainerAndSeparatorFor(indexPath: indexPath, itemsCount: arrOrder.count)
            } else {
                if arrOrder.count == 1 && indexPath.row == 0 {
                    cell.viewContainer.roundCorners([.topLeft, .topRight,.bottomLeft, .bottomRight], radius: 10)
                } else if indexPath.row == 0 {
                    cell.viewContainer.roundCorners([.topLeft, .topRight], radius: 10)
                } else if indexPath.row == arrOrder.count-1 {
                    cell.viewContainer.roundCorners([.bottomLeft, .bottomRight], radius: 10)
                }
            }
            let objData: TPastItem = TPastItem.init(object: arrOrder.object(at: indexPath.row) as! NSDictionary)
            cell.lblItemName.text = objData.itemName
            let total = Double(objData.itemTotalPrice ?? "0")!
            cell.lblPrice.text = String(format:"₹%.f",total)
            cell.lblQty.text = objData.qty
            if objData.itemType == "nonveg" {
                cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
            } else if objData.itemType == "veg" && (objData.containsEgg == "1" || objData.containsEgg == "true") {
                cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
            } else {
                cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arrAllKeys = GetAllKeys()
        switch indexPath.section {
        case arrAllKeys.count:
            if isFromTakeAway || isFromDelivery {
                switch indexPath.row {
                case 0:
                    let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "CoupenVC") as! CoupenVC
                    vc.delegateApplyCoupon = self
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    break
                }
            } else {
                switch indexPath.row {
//                case 0:
//                    let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "ZomatoGoldVC") as! ZomatoGoldVC
//                    vc.delegateZomato = self
//                    vc.arrZomatoIds = arrZomatoIds
//                    vc.modalPresentationStyle = .overCurrentContext
//                    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
//                        self.present(vc, animated: false, completion: nil)
//                    }, completion: nil)
                case 0:
                    let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "CoupenVC") as! CoupenVC
                    vc.delegateApplyCoupon = self
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    break
                }
            }
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let arrAllKeys = GetAllKeys()
        switch indexPath.section {
        case arrAllKeys.count:
            /*
            if isFromTakeAway || isFromDelivery {
                switch indexPath.row {
                case 0:
                    return 44
                default:
                    return 50.5
                }
            } else {
                switch indexPath.row {
                case 0:
                    return 53
                case 1:
                    return 44
                default:
                    return 50.5
                }
            }*/
            switch indexPath.row {
            case 0:
                return 44
            default:
                return 50.5
            }
        case arrAllKeys.count+1:
            return UITableView.automaticDimension
            /*
            if isFromTakeAway || isFromDelivery {
                return 330
            }
            return 227*/
        default:
            return UITableView.automaticDimension
        }
    }
    
    @objc func btnChangeAddress(_ sender: UIButton) {
        openAddressPicker()
    }
}

//MARK:- CellTipDelegate
extension BillingDetailsVC: CellTipDelegate, delegateZomato {
    func addZomatoIDs(_ arrIDs: NSMutableArray) {
        self.arrZomatoIds.addObjects(from: arrIDs as! [Any])
        print(self.arrZomatoIds)
    }
    
    @objc func btnFifty() {
        GrandTotal = GrandTotal-TotalTip
        TotalTip = 50
        apiTip()
        tblBillingDetails.reloadData()
    }
    
    @objc func btnHundred() {
        GrandTotal = GrandTotal-TotalTip
        TotalTip = 100
        apiTip()
        tblBillingDetails.reloadData()
    }
    func didEndEditing(_ text: String) {
        GrandTotal = GrandTotal-TotalTip
        TotalTip = CGFloat((text as NSString).doubleValue)
        apiTip()
        tblBillingDetails.reloadData()
    }
}

//MARK:- API CALLING
extension BillingDetailsVC {
    func apiGetUserInfo() {
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    self.objUser = TUser.init(object: dictData)
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }

    func apiAcknowledge(_ payment_id: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/acknowledge"
        let dictParam: NSDictionary = ["razorpay_payment_id":payment_id, "payment_status":true]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIAcknowledge,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "RatingVC") as! RatingVC
                            vc.arrPastOrders = self.arrPastOrders
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiPickupAcknowledge(_ payment_id: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APITAAcknowledgePay
        let dictParam: NSDictionary = ["razorpay_payment_id":payment_id, "payment_status":true]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APITAAcknowledgePay,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            self.apiPickupPlace()
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiPickupPlace() {
        self.view.endEditing(true)
        let service = SERVER_URL + APITAPickUpPlace
        let dictParam: NSDictionary = ["":""]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APITAPickUpPlace,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "OrderSuccessVC") as! OrderSuccessVC
                            vc.dictPastOrders = self.dictPastOrders
                            vc.arrPastOrders = self.arrPastOrders
                            vc.total = self.GrandTotal
                            vc.isFromTakeAway = self.isFromTakeAway
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiDeliveryAcknowledge(_ payment_id: String) {
        self.view.endEditing(true)
        let service = SERVER_URL + APIMenuDeliveryAcknowledgePay
        let dictParam: NSDictionary = ["razorpay_payment_id":payment_id, "payment_status":true]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuDeliveryAcknowledgePay,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            self.apiDeliveryPlace()
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiDeliveryPlace() {
        self.view.endEditing(true)
        let service = SERVER_URL + APIMenuPlaceDelivery
        let dictParam: NSDictionary = ["":""]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuPlaceDelivery,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "OrderSuccessVC") as! OrderSuccessVC
                            vc.dictPastOrders = self.dictPastOrders
                            vc.arrPastOrders = self.arrPastOrders
                            vc.total = self.GrandTotal
                            vc.isFromTakeAway = self.isFromTakeAway
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }

    
    func apiTip() {
        self.view.endEditing(true)
        GrandTotal = GrandTotal+TotalTip
        tblBillingDetails.reloadData()
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/tips"
        let dictParam: NSDictionary = ["tip_amount":TotalTip]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APITip,
            parameters: dictParam,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
            } else {
                //showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetPastOrder(_ showLoader: Bool) {
        self.view.endEditing(true)
        self.dictPastOrders = NSMutableDictionary()
        arrPastOrders = NSMutableArray()
        tblBillingDetails.reloadData()
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: showLoader)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictOrder = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let dictData = dictOrder.object(forKey: "order_details") as! NSDictionary
                    self.OrderAmount = CGFloat(((dictData.object(forKey: "order_amount") as! String) as NSString).floatValue)
                    if let dictPaymentDetails = dictOrder.object(forKey: "payment_details") as? NSDictionary {
                        self.GrandTotal = CGFloat(((dictPaymentDetails.object(forKey: "payment_amount") as! String) as NSString).floatValue)
                    }
                    
                    if let dictTipDetails = dictOrder.object(forKey: "tip_details") as? NSDictionary {
                        if (dictTipDetails.object(forKey: "tip_amount") != nil) {
                            self.TotalTip = CGFloat((dictTipDetails.object(forKey: "tip_amount") as! NSString).floatValue)
                        }
                    }

                    self.arrZomatoIds = NSMutableArray()
                    if let dictDiscountDetails = dictOrder.object(forKey: "discount_details") as? NSDictionary {
                        self.CouponDiscount = CGFloat((dictDiscountDetails.object(forKey: "coupon_discount") as! NSString).floatValue)
                        self.RestrauntDiscountAmount = CGFloat((dictDiscountDetails.object(forKey: "restraunt_discount_amount") as! NSString).floatValue)
                        self.ZomatoGoldDiscount = CGFloat((dictDiscountDetails.object(forKey: "zomato_gold_discount") as! NSString).floatValue)
                        if let arrZIds = dictDiscountDetails.object(forKey: "zomato_gold_visit_id") as? NSArray {
                            self.arrZomatoIds = arrZIds.mutableCopy() as! NSMutableArray
                        }
                    }

                    /*
                    if let dictTaxDetails = dictOrder.object(forKey: "tax_details") as? NSDictionary {
                        self.GST = CGFloat((dictTaxDetails.object(forKey: "gst_amount") as! NSString).floatValue)
                        self.Taxes = CGFloat((dictTaxDetails.object(forKey: "service_tax_amount") as! NSString).floatValue)
                    }*/
                    
                    if let serviceCharge = dictOrder.object(forKey: "service_charge") as? String {
                        print(serviceCharge)
                        self.ServiceChargePercent = CGFloat((dictOrder.object(forKey: "service_charge") as! NSString).floatValue)
                    }
                    
                    if let serviceChargeAmount = dictOrder.object(forKey: "service_charge_amount") as? String {
                        print(serviceChargeAmount)
                        self.ServiceChargeAmount = CGFloat((dictOrder.object(forKey: "service_charge_amount") as! NSString).floatValue)
                    }
                    
                    self.isRemoveServiceCharges = false
                    if let serviceChargeRemove = dictOrder.object(forKey: "service_charge_remove") as? String {
                        if serviceChargeRemove == "1" {
                            self.isRemoveServiceCharges = true
                        }
                    }
                    
                    if let packingCharge = dictOrder.object(forKey: "packing_charges") as? String {
                        print(packingCharge)
                        self.PackingCharge = CGFloat((dictOrder.object(forKey: "packing_charges") as! NSString).floatValue)
                    }
                    
                    if let deliveryFee = dictOrder.object(forKey: "delivery_fee") as? String {
                        print(deliveryFee)
                        self.DeliveryCharge = CGFloat((dictOrder.object(forKey: "delivery_fee") as! NSString).floatValue)
                    }

                    self.arrAllTaxDetails = NSMutableArray()
                    if let arrTaxDetails = dictOrder.object(forKey:"tax_details") as? NSArray {
                        for i in 0..<arrTaxDetails.count {
                            let dictTaxDetails = arrTaxDetails.object(at: i) as! NSDictionary
                            let value = CGFloat((dictTaxDetails.object(forKey: "tax_amount") as? NSString ?? "0").floatValue)
                            if value > 0 {
                                var sTaxName = dictTaxDetails.object(forKey: "tax_name") as! String
                                sTaxName = sTaxName.replacingOccurrences(of: "_", with: " ")
                                let dictData: NSDictionary = ["name":sTaxName.uppercased(), "value":value]
                                self.arrAllTaxDetails.add(dictData)
                            }
                            
//                            let sTaxName = dictTaxDetails.object(forKey: "tax_name") as! String
//                            if sTaxName == "gst" {
//                                self.GST = CGFloat((dictTaxDetails.object(forKey: "tax_amount") as! NSString).floatValue)
//                            } else if sTaxName == "service_tax" {
//                                self.Taxes = CGFloat((dictTaxDetails.object(forKey: "tax_amount") as! NSString).floatValue)
//                            }
                        }
                    }
                    
                    let arrOrderDetailsMap = dictOrder.object(forKey: "order_details_id_name_map") as! NSArray
                    for i in 0..<arrOrderDetailsMap.count {
                        let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                        let Name = dictOrd.object(forKey: "id_name") as! String
                        let sK = dictOrd.object(forKey: "id") as! String
                        let arrTmp = dictData.object(forKey: sK) as! NSArray
                        for k in 0..<arrTmp.count {
                            let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            if sK == CUSTOMER_ID {
                                dictTmp.setObject("Ordered by You", forKey: "orderby" as NSCopying)
                            } else {
                                dictTmp.setObject("Ordered by \(Name)", forKey: "orderby" as NSCopying)
                            }
                            self.arrPastOrders.add(dictTmp.copy() as! NSDictionary)
                        }
                        
                        if sK == CUSTOMER_ID {
                            self.dictPastOrders.setObject(arrTmp, forKey: "YOUR ORDER" as NSCopying)
                        } else {
                            self.dictPastOrders.setObject(arrTmp, forKey: Name as NSCopying)
                        }
                    }
                    self.tblBillingDetails.delegate = self
                    self.tblBillingDetails.dataSource = self
                    DispatchQueue.main.async {
                        self.tblBillingDetails.reloadData()
                    }
                }
            }
        }
    }
    
    func apiCheckout(_ paymentType: String) {
        self.view.endEditing(true)
        var isShowLoder = true
        if paymentType == "online" {
            isShowLoder = false
        }
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/checkout"
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "payment_type":paymentType]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICheckout,
            parameters: dictParam,
            method: .post,
            showLoader: isShowLoder)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            //showMessage("Please wait for acknowledge from restaurant.")
                            if paymentType == "online" {
                                print("Online Payment")
                            } else {
                                let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "RatingFeedbackVC") as! RatingFeedbackVC
                                vc.isfrompayment = true
                                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }, completion: nil)
                            }
                        } else {
                            showMessage("Fail to checkout, Try again!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiCheckDeliverAddress() {
        self.view.endEditing(true)
        if sSelecetdAddress == sDeliverAddress {
            let location = CLLocation(latitude: selectedLat, longitude: selectedLong)
            self.verify(location)
        } else {
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(sDeliverAddress) { (placemarks, error) in
                if (error != nil) {
                    showMessage("Please enter proper address.")
                } else {
                    if placemarks?.count ?? 0 > 0 {
                        let location = placemarks?.first?.location
                        self.verify(location)
                    } else {
                        showMessage("Please enter proper address.")
                    }
                }
            }
        }
    }
    
    func verify(_ location: CLLocation?) {
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)/delivery/verify_address"
        let dictParam: NSDictionary = ["address":self.sDeliverAddress, "lat":"\(location?.coordinate.latitude ?? 0)", "lon":"\(location?.coordinate.longitude ?? 0)"]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIVerifyAddress,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            self.ContinuePayment()
                        } else {
                            showMessage(dictData.object(forKey: "error_msg") as? String ?? "Fail to verify address, Use proper address!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}


//MARK:- RAZORPAY DELEGATE METHOD
extension BillingDetailsVC: RazorpayPaymentCompletionProtocol {
    func showPaymentForm(){
        let dictDescription: NSDictionary = ["order_id": APP_DELEGATE.sOrderID, "created_on": Date().timeIntervalSince1970, "payee_id": CUSTOMER_ID]
        let sDesc: String = convertDictionaryToJSONString(dic: dictDescription) ?? ""
        let options: [String:Any] = [
             "amount" : "\(GrandTotal*100)", //mandatory in paise like:- 1000 paise ==  10 rs
             "description": sDesc,
             "image": objUser.profilePicture ?? "",
             "name": objUser.customerName ?? "",
             "prefill": [
                "contact": objUser.phoneNo,
                "email": objUser.email
             ],
             "theme": [
                 "color": "#3CBCB4"
             ]
         ]
        razorpay?.open(options, displayController: self)
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        if code != 2 {
            showMessage("FAILURE")
        }
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        if isFromTakeAway {
            apiPickupAcknowledge(payment_id)
        } else if isFromDelivery {
            apiDeliveryAcknowledge(payment_id)
        } else {
            apiAcknowledge(payment_id)
        }
        
        //apiPlaceOrder()
    }
}

//MARK: COUPON DELEGATE
extension BillingDetailsVC: delegateApplyCoupon {
    func ApplyCoupon(_ objSelectedCoupon: TCoupon) {
        apiGetPastOrder(true)
    }
}

//MARK:- UITABLEVIEW CELLS

class CellFoodBillingDetails: UITableViewCell {
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblQty: UILabel!
    
    func setupContainerAndSeparatorFor(indexPath: IndexPath, itemsCount: Int) {
        print(String(describing: indexPath.row) + "::" + String(describing: itemsCount))
        
        if itemsCount == 1 {
            if #available(iOS 11.0, *) {
                viewContainer.layer.cornerRadius = 10
                viewContainer.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner]
            } else {
                // Fallback on earlier versions
            }
        } else if indexPath.row == 0 {
            // round top corners, it's a first cell
            if #available(iOS 11.0, *) {
                viewContainer.layer.cornerRadius = 10
                viewContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            } else {
                // Fallback on earlier versions
            }
        } else if indexPath.row == itemsCount - 1 {
            // round bottom corners, it's a last cell
            if #available(iOS 11.0, *) {
                viewContainer.layer.cornerRadius = 10
                viewContainer.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
            }
        } else {
            if #available(iOS 11.0, *) {
                viewContainer.layer.cornerRadius = 10
                viewContainer.layer.maskedCorners = []
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
}

class CellZomatoGold: UITableViewCell {
    
}

class CellCoupen: UITableViewCell {
    
}

class CellTip: UITableViewCell, UITextFieldDelegate {
    @IBOutlet var btnFifty: UIButton!
    @IBOutlet var btnHundred: UIButton!
    @IBOutlet var txtTip: UITextField!
    weak var delegate: CellTipDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtTip.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.didEndEditing(TRIM(string: textField.text ?? "0"))
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension BillingDetailsVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}


//MARK: OPEN ADDRESS PICKER
extension BillingDetailsVC: delegateMapAdd, delegateMapAddCancel {
    
    func openAddressPicker() {
        self.view.endEditing(true)
        let vc = loadVC(strStoryboardId: "Map", strVCId: "MapVC") as! MapVC
        vc.delegateMapAdd = self
        vc.delegateMapAddCancel = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getAddressLatLong(_ lattitude: Double, _ longitude: Double, _ strAddress: String) {
        self.sDeliverAddress = strAddress
        self.sSelecetdAddress = strAddress
        self.selectedLat = longitude
        self.selectedLong = lattitude
        self.tblBillingDetails.reloadData()
    }
    
    func cancelAddSelection() {
 
    }
}
