//
//  ZomatoGoldVC.swift
//  Toast
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateZomato {
    func addZomatoIDs(_ arrIDs: NSMutableArray)
}

class ZomatoGoldVC: UIViewController {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    @IBOutlet var fifth: MyTextField!
    @IBOutlet var six: MyTextField!
    @IBOutlet var seven: MyTextField!
    
    @IBOutlet var eight: MyTextField!
    @IBOutlet var nine: MyTextField!
    @IBOutlet var ten: MyTextField!
    @IBOutlet var eleven: MyTextField!
    @IBOutlet var twel: MyTextField!
    @IBOutlet var thirty: MyTextField!
    @IBOutlet var fourteen: MyTextField!

    @IBOutlet var viewGradient: UIView!
    @IBOutlet var viewBlur: UIView!
    @IBOutlet var viewDetails: UIView!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var viewSecondCode: UIView!
    
    @IBOutlet var btnUseCode: UIButton!
    @IBOutlet var btnAddMoreCode: UIButton!
    
    var currentTextFiled:MyTextField! = nil
    var delegateZomato: delegateZomato?
    var arrZomatoIds = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        apiGetUserInfo()
        viewDetails.alpha = 0
        AddBlurEffect(viewBlur)
        AddGradient(viewGradient)
        setUpTextField()
        viewSecondCode.alpha = 0
        ShowZomatoIDs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.viewDetails.alpha = 1
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnAddMoreCode(_ sender: Any) {
        if viewSecondCode.alpha == 0 {
            currentTextFiled = eight
            UIView.animate(withDuration: 0.3) {
                self.viewSecondCode.alpha = 1
                self.btnAddMoreCode.alpha = 0
            }
        }
    }
    
    @IBAction func btnUseCode(_ sender: Any) {
        apiZomatoGold()
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension ZomatoGoldVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellEventImages = collectionView.dequeueReusableCell(withReuseIdentifier: "CellEventImages", for: indexPath) as! CellEventImages
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}

//MARK: API CALLING
extension ZomatoGoldVC {
    func apiZomatoGold() {
        self.view.endEditing(true)
        let arrZomatoCode: NSMutableArray = NSMutableArray()
        if first.isEnabled {
            first.text = TRIM(string: first.text ?? "")
            second.text = TRIM(string: second.text ?? "")
            third.text = TRIM(string: third.text ?? "")
            fourth.text = TRIM(string: fourth.text ?? "")
            fifth.text = TRIM(string: fifth.text ?? "")
            six.text = TRIM(string: six.text ?? "")
            seven.text = TRIM(string: seven.text ?? "")
            let strCode = "\(first.text ?? "")\(second.text ?? "")\(third.text ?? "")\(fourth.text ?? "")\(fifth.text ?? "")\(six.text ?? "")\(seven.text ?? "")"
            if strCode.count == 7 {
                arrZomatoCode.add(strCode)
            }
        }

        if TRIM(string: eight.text ?? "").count > 0 {
            eight.text = TRIM(string: eight.text ?? "")
            nine.text = TRIM(string: nine.text ?? "")
            ten.text = TRIM(string: ten.text ?? "")
            eleven.text = TRIM(string: eleven.text ?? "")
            twel.text = TRIM(string: twel.text ?? "")
            thirty.text = TRIM(string: thirty.text ?? "")
            fourteen.text = TRIM(string: fourteen.text ?? "")
            let strCode = "\(eight.text ?? "")\(nine.text ?? "")\(ten.text ?? "")\(eleven.text ?? "")\(twel.text ?? "")\(thirty.text ?? "")\(fourteen.text ?? "")"
            if strCode.count == 7 {
                arrZomatoCode.add(strCode)
            }
        }
        if arrZomatoCode.count <= 0 {
            return
        }
        
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/zomato_gold/\(CUSTOMER_ID)"
        let dictParam: NSDictionary = ["zomato_gold_visit_id":arrZomatoCode,"qr_code":APP_DELEGATE.strScannedQR]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIZomatoGold,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print("Zomato Response: ",responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            showMessage("Please wait a moment while restaurant updates your bill :)")
                            self.delegateZomato?.addZomatoIDs(arrZomatoCode)
                            self.btnBack(UIButton())
                        } else {
                            showMessage("Fail to apply zomato gold, Try again!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    let objUser = TUser.init(object: dictData)
                    self.lblDetails.text = "Hi, \(objUser.customerName ?? "")! Enter your 7 digit visit ID & enjoy your Zomato Gold privilege"
                }
            }
        }
    }
}

//MARK:- MyTextFieldDelegate Delegate
extension ZomatoGoldVC: UITextFieldDelegate, MyTextFieldDelegate {
    func setUpTextField() {
        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        fifth.myDelegate = self
        six.myDelegate = self
        seven.myDelegate = self

        eight.myDelegate = self
        nine.myDelegate = self
        ten.myDelegate = self
        eleven.myDelegate = self
        twel.myDelegate = self
        thirty.myDelegate = self
        fourteen.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fifth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        six.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        seven.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        
        eight.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        nine.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        ten.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        eleven.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        twel.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirty.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourteen.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fifth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        six.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        seven.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        eight.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        nine.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        ten.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        eleven.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        twel.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        thirty.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourteen.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
    
    func textFieldDidDelete() {
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0)
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0)
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0)
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0)
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fifth)
        {
            if(fifth.text!.count > 0)
            {
                fifth.text = ""
            }
            else
            {
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == six)
        {
            if(six.text!.count > 0)
            {
                six.text = ""
            }
            else
            {
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == seven)
        {
            if(seven.text!.count > 0)
            {
                seven.text = ""
            }
            else
            {
                currentTextFiled = six
                six.text = ""
                six.becomeFirstResponder()
            }
        } else if(currentTextFiled == eight)
        {
            if(eight.text!.count > 0)
            {
                eight.text = ""
                currentTextFiled = eight
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == nine)
        {
            if(nine.text!.count > 0)
            {
                nine.text = ""
            }
            else
            {
                eight.text = ""
                eight.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == ten)
        {
            if(ten.text!.count > 0)
            {
                ten.text = ""
            }
            else
            {
                currentTextFiled = nine
                nine.text = ""
                nine.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == eleven)
        {
            if(eleven.text!.count > 0)
            {
                eleven.text = ""
            }
            else
            {
                currentTextFiled = ten
                ten.text = ""
                ten.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == twel)
        {
            if(twel.text!.count > 0)
            {
                twel.text = ""
            }
            else
            {
                currentTextFiled = eleven
                eleven.text = ""
                eleven.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == thirty)
        {
            if(thirty.text!.count > 0)
            {
                thirty.text = ""
            }
            else
            {
                currentTextFiled = twel
                twel.text = ""
                twel.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourteen)
        {
            if(fourteen.text!.count > 0)
            {
                fourteen.text = ""
            }
            else
            {
                currentTextFiled = thirty
                thirty.text = ""
                thirty.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
        else if(textField == fifth)
        {
            currentTextFiled = fifth
            fifth.text = ""
        }
        else if(textField == six)
        {
            currentTextFiled = six
            six.text = ""
        }
        else if(textField == seven)
        {
            currentTextFiled = seven
            seven.text = ""
        }
        else if(textField == eight)
        {
            currentTextFiled = eight
            eight.text = ""
        }
        else if(textField == nine)
        {
            currentTextFiled = nine
            nine.text = ""
        }
        else if(textField == ten)
        {
            currentTextFiled = ten
            ten.text = ""
        }
        else if(textField == eleven)
        {
            currentTextFiled = eleven
            eleven.text = ""
        }
        else if(textField == twel)
        {
            currentTextFiled = twel
            twel.text = ""
        }
        else if(textField == thirty)
        {
            currentTextFiled = thirty
            thirty.text = ""
        }
        else if(textField == fourteen)
        {
            currentTextFiled = fourteen
            fourteen.text = ""
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            case fifth:
                currentTextFiled = six
                six.text = ""
                six.becomeFirstResponder()
            case six:
                currentTextFiled = seven
                seven.text = ""
                seven.becomeFirstResponder()
            case seven:
                self.view.endEditing(true)
            case eight:
                currentTextFiled = nine
                nine.text = ""
                nine.becomeFirstResponder()
            case nine:
                currentTextFiled = ten
                ten.text = ""
                ten.becomeFirstResponder()
            case ten:
                currentTextFiled = eleven
                eleven.text = ""
                eleven.becomeFirstResponder()
            case eleven:
                currentTextFiled = twel
                twel.text = ""
                twel.becomeFirstResponder()
            case twel:
                currentTextFiled = thirty
                thirty.text = ""
                thirty.becomeFirstResponder()
            case thirty:
                currentTextFiled = fourteen
                fourteen.text = ""
                fourteen.becomeFirstResponder()
            case fourteen:
                self.view.endEditing(true)
            default:
                break
            }
        }
    }
    
    
    //MARK:- TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFiled = textField as? MyTextField
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension ZomatoGoldVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}

extension ZomatoGoldVC {
    func ShowZomatoIDs() {
        if arrZomatoIds.count == 2 {
            btnUseCode.isEnabled = false
            btnUseCode.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        }
        if arrZomatoIds.count > 0 {
            for i in 0..<arrZomatoIds.count {
                let sZIds = arrZomatoIds[i] as! String
                let characters = Array(sZIds) as NSArray
                if i == 0 {
                    for j in 1..<8 {
                        let textF: MyTextField = self.view.viewWithTag(j) as! MyTextField
                        textF.isEnabled = false
                        textF.text = String(characters[j-1] as! Character)
                    }
                } else {
                    self.btnAddMoreCode(UIButton())
                    for j in 8..<15 {
                        let textF: MyTextField = self.view.viewWithTag(j) as! MyTextField
                        textF.isEnabled = false
                        textF.text = String(characters[j-8] as! Character)
                    }
                }
            }
        }
    }
}
