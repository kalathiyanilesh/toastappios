//
//  MenuVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import FloatingPanel
import GLNotificationBar
import MXParallaxHeader

class MenuVC: UIViewController, MXScrollViewDelegate {

    @IBOutlet var viewParallax: UIView!
    @IBOutlet var viewPage: UIView!
    @IBOutlet var scrMain: MXScrollView!
    @IBOutlet var btnSettingMenu: UIButton!
    @IBOutlet var bottomMenu: NSLayoutConstraint!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var imgRestaurant: UIImageView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var constraintViewAddressHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonKey: UIButton!
    @IBOutlet weak var buttonManage: UIButton!
    @IBOutlet weak var buttonLogOut: UIButton!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var buttonSeeMap: UIButton!
    @IBOutlet weak var labelShopStatus: UILabel!
    @IBOutlet weak var IabelTime: UILabel!
    @IBOutlet weak var buttonContact: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet var viewDTD: UIView!
    @IBOutlet var heightDTD: NSLayoutConstraint!
    @IBOutlet weak var viewDineIn: UIView!
    @IBOutlet weak var viewTakeAway: UIView!
    @IBOutlet weak var viewDeliver: UIView!
    @IBOutlet weak var btnDineIn: UIButton!
    @IBOutlet weak var btnTakeAway: UIButton!
    @IBOutlet weak var btnDeliver: UIButton!
    @IBOutlet var viewMain: UIView!
    
    var pageMenu : CAPSPageMenu?
    var fpc: FloatingPanelController!
    
    var dictMenu = NSDictionary()
    var objRestaurantDetails: TRestaurantDetails?
    
    var arrTypes = NSArray()
    var arrPastOrders = NSMutableArray()
    var arrMenus = NSMutableArray()
    var isFromTakeWay = Bool()
    var isFromDelivery = Bool()
    var isFromPriceLess = Bool()
    var PrcelessRestaurantID: String = ""
    
    var sContactNo: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APP_DELEGATE.isOnMenu = true
        let strjoinCode = getMyJoinCode()
        if strjoinCode.count <= 0 {
            GetTableCode()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)

        apiGetRestaurantDetails()
        
        AddRightSwipeGesture()
        imgRestaurant.image = RESTAURANT_PLACE_HOLDER
        if APP_DELEGATE.objRestaurantDetails != nil {
            lblRestaurantName.text = APP_DELEGATE.objRestaurantDetails?.restrauntName
            let resimg = (APP_DELEGATE.objRestaurantDetails?.featureImage ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            imgRestaurant.sd_setImage(with: URL(string: resimg), placeholderImage: RESTAURANT_PLACE_HOLDER)
        }
        setupUI()
        if !isFromPriceLess {
            SetupFloatingView()
            heightDTD.constant = 0
            viewDTD.alpha = 0
        } else {
            heightDTD.constant = 54
            viewDTD.alpha = 1
        }
        SetPageMenu()
        if !isFromTakeWay && !isFromDelivery && !isFromPriceLess {
            UserDefaults.standard.set("1", forKey: IS_ENTER_TABLE)
            UserDefaults.standard.synchronize()
            GetTableCode()
        }
        if isFromPriceLess {
            if APP_DELEGATE.objRestaurantDetails != nil {
                if APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.dineIn == "0" {
                    viewDineIn.alpha = 0.1
                    btnDineIn.isEnabled = false
                }
                if APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.pickUp == "0" {
                    viewTakeAway.alpha = 0.1
                    btnTakeAway.isEnabled = false
                }
                if APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.delivery == "0" {
                    viewDeliver.alpha = 0.1
                    btnDeliver.isEnabled = false
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if !isFromPriceLess {
            fpc.move(to: .tip, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if APP_DELEGATE.isShowDisZomPopup {
            APP_DELEGATE.isShowDisZomPopup = false
            let notificationBar = GLNotificationBar(title: APP_DELEGATE.PushTitle, message: APP_DELEGATE.PushMessage, preferredStyle: .detailedBanner, handler: nil)
            notificationBar.addAction(GLNotifyAction(title: "Go To Billing Page", style: .default) { (result) in
                APP_DELEGATE.GoToBillingPage()
            })
        }
        if APP_DELEGATE.isShowNotificationPopup {
            APP_DELEGATE.isShowNotificationPopup = false
            let notificationBar = GLNotificationBar(title: APP_DELEGATE.PushTitle, message: APP_DELEGATE.PushMessage, preferredStyle: .detailedBanner, handler: nil)
            notificationBar.addAction(GLNotifyAction(title: "Go To Notification", style: .default) { (result) in
                APP_DELEGATE.GoToNotificationTab(APP_DELEGATE.PushModule)
            })
        }
    }
    
    private func setupUI() {
        lbltype.text = ""
        if isFromTakeWay || isFromDelivery || isFromPriceLess {
            viewAddress.isHidden = false
            constraintViewAddressHeight.constant = 50.0
            buttonKey.isHidden = true
            buttonManage.isHidden = true
            buttonLogOut.isHidden = true
            
            buttonBack.isHidden = false
            
            if isFromTakeWay {
                lbltype.text = "TAKE AWAY"
            } else if isFromDelivery {
                lbltype.text = "DELIVERY MENU"
            }
            IabelTime.text = ""
            labelShopStatus.text = "Open Now"
            labelAddress.text = "\(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.add1 ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.add2 ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.area ?? ""), \(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.city ?? "")"
        } else {
            viewAddress.isHidden = true
            constraintViewAddressHeight.constant = 0.0
            buttonKey.isHidden = false
            buttonManage.isHidden = false
            buttonLogOut.isHidden = false
            
            buttonBack.isHidden = true
        }
        buttonFilter.isHidden = false
        if isFromPriceLess {
            bottomMenu.constant = 15
        }
    }
    
    @objc func didBecomeActive() {
        if isFromDelivery || isFromTakeWay || isFromPriceLess {
            return
        }
        apiGetPastOrder(false) { (dictOrder) in
            if dictOrder != nil {
                if !APP_DELEGATE.isOnRatingPage {
                    let orderStatus = dictOrder?.object(forKey: "order_status") as? String ?? ""
                    if orderStatus == "occupied" { return }
                    if isTableClose(dictOrder!) {
                        ClearTableCodes()
                        GoToHomePage()
                    }
                }
            }
        }
    }

    @IBAction func btnDineIn(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_TAB, strVCId: "TabVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnTakeAway(_ sender: Any) {
        self.apiJoinViaDelivery(isDelivery: false)
    }
    
    @IBAction func btnDelivery(_ sender: Any) {
        self.apiJoinViaDelivery(isDelivery: true)
    }
    
    @IBAction func buttonSeeOnMapTouchUpInside(_ sender: Any) {
        if APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.lat ?? "" != "" && APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.lon ?? "" != "" {
            let url = "https://www.google.com/maps?saddr=My+Location&daddr=\(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.lat ?? ""),\(APP_DELEGATE.objRestaurantDetails?.restrauntStoreDetails?.lon ?? "")"
            UIApplication.shared.open(URL(string:url)!, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func buttonContactTouchUpInside(_ sender: Any) {
        if !isFromPriceLess {
            if sContactNo.count > 0 {
                if let url = NSURL(string: "tel://\(sContactNo)"), UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnExit(_ sender: Any) {
        let alertController = UIAlertController(title: "", message: "Are you sure you want to exit from this table?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.apiLeaveTable()
        }
        
        let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnSettingMenu(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuSettingsVC") as! MenuSettingsVC
        vc.arrTypes = arrTypes
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnLock(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "ShareCodeVC")
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnManager(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "ManagerVC") as! ManagerVC
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func SetUpSpecialArray(_ dictData: NSDictionary, _ arrSpecial: NSMutableArray) {
        let arrfood = (dictData.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
        for i in 0..<arrfood.count {
            let sKey = arrfood[i] as! String
            let arrData = dictData[sKey] as! NSArray
            for k in 0..<arrData.count {
                if let dictData = arrData.object(at: k) as? NSDictionary {
                    if dictData.object(forKey: "is_special") as? String == "1" {
                        arrSpecial.add(dictData)
                    }
                }
            }
        }
    }
}

extension MenuVC: delegateSpecial, delegateFood {
    func CurrentFoodType(_ sType: String) {
        APP_DELEGATE.strCurrentMenu = sType
    }
    
    func SpecialFood() {
        APP_DELEGATE.dictMenuSection = NSMutableDictionary()
        APP_DELEGATE.strCurrentMenu = "special"
    }
}

//MARK:- SET PAGEMENU
extension MenuVC: CAPSPageMenuDelegate {
    
    func SetParallaxMenu() {
        var parallaxHeight: CGFloat = 0
        if isFromPriceLess {
            parallaxHeight = 150+constraintViewAddressHeight.constant+heightDTD.constant // (121+29 = 150)
        } else {
            parallaxHeight = 142+constraintViewAddressHeight.constant+heightDTD.constant // (121+21 = 142)
        }
        scrMain.parallaxHeader.view = viewParallax
        scrMain.parallaxHeader.height = parallaxHeight
        scrMain.parallaxHeader.mode = MXParallaxHeaderMode.fill
        scrMain.delegate = self
    }

    func SetPageMenu() {
        SetParallaxMenu()
        
        APP_DELEGATE.isFoodVeg = false
        APP_DELEGATE.isContainsEgg = false
        APP_DELEGATE.dictMenuSection = NSMutableDictionary()
        APP_DELEGATE.arrCartItems = NSMutableArray()
        
        UserDefaults.standard.removeObject(forKey: MENU_DICT)
        var arrController : [UIViewController] = []
        let arrSpecial = NSMutableArray()
        arrMenus = NSMutableArray()
        if dictMenu.allKeys.count > 0 {
            UserDefaults.standard.set(dictMenu, forKey: MENU_DICT)
            UserDefaults.standard.synchronize()
            if let dictfood = dictMenu["food"] as? NSDictionary {
                arrMenus.add("food")
                SetUpSpecialArray(dictfood, arrSpecial)
            }
            if let dictbeverages = dictMenu["beverages"] as? NSDictionary {
                arrMenus.add("beverages")
                SetUpSpecialArray(dictbeverages, arrSpecial)
            }
            if let dictdrinks = dictMenu["drinks"] as? NSDictionary {
                arrMenus.add("drinks")
                SetUpSpecialArray(dictdrinks, arrSpecial)
            }
            if let dictdesserts = dictMenu["desserts"] as? NSDictionary {
                arrMenus.add("desserts")
                SetUpSpecialArray(dictdesserts, arrSpecial)
            }
            if let dictstore = dictMenu["store"] as? NSDictionary {
                arrMenus.add("store")
                SetUpSpecialArray(dictstore, arrSpecial)
            }
        }
        
        if arrSpecial.count > 0 {
            let special = loadVC(strStoryboardId: SB_MENU, strVCId: "SpecialMenuVC") as! SpecialMenuVC
            special.title = "Special"
            special.isFromPriceLess = self.isFromPriceLess
            special.delegateSpecial = self
            special.arrSpecial = arrSpecial
            special.arrAllSpecial = arrSpecial
            arrController.append(special)
        }
    
//        let arrMenus = ["food", "beverages", "drinks", "desserts"]
        for i in 0..<arrMenus.count {
            let sKey = arrMenus[i] as! String
            var sTitle = ""
            switch sKey {
            case "food":
                sTitle = "Food Menu"
            case "beverages":
                sTitle = "Beverages"
            case "drinks":
                sTitle = "Drinks"
            case "desserts":
                sTitle = "Dessert"
            case "store":
                sTitle = "Store"
            default:
                sTitle = ""
            }
            
            let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "FoodMenuVC") as! FoodMenuVC
            vc.title = sTitle
            vc.sType = sKey
            vc.isFromPriceLess = self.isFromPriceLess
            vc.delegateFood = self
            if dictMenu.allKeys.count > 0 {
                if let dictData = dictMenu[sKey] as? NSDictionary {
                    vc.dictData = dictData
                    saveInUserDefault(obj: dictData, key: sKey)
                } else {
                    UserDefaults.standard.removeObject(forKey: sKey)
                    UserDefaults.standard.synchronize()
                }
            }
            arrController.append(vc)
        }
        
        if arrSpecial.count > 0 {
            arrMenus.insert("special", at: 0)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.lightGray),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectedMenuItemLabelColor(Color_Hex(hex: "#3CBCB4")),
            .unselectedMenuItemLabelColor(Color_Hex(hex: "#404040").withAlphaComponent(0.40)),
            .selectionIndicatorHeight(0),
            .menuHeight(47),
            .menuMargin(0),
            .menuItemFont(FontWithSize("Montserrat-Medium", 14)),
            .titleTextSizeBasedOnMenuItemWidth(true),
            .centerMenuItems(false)
        ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.controllerScrollView.alwaysBounceHorizontal = false
        pageMenu?.controllerScrollView.bounces = false
        //pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        /*
        switch index {
        case 0:
            APP_DELEGATE.strCurrentMenu = "special"
        case 1, 2, 3, 4:
            let arrMenus = ["food", "beverages", "drinks", "desserts"]
            APP_DELEGATE.strCurrentMenu = arrMenus[index-1]
        default:
            APP_DELEGATE.strCurrentMenu = ""
        }*/
        if arrMenus.contains("special") {
            APP_DELEGATE.strCurrentMenu = "special"
        } else {
            APP_DELEGATE.strCurrentMenu = arrMenus[index] as! String
        }
        
        if APP_DELEGATE.strCurrentMenu == "special" {
            NotificationCenter.default.post(name: Notification.Name(k_FILTER_SPECIAL_FOOD), object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name(k_FILTER_FOOD), object: nil)
        }
    }
}

//MARK:- SET FLOATING VIEW
extension MenuVC: FloatingPanelControllerDelegate, delegateCollapsFloat {
    func SetupFloatingView() {
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.white
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.layer.cornerRadius = 15.0
        fpc.surfaceView.clipsToBounds = true
        fpc.addPanel(toParent: self)
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "PlaceOrderVC") as! PlaceOrderVC
        vc.delegateCollapsFloat = self
        vc.isFromTakeAway = self.isFromTakeWay
        vc.isFromDelivery = self.isFromDelivery
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.track(scrollView: vc.scrMain)
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyFloatingPanelLayout()
    }
    
    func ChangeViewMode() {
        fpc.move(to: .full, animated: true)
    }
    

    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        if vc.position == .full {
            return
        }
        var height: CGFloat = 0
        if #available(iOS 11.0, *) {
            height = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            height = bottomLayoutGuide.length
        }
        let bottom = (vc.view.frame.size.height - height - vc.surfaceView.frame.origin.y)
        bottomMenu.constant = bottom
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        if vc.position == .tip {
            NotificationCenter.default.post(name: Notification.Name("hideshowgradientview"), object: 1.0)
            fpc.surfaceView.backgroundColor = UIColor.white
        } else {
            NotificationCenter.default.post(name: Notification.Name("hideshowgradientview"), object: 0.0)
            fpc.surfaceView.backgroundColor = Color_Hex(hex: "#656565")
        }
        
        let position = (vc.layout.insetFor(position: vc.position) ?? 0)
        bottomMenu.constant = position
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension MenuVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}

//MARK: API CALLING
extension MenuVC {
    func apiLeaveTable() {
        self.view.endEditing(true)
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/" + APILeave
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APILeave,
            parameters: NSDictionary(),
            method: .post,
            getReturn: true,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            RemoveAllTempData()
                            GoToHomePage()
                        } else {
                            if let strError = dictData.object(forKey: "error_message") as? String {
                                let alertController = UIAlertController(title: "", message: strError, preferredStyle: .alert)
                                let action1 = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                                }
                                alertController.addAction(action1)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func GetTableCode() {
        if isFromTakeWay || isFromDelivery || isFromPriceLess { return }
        apiGetTableCode { (strCode) in
            if strCode.count > 0 {
                SaveMyJoinCode(strCode)
            }
        }
    }
    
    func apiGetRestaurantDetails() {
        self.view.endEditing(true)
        let service = SERVER_URL + "restraunt/\(APP_DELEGATE.restaurantID)/details/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIRestaurantDetails,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
               print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let dictRes = dictData.object(forKey: "restraunt_store_details") as? NSDictionary {
                        self.sContactNo = dictRes.object(forKey: "contact_no") as! String
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiJoinViaDelivery(isDelivery: Bool) {
        self.view.endEditing(true)
        var strType = ""
        if isDelivery {
            strType = "delivery"
        } else {
            strType = "pick_up"
        }
        let service = SERVER_URL + APIMenuDelivery + strType
        APP_DELEGATE.restaurantID = PrcelessRestaurantID
        let dictParam: NSDictionary = ["restraunt_id":PrcelessRestaurantID]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuDelivery,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let dictTemp = dictData.object(forKey: "menu") as? NSDictionary {
                        //print(convertDictionaryToJSONString(dic: dictTemp) ?? "")
                        if let sOrderID = dictData.object(forKey: "order_id") as? String {
                            APP_DELEGATE.sOrderID = sOrderID
                        }
                        
                        SaveMyQRCode(APP_DELEGATE.strScannedQR)
                        SaveMyOrderID(APP_DELEGATE.sOrderID)
                        
                        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "MenuVC") as! MenuVC
                        if let dictMenu = dictTemp.object(forKey: "menu") as? NSDictionary {
                            vc.dictMenu = dictMenu
                        }
                        if let dictRestaurantDetails = dictTemp.object(forKey: "restraunt_details") as? NSDictionary {
                            APP_DELEGATE.objRestaurantDetails = TRestaurantDetails.init(object: dictRestaurantDetails)
                        }
                        
                        if isDelivery {
                            vc.isFromDelivery = true
                            vc.isFromTakeWay = false
                        } else {
                            vc.isFromTakeWay = true
                            vc.isFromDelivery = false
                        }
                        
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}


