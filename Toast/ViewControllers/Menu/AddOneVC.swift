//
//  AddOneVC.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateAddTopingsDetails {
    func ReloadData(_ arrSelectedSize: NSMutableArray, _ arrSelectedToppings: NSMutableArray, _ objTMenu: TMenu?, _ isRepeat: Bool)
}

class AddOneVC: UIViewController {

    @IBOutlet weak var tableViewAddOne: UITableView!
    @IBOutlet weak var constTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var buttonAddToTray: UIButton!
    @IBOutlet var lblTotalPrice: UILabel!
    
    //MARK:- Variables
    var objMenu: TMenu?
    var delegateAddTopingsDetails: delegateAddTopingsDetails?
    var arrSelectedSize = NSMutableArray()
    var arrSelectedAddOn = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func updateViewConstraints() {
        
//        if (SCREENHEIGHT() - 150) < tableViewAddOne.contentSize.height {
//            constTableViewHeight.constant = SCREENHEIGHT() - 150
//        } else {
//            constTableViewHeight.constant = tableViewAddOne.contentSize.height
//        }
        super.updateViewConstraints()
    }
    
    private func setup() {
        
        //AddBlurEffect(viewBlur)

        tableViewAddOne.delegate = self
        tableViewAddOne.dataSource = self
        tableViewAddOne.separatorStyle = .none
        tableViewAddOne.register(UINib(nibName: "TableViewCellAddTopings", bundle: nil), forCellReuseIdentifier: "TableViewCellAddTopings")
        tableViewAddOne.register(UINib(nibName: "TableViewCellSelectSize", bundle: nil), forCellReuseIdentifier: "TableViewCellSelectSize")
        tableViewAddOne.register(UINib(nibName: "TableViewHeaderAddOnes", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderAddOnes")
        tableViewAddOne.register(UINib(nibName: "TableViewHeaderAddOneDetails", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderAddOneDetails")
        tableViewAddOne.contentInset = UIEdgeInsets(top: 0.0, left: 0, bottom: 55.0, right: 0)
        
        tableViewAddOne.sectionHeaderHeight = UITableView.automaticDimension
        tableViewAddOne.estimatedSectionHeaderHeight = 85
     
        lblTotalPrice.text = "Total : ₹0.0"
        if objMenu?.itemPriceDetails?.count ?? 0 > 0 {
            let model = objMenu?.itemPriceDetails?[0]
            let dictSize = model!.dictionaryRepresentation()
            arrSelectedSize.add(dictSize)
            SetPrice()
        }
    }
    
    @IBAction func buttonAddTotTrayTouchUpInside(_ sender: Any) {
        //AddNewItem(objMenu!) // today
        /*
        let addOnsLimit: Int = Int(objMenu?.addonDetails?.addonLimit ?? "0") ?? 0
        let isOptional = objMenu?.addonDetails?.addonLimitOptional
        if isOptional == "0" && arrSelectedAddOn.count <= 0 {
            showMessage("Please select a maximum of \(addOnsLimit) toppings")
            return
        }*/
        print(arrSelectedSize)
        print(arrSelectedAddOn)
        for objAddOns in objMenu?.addonDetails ?? [AddonDetails]() {
            var SelectedCatAddons: Int = 0
            let max = Int(objAddOns.addonLimit ?? "0") ?? 0
            for i in 0..<arrSelectedAddOn.count {
                let objAddOnsItem = AddonItems.init(object: arrSelectedAddOn[i] as! NSDictionary)
                if objAddOnsItem.addonCategoryId == objAddOns.addonCateogryId {
                    SelectedCatAddons += 1
                }
            }
            
            if objAddOns.addonLimitOptional == "0" {
                if SelectedCatAddons <= 0{
                    showMessage("Please select at least one \(objAddOns.addonCategoryName ?? "")")
                    return
                }
                if max < SelectedCatAddons{
                    showMessage("Maximum of \(max) \(objAddOns.addonCategoryName ?? "") can only be selected")
                    return
                }
            } else {
                if max < SelectedCatAddons {
                    showMessage("Maximum of \(max) \(objAddOns.addonCategoryName ?? "") can only be selected")
                    return
                }
            }
        }
        delegateAddTopingsDetails?.ReloadData(arrSelectedSize, arrSelectedAddOn, objMenu, false)
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @objc func buttonCloseTouchUpInside(_ sender: UIButton) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
}

extension AddOneVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print((objMenu?.addonDetails?.count ?? 0))
        return 2 + (objMenu?.addonDetails?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            /*
            if objMenu?.tags?.count ?? 0 > 0 {
                return 120
            }
            return 85.0*/
            return UITableView.automaticDimension
        case 1:
            return objMenu?.itemPriceDetails?.count ?? 0 > 0 ? 56.0 : CGFloat.leastNormalMagnitude
        default:
            let objAddOns = objMenu?.addonDetails?[section-2]
            return objAddOns?.addonItems?.count ?? 0 > 0 ? 56.0 : CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderAddOneDetails") as! TableViewHeaderAddOneDetails
            headerView.buttonClose.addTarget(self, action: #selector(buttonCloseTouchUpInside(_:)), for: .touchUpInside)
            
            headerView.labelTitle.text = objMenu?.itemName
            
            headerView.labelDesc.text = objMenu?.itemDescription
            
            if objMenu?.itemType == "nonveg" {
                headerView.imageVeg.image = UIImage(named: "ic_nonveg")
            } else if objMenu?.itemType == "veg" && objMenu?.containsEgg == "1" {
                headerView.imageVeg.image = UIImage(named: "eggitarian")
            } else {
                headerView.imageVeg.image = UIImage(named: "ic_veg")
            }
            if objMenu?.tags?.count ?? 0 > 0 {
                headerView.arrTags = objMenu?.tags
                headerView.initCollectionView()
                headerView.heightColl.constant = 24
                headerView.bottomColl.constant = 15.5
                headerView.collTags.isHidden = false
            } else {
                headerView.heightColl.constant = 0
                headerView.bottomColl.constant = 10
                headerView.collTags.isHidden = true
            }
            self.view.layoutIfNeeded()
            
            return headerView
        case 1:
            
            if objMenu?.itemPriceDetails?.count ?? 0 > 0 {
                let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderAddOnes") as! TableViewHeaderAddOnes
                headerView.lblOptional.isHidden = true
                headerView.labelTitle.text = "Select Size"
                headerView.labelTitleDesc.text = "Please select any one size"
                
                return headerView
            } else {
                return UIView (frame: .zero)
            }
            
        default:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderAddOnes") as! TableViewHeaderAddOnes
            let objAddOns = objMenu?.addonDetails?[section-2]
            headerView.labelTitle.text = objAddOns?.addonCategoryName
            let addOnsLimit: Int = Int(objAddOns?.addonLimit ?? "0") ?? 0
            if addOnsLimit > 0 {
                headerView.labelTitleDesc.text = "Select a maximum of \(addOnsLimit) toppings"
                headerView.lblOptional.isHidden = true
                if objAddOns?.addonLimitOptional == "1" {
                    headerView.lblOptional.isHidden = false
                }
            } else {
                headerView.labelTitleDesc.text = "Select a toppings"
            }
            
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView (frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        case 1:
            return objMenu?.itemPriceDetails?.count ?? 0
        default:
            let objAddOns = objMenu?.addonDetails?[section-2]
            return objAddOns?.addonItems?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 0.0
        case 1:
            return 35.0
        default:
            return 35.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return UITableViewCell()
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellSelectSize", for: indexPath) as! TableViewCellSelectSize
            cell.selectionStyle = .none
            
            let model = objMenu?.itemPriceDetails?[indexPath.row]
            
            cell.labelName.text = (model?.itemSize ?? "").capitalized
            cell.labelPrice.text = "₹\(model?.itemPrice ?? "0")"
            
            let dictSize = model!.dictionaryRepresentation()
            cell.btnSelectSize.isSelected = arrSelectedSize.contains(dictSize)
            cell.btnSelectSize.isUserInteractionEnabled = false
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellAddTopings", for: indexPath) as! TableViewCellAddTopings
            cell.selectionStyle = .none
            
            let objAddOns = objMenu?.addonDetails?[indexPath.section-2]
            let model = objAddOns?.addonItems?[indexPath.row]
            
            cell.labelName.text = (model?.addonName ?? "").capitalized
            if model?.addonPrice?.count ?? 0 > 0 {
                let modelAddon = model?.addonPrice?.first!
                cell.labelPrice.text = "₹\(modelAddon?.price ?? "0")"
            } else {
                cell.labelPrice.text = "₹0"
            }
            
            if model?.addonType == "nonveg" {
                cell.imageVeg.image = UIImage(named: "ic_nonveg")
            } else if model?.addonType == "veg" && model?.containsEgg == true {
                cell.imageVeg.image = UIImage(named: "eggitarian")
            } else {
                cell.imageVeg.image = UIImage(named: "ic_veg")
            }
            
            let dictAddones = model!.dictionaryRepresentation()
            cell.buttonSelectToppings.isSelected = arrSelectedAddOn.contains(dictAddones)
            cell.buttonSelectToppings.isUserInteractionEnabled = false
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            break
        case 1:
            let model = objMenu?.itemPriceDetails?[indexPath.row]
            let dictSize = model!.dictionaryRepresentation()
            arrSelectedSize = NSMutableArray()
            arrSelectedSize.add(dictSize)
            SetPrice()
            tableViewAddOne.reloadData()
        default:
            let objAddOns = objMenu?.addonDetails?[indexPath.section-2]
            let model = objAddOns?.addonItems?[indexPath.row]
            let dictAddones = model!.dictionaryRepresentation()
            if arrSelectedAddOn.contains(dictAddones) {
                arrSelectedAddOn.remove(dictAddones)
            } else {
//                let addOnsLimit: Int = Int(objAddOns?.addonLimit ?? "0") ?? 0
//                if arrSelectedAddOn.count+1 > addOnsLimit && addOnsLimit > 0 {
//                    showMessage("You can select a maximum of \(addOnsLimit) toppings")
//                    return
//                }
                arrSelectedAddOn.add(dictAddones)
            }
            SetPrice()
            tableViewAddOne.reloadData()
        }
    }
    
    func SetPrice() {
        var totalPrice: CGFloat = 0
        for i in 0..<arrSelectedSize.count {
            let dictSize = arrSelectedSize[i] as! NSDictionary
            let model = ItemPriceDetails.init(object: dictSize)
            if let n = NumberFormatter().number(from: model.itemPrice ?? "0") {
                totalPrice = totalPrice + CGFloat(truncating: n)
            }
        }
        for i in 0..<arrSelectedAddOn.count {
            let dictSize = arrSelectedAddOn[i] as! NSDictionary
            let model = AddonItems.init(object: dictSize)
            if model.addonPrice?.count ?? 0 > 0 {
                let modelAddon = model.addonPrice?.first!
                if let n = NumberFormatter().number(from: modelAddon?.price ?? "0") {
                    totalPrice = totalPrice + CGFloat(truncating: n)
                }
            }
        }
        lblTotalPrice.text = String(format:"Total : ₹%.02f",totalPrice)
    }
}
