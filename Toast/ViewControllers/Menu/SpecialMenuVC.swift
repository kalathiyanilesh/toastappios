//
//  SpecialMenuVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateSpecial {
    func SpecialFood()
}

class CellSpecialMenu: UICollectionViewCell {
    
    @IBOutlet var viewAdd: UIView!
    @IBOutlet var viewPlusMinus: UIView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgFood: UIImageView!
    @IBOutlet var lblItemCount: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnMinus: UIButton!
}

class SpecialMenuVC: UIViewController {
    @IBOutlet var collSpecial: UICollectionView!
    
    var arrSpecial = NSMutableArray()
    var arrAllSpecial = NSMutableArray()
    var delegateSpecial: delegateSpecial?
    var isFromPriceLess = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrAllSpecial = arrSpecial
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetFilterData), name: Notification.Name(k_FILTER_SPECIAL_FOOD), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReloadSpecialFood), name: Notification.Name(k_RELOAD_SPECIAL_FOOD), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.delegateSpecial?.SpecialFood()
        if collSpecial != nil {
            GetFilterData()
            collSpecial.reloadData()
        }
    }
    
    @objc func ReloadSpecialFood() {
        collSpecial.reloadData()
    }
    
    @objc func GetFilterData() {
        arrSpecial = NSMutableArray()
        
        if (!APP_DELEGATE.isFoodVeg && !APP_DELEGATE.isContainsEgg) || (!APP_DELEGATE.isFoodVeg && APP_DELEGATE.isContainsEgg) {
            arrSpecial = arrAllSpecial
        } else {
            let arrFilter = NSMutableArray()
            for k in 0..<arrAllSpecial.count {
                let objData: TMenu = TMenu.init(object: arrAllSpecial.object(at: k) as! NSDictionary)
                if APP_DELEGATE.isFoodVeg && APP_DELEGATE.isContainsEgg {
                    if (objData.itemType == "veg" || objData.containsEgg == "true") && objData.itemType != "non-veg"{
                        arrFilter.add(objData.dictionaryRepresentation())
                    }
                } else if APP_DELEGATE.isFoodVeg && objData.itemType == "veg" && objData.containsEgg != "true"  {
                    arrFilter.add(objData.dictionaryRepresentation())
                }
            }
            arrSpecial = arrFilter
        }
        
        collSpecial.reloadData()
    }
}

extension SpecialMenuVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSpecial.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellSpecialMenu = collectionView.dequeueReusableCell(withReuseIdentifier: "CellSpecialMenu", for: indexPath) as! CellSpecialMenu
        cell.viewAdd.alpha = 1
        let objData: TMenu = TMenu.init(object: arrSpecial.object(at: indexPath.row) as! NSDictionary)

        cell.lblName.text = objData.itemName
        cell.imgFood.sd_setImage(with: URL(string: objData.itemImageUrl?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        if objData.itemPriceDetails?.count ?? 0 > 0 {
            let model = objData.itemPriceDetails?.first!
            cell.lblPrice.text = "₹\(model?.itemPrice ?? "0")"
        } else {
            cell.lblPrice.text = "₹0"
        }
        if objData.itemType == "non-veg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objData.itemType == "veg" && objData.containsEgg == "true" {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
 
        cell.viewPlusMinus.isHidden = true
        cell.viewAdd.isHidden = true
        
        if isFromPriceLess {
            cell.viewPlusMinus.isHidden = true
            cell.viewAdd.isHidden = true
            cell.btnAdd.isHidden = true
            cell.lblPrice.isHidden = true
        } else {
            cell.lblPrice.isHidden = false
            let strCount = GetItemCount(objData.itemId!)
            if (Int(strCount)! >= 1) {
                cell.viewPlusMinus.isHidden = false
                cell.lblItemCount.text = strCount
                cell.btnPlus.addTarget(self, action: #selector(btnPlus(_:)), for: .touchUpInside)
                cell.btnMinus.addTarget(self, action: #selector(btnMinus(_:)), for: .touchUpInside)
            } else {
                cell.viewAdd.isHidden = false
                cell.btnAdd.addTarget(self, action: #selector(AddItem(_:)), for: .touchUpInside)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-30)/2, height: 190)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "FoodDetailsVC") as! FoodDetailsVC
        vc.delegateFoodDetails = self
        vc.objMenu = TMenu.init(object: arrSpecial.object(at: indexPath.row) as! NSDictionary)
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isFromPriceLess {
            return CGSize.zero
        } else {
            return CGSize(width: SCREENWIDTH(), height: 60)
        }
    }
    
    @objc func AddItem(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collSpecial)
        let indexPath = collSpecial.indexPathForItem(at: buttonPosition)
        let objData: TMenu = TMenu.init(object: arrSpecial.object(at: indexPath!.row) as! NSDictionary)
        //let objAddOns = objData.addonDetails?[indexPath!.section]
        if objData.itemPriceDetails?.count ?? 0 == 1 && objData.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objData)
            collSpecial.reloadItems(at: [indexPath!])
            return
        }
        if objData.itemPriceDetails?.count ?? 0 <= 0 && objData.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objData)
            collSpecial.reloadItems(at: [indexPath!])
            return
        }
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objData
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
        
        /*
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collSpecial)
        let indexPath = collSpecial.indexPathForItem(at: buttonPosition)
        let objData: TMenu = arrSpecial.object(at: indexPath!.row) as! TMenu
        AddNewItem(objData)
        collSpecial.reloadItems(at: [indexPath!])*/
    }
    
    @objc func btnPlus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collSpecial)
        let indexPath = collSpecial.indexPathForItem(at: buttonPosition)
        let objData: TMenu = TMenu.init(object: arrSpecial.object(at: indexPath!.row) as! NSDictionary)
        //let objAddOns = objData.addonDetails?[indexPath!.section]
        if objData.itemPriceDetails?.count ?? 0 == 1 && objData.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objData)
            collSpecial.reloadItems(at: [indexPath!])
            return
        }
        if objData.itemPriceDetails?.count ?? 0 <= 0 && objData.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objData)
            collSpecial.reloadItems(at: [indexPath!])
            return
        }
        
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "RepeatLastVC") as! RepeatLastVC
        vc.objMenu = objData
        vc.delegateRepeatNewDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
        /*
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collSpecial)
        let indexPath = collSpecial.indexPathForItem(at: buttonPosition)
        let objData: TMenu = arrSpecial.object(at: indexPath!.row) as! TMenu
        PlusItem(objData)
        collSpecial.reloadItems(at: [indexPath!])*/
    }
    
    @objc func btnMinus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:collSpecial)
        let indexPath = collSpecial.indexPathForItem(at: buttonPosition)
        let objData: TMenu = TMenu.init(object: arrSpecial.object(at: indexPath!.row) as! NSDictionary)
        MinusItem(objData)
        collSpecial.reloadItems(at: [indexPath!])
    }
}

extension SpecialMenuVC: delegateFoodDetails {
    func ReloadData() {
        collSpecial.reloadData()
    }
}

extension SpecialMenuVC: delegateAddTopingsDetails {
    func ReloadData(_ arrSelectedSize: NSMutableArray, _ arrSelectedToppings: NSMutableArray, _ objTMenu: TMenu?, _ isRepeat: Bool) {
        var arrTmpItemPriceDetails: [ItemPriceDetails] = [ItemPriceDetails]()
        for i in 0..<arrSelectedSize.count {
            let dictSize = arrSelectedSize[i] as! NSDictionary
            let model = ItemPriceDetails.init(object: dictSize)
            arrTmpItemPriceDetails.append(model)
        }
        
        var arrTmpAddonItems: [AddonItems] = [AddonItems]()
        for i in 0..<arrSelectedToppings.count {
            let dictSize = arrSelectedToppings[i] as! NSDictionary
            let model = AddonItems.init(object: dictSize)
            arrTmpAddonItems.append(model)
        }
        
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let obj: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            obj.isLastAdded = "0"
        }

        objTMenu?.selectedAddonItems = arrTmpAddonItems
        objTMenu?.isLastAdded = "1"
        objTMenu?.itemCount = "1"
        objTMenu?.uniqID = getUniqID()
        objTMenu?.selectedItemPriceDetails = arrTmpItemPriceDetails
        APP_DELEGATE.arrCartItems.add(objTMenu!)
        
        collSpecial.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
}

extension SpecialMenuVC: delegateRepeatNewDetails {
    func openNewAddOneViaRepeat(objMenu: TMenu) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objMenu
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func reloadData(_ objMenu: TMenu?) {
        let objLastAdded = GetLastAddedAddonsForItem(objMenu)
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objTMenu: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            if objTMenu.itemId == objLastAdded?.itemId && objTMenu.uniqID == objLastAdded?.uniqID {
                let count = (Int(objTMenu.itemCount ?? "0") ?? 0) + 1
                objTMenu.itemCount = "\(count)"
            }
        }

        collSpecial.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
}
