//
//  FoodDetailsVC.swift
//  Toast
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import AVKit
import CHIPageControl
import Lightbox

protocol delegateFoodDetails {
    func ReloadData()
}

class CellFoodUserReview: UITableViewCell {
    @IBOutlet var viewImages: UIView!
    @IBOutlet var heightViewImages: NSLayoutConstraint!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgHappyType: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblLevelReviewer: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var img3: UIImageView!
}

class FoodDetailsVC: UIViewController {
    @IBOutlet var viewBlur: UIView!
    @IBOutlet var viewPlusMinus: UIView!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var viewDetails: UIView!
    @IBOutlet var collImages: UICollectionView!
    @IBOutlet var pagecontrol: CHIPageControlChimayo!
    @IBOutlet var tblReviews: UITableView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var lblTotalReviews: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet weak var tableViewSelectSize: UITableView!
    @IBOutlet weak var constTableViewHeight: NSLayoutConstraint!
    
    var delegateFoodDetails: delegateFoodDetails?
    var objMenu: TMenu?
    var arrReviews = NSMutableArray()
    var Service = ""
    var isSend = false
    var ItemCount = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetails.alpha = 0
        AddRightSwipeGesture()
        //AddBlurEffect(viewBlur)
        
        lblPrice.isHidden = true
        btnAdd.isHidden = true
        lblTotalItem.isHidden = true
        
        if objMenu?.itemVideoUrl?.count ?? 0 > 0 && objMenu?.itemImageUrl?.count ?? 0 > 0 {
            pagecontrol.numberOfPages = 2
        } else {
            pagecontrol.numberOfPages = 1
        }
        pagecontrol.radius = 3
        pagecontrol.tintColor = UIColor.white
        pagecontrol.currentPageTintColor = UIColor.white
        pagecontrol.padding = 2
        
        AddGradient(viewGradient)
        
        tableViewSelectSize.delegate = self
        tableViewSelectSize.dataSource = self
        tableViewSelectSize.separatorStyle = .none
        tableViewSelectSize.register(UINib(nibName: "TableViewHeaderAddOnes", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableViewHeaderAddOnes")
        tableViewSelectSize.register(UINib(nibName: "TableViewCellFoodDetailsSize", bundle: nil), forCellReuseIdentifier: "TableViewCellFoodDetailsSize")
        
        lblItemName.text = objMenu?.itemName
        lblPrice.text = "₹\(objMenu?.itemPrice ?? "0")"
        lblDescription.text = objMenu?.itemDescription
        if objMenu?.itemDescription?.count ?? 0 <= 0 {
            lblDescription.text = "\n"
        }
        if objMenu?.itemType == "non-veg" {
            imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objMenu?.itemType == "veg" && objMenu?.containsEgg == "1" {
            imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        
        SetUpCount()
        
        lblTotalReviews.text = "0 Customer reviews"
        apiGetItemReview()
        tblReviews.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.apiGetItemReview()
            }else {
                self.tblReviews.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.viewDetails.alpha = 1
        }
    }
    
    override func updateViewConstraints() {
        constTableViewHeight.constant = tableViewSelectSize.contentSize.height
        super.updateViewConstraints()
    }
    
    func SetUpCount() {
        ItemCount = GetItemCount(objMenu!.itemId!)
        if Int(ItemCount)! >= 1 {
            btnAdd.isHidden = true
            viewPlusMinus.isHidden = false
            lblTotalItem.text = ItemCount
        } else {
            lblTotalItem.text = "0"
            btnAdd.isHidden = true
            viewPlusMinus.isHidden = true
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.delegateFoodDetails?.ReloadData()
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnAddItem(_ sender: Any) {
        self.btnPlus(UIButton())
        btnBack(UIButton())
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        AddNewItem(objMenu!)
        SetUpCount()
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        if Int(lblTotalItem.text ?? "0")! <= 0  {
            self.btnAdd(UIButton())
        } else {
            PlusItem(objMenu!)
            SetUpCount()
        }
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        MinusItem(objMenu!)
        SetUpCount()
    }
}

//MARK: API CALLING
extension FoodDetailsVC {
    func apiGetItemReview() {
        isSend = true
        self.view.endEditing(true)
        self.activity.startAnimating()
        var isFirstPage = false
        if Service.count <= 0 {
            isFirstPage = true
            Service = SERVER_URL + APIGetItemReview + "\(objMenu?.itemId ?? "")" + "?start=0&limit=10"
        }
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: Service,
            service: APIGetItemReview,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: false)
        { (error, responseDict) in
            self.activity.stopAnimating()
            if isFirstPage {
                self.tblReviews.showsInfiniteScrolling = true
                self.arrReviews.removeAllObjects()
            }
            self.Service = ""
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    for i in 0..<arrData.count {
                        let objReview: TItemReview = TItemReview.init(object: arrData[i] as! NSDictionary)
                        self.arrReviews.add(objReview)
                    }
                }
                var sNext = ""
                if let dictPagination = responseDict?.object(forKey: "pagination") as? NSDictionary {
                    if (dictPagination.object(forKey: "next") != nil) {
                        sNext = dictPagination.object(forKey: "next") as! String
                    }
                    if (dictPagination.object(forKey: "total") != nil) {
                        self.lblTotalReviews.text = "\(dictPagination.object(forKey: "total") as! String) Customer reviews"
                    }
                }
                if sNext.count > 0 {
                    self.Service = sNext
                }
            }
            self.tblReviews.infiniteScrollingView.stopAnimating()
            if self.arrReviews.count <= 0 || self.Service.count <= 0 { self.tblReviews.showsInfiniteScrolling = false }
            
            self.tblReviews.reloadData()
            self.isSend = false
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension FoodDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewSelectSize {
            return objMenu?.itemPriceDetails?.count ?? 0 > 0 ? 1 : 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableViewSelectSize {
            return objMenu?.itemPriceDetails?.count ?? 0 > 0 ? 56.0 : CGFloat.leastNormalMagnitude
        } else {
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == tableViewSelectSize {
            if objMenu?.itemPriceDetails?.count ?? 0 > 0 {
                let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableViewHeaderAddOnes") as! TableViewHeaderAddOnes
                
                headerView.lsbelSeperator.isHidden = true
                headerView.lblOptional.isHidden = true
                if objMenu?.addonDetails?.count ?? 0 <= 0 {
                    headerView.labelTitle.text = "Add Item"
                    headerView.labelTitleDesc.text = ""
                } else {
                    headerView.labelTitle.text = "Select Size"
                    headerView.labelTitleDesc.text = "The item is customizable"
                }
                
                
                return headerView
            } else {
                return UIView (frame: .zero)
            }
        } else {
            return UIView (frame: .zero)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView (frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewSelectSize {
            return objMenu?.itemPriceDetails?.count ?? 0 > 0 ? 1 : 0
        } else {
            return arrReviews.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewSelectSize {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellFoodDetailsSize", for: indexPath) as! TableViewCellFoodDetailsSize
            cell.selectionStyle = .none
            
            let model = objMenu?.itemPriceDetails?[indexPath.row]
            
            cell.labelSize.text = (model?.itemSize ?? "").capitalized
            cell.labelPrice.text = "₹\(model?.itemPrice ?? "0")"
            
            cell.viewPlusMinus.isHidden = true
            cell.buttonAdd.isHidden = true
            
            let strCount = GetItemCount(objMenu?.itemId! ?? "0")
            if (Int(strCount)! >= 1) {
                cell.viewPlusMinus.isHidden = false
                cell.labelCount.text = strCount
            } else {
                cell.buttonAdd.isHidden = false
            }
            cell.buttonPlus.addTarget(self, action: #selector(btnPlusNew(_:)), for: .touchUpInside)
            cell.buttonMinus.addTarget(self, action: #selector(btnMinusNew(_:)), for: .touchUpInside)
            cell.buttonAdd.addTarget(self, action: #selector(AddItemNew(_:)), for: .touchUpInside)
            return cell
        } else {
            let cell: CellFoodUserReview = tableView.dequeueReusableCell(withIdentifier: "CellFoodUserReview", for: indexPath) as! CellFoodUserReview
            let objReview: TItemReview = arrReviews[indexPath.row] as! TItemReview
            cell.lblLevelReviewer.text = "(Level \(objReview.customerLevel ?? "1") Reviewer)"
            cell.lblName.text = objReview.customerName
            cell.imgUser.sd_setImage(with: URL(string: objReview.customerProfilePicture?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
            cell.lblDesc.text = objReview.itemReview
            cell.imgHappyType.image = GetEmojiFromRate(CGFloat((objReview.itemRating! as NSString).doubleValue))
            
            cell.heightViewImages.constant = 0
            cell.viewImages.alpha = 0
            cell.img1.image = nil
            cell.img2.image = nil
            cell.img3.image = nil
            
            var arrItemReviewImg = [String]()
            arrItemReviewImg = objReview.itemReviewImgList ?? [""]
            if arrItemReviewImg.count > 0 {
                if arrItemReviewImg[0].count > 0 {
                    cell.heightViewImages.constant = 50
                    cell.viewImages.alpha = 1
                }
            }
            for i in 0..<arrItemReviewImg.count {
                switch i {
                case 0:
                    cell.img1.sd_setImage(with: URL(string: arrItemReviewImg[i].RemoveSpace()), placeholderImage: nil)
                case 1:
                    cell.img2.sd_setImage(with: URL(string: arrItemReviewImg[i].RemoveSpace()), placeholderImage: nil)
                default:
                    cell.img3.sd_setImage(with: URL(string: arrItemReviewImg[i].RemoveSpace()), placeholderImage: nil)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewSelectSize {
            return 35.0
        } else {
            return UITableView.automaticDimension
        }
        
    }
    
    @objc func AddItemNew(_ sender: UIButton) {
        if objMenu?.itemPriceDetails?.count ?? 0 == 1 && objMenu?.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objMenu!)
            tableViewSelectSize.reloadData()
            return
        }
        if objMenu?.itemPriceDetails?.count ?? 0 <= 0 && objMenu?.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objMenu!)
            tableViewSelectSize.reloadData()
            return
        }
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objMenu
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
        
    @objc func btnPlusNew(_ sender: UIButton) {
        if objMenu?.itemPriceDetails?.count ?? 0 == 1 && objMenu?.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objMenu!)
            SetUpCount()
            tableViewSelectSize.reloadData()
            return
        }
        if objMenu?.itemPriceDetails?.count ?? 0 <= 0 && objMenu?.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objMenu!)
            SetUpCount()
            tableViewSelectSize.reloadData()
            return
        }
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "RepeatLastVC") as! RepeatLastVC
        vc.objMenu = objMenu
        vc.delegateRepeatNewDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
        
        //        PlusItem(objData)
        //        tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
    }
        
    @objc func btnMinusNew(_ sender: UIButton) {
        MinusItem(objMenu!)
        tableViewSelectSize.reloadData()
    }
    
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension FoodDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if objMenu?.itemVideoUrl?.count ?? 0 > 0 && objMenu?.itemImageUrl?.count ?? 0 > 0 {
            return 2
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellEventImages = collectionView.dequeueReusableCell(withReuseIdentifier: "CellEventImages", for: indexPath) as! CellEventImages
        if objMenu?.itemVideoUrl?.count ?? 0 > 0 && indexPath.row == 0 {
            cell.btnPlay.isHidden = false
            cell.btnPlay.addTarget(self, action: #selector(btnPlay(_:)), for: .touchUpInside)
            let sVideoURL = objMenu?.itemVideoUrl
            cell.imgEvent.image = getThumbnailImage(forUrl: URL(string: sVideoURL?.RemoveSpace() ?? "")!)
        } else {
            cell.btnPlay.isHidden = true
            cell.imgEvent.sd_setImage(with: URL(string: objMenu?.itemImageUrl?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sText = "\(objMenu?.itemName ?? "")\n\(objMenu?.itemDescription ?? "")"
        let url = URL(string: objMenu?.itemImageUrl?.RemoveSpace() ?? "")
        let sVideoURL = objMenu?.itemVideoUrl
         if objMenu?.itemVideoUrl?.count ?? 0 > 0 && objMenu?.itemImageUrl?.count ?? 0 > 0 {
            let images = [
                LightboxImage( image: getThumbnailImage(forUrl: URL(string: sVideoURL?.RemoveSpace() ?? "")!) ?? UIImage(), text: sText, videoURL: URL(string: sVideoURL?.RemoveSpace() ?? "")!),
                LightboxImage(imageURL: url!, text: sText)
            ]
            let controller = LightboxController(images: images)
            controller.dynamicBackground = true
            controller.modalPresentationStyle = .fullScreen
            let nav = UINavigationController.init(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            nav.navigationBar.isHidden = true
            present(nav, animated: true, completion: nil)
         } else {
            if objMenu?.itemVideoUrl?.count ?? 0 > 0 && indexPath.row == 0 {
                let images = [ LightboxImage( image: getThumbnailImage(forUrl: URL(string: sVideoURL?.RemoveSpace() ?? "")!) ?? UIImage(), text: sText, videoURL: URL(string: sVideoURL?.RemoveSpace() ?? "")!) ]
                let controller = LightboxController(images: images)
                controller.dynamicBackground = true
                controller.modalPresentationStyle = .fullScreen
                let nav = UINavigationController.init(rootViewController: controller)
                nav.modalPresentationStyle = .fullScreen
                nav.navigationBar.isHidden = true
                present(nav, animated: true, completion: nil)
            } else {
                if objMenu?.itemImageUrl?.count ?? 0 > 0 {
                    let images = [ LightboxImage(imageURL: url!, text: sText) ]
                    let controller = LightboxController(images: images)
                    controller.dynamicBackground = true
                    let nav = UINavigationController.init(rootViewController: controller)
                    nav.modalPresentationStyle = .fullScreen
                    nav.navigationBar.isHidden = true
                    present(nav, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    @objc func btnPlay(_ sender: UIButton) {
        let sVideoURL = objMenu?.itemVideoUrl?.RemoveSpace() ?? ""
        let url = URL(string: sVideoURL)!
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
}


//MARK: ScrollView Delegate
extension FoodDetailsVC {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        pagecontrol.set(progress: Int(offSet + horizontalCenter) / Int(width), animated: true)
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension FoodDetailsVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}

extension FoodDetailsVC: delegateAddTopingsDetails {
    func ReloadData(_ arrSelectedSize: NSMutableArray, _ arrSelectedToppings: NSMutableArray, _ objTMenu: TMenu?, _ isRepeat: Bool) {
        var arrTmpItemPriceDetails: [ItemPriceDetails] = [ItemPriceDetails]()
        for i in 0..<arrSelectedSize.count {
            let dictSize = arrSelectedSize[i] as! NSDictionary
            let model = ItemPriceDetails.init(object: dictSize)
            arrTmpItemPriceDetails.append(model)
        }
        
        var arrTmpAddonItems: [AddonItems] = [AddonItems]()
        for i in 0..<arrSelectedToppings.count {
            let dictSize = arrSelectedToppings[i] as! NSDictionary
            let model = AddonItems.init(object: dictSize)
            arrTmpAddonItems.append(model)
        }
        
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let obj: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            obj.isLastAdded = "0"
        }
        
        //        let objOrginal: TMenu = APP_DELEGATE.arrCartItems.lastObject as! TMenu
        objTMenu?.selectedAddonItems = arrTmpAddonItems
        objTMenu?.isLastAdded = "1"
        objTMenu?.itemCount = "1"
        objTMenu?.uniqID = getUniqID()
        objTMenu?.selectedItemPriceDetails = arrTmpItemPriceDetails
        APP_DELEGATE.arrCartItems.add(objTMenu!)
        
        tableViewSelectSize.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
}

extension FoodDetailsVC: delegateRepeatNewDetails {
    func openNewAddOneViaRepeat(objMenu: TMenu) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objMenu
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func reloadData(_ objMenu: TMenu?) {
        let objLastAdded = GetLastAddedAddonsForItem(objMenu)
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objTMenu: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            if objTMenu.itemId == objLastAdded?.itemId && objTMenu.uniqID == objLastAdded?.uniqID {
                let count = (Int(objTMenu.itemCount ?? "0") ?? 0) + 1
                objTMenu.itemCount = "\(count)"
            }
        }

        tableViewSelectSize.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
    
    
}
