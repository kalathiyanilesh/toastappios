//
//  RepeatLastVC.swift
//  Toast
//
//  Created by Nikul on 13/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateRepeatNewDetails {
    func openNewAddOneViaRepeat(objMenu: TMenu)
    func reloadData(_ objMenu: TMenu?)
}


class RepeatLastVC: UIViewController {

    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var imageVeg: UIImageView!
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var buttonAddNew: UIButton!
    @IBOutlet weak var buttonRepeat: UIButton!
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet var lblAddedTopings: UILabel!
    
    //MARK:- Variables
    var objMenu: TMenu?
    var delegateRepeatNewDetails: delegateRepeatNewDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        
        AddBlurEffect(viewBlur)
        
        labelItemName.text = objMenu?.itemName
        
        if objMenu?.itemType == "nonveg" {
            imageVeg.image = UIImage(named: "ic_nonveg")
        } else if objMenu?.itemType == "veg" && objMenu?.containsEgg == "1" {
            imageVeg.image = UIImage(named: "eggitarian")
        } else {
            imageVeg.image = UIImage(named: "ic_veg")
        }
        
        lblAddedTopings.text = ""
        let objLastAdded = GetLastAddedAddonsForItem(objMenu)
        guard let arrSelectedAddons = objLastAdded?.selectedAddonItems else { return }
        for objAdd in arrSelectedAddons {
            if lblAddedTopings.text?.count ?? 0 <= 0 {
                lblAddedTopings.text = objAdd.addonName?.capitalized
            } else {
                lblAddedTopings.text = "\(lblAddedTopings.text?.capitalized ?? ""), \(objAdd.addonName?.capitalized ?? "")"
            }
        }
    }
    
    @IBAction func buttonCloseTouchUpInside(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func buttonAddNewTouchUpInside(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.delegateRepeatNewDetails?.openNewAddOneViaRepeat(objMenu: self.objMenu!)
        }
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func buttonRepeatTouchUpInside(_ sender: Any) {
        //PlusItem(objMenu!) //today
        delegateRepeatNewDetails?.reloadData(objMenu)
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
}
