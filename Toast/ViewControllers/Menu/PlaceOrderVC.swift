//
//  PlaceOrderVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellYourOrder: UITableViewCell {
    @IBOutlet var imgVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet var lblAddons: UILabel!
}

class CellYourPastOrder: UITableViewCell {
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblOrderBy: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblQty: UILabel!
}

protocol delegateCollapsFloat {
    func ChangeViewMode()
}

class PlaceOrderVC: UIViewController {

    @IBOutlet var lblNoItemsOrder: UILabel!
    @IBOutlet var heightYourOrder: NSLayoutConstraint!
    @IBOutlet var heightPastOrder: NSLayoutConstraint!
    @IBOutlet var tblYourOrder: UITableView!
    @IBOutlet var tblPastOrder: UITableView!
    @IBOutlet var scrMain: UIScrollView!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var lblItemsCount: UILabel!
    @IBOutlet var lblTotalOrderItem: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var yPastOrderTitle: NSLayoutConstraint!
    @IBOutlet var viewCheckOut: UIView!
    @IBOutlet var viewPlaceOrder: UIView!
    @IBOutlet var btnHaveComplaints: UIButton!
    @IBOutlet var lblPastOrders: UILabel!
    @IBOutlet var viewPastOrder: UIView!
    @IBOutlet weak var txtCustomizeItem: UITextField!
    
    var TotalAmount: CGFloat = 0.0
    var delegateCollapsFloat: delegateCollapsFloat?
    var HeightYourOrderCell: CGFloat = 30
    var HeightPastOrderCell: CGFloat = 44
    var PastOrderAmount: CGFloat = 0.0
    var arrPastOrders = NSMutableArray()
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromTakeAway || isFromDelivery {
            btnHaveComplaints.isHidden = true
        }
        
        tblYourOrder.isScrollEnabled = false
        tblPastOrder.isScrollEnabled = false
        heightPastOrder.constant = (CGFloat(arrPastOrders.count)*HeightPastOrderCell)+12
        NotificationCenter.default.addObserver(self, selector: #selector(hideShowGradientView), name: Notification.Name("hideshowgradientview"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateItemOrder), name: Notification.Name("updateitemorder"), object: nil)
        UpdateItemsDetails()
        if !isFromTakeAway && !isFromDelivery {
            apiGetPastOrder()
        } else {
            viewPastOrder.alpha = 0
            heightPastOrder.constant = 0
        }
        
    }
    
    @IBAction func btnHaveAnyComplaints(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "ComplaintsVC") as! ComplaintsVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.arrItems = arrPastOrders
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @objc func UpdateItemOrder(_ noti: Notification)  {
        UpdateItemsDetails()
    }
    
    @objc func hideShowGradientView(_ noti: Notification)  {
        let alphaObj = noti.object as? CGFloat ?? 0
        UIView.animate(withDuration: 0.25) {
            self.viewGradient.alpha = alphaObj
        }
        if alphaObj == 1 {
            scrMain.contentOffset = CGPoint.zero
        } 
    }
    
    func UpdateItemsDetails() {
        SetUpViews()
        var totalItem = 0
        TotalAmount = 0
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objData: TMenu = APP_DELEGATE.arrCartItems[i] as! TMenu
            totalItem = totalItem + Int(objData.itemCount ?? "0")!
            var itemPrice = "0"
            if objData.itemPriceDetails?.count ?? 0 > 0 {
                let model = objData.itemPriceDetails?.first!
                itemPrice = model?.itemPrice ?? "0"
                if objData.selectedItemPriceDetails?.count ?? 0 > 0 {
                    itemPrice = "\(GetSizeTotalPrice(objData))"
                }
            }
            let count: CGFloat = CGFloat(((objData.itemCount ?? "0") as NSString).floatValue)
            let AddOnsPrices = GetAddOnsTotal(objData) * count
            TotalAmount = TotalAmount + CGFloat(Int(objData.itemCount ?? "0")!) * CGFloat((itemPrice as NSString).floatValue) + AddOnsPrices
        }
        lblItemsCount.text = "\(totalItem) ITEMS"
        
        self.lblTotalAmount.text = "₹\(PastOrderAmount+TotalAmount)"
        
        let arrQty = self.arrPastOrders.value(forKeyPath: "qty") as! NSArray
        var sum = 0
        for i in 0..<arrQty.count {
            var sQty = arrQty.object(at: i) as? String ?? "0"
            if sQty.count <= 0 {
                sQty = "0"
            }
            sum = sum + Int(sQty)!
        }
        self.lblTotalOrderItem.text = "\(sum+totalItem) Items"
        UserDefaults.standard.setValue("\(sum+totalItem)", forKey: "totalitems")
        UserDefaults.standard.setValue("\(PastOrderAmount+TotalAmount)", forKey: "totalamount")
        
        tblYourOrder.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func SetUpViews() {
        viewCheckOut.alpha = 0
        viewPlaceOrder.alpha = 0
        if APP_DELEGATE.arrCartItems.count > 0 {
            var totalHeight: CGFloat = 0
            for i in 0..<APP_DELEGATE.arrCartItems.count {
                let objTMenu: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
                let arrSelectedAddons = objTMenu.selectedAddonItems
                if arrSelectedAddons?.count ?? 0 > 0 {
                    totalHeight = totalHeight + 44
                } else {
                    totalHeight = totalHeight + HeightYourOrderCell
                }
            }
            heightYourOrder.constant = totalHeight+30+12
            //yPastOrderTitle.constant = 14
            lblNoItemsOrder.alpha = 0
            viewPlaceOrder.alpha = 1
        } else {
            //yPastOrderTitle.constant = 0
            lblNoItemsOrder.alpha = 0//1
            heightYourOrder.constant = 0
            viewCheckOut.alpha = 1
        }
    }
    
    @IBAction func btnCollapseView(_ sender: Any) {
        self.delegateCollapsFloat?.ChangeViewMode()
    }
    
    @IBAction func btnPlaceOrder(_ sender: Any) {
        if isFromTakeAway {
            apiAddPlacePickupOrder()
        } else if isFromDelivery {
            apiAddDeliveryOrder()
        } else {
            apiPlaceOrder()
        }
    }
    
    @IBAction func btnCheckOut(_ sender: Any) {
        self.RemoveOrder()
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "BillingDetailsVC") as! BillingDetailsVC
        vc.isFromPlaceOrder = true
        vc.isFromTakeAway = self.isFromTakeAway
        vc.isFromDelivery = self.isFromDelivery
        let navVC = UINavigationController(rootViewController:vc)
        navVC.isNavigationBarHidden = true
        navVC.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(navVC, animated: false, completion: nil)
        }, completion: nil)
    }
}

//MARK:- UITableViewDelegate METHOD
extension PlaceOrderVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if APP_DELEGATE.arrCartItems.count <= 0 && arrPastOrders.count <= 0 {
            viewCheckOut.alpha = 0
            viewPlaceOrder.alpha = 0
            btnHaveComplaints.alpha = 0
        } else {
            btnHaveComplaints.alpha = 1
            SetUpViews()
        }
        if arrPastOrders.count > 0 {
            lblPastOrders.alpha = 1
        } else {
            lblPastOrders.alpha = 0
        }
        switch tableView {
        case tblYourOrder:
            return APP_DELEGATE.arrCartItems.count
        default:
            return arrPastOrders.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tblYourOrder:
            let cell: CellYourOrder = tableView.dequeueReusableCell(withIdentifier: "CellYourOrder", for: indexPath) as! CellYourOrder
            let objData: TMenu = APP_DELEGATE.arrCartItems.object(at: indexPath.row) as! TMenu
            cell.lblItemName.text = objData.itemName
            if objData.itemPriceDetails?.count ?? 0 > 0 {
                let model = objData.itemPriceDetails?.first!
                let AddOnsPrices = GetAddOnsTotal(objData)
                var ItemPrice = CGFloat(((model?.itemPrice ?? "0") as NSString).floatValue)
                if objData.selectedItemPriceDetails?.count ?? 0 > 0 {
                    ItemPrice = GetSizeTotalPrice(objData)
                }
                cell.lblPrice.text = "₹\(AddOnsPrices + ItemPrice)"
            } else {
                cell.lblPrice.text = "₹0"
            }
            if objData.itemType == "nonveg" {
                cell.imgVeg.image = UIImage(named: "ic_nonveg")
            } else if objData.itemType == "veg" && (objData.containsEgg == "true" || objData.containsEgg == "1") {
                cell.imgVeg.image = UIImage(named: "eggitarian")
            } else {
                cell.imgVeg.image = UIImage(named: "ic_veg")
            }
            
            cell.lblTotalItem.text = GetItemCountWithUniqID(objData)
            cell.btnPlus.addTarget(self, action: #selector(btnPlus(_:)), for: .touchUpInside)
            cell.btnMinus.addTarget(self, action: #selector(btnMinus(_:)), for: .touchUpInside)
            cell.lblAddons.text = ""
            guard let arrSelectedAddons = objData.selectedAddonItems else { return cell }
            for objAdd in arrSelectedAddons {
                if cell.lblAddons.text?.count ?? 0 <= 0 {
                    cell.lblAddons.text = objAdd.addonName?.capitalized
                } else {
                    cell.lblAddons.text = "\(cell.lblAddons.text?.capitalized ?? ""), \(objAdd.addonName?.capitalized ?? "")"
                }
            }
            return cell
        default:
            let cell: CellYourPastOrder = tableView.dequeueReusableCell(withIdentifier: "CellYourPastOrder", for: indexPath) as! CellYourPastOrder
            let dictOrder = arrPastOrders[indexPath.row] as! NSDictionary
            let sItemtype = dictOrder.object(forKey: "item_type") as! String
            let sContainsEgg = dictOrder.object(forKey: "contains_egg") as! String
            if sItemtype == "non-veg" {
                cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
            } else if sItemtype == "veg" && (sContainsEgg == "true" || sContainsEgg == "1") {
                cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
            } else {
                cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
            }
            cell.lblItemName.text = dictOrder.object(forKey: "item_name") as? String
            cell.lblPrice.text = "₹\(dictOrder.object(forKey: "item_total_price") as! String)"
            cell.lblOrderBy.text = dictOrder.object(forKey: "orderby") as? String
            cell.lblQty.text = dictOrder.object(forKey: "qty") as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case tblYourOrder:
            let objData: TMenu = APP_DELEGATE.arrCartItems.object(at: indexPath.row) as! TMenu
            let arrSelectedAddons = objData.selectedAddonItems
            return arrSelectedAddons?.count ?? 0 > 0 ? 44 : HeightYourOrderCell
        default:
            return HeightPastOrderCell
        }
    }
    
    @objc func btnPlus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblYourOrder)
        let indexPath = tblYourOrder.indexPathForRow(at: buttonPosition)
        let objData: TMenu =  APP_DELEGATE.arrCartItems.object(at: indexPath!.row) as! TMenu
        let count = (Int(objData.itemCount ?? "0") ?? 0) + 1
        objData.itemCount = "\(count)"
        //PlusItem(objData)
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
        UIView.performWithoutAnimation {
            tblYourOrder.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
        }
        
        if APP_DELEGATE.strCurrentMenu == "special" {
            NotificationCenter.default.post(name: Notification.Name(k_RELOAD_SPECIAL_FOOD), object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name(k_RELOAD_FOOD), object: nil)
        }
    }
    
    @objc func btnMinus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblYourOrder)
        let indexPath = tblYourOrder.indexPathForRow(at: buttonPosition)
        let objData: TMenu =  APP_DELEGATE.arrCartItems.object(at: indexPath!.row) as! TMenu
        let count = (Int(objData.itemCount ?? "0") ?? 0) - 1
        objData.itemCount = "\(count)"
        if count <= 0 {
            APP_DELEGATE.arrCartItems.removeObject(at: indexPath!.row)
        }
        //MinusItem(objData)
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
        UIView.performWithoutAnimation {
            if indexPath == nil {
                tblYourOrder.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
            } else {
                tblYourOrder.reloadData()
            }
        }
        
        if APP_DELEGATE.strCurrentMenu == "special" {
            NotificationCenter.default.post(name: Notification.Name(k_RELOAD_SPECIAL_FOOD), object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name(k_RELOAD_FOOD), object: nil)
        }
    }
}

//MARK:- API CALLING
extension PlaceOrderVC {
    func apiGetPastOrder() {
        self.view.endEditing(true)
        self.arrPastOrders = NSMutableArray()
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/customer/\(CUSTOMER_ID)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIGetPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictOrder = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let dictOrderDetails = dictOrder.object(forKey: "order_details") as? NSDictionary {
                        self.PastOrderAmount = CGFloat((dictOrderDetails.object(forKey: "order_amount") as! NSString).floatValue)
                        if let arrOrderDetailsMap = dictOrder.object(forKey: "order_details_id_name_map") as? NSArray {
                            for i in 0..<arrOrderDetailsMap.count {
                                let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                                let sK = dictOrd.object(forKey: "id") as! String
                                let arrTmp = dictOrderDetails.object(forKey: sK) as! NSArray
                                for k in 0..<arrTmp.count {
                                    let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                    if sK == CUSTOMER_ID {
                                        dictTmp.setObject("Ordered by You", forKey: "orderby" as NSCopying)
                                    } else {
                                        dictTmp.setObject("Ordered by \(dictOrd.object(forKey: "id_name") as! String)", forKey: "orderby" as NSCopying)
                                    }
                                    self.arrPastOrders.add(dictTmp.copy() as! NSDictionary)
                                }
                            }
                        }
                    }
                    
                    self.heightPastOrder.constant = 0
                    self.viewPastOrder.alpha = 0
                    if self.arrPastOrders.count > 0 {
                        self.viewPastOrder.alpha = 1
                        self.heightPastOrder.constant = (CGFloat(self.arrPastOrders.count)*self.HeightPastOrderCell)+12
                    }
                    self.UpdateItemsDetails()
                    self.tblPastOrder.reloadData()
                }
            }
        }
    }
    
    func apiPlaceOrder() {
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/" + APIPlaceOrder
        let arrOrderDetails = NSMutableArray()
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objData: TMenu =  APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            var sSelectedSize = ""
            if objData.itemPriceDetails?.count ?? 0 > 0 {
                let model = objData.itemPriceDetails?[0]
                sSelectedSize = (model?.itemSize ?? "").capitalized
            }
            if objData.selectedItemPriceDetails?.count ?? 0 > 0 {
                let model = objData.selectedItemPriceDetails?[0]
                sSelectedSize = model?.itemSize ?? ""
            }
            let arrSelectedAddOns = NSMutableArray()
            for objAddOns in objData.selectedAddonItems ?? [AddonItems]() {
                var sSize = ""
                if objAddOns.addonPrice?.count ?? 0 > 0 {
                    let modelAddon = objAddOns.addonPrice?.first!
                    sSize = modelAddon?.name ?? ""
                }
                let dictAddOns: NSDictionary = ["addon_id":objAddOns.addonId ?? "0", "addon_size":sSize]
                arrSelectedAddOns.add(dictAddOns)
            }
            
            let dictItem: NSDictionary = ["item_id":objData.itemId!, "qty":Int(GetItemCountWithUniqID(objData))!, "item_size":sSelectedSize, "addons":arrSelectedAddOns]
            arrOrderDetails.add(dictItem)
        }
        let dictParam: NSDictionary = ["qr_code":APP_DELEGATE.strScannedQR, "order_details":arrOrderDetails, "customize_item":TRIM(string: txtCustomizeItem.text ?? "")]
        print(dictParam)
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIPlaceOrder,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            self.RemoveOrder()
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "navorderplace")
                            vc.modalPresentationStyle = .overCurrentContext
                            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                self.present(vc, animated: false, completion: nil)
                            }, completion: nil)
                        } else {
                            showMessage("Fail to place your order!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!")
            }
        }
    }

    func apiAddPlacePickupOrder() {
        let service = SERVER_URL + APIAddTA
        let arrOrderDetails = NSMutableArray()
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objData: TMenu =  APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            var sSelectedSize = ""
            if objData.itemPriceDetails?.count ?? 0 > 0 {
                let model = objData.itemPriceDetails?[0]
                sSelectedSize = (model?.itemSize ?? "").capitalized
            }
            if objData.selectedItemPriceDetails?.count ?? 0 > 0 {
                let model = objData.selectedItemPriceDetails?[0]
                sSelectedSize = model?.itemSize ?? ""
            }
            let arrSelectedAddOns = NSMutableArray()
            for objAddOns in objData.selectedAddonItems ?? [AddonItems]() {
                var sSize = ""
                if objAddOns.addonPrice?.count ?? 0 > 0 {
                    let modelAddon = objAddOns.addonPrice?.first!
                    sSize = modelAddon?.name ?? ""
                }
                let dictAddOns: NSDictionary = ["addon_id":objAddOns.addonId ?? "0", "addon_size":sSize]
                arrSelectedAddOns.add(dictAddOns)
            }
            
            let dictItem: NSDictionary = ["item_id":objData.itemId!, "qty":Int(GetItemCountWithUniqID(objData))!, "item_size":sSelectedSize, "addons":arrSelectedAddOns]
            //let dictItem: NSDictionary = ["item_id":objData.itemId!, "qty":Int(objData.itemCount ?? "0")!, "item_size":sSelectedSize, "addons":arrSelectedAddOns]
            arrOrderDetails.add(dictItem)
        }
        var dictParam: NSDictionary = NSDictionary()
        if APP_DELEGATE.strScannedQR.count > 0 {
            dictParam = ["qr_code":APP_DELEGATE.strScannedQR, "order_details":arrOrderDetails, "restraunt_id": "", "customize_item":TRIM(string: txtCustomizeItem.text ?? "")]
        } else {
            dictParam = ["qr_code":"", "order_details":arrOrderDetails, "restraunt_id": APP_DELEGATE.restaurantID, "customize_item":TRIM(string: txtCustomizeItem.text ?? "")]
        }
        print(dictParam)
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIAddTA,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            self.btnCheckOut(UIButton())
                            /*
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "navorderplace")
                            vc.modalPresentationStyle = .overCurrentContext
                            APP_DELEGATE.isFromTakeAway = self.isFromTakeAway
                            APP_DELEGATE.isFromDelivery = self.isFromDelivery
                            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                self.present(vc, animated: false, completion: nil)
                            }, completion: nil)*/
                        } else {
                            showMessage("Fail to place your order!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!")
            }
        }
    }
    
    func apiAddDeliveryOrder() {
        let service = SERVER_URL + APIMenuAddDelivery
        let arrOrderDetails = NSMutableArray()
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objData: TMenu =  APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            var sSelectedSize = ""
            if objData.itemPriceDetails?.count ?? 0 > 0 {
                let model = objData.itemPriceDetails?[0]
                sSelectedSize = (model?.itemSize ?? "").capitalized
            }
            if objData.selectedItemPriceDetails?.count ?? 0 > 0 {
                let model = objData.selectedItemPriceDetails?[0]
                sSelectedSize = model?.itemSize ?? ""
            }
            let arrSelectedAddOns = NSMutableArray()
            for objAddOns in objData.selectedAddonItems ?? [AddonItems]() {
                var sSize = ""
                if objAddOns.addonPrice?.count ?? 0 > 0 {
                    let modelAddon = objAddOns.addonPrice?.first!
                    sSize = modelAddon?.name ?? ""
                }
                let dictAddOns: NSDictionary = ["addon_id":objAddOns.addonId ?? "0", "addon_size":sSize]
                arrSelectedAddOns.add(dictAddOns)
            }
            
            let dictItem: NSDictionary = ["item_id":objData.itemId!, "qty":Int(GetItemCountWithUniqID(objData))!, "item_size":sSelectedSize, "addons":arrSelectedAddOns]
            //let dictItem: NSDictionary = ["item_id":objData.itemId!, "qty":Int(objData.itemCount ?? "0")!, "item_size":sSelectedSize, "addons":arrSelectedAddOns]
            arrOrderDetails.add(dictItem)
        }
        let dictParam: NSDictionary = ["restraunt_id": APP_DELEGATE.restaurantID, "order_details":arrOrderDetails, "customize_item":TRIM(string: txtCustomizeItem.text ?? "")]
        print(dictParam)
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIMenuAddDelivery,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus.lowercased() == "success" {
                            self.btnCheckOut(UIButton())
                            /*
                            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "navorderplace")
                            vc.modalPresentationStyle = .overCurrentContext
                            APP_DELEGATE.isFromTakeAway = self.isFromTakeAway
                            APP_DELEGATE.isFromDelivery = self.isFromDelivery
                            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                                self.present(vc, animated: false, completion: nil)
                            }, completion: nil)*/
                        } else {
                            showMessage("Fail to place your order!")
                        }
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!")
            }
        }
    }
    
    func RemoveOrder() {
        if !isFromDelivery && !isFromTakeAway {
            APP_DELEGATE.arrCartItems = NSMutableArray()
        }
        RemoveAllTempData()
        
        /*
        self.tblYourOrder.reloadData()
        self.tblPastOrder.reloadData()
        NotificationCenter.default.post(name: Notification.Name(k_RELOAD_SPECIAL_FOOD), object: nil)
        NotificationCenter.default.post(name: Notification.Name(k_RELOAD_FOOD), object: nil)*/
    }
}

