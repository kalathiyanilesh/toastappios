//
//  MenuSettingsVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class  CellMenuSettings: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnSwitch: BigSwitch!
    @IBOutlet var lblCount: UILabel!
}

class MenuSettingsVC: UIViewController {

    @IBOutlet var tblMenuSettings: UITableView!
    @IBOutlet var bottomView: NSLayoutConstraint!
    @IBOutlet var viewSettings: UIView!
    @IBOutlet var totalHeight: NSLayoutConstraint!
    
    var selectedIndex = 0
    var arrSectionOne = ["Veg Only", "Contains Egg"]
    var arrTypes = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuSettings.isScrollEnabled = false
        bottomView.constant = -5000
        SetUpData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdateArray), name: Notification.Name(k_UPDATE_MENU_SETTING), object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        bottomView.constant = 8
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @objc func UpdateArray() {
        if APP_DELEGATE.isPostMenuSetting {
            APP_DELEGATE.isPostMenuSetting = false
            SetUpData()
        }
    }
    
    func SetUpData() {
        print(APP_DELEGATE.dictMenuSection)
        arrTypes = NSArray()
        arrTypes = (APP_DELEGATE.dictMenuSection.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
        if arrTypes.count > 0 {
            if arrTypes.count > 7 {
                totalHeight.constant = CGFloat(110 + 20 + (7.0 * 30))
            } else {
                totalHeight.constant = CGFloat(110 + 20 + (arrTypes.count * 30))
            }
        } else {
            totalHeight.constant = 130.0
        }
        UIView.animate(withDuration: 0.35) {
            self.view.layoutIfNeeded()
        }
        tblMenuSettings.reloadData()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        bottomView.constant = -5000
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            }, completion: nil)
        }
        
    }
}

extension MenuSettingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 1:
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: viewSettings.frame.size.width, height: 20))
            headerView.backgroundColor = viewSettings.backgroundColor
            let label = UILabel()
            label.frame = CGRect.init(x: 19.5, y: 3, width: headerView.frame.width-39, height: 1)
            label.backgroundColor = Color_Hex(hex: "#656565").withAlphaComponent(0.40)
            if arrTypes.count > 0 {
                headerView.addSubview(label)
            }
            return headerView
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 20
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return arrSectionOne.count
        default:
            tblMenuSettings.isScrollEnabled = false
            if arrTypes.count > 7 {
                tblMenuSettings.isScrollEnabled = true
            }
            return arrTypes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellMenuSettings = tableView.dequeueReusableCell(withIdentifier: "CellMenuSettings", for: indexPath) as! CellMenuSettings
        cell.lblName.font = FontWithSize("Montserrat-Regular", 13)
        cell.lblName.textColor = UIColor.white
        switch indexPath.section {
        case 0:
            cell.btnSwitch.alpha = 1
            cell.lblCount.alpha = 0
            cell.lblName.text = arrSectionOne[indexPath.row]
            cell.btnSwitch.isEnabled = true
            switch indexPath.row {
            case 0:
                cell.btnSwitch.isOn = APP_DELEGATE.isFoodVeg
            default:
                cell.btnSwitch.isOn = APP_DELEGATE.isContainsEgg
                cell.btnSwitch.isEnabled = APP_DELEGATE.isFoodVeg
            }
            if cell.btnSwitch.isOn {
                cell.lblName.textColor = Color_Hex(hex: "#3CBCB4")
            }
            cell.btnSwitch.tag = indexPath.row+555
            cell.btnSwitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        default:
            cell.lblCount.alpha = 1
            cell.btnSwitch.alpha = 0
            let sKey = arrTypes[indexPath.row] as? String
            cell.lblName.text = sKey?.uppercased()
            cell.lblCount.text = APP_DELEGATE.dictMenuSection.object(forKey: sKey ?? "") as? String ?? "0"
            if selectedIndex == indexPath.row {
                cell.lblName.textColor = Color_Hex(hex: "#3CBCB4")
                cell.lblName.font = FontWithSize("Montserrat-SemiBold", 13)
            }
        
            cell.lblCount.textColor = cell.lblName.textColor
            cell.lblCount.font = cell.lblName.font
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            selectedIndex = indexPath.row
            tblMenuSettings.reloadData()
            runAfterTime(time: 0.5) {
                var section = indexPath.row - 1
                if section < 0 { section = 0 }
                //APP_DELEGATE.isPostNotification = true
                NotificationCenter.default.post(name: Notification.Name(k_GO_SELECTED_FOOD_SECTION), object: section)
                self.btnClose(UIButton())
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        if sender.tag-555 == 0 {
            APP_DELEGATE.isFoodVeg = sender.isOn
            //if APP_DELEGATE.isFoodVeg {
                APP_DELEGATE.isContainsEgg = false
           // }
        } else {
            APP_DELEGATE.isContainsEgg = sender.isOn
        }
        tblMenuSettings.reloadData()
        runAfterTime(time: 0.1) {
            //APP_DELEGATE.isPostNotification = true
            if APP_DELEGATE.strCurrentMenu == "special" {
                NotificationCenter.default.post(name: Notification.Name(k_FILTER_SPECIAL_FOOD), object: nil)
            } else {
                NotificationCenter.default.post(name: Notification.Name(k_FILTER_FOOD), object: nil)
            }
        }
    }
}
