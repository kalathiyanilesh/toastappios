//
//  FoodMenuVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateFood {
    func CurrentFoodType(_ sType: String)
}

class FoodMenuVC: UIViewController {

    @IBOutlet var tblFoodMenu: UITableView!
        
    var sType = ""
    var arrAllKeys = NSArray()
    var dictData = NSDictionary()
    var delegateFood: delegateFood?
    var isFromPriceLess = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblFoodMenu.register(UINib(nibName: "CellMenu", bundle: nil), forCellReuseIdentifier: "CellMenu")
        let vHeader = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 10))
        vHeader.backgroundColor = self.view.backgroundColor
        tblFoodMenu.tableHeaderView = vHeader
        let vFooter = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 60))
        vFooter.backgroundColor = self.view.backgroundColor
        tblFoodMenu.tableFooterView = vFooter
        if isFromPriceLess { tblFoodMenu.tableFooterView = nil }
        NotificationCenter.default.addObserver(self, selector: #selector(self.ScrollToSection(notification:)), name: Notification.Name(k_GO_SELECTED_FOOD_SECTION), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetFilterData), name: Notification.Name(k_FILTER_FOOD), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReloadFood), name: Notification.Name(k_RELOAD_FOOD), object: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        self.delegateFood?.CurrentFoodType(sType)
        if tblFoodMenu != nil {
            GetFilterData()
            tblFoodMenu.reloadData()
        }
    }
    
    @objc private func ScrollToSection(notification: Notification){
        let section = notification.object as! Int
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: section)
            self.tblFoodMenu.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    @objc func ReloadFood() {
        tblFoodMenu.reloadData()
    }
    
    @objc func GetFilterData() {
        arrAllKeys = NSArray()
        dictData = NSDictionary()
        
        print(APP_DELEGATE.strCurrentMenu)
        sType = APP_DELEGATE.strCurrentMenu
        
        var dictAllData = NSDictionary()
        if(UserDefaults.standard.object(forKey: sType) != nil) {
            dictAllData = UserDefaults.standard.object(forKey: sType) as! NSDictionary
        }
        
        if (!APP_DELEGATE.isFoodVeg && !APP_DELEGATE.isContainsEgg) || (!APP_DELEGATE.isFoodVeg && APP_DELEGATE.isContainsEgg) {
            dictData = dictAllData
        } else {
            let arrKeys = (dictAllData.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
            let dictTemp = NSMutableDictionary()
            
            for i in 0..<arrKeys.count {
                let sKey = arrKeys[i] as! String
                let arrData = dictAllData[sKey] as! NSArray
                let arrFilter = NSMutableArray()
                for k in 0..<arrData.count {
                    let objData = TMenu.init(object: arrData.object(at: k) as! NSDictionary)
                    if APP_DELEGATE.isFoodVeg && APP_DELEGATE.isContainsEgg {
                        if (objData.itemType == "veg" || objData.containsEgg == "true") && objData.itemType != "non-veg"{
                            arrFilter.add(objData.dictionaryRepresentation())
                        }
                    } else if APP_DELEGATE.isFoodVeg && objData.itemType == "veg" && objData.containsEgg != "true"  {
                        arrFilter.add(objData.dictionaryRepresentation())
                    }
                    //                        else if APP_DELEGATE.isContainsEgg && objData.containsEgg == "true"  {
                    //                            arrFilter.add(objData.dictionaryRepresentation())
                    //                        }
                    if arrFilter.count > 0 {
                        dictTemp.setObject(arrFilter, forKey: sKey as NSCopying)
                    }
                }
            }
            dictData = dictTemp.copy() as! NSDictionary
        }
        
        arrAllKeys = (dictData.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray

        tblFoodMenu.reloadData()
        GetSettingTitle()
        APP_DELEGATE.isPostMenuSetting = true
        NotificationCenter.default.post(name: Notification.Name(k_UPDATE_MENU_SETTING), object: nil)
    }
    
    func GetSettingTitle() {
        APP_DELEGATE.dictMenuSection = NSMutableDictionary()
        if arrAllKeys.count > 0 {
            var total = 0
            for i in 0..<arrAllKeys.count {
                let sKey = arrAllKeys[i] as! String
                let arrData = dictData[sKey] as! NSArray
                total += arrData.count
                APP_DELEGATE.dictMenuSection.setObject("\(arrData.count)", forKey: sKey as NSCopying)
            }
            if APP_DELEGATE.dictMenuSection.allKeys.count > 0 {
                APP_DELEGATE.dictMenuSection.setObject("\(total)", forKey: "All" as NSCopying)
            }
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension FoodMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAllKeys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 40))
        headerView.backgroundColor = self.view.backgroundColor!
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 15, width: headerView.frame.width-20, height: 0.5)
        label.backgroundColor = Color_Hex(hex: "#707070")
        
        let labelType = UILabel()
        labelType.text = (arrAllKeys[section] as! String).uppercased()
        labelType.textAlignment = .center
        labelType.font = FontWithSize("Montserrat-SemiBold", 12)
        labelType.textColor = Color_Hex(hex: "#404040")
        labelType.backgroundColor = self.view.backgroundColor!
        let width: CGFloat = labelType.intrinsicContentSize.width+20
        labelType.frame = CGRect.init(x: (headerView.frame.width-width)/2, y: 8, width: width, height: 15)
        
        headerView.addSubview(label)
        headerView.addSubview(labelType)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sKey = arrAllKeys[section] as! String
        let arrData = dictData[sKey] as! NSArray
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellMenu = tableView.dequeueReusableCell(withIdentifier: "CellMenu", for: indexPath) as! CellMenu
        let sKey = arrAllKeys[indexPath.section] as! String
        let arrData = dictData[sKey] as! NSArray
        let objData = TMenu.init(object: arrData.object(at: indexPath.row) as! NSDictionary)

        if objData.itemPriceDetails?.count ?? 0 > 0 {
            let model = objData.itemPriceDetails?.first!
            cell.lblPrice.text = "₹\(model?.itemPrice ?? "0")"
        } else {
            cell.lblPrice.text = "₹0"
        }
        cell.lblItemName.text = objData.itemName
        cell.imgMenu.sd_setImage(with: URL(string: objData.itemImageUrl?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        
        
        cell.lblDescription.text = objData.itemDescription
        cell.lblReviews.text = "\(objData.reviewsCount ?? "0") reviews"
        
        cell.lblDescription.isHidden = true
        cell.lblReviews.isHidden = true
        
        if objData.itemType == "nonveg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if objData.itemType == "veg" && objData.containsEgg == "1" {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        
        cell.viewPlusMinus.isHidden = true
        cell.btnAdd.isHidden = true
        cell.imageHappy.isHidden = true
        
        if isFromPriceLess {
            cell.lblPrice.isHidden = true
            cell.viewPlusMinus.isHidden = true
            cell.btnAdd.isHidden = true
            cell.lblReviews.isHidden = true
            cell.imageHappy.isHidden = true
        } else {
//            cell.lblPrice.isHidden = false
//            cell.viewPlusMinus.isHidden = false
//            cell.btnAdd.isHidden = false
//            cell.lblReviews.isHidden = true
            cell.lblPrice.isHidden = false
            let strCount = GetItemCount(objData.itemId!)
            if (Int(strCount)! >= 1) {
                cell.viewPlusMinus.isHidden = false
                cell.lblTotalItem.text = strCount
                cell.btnPlus.addTarget(self, action: #selector(btnPlus(_:)), for: .touchUpInside)
                cell.btnMinus.addTarget(self, action: #selector(btnMinus(_:)), for: .touchUpInside)
            } else {
                cell.btnAdd.isHidden = false
                cell.btnAdd.addTarget(self, action: #selector(AddItem(_:)), for: .touchUpInside)
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if !isFromPriceLess {
            let sKey = arrAllKeys[indexPath.section] as! String
            let arrData = dictData[sKey] as! NSArray
            let objData = TMenu.init(object: arrData .object(at: indexPath.row) as! NSDictionary)
            
            let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "FoodDetailsVC") as! FoodDetailsVC
            vc.objMenu = objData
            vc.delegateFoodDetails = self
            vc.modalPresentationStyle = .overCurrentContext
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.present(vc, animated: false, completion: nil)
            }, completion: nil)
        //}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    
    @objc func AddItem(_ sender: UIButton) {
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblFoodMenu)
        let indexPath = tblFoodMenu.indexPathForRow(at: buttonPosition)
        let sKey = arrAllKeys[indexPath!.section] as! String
        let arrData = dictData[sKey] as! NSArray
        let objData = TMenu.init(object: arrData.object(at: indexPath!.row) as! NSDictionary)
        
        //let objAddOns = objData.addonDetails?[indexPath!.section]
        if objData.itemPriceDetails?.count ?? 0 == 1 && objData.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objData)
            tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
            return
        }
        if objData.itemPriceDetails?.count ?? 0 <= 0 && objData.addonDetails?.count ?? 0 <= 0 {
            AddNewItem(objData)
            tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
            return
        }
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objData
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
        
        /*let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblFoodMenu)
        let indexPath = tblFoodMenu.indexPathForRow(at: buttonPosition)
        let sKey = arrAllKeys[indexPath!.section] as! String
        let arrData = dictData[sKey] as! NSArray
        let objData = TMenu.init(object: arrData.object(at: indexPath!.row) as! NSDictionary)
        AddNewItem(objData)
        tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)*/
    }
    
    @objc func btnPlus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblFoodMenu)
        let indexPath = tblFoodMenu.indexPathForRow(at: buttonPosition)
        let sKey = arrAllKeys[indexPath!.section] as! String
        let arrData = dictData[sKey] as! NSArray
        let objData = TMenu.init(object: arrData.object(at: indexPath!.row) as! NSDictionary)
        //let objAddOns = objData.addonDetails?[indexPath!.section]
        if objData.itemPriceDetails?.count ?? 0 == 1 && objData.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objData)
            tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
            return
        }
        if objData.itemPriceDetails?.count ?? 0 <= 0 && objData.addonDetails?.count ?? 0 <= 0 {
            PlusItem(objData)
            tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
            return
        }

        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "RepeatLastVC") as! RepeatLastVC
        vc.objMenu = objData
        vc.delegateRepeatNewDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
        
//        PlusItem(objData)
//        tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
    }
    
    @objc func btnMinus(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tblFoodMenu)
        let indexPath = tblFoodMenu.indexPathForRow(at: buttonPosition)
        let sKey = arrAllKeys[indexPath!.section] as! String
        let arrData = (dictData[sKey] as! NSArray).mutableCopy() as! NSMutableArray
        let objData = TMenu.init(object: arrData.object(at: indexPath!.row) as! NSDictionary)
        MinusItem(objData)
        tblFoodMenu.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.none)
    }
}

extension FoodMenuVC: delegateFoodDetails {
    func ReloadData() {
        tblFoodMenu.reloadData()
    }
}

extension FoodMenuVC: delegateAddTopingsDetails {
    func ReloadData(_ arrSelectedSize: NSMutableArray, _ arrSelectedToppings: NSMutableArray, _ objTMenu: TMenu?, _ isRepeat: Bool) {
        var arrTmpItemPriceDetails: [ItemPriceDetails] = [ItemPriceDetails]()
        for i in 0..<arrSelectedSize.count {
            let dictSize = arrSelectedSize[i] as! NSDictionary
            let model = ItemPriceDetails.init(object: dictSize)
            arrTmpItemPriceDetails.append(model)
        }
        
        var arrTmpAddonItems: [AddonItems] = [AddonItems]()
        for i in 0..<arrSelectedToppings.count {
            let dictSize = arrSelectedToppings[i] as! NSDictionary
            let model = AddonItems.init(object: dictSize)
            arrTmpAddonItems.append(model)
        }
        
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let obj: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            obj.isLastAdded = "0"
        }
        
        objTMenu?.selectedAddonItems = arrTmpAddonItems
        objTMenu?.isLastAdded = "1"
        objTMenu?.itemCount = "1"
        objTMenu?.uniqID = getUniqID()
        objTMenu?.selectedItemPriceDetails = arrTmpItemPriceDetails
        APP_DELEGATE.arrCartItems.add(objTMenu!)

        tblFoodMenu.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
}

extension FoodMenuVC: delegateRepeatNewDetails {
    func openNewAddOneViaRepeat(objMenu: TMenu) {
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "AddOneVC") as! AddOneVC
        vc.objMenu = objMenu
        vc.delegateAddTopingsDetails = self
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func reloadData(_ objMenu: TMenu?) {
        let objLastAdded = GetLastAddedAddonsForItem(objMenu)
        for i in 0..<APP_DELEGATE.arrCartItems.count {
            let objTMenu: TMenu = APP_DELEGATE.arrCartItems.object(at: i) as! TMenu
            if objTMenu.itemId == objLastAdded?.itemId && objTMenu.uniqID == objLastAdded?.uniqID {
                let count = (Int(objTMenu.itemCount ?? "0") ?? 0) + 1
                objTMenu.itemCount = "\(count)"
            }
        }

        tblFoodMenu.reloadData()
        NotificationCenter.default.post(name: Notification.Name("updateitemorder"), object: nil)
    }
}
