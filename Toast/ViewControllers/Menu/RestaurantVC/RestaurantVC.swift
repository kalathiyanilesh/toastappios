//
//  RestaurantVC.swift
//  Toast
//
//  Created by Nikul on 05/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RestaurantVC: UIViewController {

    @IBOutlet weak var collecetionViewImages: UICollectionView!
    @IBOutlet weak var collecionViewMenus: UICollectionView!
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet var heightMainView: NSLayoutConstraint!
    
    var pageMenu : CAPSPageMenu?
    var arrayMenus = ["Special","Food Menu","Beverages","Desserts", "Drinks"]
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        
        self.collecetionViewImages.delegate = self
        self.collecetionViewImages.dataSource = self
        self.collecetionViewImages.register(UINib(nibName: "CollectionViewCellHotelImage", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellHotelImage")
        
        self.collecionViewMenus.delegate = self
        self.collecionViewMenus.dataSource = self
        self.collecionViewMenus.register(UINib(nibName: "CollectionViewCellMenuList", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellMenuList")
        
        SetPageMenu()
        
        if (UIScreen.main.nativeBounds.height > 2209) || UIScreen.main.nativeBounds.height == 1792 {
            heightMainView.constant = SCREENHEIGHT()-(373+44+34+44)
        } else {
            heightMainView.constant = SCREENHEIGHT()-351
        }
        self.view.layoutIfNeeded()
    }
    
    
    @IBAction func buttonBackTouchUpInside(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- SET PAGEMENU
extension RestaurantVC {
    func SetPageMenu() {
        pageMenu?.view.removeFromSuperview()
        pageMenu?.removeFromParent()
        
        var arrController : [UIViewController] = []
    
        var i: Int = 0
        for _ in arrayMenus {
            if i == 0 {
                let other = loadVC(strStoryboardId: SB_MENU, strVCId: "RestaurantSpecialMenuVC") as! RestaurantSpecialMenuVC
                other.isRedirect = true
                arrController.append(other)
            } else {
                let your = loadVC(strStoryboardId: SB_MENU, strVCId: "RestaurantFoodMenuVC") as! RestaurantFoodMenuVC
                your.isRedirect = true
                arrController.append(your)
            }
            i += 1
        }
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
}

extension RestaurantVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collecetionViewImages {
            return 1
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collecetionViewImages {
            return 4
        } else {
            return arrayMenus.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collecetionViewImages {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellHotelImage", for: indexPath) as! CollectionViewCellHotelImage
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellMenuList", for: indexPath) as! CollectionViewCellMenuList
            cell.labelTitle.text = arrayMenus[indexPath.row]
            if selectedIndex == indexPath.row {
                cell.labelTitle.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
            } else {
                cell.labelTitle.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 0.4)
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collecetionViewImages {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 128.0)
        } else {
            if indexPath.row == 0 {
                return CGSize(width: 70.0, height: 40.0)
            }
            return CGSize(width: 100.0, height: 40.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collecetionViewImages {
            return 0
        } else {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collecetionViewImages {
            return 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collecionViewMenus {
            selectedIndex = indexPath.row
            pageMenu?.moveToPage(selectedIndex)
            self.collecionViewMenus.reloadData()
        }
    }
    
}
