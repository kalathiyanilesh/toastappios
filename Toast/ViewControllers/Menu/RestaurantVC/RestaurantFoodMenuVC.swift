//
//  RestaurantFoodMenuVC.swift
//  Toast
//
//  Created by Nikul on 05/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RestaurantFoodMenuVC: UIViewController {

    @IBOutlet weak var tableViewMenuList: UITableView!
    
    var isShowAddCart: Bool = false
    var isRedirect: Bool = false
    
    var sType = ""
    var arrAllKeys = NSArray()
    var dictData = NSDictionary()
    var delegateFood: delegateFood?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.delegateFood?.CurrentFoodType(sType)
    }
    
    private func setupUI() {
        tableViewMenuList.delegate = self
        tableViewMenuList.dataSource = self
        tableViewMenuList.separatorStyle = .none
        tableViewMenuList.register(UINib(nibName: "TableViewCellFeedMenu", bundle: nil), forCellReuseIdentifier: "TableViewCellFeedMenu")
        tableViewMenuList.contentInset = UIEdgeInsets(top: 16.0, left: 0, bottom: 16.0, right: 0)
        
        let vHeader = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 10))
        vHeader.backgroundColor = self.view.backgroundColor
        tableViewMenuList.tableHeaderView = vHeader
        let vFooter = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 60))
        vFooter.backgroundColor = self.view.backgroundColor
        tableViewMenuList.tableFooterView = vFooter
        NotificationCenter.default.addObserver(self, selector: #selector(self.ScrollToSection(notification:)), name: Notification.Name(k_GO_SELECTED_FOOD_SECTION), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReloadFood), name: Notification.Name(k_RELOAD_FOOD), object: nil)
        
    }
    
    @objc private func ScrollToSection(notification: Notification){
        let section = notification.object as! Int
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: section)
            self.tableViewMenuList.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    @objc func ReloadFood() {
        tableViewMenuList.reloadData()
    }

}

extension RestaurantFoodMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAllKeys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: SCREENWIDTH(), height: 40))
        headerView.backgroundColor = self.view.backgroundColor!
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 15, width: headerView.frame.width-20, height: 0.5)
        label.backgroundColor = Color_Hex(hex: "#707070")
        
        let labelType = UILabel()
        labelType.text = (arrAllKeys[section] as! String).uppercased()
        labelType.textAlignment = .center
        labelType.font = FontWithSize("Montserrat-SemiBold", 12)
        labelType.textColor = Color_Hex(hex: "#404040")
        labelType.backgroundColor = self.view.backgroundColor!
        let width: CGFloat = labelType.intrinsicContentSize.width+20
        labelType.frame = CGRect.init(x: (headerView.frame.width-width)/2, y: 8, width: width, height: 15)
        
        headerView.addSubview(label)
        headerView.addSubview(labelType)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sKey = arrAllKeys[section] as! String
        let arrData = dictData[sKey] as! NSArray
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 91
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellFeedMenu", for: indexPath) as! TableViewCellFeedMenu
        cell.selectionStyle = .none
        
        let sKey = arrAllKeys[indexPath.section] as! String
        let arrData = dictData[sKey] as! NSArray
        let objData = TMenu.init(object: arrData.object(at: indexPath.row) as! NSDictionary)
        
        if objData.itemPriceDetails?.count ?? 0 > 0 {
            let model = objData.itemPriceDetails?.first!
            cell.lblPrice.text = "₹\(model?.itemPrice ?? "0")"
        } else {
            cell.lblPrice.text = "₹0"
        }
        cell.labelItemName.text = objData.itemName
        cell.imageViewItem.sd_setImage(with: URL(string: objData.itemImageUrl?.RemoveSpace() ?? ""), placeholderImage: FOOD_PLACE_HOLDER)
        
        cell.labelReview.text = "\(objData.reviewsCount ?? "0") reviews"
        
        cell.viewAdd.isHidden = true
        cell.viewPlusAndMinus.isHidden = true
        if isShowAddCart {
            cell.lblPrice.isHidden = false
            if indexPath.row > 2 {
                cell.viewAdd.isHidden = true
                cell.viewPlusAndMinus.isHidden = false
            } else {
                cell.viewPlusAndMinus.isHidden = true
                cell.viewAdd.isHidden = false
            }
        }

        return cell
    }
    
}
