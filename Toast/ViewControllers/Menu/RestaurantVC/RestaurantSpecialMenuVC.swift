//
//  RestaurantSpecialMenuVC.swift
//  Toast
//
//  Created by Nikul on 05/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RestaurantSpecialMenuVC: UIViewController {

    @IBOutlet weak var collectionViewSpecial: UICollectionView!
    var isRedirect: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.collectionViewSpecial.delegate = self
        self.collectionViewSpecial.dataSource = self
        self.collectionViewSpecial.register(UINib(nibName: "CollectionViewCellMenuSpecial", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellMenuSpecial")
        self.collectionViewSpecial.contentInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    }
}

extension RestaurantSpecialMenuVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellMenuSpecial", for: indexPath) as! CollectionViewCellMenuSpecial
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH() - 30) / 2, height: 176)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isRedirect { return }
        let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "RestaurantMenuVC") as! RestaurantMenuVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
