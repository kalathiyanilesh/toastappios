//
//  MenuVC.swift
//  Toast
//
//  Created by Nikul on 05/06/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class RestaurantMenuVC: UIViewController {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var collectionViewMenu: UICollectionView!
    
    var pageMenu : CAPSPageMenu?
    var arrayMenus = ["Special","Food Menu","Beverages","Desserts","Drinks"]
    var selectedIndex = 0
    var arrMenus = NSMutableArray()
    var dictMenu = NSDictionary()
    var sType = ""
    var arrAllKeys = NSArray()
    var dictData = NSDictionary()
    
    @IBOutlet weak var labelRestaurantName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        
        if APP_DELEGATE.objRestaurantDetails != nil {
            labelRestaurantName.text = APP_DELEGATE.objRestaurantDetails?.restrauntName
        }
        
        self.collectionViewMenu.delegate = self
        self.collectionViewMenu.dataSource = self
        self.collectionViewMenu.register(UINib(nibName: "CollectionViewCellMenuList", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCellMenuList")
        
        SetPageMenu()
        
    }
    
    func SetUpSpecialArray(_ dictData: NSDictionary, _ arrSpecial: NSMutableArray) {
        let arrfood = (dictData.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
        for i in 0..<arrfood.count {
            let sKey = arrfood[i] as! String
            let arrData = dictData[sKey] as! NSArray
            for k in 0..<arrData.count {
                let objData = TMenu.init(object: arrData.object(at: k) as! NSDictionary)
                if objData.specialItem == "1" {
                    arrSpecial.add(objData)
                }
            }
        }
    }

    @IBAction func buttonBackTouchUpInside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- SET PAGEMENU
extension RestaurantMenuVC: CAPSPageMenuDelegate {
    
    func SetPageMenu() {

        APP_DELEGATE.isFoodVeg = false
        APP_DELEGATE.isContainsEgg = false
        APP_DELEGATE.dictMenuSection = NSMutableDictionary()
        APP_DELEGATE.arrCartItems = NSMutableArray()
        
        UserDefaults.standard.removeObject(forKey: MENU_DICT)
        var arrController : [UIViewController] = []
        let arrSpecial = NSMutableArray()
        arrMenus = NSMutableArray()
        if dictMenu.allKeys.count > 0 {
            UserDefaults.standard.set(dictMenu, forKey: MENU_DICT)
            UserDefaults.standard.synchronize()
            if let dictfood = dictMenu["food"] as? NSDictionary {
                arrMenus.add("food")
                SetUpSpecialArray(dictfood, arrSpecial)
            }
            if let dictbeverages = dictMenu["beverages"] as? NSDictionary {
                arrMenus.add("beverages")
                SetUpSpecialArray(dictbeverages, arrSpecial)
            }
            if let dictdrinks = dictMenu["drinks"] as? NSDictionary {
                arrMenus.add("drinks")
                SetUpSpecialArray(dictdrinks, arrSpecial)
            }
            if let dictdesserts = dictMenu["desserts"] as? NSDictionary {
                arrMenus.add("desserts")
                SetUpSpecialArray(dictdesserts, arrSpecial)
            }
        }
        
        if arrSpecial.count > 0 {
            let special = loadVC(strStoryboardId: SB_MENU, strVCId: "SpecialMenuVC") as! SpecialMenuVC
            special.title = "Special"
            special.delegateSpecial = self
            special.arrSpecial = arrSpecial
            special.arrAllSpecial = arrSpecial
            arrController.append(special)
        }
    
//        let arrMenus = ["food", "beverages", "drinks", "desserts"]
        for i in 0..<arrMenus.count {
            let sKey = arrMenus[i] as! String
            var sTitle = ""
            switch sKey {
            case "food":
                sTitle = "Food Menu"
            case "beverages":
                sTitle = "Beverages"
            case "drinks":
                sTitle = "Drinks"
            case "desserts":
                sTitle = "Dessert"
            default:
                sTitle = ""
            }
            
            let vc = loadVC(strStoryboardId: SB_MENU, strVCId: "FoodMenuVC") as! FoodMenuVC
            vc.title = sTitle
            vc.sType = sKey
            vc.delegateFood = self
            vc.isFromPriceLess = true
            if dictMenu.allKeys.count > 0 {
                if let dictData = dictMenu[sKey] as? NSDictionary {
                    vc.dictData = dictData
                    saveInUserDefault(obj: dictData, key: sKey)
                } else {
                    UserDefaults.standard.removeObject(forKey: sKey)
                    UserDefaults.standard.synchronize()
                }
            }
            arrController.append(vc)
        }
        
        if arrSpecial.count > 0 {
            arrMenus.insert("special", at: 0)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.lightGray),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectedMenuItemLabelColor(Color_Hex(hex: "#3CBCB4")),
            .unselectedMenuItemLabelColor(Color_Hex(hex: "#404040").withAlphaComponent(0.40)),
            .selectionIndicatorHeight(0),
            .menuHeight(47),
            .menuMargin(0),
            .menuItemFont(FontWithSize("Montserrat-Medium", 14)),
            .titleTextSizeBasedOnMenuItemWidth(true),
            .centerMenuItems(false)
        ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.controllerScrollView.alwaysBounceHorizontal = false
        pageMenu?.controllerScrollView.bounces = false
        //pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        /*
        switch index {
        case 0:
            APP_DELEGATE.strCurrentMenu = "special"
        case 1, 2, 3, 4:
            let arrMenus = ["food", "beverages", "drinks", "desserts"]
            APP_DELEGATE.strCurrentMenu = arrMenus[index-1]
        default:
            APP_DELEGATE.strCurrentMenu = ""
        }*/
        if arrMenus.contains("special") {
            APP_DELEGATE.strCurrentMenu = "special"
        } else {
            APP_DELEGATE.strCurrentMenu = arrMenus[index] as! String
        }
        
        if APP_DELEGATE.strCurrentMenu == "special" {
            NotificationCenter.default.post(name: Notification.Name(k_FILTER_SPECIAL_FOOD), object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name(k_FILTER_FOOD), object: nil)
        }
    }
}

extension RestaurantMenuVC: delegateSpecial, delegateFood {
    func CurrentFoodType(_ sType: String) {
        APP_DELEGATE.strCurrentMenu = sType
    }
    
    func SpecialFood() {
        APP_DELEGATE.dictMenuSection = NSMutableDictionary()
        APP_DELEGATE.strCurrentMenu = "special"
    }
}

extension RestaurantMenuVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMenus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellMenuList", for: indexPath) as! CollectionViewCellMenuList
        cell.labelTitle.text = arrayMenus[indexPath.row]
        
        if selectedIndex == indexPath.row {
            cell.labelTitle.textColor = #colorLiteral(red: 0.2352941176, green: 0.737254902, blue: 0.7058823529, alpha: 1)
        } else {
            cell.labelTitle.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 0.4)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: 70.0, height: 40.0)
        }
        return CGSize(width: 100.0, height: 40.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        pageMenu?.moveToPage(selectedIndex)
        self.collectionViewMenu.reloadData()
    }
    
}
