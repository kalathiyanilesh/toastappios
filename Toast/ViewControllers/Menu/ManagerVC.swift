//
//  ManagerVC.swift
//  Toast
//
//  Created by iMac on 08/04/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CellManager: UITableViewCell {
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgTick: UIImageView!
}

class ManagerVC: UIViewController {

    @IBOutlet var viewBlur: UIView!
    @IBOutlet var viewGradient: UIView!
    @IBOutlet var viewDetails: UIView!
    @IBOutlet var tblRequest: UITableView!
    @IBOutlet var txtOwnRequest: UITextField!
    @IBOutlet var txtCustomizeItem: IQTextView!
    
    var arrRequest: NSArray = ["Water", "Salt & Pepper", "Tissue Paper", "Sauce & mayonnaise"]
    var arrSelectedRequest = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetails.alpha = 0
        AddBlurEffect(viewBlur)
        txtOwnRequest.delegate = self
        tblRequest.isScrollEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.viewDetails.alpha = 1
        }
    }
    
    @IBAction func btnClose(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
    
    @IBAction func btnRequest(_ sender: Any) {
        self.view.endEditing(true)
        txtOwnRequest.text = TRIM(string: txtOwnRequest.text!)
        txtCustomizeItem.text = TRIM(string: txtCustomizeItem.text!)

        if txtOwnRequest.text?.count ?? 0 > 0 {
            arrSelectedRequest.add(txtOwnRequest.text!)
        }
        if txtCustomizeItem.text?.count ?? 0 > 0 || arrSelectedRequest.count > 0{
            apiCustomizeOrder()
        }
    }
}

extension ManagerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellManager = tableView.dequeueReusableCell(withIdentifier: "CellManager", for: indexPath) as! CellManager
        let sRequest: String = arrRequest.object(at: indexPath.row) as! String
        cell.lblName.text = sRequest
        cell.imgTick.isHidden = true
        if arrSelectedRequest.contains(sRequest) {
            cell.imgTick.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sRequest: String = arrRequest.object(at: indexPath.row) as! String
        if arrSelectedRequest.contains(sRequest) {
            arrSelectedRequest.remove(sRequest)
        } else {
            arrSelectedRequest.add(sRequest)
        }
        tblRequest.reloadData()
    }
}

//MARK: API CALLING
extension ManagerVC {
    func apiCustomizeOrder() {
        let service = SERVER_URL + "orders/\(APP_DELEGATE.sOrderID)/" + APICustomizeOrder
        let dictParam: NSDictionary = ["customize_item":txtCustomizeItem.text!, "essentials_request":arrSelectedRequest, "qr_code":APP_DELEGATE.strScannedQR]
        print(dictParam)
        if self.txtOwnRequest.text?.count ?? 0 > 0 {
            self.arrSelectedRequest.remove(self.txtOwnRequest.text!)
        }
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomizeOrder,
            parameters: dictParam,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "")
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    if let strStatus = dictData.object(forKey: "status") as? String {
                        if strStatus == "success" {
                            self.btnClose(UIButton())
                        } else {
                            showMessage("Something went wrong, try again later!")
                        }
                    }
                }
            }
        }
    }
}

extension ManagerVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tblRequest.isScrollEnabled = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tblRequest.isScrollEnabled = false
    }
}
