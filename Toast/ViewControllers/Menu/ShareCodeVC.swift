//
//  ShareCodeVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class ShareCodeVC: UIViewController {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    @IBOutlet var activity: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        first.isEnabled = false
        second.isEnabled = false
        third.isEnabled = false
        fourth.isEnabled = false
        AddRightSwipeGesture()
        let strjoinCode = getMyJoinCode()
        if strjoinCode.count <= 0 {
            GetTableCode()
        } else {
            self.activity.stopAnimating()
            SetCode(strjoinCode)
        }
    }
    
    @IBAction func btnOkay(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }

}

//MARK: API CALLING
extension ShareCodeVC {
    func GetTableCode() {
        self.view.endEditing(true)
        self.activity.startAnimating()
        apiGetTableCode { (strCode) in
            self.activity.stopAnimating()
            if strCode.count > 0 {
                self.SetCode(strCode)
            }
        }
    }
    
    func SetCode(_ strCode: String) {
        let arrCode = Array(strCode)
        if arrCode.count == 4 {
            self.first.text = String(arrCode[0] as Character)
            self.second.text = String(arrCode[1] as Character)
            self.third.text = String(arrCode[2] as Character)
            self.fourth.text = String(arrCode[3] as Character)
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension ShareCodeVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnOkay(UIButton())
    }
}

