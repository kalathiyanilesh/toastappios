//
//  ReviewOrderVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class ReviewOrderVC: UIViewController {

    @IBOutlet var btnReviewRating: UIButton!
    @IBOutlet var btnPastOrders: UIButton!
    @IBOutlet var viewPage: UIView!
    @IBOutlet var lblFoodKing: UILabel!
    @IBOutlet var lblAwayReview: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    var pageMenu : CAPSPageMenu?
    var objUser:TUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        
        btnPastOrders.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        btnReviewRating.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
        
        apiGetPastOrder()
        SetPageMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isUserLogin() {
            apiGetUserInfo()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSettings(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "UserProfileVC") as! UserProfileVC
        vc.objUser = objUser
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnReviewRating(_ sender: Any) {
        btnReviewRating.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
        btnPastOrders.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        pageMenu?.moveToPage(0)
    }
    
    @IBAction func btnPastOrders(_ sender: Any) {
        btnPastOrders.setTitleColor(Color_Hex(hex: "3CBCB4"), for: .normal)
        btnReviewRating.setTitleColor(Color_Hex(hex: "656565").withAlphaComponent(0.60), for: .normal)
        pageMenu?.moveToPage(1)
    }
}

//MARK:- SET PAGEMENU
extension ReviewOrderVC {
    func SetPageMenu() {

        var arrController : [UIViewController] = []
    
        let reviewrating = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "ReviewRatingVC") as! ReviewRatingVC
        reviewrating.delegateReviewCount = self
        arrController.append(reviewrating)
        
        let pastorder = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "PastOrderVC") as! PastOrderVC
        pastorder.delegatePastOrderCount = self
        arrController.append(pastorder)
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewPage.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        self.addChild(pageMenu!)
        self.viewPage.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
    }
}

extension ReviewOrderVC: delegateReviewCount, delegatePastOrderCount {
    func totalReviews(_ sReview: String) {
        btnReviewRating.setTitle("Reviews & Ratings (\(sReview))", for: .normal)
    }
    
    func totalPastOrder(_ sOrder: String) {
       // totalPastOrder = sOrder
        
    }
}

//MARK: API CALLING
extension ReviewOrderVC {
    func apiGetUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    self.objUser = TUser.init(object: dictData)
                    self.lblFoodKing.text = "Food King (Level \(self.objUser.customerLevel ?? "1") Reviewer)"
                    self.imgProfile.sd_setImage(with: URL(string: self.objUser.profilePicture?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
                    self.lblName.text = "Hey \(self.objUser.customerName ?? "")!"
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiGetPastOrder() {
        let service = SERVER_URL + APIPastOrder
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                if let dictPagination = responseDict?.object(forKey: "pagination") as? NSDictionary {
                    if (dictPagination.object(forKey: "total") != nil) {
                        self.btnPastOrders.setTitle("Past Orders (\(dictPagination.object(forKey: "total") as! String))", for: .normal)
                    }
                }
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension ReviewOrderVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
