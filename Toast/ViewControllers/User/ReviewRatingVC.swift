//
//  ReviewRatingVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateReviewCount {
    func totalReviews(_ sReview: String)
}

class CellReviewRating: UITableViewCell {
    
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var imgEmoji: UIImageView!
    @IBOutlet var lblItemReview: UILabel!
    
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblResReview: UILabel!
    @IBOutlet var lblRate: UILabel!
    @IBOutlet var lblShowMore: UILabel!
    @IBOutlet var btnSection: UIButton!
    @IBOutlet var heightShowMore: NSLayoutConstraint!
    @IBOutlet var lblLine: UILabel!
}

class ReviewRatingVC: UIViewController {

    var delegateReviewCount: delegateReviewCount?
    
    @IBOutlet var bottom: NSLayoutConstraint!
    @IBOutlet var tblReview: UITableView!
    @IBOutlet var lblNoReview: UILabel!
    
    var arrReview = NSMutableArray()
    var selectedSection = -1
    
    var Service = ""
    var isSend = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() <= 736 {
            bottom.constant = 34
        }
        tblReview.sectionHeaderHeight = UITableView.automaticDimension
        tblReview.estimatedSectionHeaderHeight = 102
        apiGetPastOrder()
        tblReview.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.apiGetPastOrder()
            }else {
                self.tblReview.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
}

//MARK:- UITableViewDelegate METHOD
extension ReviewRatingVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        lblNoReview.alpha = arrReview.count > 0 ? 0 : 1
        return arrReview.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! CellReviewRating
        let objReview: TRReview = arrReview[section] as! TRReview
        cell.lblRestaurantName.text = objReview.resrtauntName
        cell.lblRate.text = "\(objReview.overallRatings ?? "0")/5"
        cell.lblResReview.text = objReview.overallReview
        cell.btnSection.tag = section
        cell.btnSection.addTarget(self, action: #selector(btnSection(_:)), for: .touchUpInside)
        if selectedSection == section {
            cell.heightShowMore.constant = 0
            cell.lblShowMore.isHidden = true
            cell.lblLine.isHidden = true
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        } else {
            cell.heightShowMore.constant = 13
            cell.lblShowMore.isHidden = false
            cell.lblLine.isHidden = false
        }
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSection == section {
            let objReview: TRReview = arrReview[section] as! TRReview
            return objReview.itemsReviews!.count + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objMain: TRReview = arrReview[indexPath.section] as! TRReview
        let arrItemReview = objMain.itemsReviews! as NSArray
        if indexPath.row == arrItemReview.count {
            let cell: CellReviewRating = tableView.dequeueReusableCell(withIdentifier: "showless", for: indexPath) as! CellReviewRating
            return cell
        } else {
            let cell: CellReviewRating = tableView.dequeueReusableCell(withIdentifier: "CellReviewRating", for: indexPath) as! CellReviewRating
            let objReview: ItemsReviews =  arrItemReview[indexPath.row] as! ItemsReviews
            if objReview.itemType == "non-veg" {
                cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
            } else if objReview.itemType == "veg" && objReview.containsEgg == "true" {
                cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
            } else {
                cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
            }
            cell.lblItemName.text = objReview.itemName
            cell.lblItemReview.text = objReview.itemReview
            cell.imgEmoji.image = GetEmojiFromRate(CGFloat((objReview.itemRating! as NSString).doubleValue))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objMain: TRReview = arrReview[indexPath.section] as! TRReview
        let arrItemReview = objMain.itemsReviews! as NSArray
        if indexPath.row == arrItemReview.count {
            selectedSection = -1
            UIView.performWithoutAnimation {
                self.tblReview.reloadSections(IndexSet(integer: indexPath.section), with: .none)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let objMain: TRReview = arrReview[indexPath.section] as! TRReview
        let arrItemReview = objMain.itemsReviews! as NSArray
        if indexPath.row == arrItemReview.count {
            return 26
        }
        return UITableView.automaticDimension
    }
    
    @objc func btnSection(_ sender: UIButton) {
        let objReview: TRReview = arrReview[sender.tag] as! TRReview
        if objReview.itemsReviews?.count ?? 0 > 0 {
            selectedSection = sender.tag
            UIView.performWithoutAnimation {
                self.tblReview.reloadData()
            }
        }
    }
}

//MARK: API CALLING
extension ReviewRatingVC {
    func apiGetPastOrder() {
        isSend = true
        self.view.endEditing(true)
        var isFirstPage = false
        if Service.count <= 0 {
            isFirstPage = true
            Service = SERVER_URL + APIPast + "sort_by=created_at&sort_type=-1&start=0&limit=10"
        }
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: Service,
            service: APIPast,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            if isFirstPage {
                self.tblReview.showsInfiniteScrolling = true
                self.arrReview.removeAllObjects()
            }
            self.Service = ""
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    for i in 0..<arrData.count {
                        let dictData = arrData[i] as! NSDictionary
                        let obj: TRReview = TRReview.init(object: dictData)
                        self.arrReview.add(obj)
                    }
                    self.tblReview.reloadData()
                    var sNext = ""
                    if let dictPagination = responseDict?.object(forKey: "pagination") as? NSDictionary {
                        if (dictPagination.object(forKey: "next") != nil) {
                            sNext = dictPagination.object(forKey: "next") as! String
                        }
                        if (dictPagination.object(forKey: "total") != nil) {
                            self.delegateReviewCount?.totalReviews(dictPagination.object(forKey: "total") as! String)
                        }
                    }
                    if sNext.count > 0 {
                        self.Service = sNext
                    }
                }
            }
            
            self.tblReview.infiniteScrollingView.stopAnimating()
            if self.arrReview.count <= 0 || self.Service.count <= 0 { self.tblReview.showsInfiniteScrolling = false }
            
            self.tblReview.reloadData()
            self.isSend = false
        }
    }
}
