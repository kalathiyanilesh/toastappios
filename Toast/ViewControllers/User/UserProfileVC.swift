
//
//  UserProfileVC.swift
//  Toast
//
//  Created by iMac on 31/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet weak var textFieldAddress: UITextField!
    
    var objUser:TUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddRightSwipeGesture()
        txtMobileNo.isEnabled = false
        if objUser == nil {
            apiGetUserInfo()
        } else {
            SetUserInfo()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        //Logoutuser()
        apiLogout()
    }
    
    @IBAction func btnImage(_ sender: Any) {
        ImagePickerManager().pickImage(self, true) { (image, path) in
            self.imgProfile.image = image
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if validateTxtFieldLength(txtName, withMessage: EnterName) && validateEmailAddress(txtEmail, withMessage: EnterValidEmail) {
            apiUpdateUserInfo()
        }
    }
    
    func SetUserInfo() {
        txtName.text = objUser.customerName ?? ""
        txtEmail.text = objUser.email ?? ""
        txtMobileNo.text = objUser.phoneNo ?? ""
        imgProfile.sd_setImage(with: URL(string: objUser.profilePicture?.RemoveSpace() ?? ""), placeholderImage: USER_PLACE_HOLDER)
    }
}

//MARK: API CALLING
extension UserProfileVC {
    func apiGetUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APICustomerProfile,
            parameters: NSDictionary(),
            method: .get,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                    self.objUser = TUser.init(object: dictData)
                    self.SetUserInfo()
                }
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
    
    func apiUpdateUserInfo() {
        self.view.endEditing(true)
        let service = SERVER_URL + APICustomerProfile
        let dictParam: NSDictionary = ["customer_name":TRIM(string: txtName.text!), "email":TRIM(string: txtEmail.text!), "profile_picture": compressImage(image: imgProfile.image!)]
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
            endpointurl: service,
            showLoader: true,
            parameters: dictParam) { (error, responseDict) in
                print(responseDict ?? "")
                if error == nil {
                    if let dictData = responseDict?.object(forKey: kDATA) as? NSDictionary {
                        if let strStatus = dictData.object(forKey: "status") as? String {
                            if strStatus.lowercased() == "success" {
                                showMessage("Successfully updated your profile!")
                                self.btnBack(self)
                            }
                        }
                    }
                } else {
                    showMessage(error?.localizedDescription ?? "Try again!" )
                }
        }
    }
    
    func apiLogout() {
        let service = SERVER_URL + APILogout
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APILogout,
            parameters: NSDictionary(),
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            print(responseDict ?? "")
            if error == nil {
                Logoutuser()
            } else {
                showMessage(error?.localizedDescription ?? "Try again!" )
            }
        }
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension UserProfileVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
