//
//  PastOrderVC.swift
//  Toast
//
//  Created by iMac on 20/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegatePastOrderCount {
    func totalPastOrder(_ sOrder: String)
}

class CellPastOrder: UITableViewCell {
    
    @IBOutlet var imgOrder: UIImageView!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblDateOrderNo: UILabel!
    @IBOutlet var lblDesc: UILabel!
}

class PastOrderVC: UIViewController {
    
    @IBOutlet var tblPastOrder: UITableView!
    @IBOutlet var bottom: NSLayoutConstraint!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var lblNoPastOrder: UILabel!
    
    var delegatePastOrderCount: delegatePastOrderCount?
    var arrAllKeys = NSArray()
    var dictPastOrder = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SCREENHEIGHT() <= 736 { bottom.constant = 34 }
        apiGetPastOrder()
    }
}

//MARK:- UITableViewDelegate METHOD
extension PastOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        lblNoPastOrder.alpha = arrAllKeys.count > 0 ? 0 : 1
        return arrAllKeys.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = self.view.backgroundColor
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 15, width: headerView.frame.width-20, height: 0.5)
        label.backgroundColor = Color_Hex(hex: "#707070")
        
        let labelDate = UILabel()
        labelDate.frame = CGRect.init(x: (headerView.frame.width-95)/2, y: 8, width: 95, height: 15)
        labelDate.text = arrAllKeys[section] as? String
        labelDate.textAlignment = .center
        labelDate.font = FontWithSize("Montserrat-Regular", 12)
        labelDate.textColor = UIColor.black
        labelDate.backgroundColor = self.view.backgroundColor
        
        headerView.addSubview(label)
        headerView.addSubview(labelDate)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sKey = arrAllKeys[section] as! String
        let arrData = dictPastOrder[sKey] as! NSArray
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellPastOrder = tableView.dequeueReusableCell(withIdentifier: "CellPastOrder", for: indexPath) as! CellPastOrder
        let sKey = arrAllKeys[indexPath.section] as! String
        let arrData = dictPastOrder[sKey] as! NSArray
        let dictMain = arrData.object(at: indexPath.row) as! NSDictionary
        
        let dictRestDetails = dictMain.object(forKey: "restraunt_details") as! NSDictionary
        cell.lblRestaurantName.text = dictRestDetails.object(forKey: "restraunt_name") as? String
        cell.imgOrder.sd_setImage(with: URL(string: (dictRestDetails.object(forKey: "restraunt_image") as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: RESTAURANT_PLACE_HOLDER)

        let dictPaymentDetails = dictMain.object(forKey: "payment_details") as! NSDictionary
        let milisecond = TimeInterval(dictPaymentDetails["created_on"] as! String)
        let dateVar = Date.init(timeIntervalSinceNow: (milisecond ?? 0)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a" 
        cell.lblDateOrderNo.text = "\(dateFormatter.string(from: dateVar)) | Order ID: \(dictMain.object(forKey: "order_id") as! String)"
        cell.lblDesc.text = dictMain.object(forKey: "itemnametext") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sKey = arrAllKeys[indexPath.section] as! String
        let arrData = dictPastOrder[sKey] as! NSArray
        let dictMain = arrData.object(at: indexPath.row) as! NSDictionary
        
        let vc = loadVC(strStoryboardId: SB_REVIEWORDER, strVCId: "OrderDetailsVC") as! OrderDetailsVC
        vc.dictOrder = dictMain
        vc.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
}

//MARK: API CALLING
extension PastOrderVC {
    func apiGetPastOrder() {
        self.view.endEditing(true)
        self.activity.startAnimating()
        let service = SERVER_URL + APIPastOrder
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: service,
            service: APIPastOrder,
            parameters: NSDictionary(),
            method: .get,
            getReturn: true,
            showLoader: false)
        { (error, responseDict) in
            self.activity.stopAnimating()
            if error == nil {
                print(responseDict ?? "")
                if let arrData = responseDict?.object(forKey: kDATA) as? NSArray {
                    for i in 0..<arrData.count {
                        var dictData = arrData[i] as! NSDictionary
                        let dictTmp = dictData.mutableCopy() as! NSMutableDictionary
                        let ary_mutable = NSMutableArray()
                        var sFirstItemName = ""
                        let dictOrderDetails = dictData.object(forKey: "order_details") as! NSDictionary
                        let arrOrderDetailsMap = dictData.object(forKey: "order_details_id_name_map") as! NSArray
                        for i in 0..<arrOrderDetailsMap.count {
                            let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
                            let sK = dictOrd.object(forKey: "id") as! String
                            let arrOrder = dictOrderDetails.object(forKey: sK) as! NSArray
                            if sFirstItemName.count <= 0 {
                                if arrOrder.count > 0 {
                                    let dict = arrOrder[0] as! NSDictionary
                                    sFirstItemName = dict.object(forKey: "item_name") as! String
                                }
                            }
                            ary_mutable.addObjects(from: arrOrder as! [Any])
                            let sFinal = "\(sFirstItemName) + \(ary_mutable.count-1) more"
                            dictTmp.setObject(sFinal, forKey: "itemnametext" as NSCopying)
                        }
                        dictData = dictTmp.copy() as! NSDictionary
                                            
                        let dictPaymentDetails = dictData.object(forKey: "payment_details") as! NSDictionary
                        let milisecond = TimeInterval(dictPaymentDetails["created_on"] as! String)
                        let dateVar = Date.init(timeIntervalSinceNow: (milisecond ?? 0)/1000)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd MMM yyyy"
                        let sKey = dateFormatter.string(from: dateVar)
                        if (self.dictPastOrder.object(forKey: sKey) != nil) {
                            let arrData = (self.dictPastOrder.object(forKey: sKey) as! NSArray).mutableCopy() as! NSMutableArray
                            arrData.add(dictData)
                            self.dictPastOrder.setObject(arrData.copy() as! NSArray, forKey: sKey as NSCopying)
                        } else {
                            let arrTmp = NSMutableArray()
                            arrTmp.add(dictData)
                            self.dictPastOrder.setObject(arrTmp.copy() as! NSArray, forKey: sKey as NSCopying)
                        }
                    }
                    self.arrAllKeys = (self.dictPastOrder.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
                    if let dictPagination = responseDict?.object(forKey: "pagination") as? NSDictionary {
                        if (dictPagination.object(forKey: "total") != nil) {
                            self.delegatePastOrderCount?.totalPastOrder(dictPagination.object(forKey: "total") as! String)
                        }
                    }
                    self.tblPastOrder.reloadData()
                }
            }
        }
    }
}
