//
//  OrderDetailsVC.swift
//  Toast
//
//  Created by iMac on 23/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import CHIPageControl

class CellOrder: UITableViewCell {
    
    @IBOutlet var imgVegNonVeg: UIImageView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblOrderBy: UILabel!
    @IBOutlet var lblPrice: UILabel!
}

class OrderDetailsVC: UIViewController {

    @IBOutlet var viewBlur: UIView!
    @IBOutlet var viewDetails: UIView!
    @IBOutlet var collImages: UICollectionView!
    @IBOutlet var pagecontrol: CHIPageControlChimayo!
    @IBOutlet var tblOrder: UITableView!
    @IBOutlet var lblResName: UILabel!
    @IBOutlet var lblResAddress: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
    var dictOrder = NSDictionary()
    var arrOrder = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetails.alpha = 0
        AddRightSwipeGesture()
        AddBlurEffect(viewBlur)
        
        pagecontrol.numberOfPages = 1
        pagecontrol.radius = 3
        pagecontrol.tintColor = UIColor.white
        pagecontrol.currentPageTintColor = UIColor.white
        pagecontrol.padding = 2

        
        let dictRestDetails = dictOrder.object(forKey: "restraunt_details") as! NSDictionary
        lblResName.text = dictRestDetails.object(forKey: "restraunt_name") as? String
        
        let milisecond = TimeInterval(dictOrder["created_on"] as! String)
        let dateVar = Date.init(timeIntervalSinceNow: (milisecond ?? 0)/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mma"
        let sTime = dateFormatter.string(from: dateVar)
        dateFormatter.dateFormat = "dd MMM yyyy"
        let sDate = dateFormatter.string(from: dateVar)
        lblDateTime.text = "\(sDate) | \(sTime)"
        lblOrderID.text = dictOrder.object(forKey: "order_id") as? String
        
        let dictOrderDetails = dictOrder.object(forKey: "order_details") as! NSDictionary
        lblPrice.text = "₹\(dictOrderDetails.object(forKey: "order_amount") as! String)"
        
        let arrOrderDetailsMap = dictOrder.object(forKey: "order_details_id_name_map") as! NSArray
        for i in 0..<arrOrderDetailsMap.count {
            let dictOrd = arrOrderDetailsMap[i] as! NSDictionary
            let sK = dictOrd.object(forKey: "id") as! String
            let arrTmp = dictOrderDetails.object(forKey: sK) as! NSArray
            for k in 0..<arrTmp.count {
                let dictTmp: NSMutableDictionary = (arrTmp[k] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                if sK == CUSTOMER_ID {
                    dictTmp.setObject("You", forKey: "orderby" as NSCopying)
                } else {
                    dictTmp.setObject(dictOrd.object(forKey: "id_name") as! String, forKey: "orderby" as NSCopying)
                }
                arrOrder.add(dictTmp.copy() as! NSDictionary)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.viewDetails.alpha = 1
        }
    }
    
    @IBAction func btnShare(_ sender: Any) {
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion: nil)
    }
}

//MARK:- UITableViewDelegate METHOD
extension OrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellOrder = tableView.dequeueReusableCell(withIdentifier: "CellOrder", for: indexPath) as! CellOrder
        let dictOrder: NSDictionary = arrOrder[indexPath.row] as! NSDictionary
        let sItemtype = dictOrder.object(forKey: "item_type") as! String
        let sContainsEgg = dictOrder.object(forKey: "contains_egg") as! String
        if sItemtype == "non-veg" {
            cell.imgVegNonVeg.image = UIImage(named: "ic_nonveg")
        } else if sItemtype == "veg" && sContainsEgg == "true" {
            cell.imgVegNonVeg.image = UIImage(named: "eggitarian")
        } else {
            cell.imgVegNonVeg.image = UIImage(named: "ic_veg")
        }
        cell.lblItemName.text = dictOrder.object(forKey: "item_name") as? String
        cell.lblPrice.text = "₹\(dictOrder.object(forKey: "item_total_price") as! String)"
        cell.lblOrderBy.text = dictOrder.object(forKey: "orderby") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension OrderDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellEventImages = collectionView.dequeueReusableCell(withReuseIdentifier: "CellEventImages", for: indexPath) as! CellEventImages
        let dictRestDetails = dictOrder.object(forKey: "restraunt_details") as! NSDictionary
        cell.imgEvent.sd_setImage(with: URL(string: (dictRestDetails.object(forKey: "restraunt_image") as? String ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: RESTAURANT_PLACE_HOLDER)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}

//MARK:- SWIPE GESTURE FOR BACK
extension OrderDetailsVC {
    func AddRightSwipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        self.btnBack(UIButton())
    }
}
