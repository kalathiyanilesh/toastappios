//
//  TCoupon.swift
//
//  Created by iMac on 10/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TCoupon: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let couponDiscountType = "coupon_discount_type"
    static let couponId = "coupon_id"
    static let couponCode = "coupon_code"
    static let couponMaxUsePerUser = "coupon_max_use_per_user"
    static let couponValidFrom = "coupon_valid_from"
    static let couponTitle = "coupon_title"
    static let couponValidTill = "coupon_valid_till"
    static let couponDescription = "coupon_description"
    static let couponDiscountPercentage = "coupon_discount_percentage"
    static let restrauntId = "restraunt_id"
    static let couponMaxAmount = "coupon_max_amount"
  }

  // MARK: Properties
  public var couponDiscountType: String?
  public var couponId: String?
  public var couponCode: String?
  public var couponMaxUsePerUser: String?
  public var couponValidFrom: String?
  public var couponTitle: String?
  public var couponValidTill: String?
  public var couponDescription: String?
  public var couponDiscountPercentage: String?
  public var restrauntId: String?
  public var couponMaxAmount: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    couponDiscountType = json[SerializationKeys.couponDiscountType].string
    couponId = json[SerializationKeys.couponId].string
    couponCode = json[SerializationKeys.couponCode].string
    couponMaxUsePerUser = json[SerializationKeys.couponMaxUsePerUser].string
    couponValidFrom = json[SerializationKeys.couponValidFrom].string
    couponTitle = json[SerializationKeys.couponTitle].string
    couponValidTill = json[SerializationKeys.couponValidTill].string
    couponDescription = json[SerializationKeys.couponDescription].string
    couponDiscountPercentage = json[SerializationKeys.couponDiscountPercentage].string
    restrauntId = json[SerializationKeys.restrauntId].string
    couponMaxAmount = json[SerializationKeys.couponMaxAmount].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = couponDiscountType { dictionary[SerializationKeys.couponDiscountType] = value }
    if let value = couponId { dictionary[SerializationKeys.couponId] = value }
    if let value = couponCode { dictionary[SerializationKeys.couponCode] = value }
    if let value = couponMaxUsePerUser { dictionary[SerializationKeys.couponMaxUsePerUser] = value }
    if let value = couponValidFrom { dictionary[SerializationKeys.couponValidFrom] = value }
    if let value = couponTitle { dictionary[SerializationKeys.couponTitle] = value }
    if let value = couponValidTill { dictionary[SerializationKeys.couponValidTill] = value }
    if let value = couponDescription { dictionary[SerializationKeys.couponDescription] = value }
    if let value = couponDiscountPercentage { dictionary[SerializationKeys.couponDiscountPercentage] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = couponMaxAmount { dictionary[SerializationKeys.couponMaxAmount] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.couponDiscountType = aDecoder.decodeObject(forKey: SerializationKeys.couponDiscountType) as? String
    self.couponId = aDecoder.decodeObject(forKey: SerializationKeys.couponId) as? String
    self.couponCode = aDecoder.decodeObject(forKey: SerializationKeys.couponCode) as? String
    self.couponMaxUsePerUser = aDecoder.decodeObject(forKey: SerializationKeys.couponMaxUsePerUser) as? String
    self.couponValidFrom = aDecoder.decodeObject(forKey: SerializationKeys.couponValidFrom) as? String
    self.couponTitle = aDecoder.decodeObject(forKey: SerializationKeys.couponTitle) as? String
    self.couponValidTill = aDecoder.decodeObject(forKey: SerializationKeys.couponValidTill) as? String
    self.couponDescription = aDecoder.decodeObject(forKey: SerializationKeys.couponDescription) as? String
    self.couponDiscountPercentage = aDecoder.decodeObject(forKey: SerializationKeys.couponDiscountPercentage) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.couponMaxAmount = aDecoder.decodeObject(forKey: SerializationKeys.couponMaxAmount) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(couponDiscountType, forKey: SerializationKeys.couponDiscountType)
    aCoder.encode(couponId, forKey: SerializationKeys.couponId)
    aCoder.encode(couponCode, forKey: SerializationKeys.couponCode)
    aCoder.encode(couponMaxUsePerUser, forKey: SerializationKeys.couponMaxUsePerUser)
    aCoder.encode(couponValidFrom, forKey: SerializationKeys.couponValidFrom)
    aCoder.encode(couponTitle, forKey: SerializationKeys.couponTitle)
    aCoder.encode(couponValidTill, forKey: SerializationKeys.couponValidTill)
    aCoder.encode(couponDescription, forKey: SerializationKeys.couponDescription)
    aCoder.encode(couponDiscountPercentage, forKey: SerializationKeys.couponDiscountPercentage)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(couponMaxAmount, forKey: SerializationKeys.couponMaxAmount)
  }

}
