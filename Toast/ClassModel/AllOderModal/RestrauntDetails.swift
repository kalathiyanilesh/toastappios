//
//  RestrauntDetails.swift
//
//  Created by iMac on 24/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RestrauntDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let restrauntName = "restraunt_name"
    static let restrauntImage = "restraunt_image"
  }

  // MARK: Properties
  public var restrauntName: String?
  public var restrauntImage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    restrauntName = json[SerializationKeys.restrauntName].string
    restrauntImage = json[SerializationKeys.restrauntImage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = restrauntName { dictionary[SerializationKeys.restrauntName] = value }
    if let value = restrauntImage { dictionary[SerializationKeys.restrauntImage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.restrauntName = aDecoder.decodeObject(forKey: SerializationKeys.restrauntName) as? String
    self.restrauntImage = aDecoder.decodeObject(forKey: SerializationKeys.restrauntImage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(restrauntName, forKey: SerializationKeys.restrauntName)
    aCoder.encode(restrauntImage, forKey: SerializationKeys.restrauntImage)
  }

}
