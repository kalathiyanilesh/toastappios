//
//  PaymentDetails.swift
//
//  Created by iMac on 24/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PaymentDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let payeeCustomerId = "payee_customer_id"
    static let updatedBy = "updated_by"
    static let updatedOn = "updated_on"
    static let createdBy = "created_by"
    static let razorpayPaymentId = "razorpay_payment_id"
    static let paymentAmount = "payment_amount"
    static let paidOn = "paid_on"
    static let createdOn = "created_on"
    static let paymentStatus = "payment_status"
    static let paymentType = "payment_type"
  }

  // MARK: Properties
  public var payeeCustomerId: String?
  public var updatedBy: String?
  public var updatedOn: String?
  public var createdBy: String?
  public var razorpayPaymentId: String?
  public var paymentAmount: String?
  public var paidOn: String?
  public var createdOn: String?
  public var paymentStatus: String?
  public var paymentType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    payeeCustomerId = json[SerializationKeys.payeeCustomerId].string
    updatedBy = json[SerializationKeys.updatedBy].string
    updatedOn = json[SerializationKeys.updatedOn].string
    createdBy = json[SerializationKeys.createdBy].string
    razorpayPaymentId = json[SerializationKeys.razorpayPaymentId].string
    paymentAmount = json[SerializationKeys.paymentAmount].string
    paidOn = json[SerializationKeys.paidOn].string
    createdOn = json[SerializationKeys.createdOn].string
    paymentStatus = json[SerializationKeys.paymentStatus].string
    paymentType = json[SerializationKeys.paymentType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = payeeCustomerId { dictionary[SerializationKeys.payeeCustomerId] = value }
    if let value = updatedBy { dictionary[SerializationKeys.updatedBy] = value }
    if let value = updatedOn { dictionary[SerializationKeys.updatedOn] = value }
    if let value = createdBy { dictionary[SerializationKeys.createdBy] = value }
    if let value = razorpayPaymentId { dictionary[SerializationKeys.razorpayPaymentId] = value }
    if let value = paymentAmount { dictionary[SerializationKeys.paymentAmount] = value }
    if let value = paidOn { dictionary[SerializationKeys.paidOn] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
    if let value = paymentType { dictionary[SerializationKeys.paymentType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.payeeCustomerId = aDecoder.decodeObject(forKey: SerializationKeys.payeeCustomerId) as? String
    self.updatedBy = aDecoder.decodeObject(forKey: SerializationKeys.updatedBy) as? String
    self.updatedOn = aDecoder.decodeObject(forKey: SerializationKeys.updatedOn) as? String
    self.createdBy = aDecoder.decodeObject(forKey: SerializationKeys.createdBy) as? String
    self.razorpayPaymentId = aDecoder.decodeObject(forKey: SerializationKeys.razorpayPaymentId) as? String
    self.paymentAmount = aDecoder.decodeObject(forKey: SerializationKeys.paymentAmount) as? String
    self.paidOn = aDecoder.decodeObject(forKey: SerializationKeys.paidOn) as? String
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? String
    self.paymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.paymentStatus) as? String
    self.paymentType = aDecoder.decodeObject(forKey: SerializationKeys.paymentType) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(payeeCustomerId, forKey: SerializationKeys.payeeCustomerId)
    aCoder.encode(updatedBy, forKey: SerializationKeys.updatedBy)
    aCoder.encode(updatedOn, forKey: SerializationKeys.updatedOn)
    aCoder.encode(createdBy, forKey: SerializationKeys.createdBy)
    aCoder.encode(razorpayPaymentId, forKey: SerializationKeys.razorpayPaymentId)
    aCoder.encode(paymentAmount, forKey: SerializationKeys.paymentAmount)
    aCoder.encode(paidOn, forKey: SerializationKeys.paidOn)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(paymentStatus, forKey: SerializationKeys.paymentStatus)
    aCoder.encode(paymentType, forKey: SerializationKeys.paymentType)
  }

}
