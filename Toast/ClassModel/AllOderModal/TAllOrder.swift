//
//  TAllOrder.swift
//
//  Created by iMac on 24/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TAllOrder: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let orderStatus = "order_status"
    static let orderId = "order_id"
    static let orderDetailsIdNameMap = "order_details_id_name_map"
    static let orderType = "order_type"
    static let createdOn = "created_on"
    static let paymentDetails = "payment_details"
    static let restrauntDetails = "restraunt_details"
  }

  // MARK: Properties
  public var orderStatus: String?
  public var orderId: String?
  public var orderDetailsIdNameMap: [OrderDetailsIdNameMap]?
  public var orderType: String?
  public var createdOn: String?
  public var paymentDetails: PaymentDetails?
  public var restrauntDetails: RestrauntDetails?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    orderStatus = json[SerializationKeys.orderStatus].string
    orderId = json[SerializationKeys.orderId].string
    if let items = json[SerializationKeys.orderDetailsIdNameMap].array { orderDetailsIdNameMap = items.map { OrderDetailsIdNameMap(json: $0) } }
    orderType = json[SerializationKeys.orderType].string
    createdOn = json[SerializationKeys.createdOn].string
    paymentDetails = PaymentDetails(json: json[SerializationKeys.paymentDetails])
    restrauntDetails = RestrauntDetails(json: json[SerializationKeys.restrauntDetails])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = orderStatus { dictionary[SerializationKeys.orderStatus] = value }
    if let value = orderId { dictionary[SerializationKeys.orderId] = value }
    if let value = orderDetailsIdNameMap { dictionary[SerializationKeys.orderDetailsIdNameMap] = value.map { $0.dictionaryRepresentation() } }
    if let value = orderType { dictionary[SerializationKeys.orderType] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = paymentDetails { dictionary[SerializationKeys.paymentDetails] = value.dictionaryRepresentation() }
    if let value = restrauntDetails { dictionary[SerializationKeys.restrauntDetails] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.orderStatus = aDecoder.decodeObject(forKey: SerializationKeys.orderStatus) as? String
    self.orderId = aDecoder.decodeObject(forKey: SerializationKeys.orderId) as? String
    self.orderDetailsIdNameMap = aDecoder.decodeObject(forKey: SerializationKeys.orderDetailsIdNameMap) as? [OrderDetailsIdNameMap]
    self.orderType = aDecoder.decodeObject(forKey: SerializationKeys.orderType) as? String
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? String
    self.paymentDetails = aDecoder.decodeObject(forKey: SerializationKeys.paymentDetails) as? PaymentDetails
    self.restrauntDetails = aDecoder.decodeObject(forKey: SerializationKeys.restrauntDetails) as? RestrauntDetails
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(orderStatus, forKey: SerializationKeys.orderStatus)
    aCoder.encode(orderId, forKey: SerializationKeys.orderId)
    aCoder.encode(orderDetailsIdNameMap, forKey: SerializationKeys.orderDetailsIdNameMap)
    aCoder.encode(orderType, forKey: SerializationKeys.orderType)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(paymentDetails, forKey: SerializationKeys.paymentDetails)
    aCoder.encode(restrauntDetails, forKey: SerializationKeys.restrauntDetails)
  }

}
