//
//  TPastItem.swift
//
//  Created by iMac on 28/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TPastItem: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let itemType = "item_type"
    static let itemName = "item_name"
    static let containsEgg = "contains_egg"
    static let itemId = "item_id"
    static let qty = "qty"
    static let itemPrice = "item_price"
    static let itemTotalPrice = "item_total_price"
    static let itemOrderId = "item_order_id"
    static let orderby = "orderby"
    static let itemIndex = "itemIndex"
    static let itemDescription = "item_description"
  }

  // MARK: Properties
  public var itemType: String?
  public var itemName: String?
  public var containsEgg: String?
  public var itemId: String?
  public var qty: String?
  public var itemPrice: String?
  public var itemTotalPrice: String?
  public var itemOrderId: String?
  public var orderby: String?
  public var itemIndex: String?
  public var itemDescription: String?
    
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    itemType = json[SerializationKeys.itemType].string
    itemName = json[SerializationKeys.itemName].string
    containsEgg = json[SerializationKeys.containsEgg].string
    itemId = json[SerializationKeys.itemId].string
    qty = json[SerializationKeys.qty].string
    itemPrice = json[SerializationKeys.itemPrice].string
    itemTotalPrice = json[SerializationKeys.itemTotalPrice].string
    itemOrderId = json[SerializationKeys.itemOrderId].string
    orderby = json[SerializationKeys.orderby].string
    itemIndex = json[SerializationKeys.itemIndex].string
    itemDescription = json[SerializationKeys.itemDescription].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = itemType { dictionary[SerializationKeys.itemType] = value }
    if let value = itemName { dictionary[SerializationKeys.itemName] = value }
    if let value = containsEgg { dictionary[SerializationKeys.containsEgg] = value }
    if let value = itemId { dictionary[SerializationKeys.itemId] = value }
    if let value = qty { dictionary[SerializationKeys.qty] = value }
    if let value = itemPrice { dictionary[SerializationKeys.itemPrice] = value }
    if let value = itemTotalPrice { dictionary[SerializationKeys.itemTotalPrice] = value }
    if let value = itemOrderId { dictionary[SerializationKeys.itemOrderId] = value }
    if let value = orderby { dictionary[SerializationKeys.orderby] = value }
    if let value = itemIndex { dictionary[SerializationKeys.itemIndex] = value }
    if let value = itemDescription { dictionary[SerializationKeys.itemDescription] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.itemType = aDecoder.decodeObject(forKey: SerializationKeys.itemType) as? String
    self.itemName = aDecoder.decodeObject(forKey: SerializationKeys.itemName) as? String
    self.containsEgg = aDecoder.decodeObject(forKey: SerializationKeys.containsEgg) as? String
    self.itemId = aDecoder.decodeObject(forKey: SerializationKeys.itemId) as? String
    self.qty = aDecoder.decodeObject(forKey: SerializationKeys.qty) as? String
    self.itemPrice = aDecoder.decodeObject(forKey: SerializationKeys.itemPrice) as? String
    self.itemTotalPrice = aDecoder.decodeObject(forKey: SerializationKeys.itemTotalPrice) as? String
    self.itemOrderId = aDecoder.decodeObject(forKey: SerializationKeys.itemOrderId) as? String
    self.orderby = aDecoder.decodeObject(forKey: SerializationKeys.orderby) as? String
    self.itemIndex = aDecoder.decodeObject(forKey: SerializationKeys.itemIndex) as? String
    self.itemDescription = aDecoder.decodeObject(forKey: SerializationKeys.itemDescription) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(itemType, forKey: SerializationKeys.itemType)
    aCoder.encode(itemName, forKey: SerializationKeys.itemName)
    aCoder.encode(containsEgg, forKey: SerializationKeys.containsEgg)
    aCoder.encode(itemId, forKey: SerializationKeys.itemId)
    aCoder.encode(qty, forKey: SerializationKeys.qty)
    aCoder.encode(itemPrice, forKey: SerializationKeys.itemPrice)
    aCoder.encode(itemTotalPrice, forKey: SerializationKeys.itemTotalPrice)
    aCoder.encode(itemOrderId, forKey: SerializationKeys.itemOrderId)
    aCoder.encode(orderby, forKey: SerializationKeys.orderby)
    aCoder.encode(itemIndex, forKey: SerializationKeys.itemIndex)
    aCoder.encode(itemDescription, forKey: SerializationKeys.itemDescription)
  }

}
