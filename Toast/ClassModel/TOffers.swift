//
//  TOffers.swift
//
//  Created by macOS on 04/07/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TOffers: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let noOfTimeUsed = "no_of_time_used"
    static let couponDiscountType = "coupon_discount_type"
    static let couponCode = "coupon_code"
    static let couponDescription = "coupon_description"
    static let couponDiscountPercentage = "coupon_discount_percentage"
    static let couponTitle = "coupon_title"
    static let customerId = "customer_id"
    static let couponMaxAmount = "coupon_max_amount"
    static let notificationId = "notification_id"
    static let restrauntId = "restraunt_id"
    static let couponId = "coupon_id"
    static let couponValidFrom = "coupon_valid_from"
    static let couponMaxUsePerUser = "coupon_max_use_per_user"
    static let couponValidTill = "coupon_valid_till"
    static let createdOn = "created_on"
    static let isViewed = "is_viewed"
    static let restrauntName = "restraunt_name"
    static let viewedOn = "viewed_on"
  }

  // MARK: Properties
  public var noOfTimeUsed: String?
  public var couponDiscountType: String?
  public var couponCode: String?
  public var couponDescription: String?
  public var couponDiscountPercentage: String?
  public var couponTitle: String?
  public var customerId: String?
  public var couponMaxAmount: String?
  public var notificationId: String?
  public var restrauntId: String?
  public var couponId: String?
  public var couponValidFrom: String?
  public var couponMaxUsePerUser: String?
  public var couponValidTill: String?
  public var createdOn: String?
  public var isViewed: String?
  public var restrauntName: String?
  public var viewedOn: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    noOfTimeUsed = json[SerializationKeys.noOfTimeUsed].string
    couponDiscountType = json[SerializationKeys.couponDiscountType].string
    couponCode = json[SerializationKeys.couponCode].string
    couponDescription = json[SerializationKeys.couponDescription].string
    couponDiscountPercentage = json[SerializationKeys.couponDiscountPercentage].string
    couponTitle = json[SerializationKeys.couponTitle].string
    customerId = json[SerializationKeys.customerId].string
    couponMaxAmount = json[SerializationKeys.couponMaxAmount].string
    notificationId = json[SerializationKeys.notificationId].string
    restrauntId = json[SerializationKeys.restrauntId].string
    couponId = json[SerializationKeys.couponId].string
    couponValidFrom = json[SerializationKeys.couponValidFrom].string
    couponMaxUsePerUser = json[SerializationKeys.couponMaxUsePerUser].string
    couponValidTill = json[SerializationKeys.couponValidTill].string
    createdOn = json[SerializationKeys.createdOn].string
    isViewed = json[SerializationKeys.isViewed].string
    restrauntName = json[SerializationKeys.restrauntName].string
    viewedOn = json[SerializationKeys.viewedOn].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = noOfTimeUsed { dictionary[SerializationKeys.noOfTimeUsed] = value }
    if let value = couponDiscountType { dictionary[SerializationKeys.couponDiscountType] = value }
    if let value = couponCode { dictionary[SerializationKeys.couponCode] = value }
    if let value = couponDescription { dictionary[SerializationKeys.couponDescription] = value }
    if let value = couponDiscountPercentage { dictionary[SerializationKeys.couponDiscountPercentage] = value }
    if let value = couponTitle { dictionary[SerializationKeys.couponTitle] = value }
    if let value = customerId { dictionary[SerializationKeys.customerId] = value }
    if let value = couponMaxAmount { dictionary[SerializationKeys.couponMaxAmount] = value }
    if let value = notificationId { dictionary[SerializationKeys.notificationId] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = couponId { dictionary[SerializationKeys.couponId] = value }
    if let value = couponValidFrom { dictionary[SerializationKeys.couponValidFrom] = value }
    if let value = couponMaxUsePerUser { dictionary[SerializationKeys.couponMaxUsePerUser] = value }
    if let value = couponValidTill { dictionary[SerializationKeys.couponValidTill] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = isViewed { dictionary[SerializationKeys.isViewed] = value }
    if let value = restrauntName { dictionary[SerializationKeys.restrauntName] = value }
    if let value = viewedOn { dictionary[SerializationKeys.viewedOn] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.noOfTimeUsed = aDecoder.decodeObject(forKey: SerializationKeys.noOfTimeUsed) as? String
    self.couponDiscountType = aDecoder.decodeObject(forKey: SerializationKeys.couponDiscountType) as? String
    self.couponCode = aDecoder.decodeObject(forKey: SerializationKeys.couponCode) as? String
    self.couponDescription = aDecoder.decodeObject(forKey: SerializationKeys.couponDescription) as? String
    self.couponDiscountPercentage = aDecoder.decodeObject(forKey: SerializationKeys.couponDiscountPercentage) as? String
    self.couponTitle = aDecoder.decodeObject(forKey: SerializationKeys.couponTitle) as? String
    self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? String
    self.couponMaxAmount = aDecoder.decodeObject(forKey: SerializationKeys.couponMaxAmount) as? String
    self.notificationId = aDecoder.decodeObject(forKey: SerializationKeys.notificationId) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.couponId = aDecoder.decodeObject(forKey: SerializationKeys.couponId) as? String
    self.couponValidFrom = aDecoder.decodeObject(forKey: SerializationKeys.couponValidFrom) as? String
    self.couponMaxUsePerUser = aDecoder.decodeObject(forKey: SerializationKeys.couponMaxUsePerUser) as? String
    self.couponValidTill = aDecoder.decodeObject(forKey: SerializationKeys.couponValidTill) as? String
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? String
    self.isViewed = aDecoder.decodeObject(forKey: SerializationKeys.isViewed) as? String
    self.restrauntName = aDecoder.decodeObject(forKey: SerializationKeys.restrauntName) as? String
    self.viewedOn = aDecoder.decodeObject(forKey: SerializationKeys.viewedOn) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(noOfTimeUsed, forKey: SerializationKeys.noOfTimeUsed)
    aCoder.encode(couponDiscountType, forKey: SerializationKeys.couponDiscountType)
    aCoder.encode(couponCode, forKey: SerializationKeys.couponCode)
    aCoder.encode(couponDescription, forKey: SerializationKeys.couponDescription)
    aCoder.encode(couponDiscountPercentage, forKey: SerializationKeys.couponDiscountPercentage)
    aCoder.encode(couponTitle, forKey: SerializationKeys.couponTitle)
    aCoder.encode(customerId, forKey: SerializationKeys.customerId)
    aCoder.encode(couponMaxAmount, forKey: SerializationKeys.couponMaxAmount)
    aCoder.encode(notificationId, forKey: SerializationKeys.notificationId)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(couponId, forKey: SerializationKeys.couponId)
    aCoder.encode(couponValidFrom, forKey: SerializationKeys.couponValidFrom)
    aCoder.encode(couponMaxUsePerUser, forKey: SerializationKeys.couponMaxUsePerUser)
    aCoder.encode(couponValidTill, forKey: SerializationKeys.couponValidTill)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(isViewed, forKey: SerializationKeys.isViewed)
    aCoder.encode(restrauntName, forKey: SerializationKeys.restrauntName)
    aCoder.encode(viewedOn, forKey: SerializationKeys.viewedOn)
  }

}
