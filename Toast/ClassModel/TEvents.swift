//
//  TEvents.swift
//
//  Created by iMac on 17/01/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TEvents: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let eventImageUrl = "event_image_url"
    static let attending = "attending"
    static let customerId = "customer_id"
    static let eventDescription = "event_description"
    static let eventFee = "event_fee"
    static let eventName = "event_name"
    static let restrauntId = "restraunt_id"
    static let eventLocation = "event_location"
    static let createdOn = "created_on"
    static let eventType = "event_type"
    static let eventOn = "event_on"
    static let eventId = "event_id"
    static let noOfPaxAttenting = "no_of_pax_attenting"
    static let restrauntName = "restraunt_name"
  }

  // MARK: Properties
  public var eventImageUrl: String?
  public var attending: String?
  public var customerId: String?
  public var eventDescription: String?
  public var eventFee: String?
  public var eventName: String?
  public var restrauntId: String?
  public var eventLocation: String?
  public var createdOn: String?
  public var eventType: String?
  public var eventOn: String?
  public var eventId: String?
  public var noOfPaxAttenting: String?
    public var restrauntName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    eventImageUrl = json[SerializationKeys.eventImageUrl].string
    attending = json[SerializationKeys.attending].string
    customerId = json[SerializationKeys.customerId].string
    eventDescription = json[SerializationKeys.eventDescription].string
    eventFee = json[SerializationKeys.eventFee].string
    eventName = json[SerializationKeys.eventName].string
    restrauntId = json[SerializationKeys.restrauntId].string
    eventLocation = json[SerializationKeys.eventLocation].string
    createdOn = json[SerializationKeys.createdOn].string
    eventType = json[SerializationKeys.eventType].string
    eventOn = json[SerializationKeys.eventOn].string
    eventId = json[SerializationKeys.eventId].string
    noOfPaxAttenting = json[SerializationKeys.noOfPaxAttenting].string
    restrauntName = json[SerializationKeys.restrauntName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = eventImageUrl { dictionary[SerializationKeys.eventImageUrl] = value }
    if let value = attending { dictionary[SerializationKeys.attending] = value }
    if let value = customerId { dictionary[SerializationKeys.customerId] = value }
    if let value = eventDescription { dictionary[SerializationKeys.eventDescription] = value }
    if let value = eventFee { dictionary[SerializationKeys.eventFee] = value }
    if let value = eventName { dictionary[SerializationKeys.eventName] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = eventLocation { dictionary[SerializationKeys.eventLocation] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = eventType { dictionary[SerializationKeys.eventType] = value }
    if let value = eventOn { dictionary[SerializationKeys.eventOn] = value }
    if let value = eventId { dictionary[SerializationKeys.eventId] = value }
    if let value = noOfPaxAttenting { dictionary[SerializationKeys.noOfPaxAttenting] = value }
    if let value = restrauntName { dictionary[SerializationKeys.restrauntName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.eventImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.eventImageUrl) as? String
    self.attending = aDecoder.decodeObject(forKey: SerializationKeys.attending) as? String
    self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? String
    self.eventDescription = aDecoder.decodeObject(forKey: SerializationKeys.eventDescription) as? String
    self.eventFee = aDecoder.decodeObject(forKey: SerializationKeys.eventFee) as? String
    self.eventName = aDecoder.decodeObject(forKey: SerializationKeys.eventName) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.eventLocation = aDecoder.decodeObject(forKey: SerializationKeys.eventLocation) as? String
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? String
    self.eventType = aDecoder.decodeObject(forKey: SerializationKeys.eventType) as? String
    self.eventOn = aDecoder.decodeObject(forKey: SerializationKeys.eventOn) as? String
    self.eventId = aDecoder.decodeObject(forKey: SerializationKeys.eventId) as? String
    self.noOfPaxAttenting = aDecoder.decodeObject(forKey: SerializationKeys.noOfPaxAttenting) as? String
    self.restrauntName = aDecoder.decodeObject(forKey: SerializationKeys.restrauntName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(eventImageUrl, forKey: SerializationKeys.eventImageUrl)
    aCoder.encode(attending, forKey: SerializationKeys.attending)
    aCoder.encode(customerId, forKey: SerializationKeys.customerId)
    aCoder.encode(eventDescription, forKey: SerializationKeys.eventDescription)
    aCoder.encode(eventFee, forKey: SerializationKeys.eventFee)
    aCoder.encode(eventName, forKey: SerializationKeys.eventName)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(eventLocation, forKey: SerializationKeys.eventLocation)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(eventType, forKey: SerializationKeys.eventType)
    aCoder.encode(eventOn, forKey: SerializationKeys.eventOn)
    aCoder.encode(eventId, forKey: SerializationKeys.eventId)
    aCoder.encode(noOfPaxAttenting, forKey: SerializationKeys.noOfPaxAttenting)
    aCoder.encode(restrauntName, forKey: SerializationKeys.restrauntName)
  }

}
