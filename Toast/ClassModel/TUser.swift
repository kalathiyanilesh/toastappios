//
//  TUser.swift
//
//  Created by iMac on 31/01/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TUser: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let customerLevel = "customer_level"
    static let email = "email"
    static let profilePicture = "profile_picture"
    static let phoneNo = "phone_no"
    static let customerName = "customer_name"
  }

  // MARK: Properties
  public var customerLevel: String?
  public var email: String?
  public var profilePicture: String?
  public var phoneNo: String?
  public var customerName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    customerLevel = json[SerializationKeys.customerLevel].string
    email = json[SerializationKeys.email].string
    profilePicture = json[SerializationKeys.profilePicture].string
    phoneNo = json[SerializationKeys.phoneNo].string
    customerName = json[SerializationKeys.customerName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = customerLevel { dictionary[SerializationKeys.customerLevel] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
    if let value = phoneNo { dictionary[SerializationKeys.phoneNo] = value }
    if let value = customerName { dictionary[SerializationKeys.customerName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.customerLevel = aDecoder.decodeObject(forKey: SerializationKeys.customerLevel) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
    self.phoneNo = aDecoder.decodeObject(forKey: SerializationKeys.phoneNo) as? String
    self.customerName = aDecoder.decodeObject(forKey: SerializationKeys.customerName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(customerLevel, forKey: SerializationKeys.customerLevel)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
    aCoder.encode(phoneNo, forKey: SerializationKeys.phoneNo)
    aCoder.encode(customerName, forKey: SerializationKeys.customerName)
  }

}
