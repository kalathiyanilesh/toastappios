//
//  Replies.swift
//
//  Created by iMac on 08/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Replies: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let accountId = "account_id"
    static let repliedBy = "replied_by"
    static let reply = "reply"
    static let repliedOn = "replied_on"
  }

  // MARK: Properties
  public var accountId: String?
  public var repliedBy: String?
  public var reply: String?
  public var repliedOn: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    accountId = json[SerializationKeys.accountId].string
    repliedBy = json[SerializationKeys.repliedBy].string
    reply = json[SerializationKeys.reply].string
    repliedOn = json[SerializationKeys.repliedOn].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = accountId { dictionary[SerializationKeys.accountId] = value }
    if let value = repliedBy { dictionary[SerializationKeys.repliedBy] = value }
    if let value = reply { dictionary[SerializationKeys.reply] = value }
    if let value = repliedOn { dictionary[SerializationKeys.repliedOn] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.accountId = aDecoder.decodeObject(forKey: SerializationKeys.accountId) as? String
    self.repliedBy = aDecoder.decodeObject(forKey: SerializationKeys.repliedBy) as? String
    self.reply = aDecoder.decodeObject(forKey: SerializationKeys.reply) as? String
    self.repliedOn = aDecoder.decodeObject(forKey: SerializationKeys.repliedOn) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(accountId, forKey: SerializationKeys.accountId)
    aCoder.encode(repliedBy, forKey: SerializationKeys.repliedBy)
    aCoder.encode(reply, forKey: SerializationKeys.reply)
    aCoder.encode(repliedOn, forKey: SerializationKeys.repliedOn)
  }

}
