//
//  TRReview.swift
//
//  Created by iMac on 08/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TRReview: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let replies = "replies"
    static let orderId = "order_id"
    static let itemsReviews = "items_reviews"
    static let restrauntId = "restraunt_id"
    static let overallReview = "overall_review"
    static let resrtauntName = "resrtaunt_name"
    static let reviewId = "review_id"
    static let createdOn = "created_on"
    static let overallRatings = "overall_ratings"
    static let customerId = "customer_id"
  }

  // MARK: Properties
  public var replies: [Replies]?
  public var orderId: String?
  public var itemsReviews: [ItemsReviews]?
  public var restrauntId: String?
  public var overallReview: String?
  public var resrtauntName: String?
  public var reviewId: String?
  public var createdOn: String?
  public var overallRatings: String?
  public var customerId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.replies].array { replies = items.map { Replies(json: $0) } }
    orderId = json[SerializationKeys.orderId].string
    if let items = json[SerializationKeys.itemsReviews].array { itemsReviews = items.map { ItemsReviews(json: $0) } }
    restrauntId = json[SerializationKeys.restrauntId].string
    overallReview = json[SerializationKeys.overallReview].string
    resrtauntName = json[SerializationKeys.resrtauntName].string
    reviewId = json[SerializationKeys.reviewId].string
    createdOn = json[SerializationKeys.createdOn].string
    overallRatings = json[SerializationKeys.overallRatings].string
    customerId = json[SerializationKeys.customerId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = replies { dictionary[SerializationKeys.replies] = value.map { $0.dictionaryRepresentation() } }
    if let value = orderId { dictionary[SerializationKeys.orderId] = value }
    if let value = itemsReviews { dictionary[SerializationKeys.itemsReviews] = value.map { $0.dictionaryRepresentation() } }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = overallReview { dictionary[SerializationKeys.overallReview] = value }
    if let value = resrtauntName { dictionary[SerializationKeys.resrtauntName] = value }
    if let value = reviewId { dictionary[SerializationKeys.reviewId] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = overallRatings { dictionary[SerializationKeys.overallRatings] = value }
    if let value = customerId { dictionary[SerializationKeys.customerId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.replies = aDecoder.decodeObject(forKey: SerializationKeys.replies) as? [Replies]
    self.orderId = aDecoder.decodeObject(forKey: SerializationKeys.orderId) as? String
    self.itemsReviews = aDecoder.decodeObject(forKey: SerializationKeys.itemsReviews) as? [ItemsReviews]
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.overallReview = aDecoder.decodeObject(forKey: SerializationKeys.overallReview) as? String
    self.resrtauntName = aDecoder.decodeObject(forKey: SerializationKeys.resrtauntName) as? String
    self.reviewId = aDecoder.decodeObject(forKey: SerializationKeys.reviewId) as? String
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? String
    self.overallRatings = aDecoder.decodeObject(forKey: SerializationKeys.overallRatings) as? String
    self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(replies, forKey: SerializationKeys.replies)
    aCoder.encode(orderId, forKey: SerializationKeys.orderId)
    aCoder.encode(itemsReviews, forKey: SerializationKeys.itemsReviews)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(overallReview, forKey: SerializationKeys.overallReview)
    aCoder.encode(resrtauntName, forKey: SerializationKeys.resrtauntName)
    aCoder.encode(reviewId, forKey: SerializationKeys.reviewId)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(overallRatings, forKey: SerializationKeys.overallRatings)
    aCoder.encode(customerId, forKey: SerializationKeys.customerId)
  }

}
