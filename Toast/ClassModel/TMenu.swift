//
//  TMenu.swift
//
//  Created by iMac on 13/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TMenu: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let itemName = "item_name"
    static let itemType = "item_type"
    static let itemImageUrl = "item_image_url"
    static let containsEgg = "contains_egg"
    static let specialItem = "is_special"
    static let reviewsCount = "reviews_count"
    static let itemDescription = "item_description"
    static let restrauntId = "restraunt_id"
    static let itemPrice = "item_price"
    static let itemId = "item_id"
    static let itemDiscountPercentage = "item_discount_percentage"
    static let itemCategoryName = "item_category_name"
    static let itemCategoryId = "item_category_id"
    static let superCategoryName = "super_category_name"
    static let itemCount = "itemcount"
    static let itemVideoUrl = "item_video_url"
    static let addonDetails = "addon_details"
    static let tags = "tags"
    static let noOfTimesOrdered = "no_of_times_ordered"
    static let createdOn = "created_on"
    static let storeIds = "store_ids"
    static let itemPriceDetails = "item_price_details"
    static let uniqID = "uniqID"
    static let isLastAdded = "isLastAdded"
    static let selectedAddonItems = "SelectedAddonItems"
    static let selectedItemPriceDetails = "SelectedItemPriceDetails"
  }

    // MARK: Properties
    public var itemName: String?
    public var itemType: String?
    public var itemImageUrl: String?
    public var containsEgg: String?
    public var specialItem: String?
    public var reviewsCount: String?
    public var itemDescription: String?
    public var restrauntId: String?
    public var itemPrice: String?
    public var itemId: String?
    public var itemDiscountPercentage: String?
    public var itemCategoryName: String?
    public var itemCategoryId: String?
    public var superCategoryName: String?
    public var itemCount: String?
    public var itemVideoUrl: String?
    public var addonDetails: [AddonDetails]?
    public var tags: [String]?
    public var noOfTimesOrdered: Int?
    public var createdOn: Int?
    public var storeIds: [Any]?
    public var itemPriceDetails: [ItemPriceDetails]?
    public var uniqID: String?
    public var isLastAdded: String?
    public var selectedAddonItems: [AddonItems]?
    public var selectedItemPriceDetails: [ItemPriceDetails]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    itemName = json[SerializationKeys.itemName].string
    itemType = json[SerializationKeys.itemType].string
    itemImageUrl = json[SerializationKeys.itemImageUrl].string
    containsEgg = json[SerializationKeys.containsEgg].string
    specialItem = json[SerializationKeys.specialItem].string
    reviewsCount = json[SerializationKeys.reviewsCount].string
    itemDescription = json[SerializationKeys.itemDescription].string
    restrauntId = json[SerializationKeys.restrauntId].string
    itemPrice = json[SerializationKeys.itemPrice].string
    itemId = json[SerializationKeys.itemId].string
    itemDiscountPercentage = json[SerializationKeys.itemDiscountPercentage].string
    itemCategoryName = json[SerializationKeys.itemCategoryName].string
    itemCategoryId = json[SerializationKeys.itemCategoryId].string
    superCategoryName = json[SerializationKeys.superCategoryName].string
    itemCount = json[SerializationKeys.itemCount].string
    itemVideoUrl = json[SerializationKeys.itemVideoUrl].string
    if let items = json[SerializationKeys.addonDetails].array { addonDetails = items.map { AddonDetails(json: $0) } }
    if let items = json[SerializationKeys.tags].array { tags = items.map { $0.stringValue } }
    noOfTimesOrdered = json[SerializationKeys.noOfTimesOrdered].int
    createdOn = json[SerializationKeys.createdOn].int
    if let items = json[SerializationKeys.storeIds].array { storeIds = items.map { $0.object} }
    if let items = json[SerializationKeys.itemPriceDetails].array { itemPriceDetails = items.map { ItemPriceDetails(json: $0) } }
    uniqID = json[SerializationKeys.uniqID].string
    isLastAdded = json[SerializationKeys.isLastAdded].string
    if let items = json[SerializationKeys.selectedItemPriceDetails].array { selectedItemPriceDetails = items.map { ItemPriceDetails(json: $0) } }
    if let items = json[SerializationKeys.selectedAddonItems].array { selectedAddonItems = items.map { AddonItems(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = itemName { dictionary[SerializationKeys.itemName] = value }
    if let value = itemType { dictionary[SerializationKeys.itemType] = value }
    if let value = itemImageUrl { dictionary[SerializationKeys.itemImageUrl] = value }
    if let value = containsEgg { dictionary[SerializationKeys.containsEgg] = value }
    if let value = specialItem { dictionary[SerializationKeys.specialItem] = value }
    if let value = reviewsCount { dictionary[SerializationKeys.reviewsCount] = value }
    if let value = itemDescription { dictionary[SerializationKeys.itemDescription] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = itemPrice { dictionary[SerializationKeys.itemPrice] = value }
    if let value = itemId { dictionary[SerializationKeys.itemId] = value }
    if let value = itemDiscountPercentage { dictionary[SerializationKeys.itemDiscountPercentage] = value }
    if let value = itemCategoryName { dictionary[SerializationKeys.itemCategoryName] = value }
    if let value = itemCategoryId { dictionary[SerializationKeys.itemCategoryId] = value }
    if let value = superCategoryName { dictionary[SerializationKeys.superCategoryName] = value }
    if let value = itemCount { dictionary[SerializationKeys.itemCount] = value }
    if let value = itemVideoUrl { dictionary[SerializationKeys.itemVideoUrl] = value }
    if let value = addonDetails { dictionary[SerializationKeys.addonDetails] = value.map { $0.dictionaryRepresentation() } }
    if let value = tags { dictionary[SerializationKeys.tags] = value }
    if let value = noOfTimesOrdered { dictionary[SerializationKeys.noOfTimesOrdered] = value }
    if let value = createdOn { dictionary[SerializationKeys.createdOn] = value }
    if let value = storeIds { dictionary[SerializationKeys.storeIds] = value }
    if let value = itemPriceDetails { dictionary[SerializationKeys.itemPriceDetails] = value.map { $0.dictionaryRepresentation() } }
    if let value = uniqID { dictionary[SerializationKeys.uniqID] = value }
    if let value = isLastAdded { dictionary[SerializationKeys.isLastAdded] = value }
    if let value = selectedItemPriceDetails { dictionary[SerializationKeys.selectedItemPriceDetails] = value.map { $0.dictionaryRepresentation() } }
    if let value = selectedAddonItems { dictionary[SerializationKeys.selectedAddonItems] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.itemName = aDecoder.decodeObject(forKey: SerializationKeys.itemName) as? String
    self.itemType = aDecoder.decodeObject(forKey: SerializationKeys.itemType) as? String
    self.itemImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.itemImageUrl) as? String
    self.containsEgg = aDecoder.decodeObject(forKey: SerializationKeys.containsEgg) as? String
    self.specialItem = aDecoder.decodeObject(forKey: SerializationKeys.specialItem) as? String
    self.reviewsCount = aDecoder.decodeObject(forKey: SerializationKeys.reviewsCount) as? String
    self.itemDescription = aDecoder.decodeObject(forKey: SerializationKeys.itemDescription) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.itemPrice = aDecoder.decodeObject(forKey: SerializationKeys.itemPrice) as? String
    self.itemId = aDecoder.decodeObject(forKey: SerializationKeys.itemId) as? String
    self.itemDiscountPercentage = aDecoder.decodeObject(forKey: SerializationKeys.itemDiscountPercentage) as? String
    self.itemCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.itemCategoryName) as? String
    self.itemCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.itemCategoryId) as? String
    self.superCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.superCategoryName) as? String
    self.itemCount = aDecoder.decodeObject(forKey: SerializationKeys.itemCount) as? String
    self.itemVideoUrl = aDecoder.decodeObject(forKey: SerializationKeys.itemVideoUrl) as? String
    self.addonDetails = aDecoder.decodeObject(forKey: SerializationKeys.addonDetails) as? [AddonDetails]
    self.tags = aDecoder.decodeObject(forKey: SerializationKeys.tags) as? [String]
    self.noOfTimesOrdered = aDecoder.decodeObject(forKey: SerializationKeys.noOfTimesOrdered) as? Int
    self.createdOn = aDecoder.decodeObject(forKey: SerializationKeys.createdOn) as? Int
    self.storeIds = aDecoder.decodeObject(forKey: SerializationKeys.storeIds) as? [Any]
    self.itemPriceDetails = aDecoder.decodeObject(forKey: SerializationKeys.itemPriceDetails) as? [ItemPriceDetails]
    self.uniqID = aDecoder.decodeObject(forKey: SerializationKeys.uniqID) as? String
    self.isLastAdded = aDecoder.decodeObject(forKey: SerializationKeys.isLastAdded) as? String
    self.selectedItemPriceDetails = aDecoder.decodeObject(forKey: SerializationKeys.itemPriceDetails) as? [ItemPriceDetails]
    self.selectedAddonItems = aDecoder.decodeObject(forKey: SerializationKeys.itemPriceDetails) as? [AddonItems]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(itemName, forKey: SerializationKeys.itemName)
    aCoder.encode(itemType, forKey: SerializationKeys.itemType)
    aCoder.encode(itemImageUrl, forKey: SerializationKeys.itemImageUrl)
    aCoder.encode(containsEgg, forKey: SerializationKeys.containsEgg)
    aCoder.encode(specialItem, forKey: SerializationKeys.specialItem)
    aCoder.encode(reviewsCount, forKey: SerializationKeys.reviewsCount)
    aCoder.encode(itemDescription, forKey: SerializationKeys.itemDescription)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(itemPrice, forKey: SerializationKeys.itemPrice)
    aCoder.encode(itemId, forKey: SerializationKeys.itemId)
    aCoder.encode(itemDiscountPercentage, forKey: SerializationKeys.itemDiscountPercentage)
    aCoder.encode(itemCategoryName, forKey: SerializationKeys.itemCategoryName)
    aCoder.encode(itemCategoryId, forKey: SerializationKeys.itemCategoryId)
    aCoder.encode(superCategoryName, forKey: SerializationKeys.superCategoryName)
    aCoder.encode(itemCount, forKey: SerializationKeys.itemCount)
    aCoder.encode(itemVideoUrl, forKey: SerializationKeys.itemVideoUrl)
    aCoder.encode(addonDetails, forKey: SerializationKeys.addonDetails)
    aCoder.encode(tags, forKey: SerializationKeys.tags)
    aCoder.encode(noOfTimesOrdered, forKey: SerializationKeys.noOfTimesOrdered)
    aCoder.encode(createdOn, forKey: SerializationKeys.createdOn)
    aCoder.encode(storeIds, forKey: SerializationKeys.storeIds)
    aCoder.encode(itemPriceDetails, forKey: SerializationKeys.itemPriceDetails)
    aCoder.encode(uniqID, forKey: SerializationKeys.uniqID)
    aCoder.encode(isLastAdded, forKey: SerializationKeys.isLastAdded)
    aCoder.encode(selectedItemPriceDetails, forKey: SerializationKeys.selectedItemPriceDetails)
    aCoder.encode(selectedAddonItems, forKey: SerializationKeys.selectedAddonItems)
  }

}
