//
//  TNotificationSettings.swift
//
//  Created by iMac on 17/01/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TNotificationSettings: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let events = "events"
    static let toast = "toast"
    static let offers = "offers"
    static let customerId = "customer_id"
    static let all = "all"
  }

  // MARK: Properties
  public var events: Bool?
  public var toast: Bool?
  public var offers: Bool?
  public var customerId: Bool?
  public var all: Bool?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    events = json[SerializationKeys.events].bool
    toast = json[SerializationKeys.toast].bool
    offers = json[SerializationKeys.offers].bool
    customerId = json[SerializationKeys.customerId].bool
    all = json[SerializationKeys.all].bool
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = events { dictionary[SerializationKeys.events] = value }
    if let value = toast { dictionary[SerializationKeys.toast] = value }
    if let value = offers { dictionary[SerializationKeys.offers] = value }
    if let value = customerId { dictionary[SerializationKeys.customerId] = value }
    if let value = all { dictionary[SerializationKeys.all] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.events = aDecoder.decodeObject(forKey: SerializationKeys.events) as? Bool
    self.toast = aDecoder.decodeObject(forKey: SerializationKeys.toast) as? Bool
    self.offers = aDecoder.decodeObject(forKey: SerializationKeys.offers) as? Bool
    self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? Bool
    self.all = aDecoder.decodeObject(forKey: SerializationKeys.all) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(events, forKey: SerializationKeys.events)
    aCoder.encode(toast, forKey: SerializationKeys.toast)
    aCoder.encode(offers, forKey: SerializationKeys.offers)
    aCoder.encode(customerId, forKey: SerializationKeys.customerId)
    aCoder.encode(all, forKey: SerializationKeys.all)
  }

}
