//
//  AddonItems.swift
//
//  Created by iMac on 13/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AddonItems: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let addonPrice = "addon_price"
    static let parentRestrauntId = "parent_restraunt_id"
    static let containsEgg = "contains_egg"
    static let addonName = "addon_name"
    static let createdBy = "created_by"
    static let restrauntIdList = "restraunt_id_list"
    static let isDeleted = "is_deleted"
    static let addonCategoryName = "addon_category_name"
    static let addonType = "addon_type"
    static let addonCategoryId = "addon_category_id"
    static let addonId = "addon_id"
    static let itemID = "itemID"
    static let isLastAdded = "isLastAdded"
    static let uniqID = "uniqID"
  }

  // MARK: Properties
  public var addonPrice: [AddonPrice]?
  public var parentRestrauntId: String?
  public var containsEgg: Bool? = false
  public var addonName: String?
  public var createdBy: String?
  public var restrauntIdList: [String]?
  public var isDeleted: Bool? = false
  public var addonCategoryName: String?
  public var addonType: String?
  public var addonCategoryId: String?
  public var addonId: String?
  public var itemID: String?
  public var isLastAdded: String?
  public var uniqID: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.addonPrice].array { addonPrice = items.map { AddonPrice(json: $0) } }
    parentRestrauntId = json[SerializationKeys.parentRestrauntId].string
    containsEgg = json[SerializationKeys.containsEgg].boolValue
    addonName = json[SerializationKeys.addonName].string
    createdBy = json[SerializationKeys.createdBy].string
    if let items = json[SerializationKeys.restrauntIdList].array { restrauntIdList = items.map { $0.stringValue } }
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    addonCategoryName = json[SerializationKeys.addonCategoryName].string
    addonType = json[SerializationKeys.addonType].string
    addonCategoryId = json[SerializationKeys.addonCategoryId].string
    addonId = json[SerializationKeys.addonId].string
    itemID = json[SerializationKeys.itemID].string
    isLastAdded = json[SerializationKeys.isLastAdded].string
    uniqID = json[SerializationKeys.uniqID].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = addonPrice { dictionary[SerializationKeys.addonPrice] = value.map { $0.dictionaryRepresentation() } }
    if let value = parentRestrauntId { dictionary[SerializationKeys.parentRestrauntId] = value }
    dictionary[SerializationKeys.containsEgg] = containsEgg
    if let value = addonName { dictionary[SerializationKeys.addonName] = value }
    if let value = createdBy { dictionary[SerializationKeys.createdBy] = value }
    if let value = restrauntIdList { dictionary[SerializationKeys.restrauntIdList] = value }
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = addonCategoryName { dictionary[SerializationKeys.addonCategoryName] = value }
    if let value = addonType { dictionary[SerializationKeys.addonType] = value }
    if let value = addonCategoryId { dictionary[SerializationKeys.addonCategoryId] = value }
    if let value = addonId { dictionary[SerializationKeys.addonId] = value }
    if let value = itemID { dictionary[SerializationKeys.itemID] = value }
    if let value = isLastAdded { dictionary[SerializationKeys.isLastAdded] = value }
    if let value = uniqID { dictionary[SerializationKeys.uniqID] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.addonPrice = aDecoder.decodeObject(forKey: SerializationKeys.addonPrice) as? [AddonPrice]
    self.parentRestrauntId = aDecoder.decodeObject(forKey: SerializationKeys.parentRestrauntId) as? String
    self.containsEgg = aDecoder.decodeBool(forKey: SerializationKeys.containsEgg)
    self.addonName = aDecoder.decodeObject(forKey: SerializationKeys.addonName) as? String
    self.createdBy = aDecoder.decodeObject(forKey: SerializationKeys.createdBy) as? String
    self.restrauntIdList = aDecoder.decodeObject(forKey: SerializationKeys.restrauntIdList) as? [String]
    self.isDeleted = aDecoder.decodeBool(forKey: SerializationKeys.isDeleted)
    self.addonCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.addonCategoryName) as? String
    self.addonType = aDecoder.decodeObject(forKey: SerializationKeys.addonType) as? String
    self.addonCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.addonCategoryId) as? String
    self.addonId = aDecoder.decodeObject(forKey: SerializationKeys.addonId) as? String
    self.itemID = aDecoder.decodeObject(forKey: SerializationKeys.itemID) as? String
    self.isLastAdded = aDecoder.decodeObject(forKey: SerializationKeys.isLastAdded) as? String
    self.uniqID = aDecoder.decodeObject(forKey: SerializationKeys.uniqID) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(addonPrice, forKey: SerializationKeys.addonPrice)
    aCoder.encode(parentRestrauntId, forKey: SerializationKeys.parentRestrauntId)
    aCoder.encode(containsEgg, forKey: SerializationKeys.containsEgg)
    aCoder.encode(addonName, forKey: SerializationKeys.addonName)
    aCoder.encode(createdBy, forKey: SerializationKeys.createdBy)
    aCoder.encode(restrauntIdList, forKey: SerializationKeys.restrauntIdList)
    aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
    aCoder.encode(addonCategoryName, forKey: SerializationKeys.addonCategoryName)
    aCoder.encode(addonType, forKey: SerializationKeys.addonType)
    aCoder.encode(addonCategoryId, forKey: SerializationKeys.addonCategoryId)
    aCoder.encode(addonId, forKey: SerializationKeys.addonId)
    aCoder.encode(itemID, forKey: SerializationKeys.itemID)
    aCoder.encode(isLastAdded, forKey: SerializationKeys.isLastAdded)
    aCoder.encode(uniqID, forKey: SerializationKeys.uniqID)
  }

}
