//
//  TRecipeDetail.swift
//
//  Created by iMac on 06/05/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TRecipeDetail: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cookingMins = "cooking_mins"
    static let preparationHours = "preparation_hours"
    static let ingredients = "ingredients"
    static let instructions = "instructions"
    static let likesCount = "likes_count"
    static let name = "name"
    static let descriptionValue = "description"
    static let preparationMins = "preparation_mins"
    static let rating = "rating"
    static let cookingHours = "cooking_hours"
    static let recipeType = "recipe_type"
    static let isLiked = "is_liked"
    static let likeId = "like_id"
    static let publisher = "publisher"
    static let imagesUrls = "images_urls"
    static let recipeYield = "recipe_yield"
    static let recipeId = "recipe_id"
    static let comments = "comments"
  }

  // MARK: Properties
  public var cookingMins: String?
  public var preparationHours: String?
  public var ingredients: [TIngredients]?
  public var instructions: [String]?
  public var likesCount: String?
  public var name: String?
  public var descriptionValue: String?
  public var preparationMins: String?
  public var rating: String?
  public var cookingHours: String?
  public var recipeType: String?
  public var isLiked: String?
  public var likeId: String?
  public var publisher: TPublisher?
  public var imagesUrls: [String]?
  public var recipeYield: String?
  public var recipeId: String?
  public var comments: [TComments]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    cookingMins = json[SerializationKeys.cookingMins].string
    preparationHours = json[SerializationKeys.preparationHours].string
    if let items = json[SerializationKeys.ingredients].array { ingredients = items.map { TIngredients(json: $0) } }
    if let items = json[SerializationKeys.instructions].array { instructions = items.map { $0.stringValue } }
    likesCount = json[SerializationKeys.likesCount].string
    name = json[SerializationKeys.name].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    preparationMins = json[SerializationKeys.preparationMins].string
    rating = json[SerializationKeys.rating].string
    cookingHours = json[SerializationKeys.cookingHours].string
    recipeType = json[SerializationKeys.recipeType].string
    isLiked = json[SerializationKeys.isLiked].string
    likeId = json[SerializationKeys.likeId].string
    publisher = TPublisher(json: json[SerializationKeys.publisher])
    if let items = json[SerializationKeys.imagesUrls].array { imagesUrls = items.map { $0.stringValue } }
    recipeYield = json[SerializationKeys.recipeYield].string
    recipeId = json[SerializationKeys.recipeId].string
    if let items = json[SerializationKeys.comments].array { comments = items.map { TComments(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cookingMins { dictionary[SerializationKeys.cookingMins] = value }
    if let value = preparationHours { dictionary[SerializationKeys.preparationHours] = value }
    if let value = ingredients { dictionary[SerializationKeys.ingredients] = value.map { $0.dictionaryRepresentation() } }
    if let value = instructions { dictionary[SerializationKeys.instructions] = value }
    if let value = likesCount { dictionary[SerializationKeys.likesCount] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = preparationMins { dictionary[SerializationKeys.preparationMins] = value }
    if let value = rating { dictionary[SerializationKeys.rating] = value }
    if let value = cookingHours { dictionary[SerializationKeys.cookingHours] = value }
    if let value = recipeType { dictionary[SerializationKeys.recipeType] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    if let value = likeId { dictionary[SerializationKeys.likeId] = value }
    if let value = publisher { dictionary[SerializationKeys.publisher] = value.dictionaryRepresentation() }
    if let value = imagesUrls { dictionary[SerializationKeys.imagesUrls] = value }
    if let value = recipeYield { dictionary[SerializationKeys.recipeYield] = value }
    if let value = recipeId { dictionary[SerializationKeys.recipeId] = value }
    if let value = comments { dictionary[SerializationKeys.comments] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cookingMins = aDecoder.decodeObject(forKey: SerializationKeys.cookingMins) as? String
    self.preparationHours = aDecoder.decodeObject(forKey: SerializationKeys.preparationHours) as? String
    self.ingredients = aDecoder.decodeObject(forKey: SerializationKeys.ingredients) as? [TIngredients]
    self.instructions = aDecoder.decodeObject(forKey: SerializationKeys.instructions) as? [String]
    self.likesCount = aDecoder.decodeObject(forKey: SerializationKeys.likesCount) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.preparationMins = aDecoder.decodeObject(forKey: SerializationKeys.preparationMins) as? String
    self.rating = aDecoder.decodeObject(forKey: SerializationKeys.rating) as? String
    self.cookingHours = aDecoder.decodeObject(forKey: SerializationKeys.cookingHours) as? String
    self.recipeType = aDecoder.decodeObject(forKey: SerializationKeys.recipeType) as? String
    self.isLiked = aDecoder.decodeObject(forKey: SerializationKeys.isLiked) as? String
    self.likeId = aDecoder.decodeObject(forKey: SerializationKeys.likeId) as? String
    self.publisher = aDecoder.decodeObject(forKey: SerializationKeys.publisher) as? TPublisher
    self.imagesUrls = aDecoder.decodeObject(forKey: SerializationKeys.imagesUrls) as? [String]
    self.recipeYield = aDecoder.decodeObject(forKey: SerializationKeys.recipeYield) as? String
    self.recipeId = aDecoder.decodeObject(forKey: SerializationKeys.recipeId) as? String
    self.comments = aDecoder.decodeObject(forKey: SerializationKeys.comments) as? [TComments]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cookingMins, forKey: SerializationKeys.cookingMins)
    aCoder.encode(preparationHours, forKey: SerializationKeys.preparationHours)
    aCoder.encode(ingredients, forKey: SerializationKeys.ingredients)
    aCoder.encode(instructions, forKey: SerializationKeys.instructions)
    aCoder.encode(likesCount, forKey: SerializationKeys.likesCount)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(preparationMins, forKey: SerializationKeys.preparationMins)
    aCoder.encode(rating, forKey: SerializationKeys.rating)
    aCoder.encode(cookingHours, forKey: SerializationKeys.cookingHours)
    aCoder.encode(recipeType, forKey: SerializationKeys.recipeType)
    aCoder.encode(isLiked, forKey: SerializationKeys.isLiked)
    aCoder.encode(likeId, forKey: SerializationKeys.likeId)
    aCoder.encode(publisher, forKey: SerializationKeys.publisher)
    aCoder.encode(imagesUrls, forKey: SerializationKeys.imagesUrls)
    aCoder.encode(recipeYield, forKey: SerializationKeys.recipeYield)
    aCoder.encode(recipeId, forKey: SerializationKeys.recipeId)
    aCoder.encode(comments, forKey: SerializationKeys.comments)
  }

}
