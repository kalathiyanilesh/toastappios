//
//  LikeRecipeModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 5, 2020

import Foundation

struct LikeRecipeModel : Codable {

        let likeId : String?
        let publisher : Publisher?
        let recipeId : String?

        enum CodingKeys: String, CodingKey {
                case likeId = "like_id"
                case publisher = "publisher"
                case recipeId = "recipe_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                likeId = try values.decodeIfPresent(String.self, forKey: .likeId)
                publisher = try values.decodeIfPresent(Publisher.self, forKey: .publisher)
                recipeId = try values.decodeIfPresent(String.self, forKey: .recipeId)
        }

}
