//
//  TPublisher.swift
//
//  Created by iMac on 06/05/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TPublisher: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let avatarUrl = "avatar_url"
    static let name = "name"
    static let userId = "user_id"
    static let isverified = "is_verified"
    static let verifiedtype = "verified_type"
  }

  // MARK: Properties
  public var avatarUrl: String?
  public var name: String?
  public var userId: String?
  public var isverified: String?
  public var verifiedtype: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    avatarUrl = json[SerializationKeys.avatarUrl].string
    name = json[SerializationKeys.name].string
    userId = json[SerializationKeys.userId].string
    isverified = json[SerializationKeys.isverified].string
    verifiedtype = json[SerializationKeys.verifiedtype].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = avatarUrl { dictionary[SerializationKeys.avatarUrl] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = isverified { dictionary[SerializationKeys.isverified] = value }
    if let value = verifiedtype { dictionary[SerializationKeys.verifiedtype] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.avatarUrl = aDecoder.decodeObject(forKey: SerializationKeys.avatarUrl) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.isverified = aDecoder.decodeObject(forKey: SerializationKeys.isverified) as? String
    self.verifiedtype = aDecoder.decodeObject(forKey: SerializationKeys.verifiedtype) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(avatarUrl, forKey: SerializationKeys.avatarUrl)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(isverified, forKey: SerializationKeys.isverified)
    aCoder.encode(verifiedtype, forKey: SerializationKeys.verifiedtype)
  }

}
