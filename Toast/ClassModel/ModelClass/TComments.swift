//
//  TComments.swift
//
//  Created by iMac on 06/05/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TComments: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let publisher = "publisher"
    static let commentId = "comment_id"
    static let createdAt = "created_at"
  }

  // MARK: Properties
  public var content: String?
  public var publisher: TPublisher?
  public var commentId: String?
  public var createdAt: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    publisher = TPublisher(json: json[SerializationKeys.publisher])
    commentId = json[SerializationKeys.commentId].string
    createdAt = json[SerializationKeys.createdAt].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = publisher { dictionary[SerializationKeys.publisher] = value.dictionaryRepresentation() }
    if let value = commentId { dictionary[SerializationKeys.commentId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.publisher = aDecoder.decodeObject(forKey: SerializationKeys.publisher) as? TPublisher
    self.commentId = aDecoder.decodeObject(forKey: SerializationKeys.commentId) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(publisher, forKey: SerializationKeys.publisher)
    aCoder.encode(commentId, forKey: SerializationKeys.commentId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
