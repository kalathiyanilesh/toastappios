//
//  Publisher.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 5, 2020

import Foundation

struct Publisher : Codable {

        let avatarUrl : String?
        let name : String?
        let userId : String?

        enum CodingKeys: String, CodingKey {
                case avatarUrl = "avatar_url"
                case name = "name"
                case userId = "user_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                avatarUrl = try values.decodeIfPresent(String.self, forKey: .avatarUrl)
                name = try values.decodeIfPresent(String.self, forKey: .name)
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
        }

}
