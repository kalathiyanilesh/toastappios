//
//  TIngredients.swift
//
//  Created by iMac on 06/05/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TIngredients: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let unit = "unit"
    static let name = "name"
    static let qty = "qty"
  }

  // MARK: Properties
  public var unit: String?
  public var name: String?
  public var qty: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    unit = json[SerializationKeys.unit].string
    name = json[SerializationKeys.name].string
    qty = json[SerializationKeys.qty].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = unit { dictionary[SerializationKeys.unit] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = qty { dictionary[SerializationKeys.qty] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.unit = aDecoder.decodeObject(forKey: SerializationKeys.unit) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.qty = aDecoder.decodeObject(forKey: SerializationKeys.qty) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(unit, forKey: SerializationKeys.unit)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(qty, forKey: SerializationKeys.qty)
  }

}
