//
//  TItemReview.swift
//
//  Created by iMac on 24/01/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TItemReview: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let itemReviewImgList = "item_review_img_list"
    static let itemReview = "item_review"
    static let customerProfilePicture = "customer_profile_picture"
    static let itemRating = "item_rating"
    static let itemId = "item_id"
    static let customerName = "customer_name"
    static let customerId = "customer_id"
    static let customerLevel = "customer_level"
  }

  // MARK: Properties
  public var itemReviewImgList: [String]?
  public var itemReview: String?
  public var customerProfilePicture: String?
  public var itemRating: String?
  public var itemId: String?
  public var customerName: String?
  public var customerId: String?
  public var customerLevel: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.itemReviewImgList].array { itemReviewImgList = items.map { $0.stringValue } }
    itemReview = json[SerializationKeys.itemReview].string
    customerProfilePicture = json[SerializationKeys.customerProfilePicture].string
    itemRating = json[SerializationKeys.itemRating].string
    itemId = json[SerializationKeys.itemId].string
    customerName = json[SerializationKeys.customerName].string
    customerId = json[SerializationKeys.customerId].string
    customerLevel = json[SerializationKeys.customerLevel].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = itemReviewImgList { dictionary[SerializationKeys.itemReviewImgList] = value }
    if let value = itemReview { dictionary[SerializationKeys.itemReview] = value }
    if let value = customerProfilePicture { dictionary[SerializationKeys.customerProfilePicture] = value }
    if let value = itemRating { dictionary[SerializationKeys.itemRating] = value }
    if let value = itemId { dictionary[SerializationKeys.itemId] = value }
    if let value = customerName { dictionary[SerializationKeys.customerName] = value }
    if let value = customerId { dictionary[SerializationKeys.customerId] = value }
    if let value = customerLevel { dictionary[SerializationKeys.customerLevel] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.itemReviewImgList = aDecoder.decodeObject(forKey: SerializationKeys.itemReviewImgList) as? [String]
    self.itemReview = aDecoder.decodeObject(forKey: SerializationKeys.itemReview) as? String
    self.customerProfilePicture = aDecoder.decodeObject(forKey: SerializationKeys.customerProfilePicture) as? String
    self.itemRating = aDecoder.decodeObject(forKey: SerializationKeys.itemRating) as? String
    self.itemId = aDecoder.decodeObject(forKey: SerializationKeys.itemId) as? String
    self.customerName = aDecoder.decodeObject(forKey: SerializationKeys.customerName) as? String
    self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? String
    self.customerLevel = aDecoder.decodeObject(forKey: SerializationKeys.customerLevel) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(itemReviewImgList, forKey: SerializationKeys.itemReviewImgList)
    aCoder.encode(itemReview, forKey: SerializationKeys.itemReview)
    aCoder.encode(customerProfilePicture, forKey: SerializationKeys.customerProfilePicture)
    aCoder.encode(itemRating, forKey: SerializationKeys.itemRating)
    aCoder.encode(itemId, forKey: SerializationKeys.itemId)
    aCoder.encode(customerName, forKey: SerializationKeys.customerName)
    aCoder.encode(customerId, forKey: SerializationKeys.customerId)
    aCoder.encode(customerId, forKey: SerializationKeys.customerLevel)
  }

}
