//
//  TRestaurantDetails.swift
//
//  Created by iMac on 23/01/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TRestaurantDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let restrauntName = "restraunt_name"
    static let restrauntId = "restraunt_id"
    static let restrauntImage = "restraunt_image"
    static let featureImage = "feature_image"
    static let restrauntStoreDetails = "restraunt_store_details"
  }

  // MARK: Properties
  public var imgList: [Any]?
  public var restrauntName: String?
  public var restrauntId: String?
  public var restrauntStoreDetails: RestrauntStoreDetails?
  public var restrauntImage: String?
  public var featureImage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    restrauntName = json[SerializationKeys.restrauntName].string
    restrauntId = json[SerializationKeys.restrauntId].string
    restrauntStoreDetails = RestrauntStoreDetails(json: json[SerializationKeys.restrauntStoreDetails])
    restrauntImage = json[SerializationKeys.restrauntImage].string
    featureImage = json[SerializationKeys.featureImage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = restrauntName { dictionary[SerializationKeys.restrauntName] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = restrauntImage { dictionary[SerializationKeys.restrauntImage] = value }
    if let value = featureImage { dictionary[SerializationKeys.featureImage] = value }
    if let value = restrauntStoreDetails { dictionary[SerializationKeys.restrauntStoreDetails] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.restrauntName = aDecoder.decodeObject(forKey: SerializationKeys.restrauntName) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.restrauntImage = aDecoder.decodeObject(forKey: SerializationKeys.restrauntImage) as? String
    self.featureImage = aDecoder.decodeObject(forKey: SerializationKeys.featureImage) as? String
    self.restrauntStoreDetails = aDecoder.decodeObject(forKey: SerializationKeys.restrauntStoreDetails) as? RestrauntStoreDetails
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(restrauntName, forKey: SerializationKeys.restrauntName)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntImage)
    aCoder.encode(restrauntId, forKey: SerializationKeys.featureImage)
    aCoder.encode(restrauntStoreDetails, forKey: SerializationKeys.restrauntStoreDetails)
  }

}
