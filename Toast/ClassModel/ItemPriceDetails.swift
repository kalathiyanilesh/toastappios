//
//  ItemPriceDetails.swift
//
//  Created by iMac on 13/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ItemPriceDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let itemBasePrice = "item_base_price"
    static let itemPrice = "item_price"
    static let itemSize = "item_size"
    static let itemID = "itemID"
    static let uniqID = "uniqID"
  }

  // MARK: Properties
  public var itemBasePrice: String?
  public var itemPrice: String?
  public var itemSize: String?
  public var itemID: String?
  public var uniqID: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    itemBasePrice = json[SerializationKeys.itemBasePrice].string
    itemPrice = json[SerializationKeys.itemPrice].string
    itemSize = json[SerializationKeys.itemSize].string
    itemID = json[SerializationKeys.itemID].string
    uniqID = json[SerializationKeys.uniqID].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = itemBasePrice { dictionary[SerializationKeys.itemBasePrice] = value }
    if let value = itemPrice { dictionary[SerializationKeys.itemPrice] = value }
    if let value = itemSize { dictionary[SerializationKeys.itemSize] = value }
    if let value = itemID { dictionary[SerializationKeys.itemID] = value }
    if let value = uniqID { dictionary[SerializationKeys.uniqID] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.itemBasePrice = aDecoder.decodeObject(forKey: SerializationKeys.itemBasePrice) as? String
    self.itemPrice = aDecoder.decodeObject(forKey: SerializationKeys.itemPrice) as? String
    self.itemSize = aDecoder.decodeObject(forKey: SerializationKeys.itemSize) as? String
    self.itemID = aDecoder.decodeObject(forKey: SerializationKeys.itemID) as? String
    self.uniqID = aDecoder.decodeObject(forKey: SerializationKeys.uniqID) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(itemBasePrice, forKey: SerializationKeys.itemBasePrice)
    aCoder.encode(itemPrice, forKey: SerializationKeys.itemPrice)
    aCoder.encode(itemSize, forKey: SerializationKeys.itemSize)
    aCoder.encode(itemID, forKey: SerializationKeys.itemID)
    aCoder.encode(uniqID, forKey: SerializationKeys.uniqID)
  }

}
