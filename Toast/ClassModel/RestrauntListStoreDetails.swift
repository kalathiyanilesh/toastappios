//
//  RestrauntStoreDetails.swift
//
//  Created by iMac on 19/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RestrauntListStoreDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let state = "state"
    static let city = "city"
    static let delivery = "delivery"
    static let deliveryKmRadius = "delivery_km_radius"
    static let dineIn = "dine_in"
    static let pickUp = "pick_up"
    static let add2 = "add2"
    static let lat = "lat"
    static let pincode = "pincode"
    static let restrauntId = "restraunt_id"
    static let add1 = "add1"
    static let lon = "lon"
    static let selfDelivery = "self_delivery"
    static let area = "area"
    static let country = "country"
  }

  // MARK: Properties
  public var state: String?
  public var city: String?
  public var delivery: Bool? = false
  public var deliveryKmRadius: String?
  public var dineIn: Bool? = false
  public var pickUp: Bool? = false
  public var add2: String?
  public var lat: String?
  public var pincode: String?
  public var restrauntId: String?
  public var add1: String?
  public var lon: String?
  public var selfDelivery: Bool? = false
  public var area: String?
  public var country: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    state = json[SerializationKeys.state].string
    city = json[SerializationKeys.city].string
    delivery = json[SerializationKeys.delivery].boolValue
    deliveryKmRadius = json[SerializationKeys.deliveryKmRadius].string
    dineIn = json[SerializationKeys.dineIn].boolValue
    pickUp = json[SerializationKeys.pickUp].boolValue
    add2 = json[SerializationKeys.add2].string
    lat = json[SerializationKeys.lat].string
    pincode = json[SerializationKeys.pincode].string
    restrauntId = json[SerializationKeys.restrauntId].string
    add1 = json[SerializationKeys.add1].string
    lon = json[SerializationKeys.lon].string
    selfDelivery = json[SerializationKeys.selfDelivery].boolValue
    area = json[SerializationKeys.area].string
    country = json[SerializationKeys.country].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = state { dictionary[SerializationKeys.state] = value }
    if let value = city { dictionary[SerializationKeys.city] = value }
    dictionary[SerializationKeys.delivery] = delivery
    if let value = deliveryKmRadius { dictionary[SerializationKeys.deliveryKmRadius] = value }
    dictionary[SerializationKeys.dineIn] = dineIn
    dictionary[SerializationKeys.pickUp] = pickUp
    if let value = add2 { dictionary[SerializationKeys.add2] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = pincode { dictionary[SerializationKeys.pincode] = value }
    if let value = restrauntId { dictionary[SerializationKeys.restrauntId] = value }
    if let value = add1 { dictionary[SerializationKeys.add1] = value }
    if let value = lon { dictionary[SerializationKeys.lon] = value }
    dictionary[SerializationKeys.selfDelivery] = selfDelivery
    if let value = area { dictionary[SerializationKeys.area] = value }
    if let value = country { dictionary[SerializationKeys.country] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.state = aDecoder.decodeObject(forKey: SerializationKeys.state) as? String
    self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
    self.delivery = aDecoder.decodeBool(forKey: SerializationKeys.delivery)
    self.deliveryKmRadius = aDecoder.decodeObject(forKey: SerializationKeys.deliveryKmRadius) as? String
    self.dineIn = aDecoder.decodeBool(forKey: SerializationKeys.dineIn)
    self.pickUp = aDecoder.decodeBool(forKey: SerializationKeys.pickUp)
    self.add2 = aDecoder.decodeObject(forKey: SerializationKeys.add2) as? String
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? String
    self.pincode = aDecoder.decodeObject(forKey: SerializationKeys.pincode) as? String
    self.restrauntId = aDecoder.decodeObject(forKey: SerializationKeys.restrauntId) as? String
    self.add1 = aDecoder.decodeObject(forKey: SerializationKeys.add1) as? String
    self.lon = aDecoder.decodeObject(forKey: SerializationKeys.lon) as? String
    self.selfDelivery = aDecoder.decodeBool(forKey: SerializationKeys.selfDelivery)
    self.area = aDecoder.decodeObject(forKey: SerializationKeys.area) as? String
    self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(state, forKey: SerializationKeys.state)
    aCoder.encode(city, forKey: SerializationKeys.city)
    aCoder.encode(delivery, forKey: SerializationKeys.delivery)
    aCoder.encode(deliveryKmRadius, forKey: SerializationKeys.deliveryKmRadius)
    aCoder.encode(dineIn, forKey: SerializationKeys.dineIn)
    aCoder.encode(pickUp, forKey: SerializationKeys.pickUp)
    aCoder.encode(add2, forKey: SerializationKeys.add2)
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(pincode, forKey: SerializationKeys.pincode)
    aCoder.encode(restrauntId, forKey: SerializationKeys.restrauntId)
    aCoder.encode(add1, forKey: SerializationKeys.add1)
    aCoder.encode(lon, forKey: SerializationKeys.lon)
    aCoder.encode(selfDelivery, forKey: SerializationKeys.selfDelivery)
    aCoder.encode(area, forKey: SerializationKeys.area)
    aCoder.encode(country, forKey: SerializationKeys.country)
  }

}
