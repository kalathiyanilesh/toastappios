//
//  AddonDetails.swift
//
//  Created by iMac on 13/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AddonDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let addonItems = "addon_items"
    static let addonCategoryName = "addon_category_name"
    static let addonCateogryId = "addon_cateogry_id"
    static let addonLimit = "addon_limit"
    static let addonLimitOptional = "addon_limit_optional"
  }

  // MARK: Properties
  public var addonItems: [AddonItems]?
  public var addonCategoryName: String?
  public var addonCateogryId: String?
  public var addonLimit: String?
  public var addonLimitOptional: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.addonItems].array { addonItems = items.map { AddonItems(json: $0) } }
    addonCategoryName = json[SerializationKeys.addonCategoryName].string
    addonCateogryId = json[SerializationKeys.addonCateogryId].string
    addonLimit = json[SerializationKeys.addonLimit].string
    addonLimitOptional = json[SerializationKeys.addonLimitOptional].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = addonItems { dictionary[SerializationKeys.addonItems] = value.map { $0.dictionaryRepresentation() } }
    if let value = addonCategoryName { dictionary[SerializationKeys.addonCategoryName] = value }
    if let value = addonCateogryId { dictionary[SerializationKeys.addonCateogryId] = value }
    if let value = addonLimit { dictionary[SerializationKeys.addonLimit] = value }
    if let value = addonLimitOptional { dictionary[SerializationKeys.addonLimitOptional] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.addonItems = aDecoder.decodeObject(forKey: SerializationKeys.addonItems) as? [AddonItems]
    self.addonCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.addonCategoryName) as? String
    self.addonCateogryId = aDecoder.decodeObject(forKey: SerializationKeys.addonCateogryId) as? String
    self.addonLimit = aDecoder.decodeObject(forKey: SerializationKeys.addonLimit) as? String
    self.addonLimitOptional = aDecoder.decodeObject(forKey: SerializationKeys.addonLimitOptional) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(addonItems, forKey: SerializationKeys.addonItems)
    aCoder.encode(addonCategoryName, forKey: SerializationKeys.addonCategoryName)
    aCoder.encode(addonCateogryId, forKey: SerializationKeys.addonCateogryId)
    aCoder.encode(addonLimit, forKey: SerializationKeys.addonLimit)
    aCoder.encode(addonLimitOptional, forKey: SerializationKeys.addonLimitOptional)
  }

}
