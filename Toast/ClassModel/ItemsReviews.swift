//
//  ItemsReviews.swift
//
//  Created by iMac on 08/02/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ItemsReviews: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let itemType = "item_type"
    static let itemReview = "item_review"
    static let itemName = "item_name"
    static let itemReviewImgList = "item_review_img_list"
    static let itemRating = "item_rating"
    static let itemId = "item_id"
    static let containsEgg = "contains_egg"
  }

  // MARK: Properties
  public var itemType: String?
  public var itemReview: String?
  public var itemName: String?
  public var itemReviewImgList: [Any]?
  public var itemRating: String?
  public var itemId: String?
  public var containsEgg: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    itemType = json[SerializationKeys.itemType].string
    itemReview = json[SerializationKeys.itemReview].string
    itemName = json[SerializationKeys.itemName].string
    if let items = json[SerializationKeys.itemReviewImgList].array { itemReviewImgList = items.map { $0.object} }
    itemRating = json[SerializationKeys.itemRating].string
    itemId = json[SerializationKeys.itemId].string
    containsEgg = json[SerializationKeys.containsEgg].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = itemType { dictionary[SerializationKeys.itemType] = value }
    if let value = itemReview { dictionary[SerializationKeys.itemReview] = value }
    if let value = itemName { dictionary[SerializationKeys.itemName] = value }
    if let value = itemReviewImgList { dictionary[SerializationKeys.itemReviewImgList] = value }
    if let value = itemRating { dictionary[SerializationKeys.itemRating] = value }
    if let value = itemId { dictionary[SerializationKeys.itemId] = value }
    if let value = containsEgg { dictionary[SerializationKeys.containsEgg] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.itemType = aDecoder.decodeObject(forKey: SerializationKeys.itemType) as? String
    self.itemReview = aDecoder.decodeObject(forKey: SerializationKeys.itemReview) as? String
    self.itemName = aDecoder.decodeObject(forKey: SerializationKeys.itemName) as? String
    self.itemReviewImgList = aDecoder.decodeObject(forKey: SerializationKeys.itemReviewImgList) as? [Any]
    self.itemRating = aDecoder.decodeObject(forKey: SerializationKeys.itemRating) as? String
    self.itemId = aDecoder.decodeObject(forKey: SerializationKeys.itemId) as? String
    self.containsEgg = aDecoder.decodeObject(forKey: SerializationKeys.containsEgg) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(itemType, forKey: SerializationKeys.itemType)
    aCoder.encode(itemReview, forKey: SerializationKeys.itemReview)
    aCoder.encode(itemName, forKey: SerializationKeys.itemName)
    aCoder.encode(itemReviewImgList, forKey: SerializationKeys.itemReviewImgList)
    aCoder.encode(itemRating, forKey: SerializationKeys.itemRating)
    aCoder.encode(itemId, forKey: SerializationKeys.itemId)
    aCoder.encode(containsEgg, forKey: SerializationKeys.containsEgg)
  }

}
