//
//  TRestrauntList.swift
//
//  Created by iMac on 28/06/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TRestrauntList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let restrauntStoreDetails = "restraunt_store_details"
    static let restrauntImage = "restraunt_image"
    static let featureImage = "feature_image"
    static let distanceKm = "distance_km"
    static let restrauntName = "restraunt_name"
  }

  // MARK: Properties
  public var descriptionValue: String?
  public var restrauntStoreDetails: RestrauntStoreDetails?
  public var restrauntImage: String?
  public var featureImage: String?
  public var distanceKm: String?
  public var restrauntName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    descriptionValue = json[SerializationKeys.descriptionValue].string
    restrauntStoreDetails = RestrauntStoreDetails(json: json[SerializationKeys.restrauntStoreDetails])
    restrauntImage = json[SerializationKeys.restrauntImage].string
    featureImage = json[SerializationKeys.featureImage].string
    distanceKm = json[SerializationKeys.distanceKm].string
    restrauntName = json[SerializationKeys.restrauntName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = restrauntStoreDetails { dictionary[SerializationKeys.restrauntStoreDetails] = value.dictionaryRepresentation() }
    if let value = restrauntImage { dictionary[SerializationKeys.restrauntImage] = value }
    if let value = featureImage { dictionary[SerializationKeys.featureImage] = value }
    if let value = distanceKm { dictionary[SerializationKeys.distanceKm] = value }
    if let value = restrauntName { dictionary[SerializationKeys.restrauntName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.restrauntStoreDetails = aDecoder.decodeObject(forKey: SerializationKeys.restrauntStoreDetails) as? RestrauntStoreDetails
    self.restrauntImage = aDecoder.decodeObject(forKey: SerializationKeys.restrauntImage) as? String
    self.featureImage = aDecoder.decodeObject(forKey: SerializationKeys.featureImage) as? String
    self.distanceKm = aDecoder.decodeObject(forKey: SerializationKeys.distanceKm) as? String
    self.restrauntName = aDecoder.decodeObject(forKey: SerializationKeys.restrauntName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(restrauntStoreDetails, forKey: SerializationKeys.restrauntStoreDetails)
    aCoder.encode(restrauntImage, forKey: SerializationKeys.restrauntImage)
    aCoder.encode(featureImage, forKey: SerializationKeys.featureImage)
    aCoder.encode(distanceKm, forKey: SerializationKeys.distanceKm)
    aCoder.encode(restrauntName, forKey: SerializationKeys.restrauntName)
  }

}
