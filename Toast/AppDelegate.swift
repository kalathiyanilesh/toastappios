//
//  AppDelegate.swift
//  Toast
//
//  Created by iMac on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

//26062020
import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import Firebase
import Pushy
import GLNotificationBar
import CoreLocation
import GoogleMaps
import Siren

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var strScannedQR: String = ""
    var isPostNotification: Bool = false
    var isPostMenuSetting: Bool = false
    var strDefualtUserName = ""
    var isFromLaunching: Bool = false
    
    var isOnScanPage: Bool = false
    var isFromTakeAway: Bool = false
    var isFromDelivery: Bool = false
    
    //for menu
    var strCurrentMenu = ""
    var isFoodVeg: Bool = false
    var isContainsEgg: Bool = false
    var dictMenuSection = NSMutableDictionary()
    var arrCartItems = NSMutableArray()
    var sOrderID: String = ""
    var objRestaurantDetails: TRestaurantDetails?
    
    //for notification
    var isOnMenu: Bool = false
    var isOnBillingDetailsPage: Bool = false
    var isPostNoti: Bool = false
    var isFromLaunchPushClick: Bool = false
    var isOnNotificationTab: Bool = false
    var isOnRatingPage: Bool = false
    var NotiPageIndex: Int = -1
    var isOnEvents: Bool = false
    var isOnOffers: Bool = false
    var isOnToast: Bool = false
    var isShowDisZomPopup: Bool = false
    var isInHomeScreen: Bool = false
    var isInActiveOrder: Bool = false
    var isInOrderSuccess: Bool = false
    var PushTitle: String = ""
    var PushMessage: String = ""
    var PushModule: String = ""
    var isShowNotificationPopup: Bool = false
    var restaurantID: String = ""
    
    var locationManager = CLLocationManager()
    var strCurrentCity: String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        Siren.shared.wail()
        
        IQKeyboardManager.shared.enable = true
        
        if (UserDefaults.standard.object(forKey: "useroriginalcity") != nil) {
            strCurrentCity = UserDefaults.standard.object(forKey: "useroriginalcity") as? String ?? ""
        }
        if (UserDefaults.standard.object(forKey: "selectedCityLat") != nil) {
            getLatLongFromCache()
        }
        
        GMSServices.provideAPIKey("AIzaSyDaY3dktlddOuAFvM3Hg1U4tXorzvma0mQ")
        
        //PUSH NOTIFICATION
        PushNotification()
        
        //GET CURRENT LOCATION
        GetCurrentLocation()
        
        //FIREBASE
        let filePath = Bundle.main.path(forResource: "GoogleService-Info_FIREBASE", ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        
        if let remoteNotification = launchOptions?[.remoteNotification] as?  [AnyHashable : Any] {
            print(remoteNotification)
            isFromLaunchPushClick = true
            let dictPushData: NSDictionary = remoteNotification as NSDictionary
            self.HandelPushNotificationData(dictPushData)
        }
    
        if isUserLogin() {
            isFromLaunching = true
            CUSTOMER_ID = UserDefaults.standard.object(forKey: UD_CUSTOMERID) as! String
            
            //let vc = loadVC(strStoryboardId: SB_RECIPE, strVCId: "navrecipe")
            let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
            UIView.transition(with: self.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.window?.rootViewController = vc
            }, completion: nil)
        } else {
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "navonboarding")
            UIView.transition(with: self.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.window?.rootViewController = vc
            }, completion: nil)
        }
        //sleep(2)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        //AppEvents.activateApp()
        if APP_DELEGATE.isOnBillingDetailsPage {
            self.isPostNoti = true
            NotificationCenter.default.post(name: Notification.Name(BILLING_PAGE_REFRESH), object: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK:- UINotification Methods
extension AppDelegate {
    func PushNotification() {
        let pushy = Pushy(UIApplication.shared)

        // Register the device for push notifications
        pushy.register({ (error, deviceToken) in
            // Handle registration errors
            if error != nil {
                return print ("Registration failed: \(error!)")
            }
            
            // Print device token to console
            print("Pushy device token: \(deviceToken)")
            
            // Persist the token locally and send it to your backend later
            UserDefaults.standard.set(deviceToken, forKey: "pushyToken")
        })
        
        // Handle push notifications
        pushy.setNotificationHandler({ (data, completionHandler) in
            print("Received notification: \(data)")
            let dictPushData: NSDictionary = data as NSDictionary
            self.HandelPushNotificationData(dictPushData)
        })
    }
    
    func HandelPushNotificationData(_ dictPushData: NSDictionary) {
        print("Push Data: ", dictPushData)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        if !isUserLogin() {
            isFromLaunchPushClick = false
            return
        }
        var isShowTopView: Bool = true
        
        let title: String = dictPushData.object(forKey: "title") as? String ?? ""
        var Message:String = ""
        if let text: String = dictPushData.object(forKey: "text") as? String {
            Message = text
        }
        if let message: String = dictPushData.object(forKey: "message") as? String {
            Message = message
        }
        
        APP_DELEGATE.PushTitle = title
        APP_DELEGATE.PushMessage = Message
        
        if let moduleName = dictPushData.object(forKey: "module_name") as? String {
            APP_DELEGATE.PushModule = moduleName
            let clickAction = dictPushData.object(forKey: "click_action") as! String
            if clickAction == "NOTIFICATION" {
                isShowTopView = false
                NotiPageIndex = -1
                if moduleName == MODULE_NAME.EVENTS.rawValue {
                    NotiPageIndex = 0
                } else if moduleName == MODULE_NAME.OFFERS.rawValue {
                    NotiPageIndex = 1
                } else if moduleName == MODULE_NAME.TOAST.rawValue {
                    NotiPageIndex = 2
                }
                if isOnNotificationTab {
                    self.isPostNoti = true
                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_PAGE_REFRESH), object: nil)
                } else {
                    if isFromLaunchPushClick {
                        APP_DELEGATE.isShowNotificationPopup = true
                    } else {
                        let notificationBar = GLNotificationBar(title: title, message: Message, preferredStyle: .detailedBanner, handler: nil)
                        notificationBar.addAction(GLNotifyAction(title: "Go To Notification", style: .default) { (result) in
                            self.GoToNotificationTab(moduleName)
                        })
                    }
                }
            } else {
                if APP_DELEGATE.isOnMenu {
                    if moduleName == MODULE_NAME.ACKNOWLEDGEPAYMENT.rawValue {
                        isShowTopView = false
                        APP_DELEGATE.isOnRatingPage = true
                        runAfterTime(time: 2) {
                            self.GoForRating()
                        }
                    } else {
                        if moduleName == MODULE_NAME.DISCOUNT.rawValue || moduleName == MODULE_NAME.ZOMATOGOLD.rawValue {
                            isShowTopView = false
                            if APP_DELEGATE.isOnBillingDetailsPage {
                                showMessage(Message)
                                self.isPostNoti = true
                                NotificationCenter.default.post(name: Notification.Name(BILLING_PAGE_REFRESH), object: nil)
                            } else {
                                let notificationBar = GLNotificationBar(title: title, message: Message, preferredStyle: .detailedBanner, handler: nil)
                                notificationBar.addAction(GLNotifyAction(title: "Go To Billing Page", style: .default) { (result) in
                                    self.GoToBillingPage()
                                })
                            }
                        }
                    }
                } else if isFromLaunchPushClick {
                    isShowTopView = false
                    if moduleName == MODULE_NAME.ACKNOWLEDGEPAYMENT.rawValue {
                        APP_DELEGATE.isOnRatingPage = true
                        runAfterTime(time: 2) {
                            self.GoForRating()
                        }
                    } else if moduleName == MODULE_NAME.DISCOUNT.rawValue || moduleName == MODULE_NAME.ZOMATOGOLD.rawValue {
                        APP_DELEGATE.isShowDisZomPopup = true
                    }
                }
            }
        }

        if isShowTopView && !self.isFromLaunchPushClick {
            let _ = GLNotificationBar(title: title, message: Message, preferredStyle: .simpleBanner, handler: nil)
        }
        if !self.isFromLaunchPushClick {
            if isInHomeScreen {
                self.isPostNoti = true
                NotificationCenter.default.post(name: Notification.Name(HOME_PAGE_REFRESH), object: nil)
            } else if isInActiveOrder {
                self.isPostNoti = true
                NotificationCenter.default.post(name: Notification.Name(ACTIVE_ORDER_PAGE_REFRESH), object: nil)
            } else if isInOrderSuccess {
                self.isPostNoti = true
                NotificationCenter.default.post(name: Notification.Name(ORDER_SUCCESS_PAGE_REFRESH), object: nil)
            }
        }
        self.isFromLaunchPushClick = false
    }
    
    func GoForRating() {
        let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "RatingVC") as! RatingVC
        vc.isFromPush = true
        let navVC: UINavigationController = UINavigationController.init(rootViewController: vc)
        navVC.navigationBar.isHidden = true
        navVC.modalPresentationStyle = .overCurrentContext
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = navVC
            self.window?.makeKeyAndVisible()
        }, completion: nil)
        /*
         if var topController = UIApplication.shared.keyWindow?.rootViewController {
         while let presentedViewController = topController.presentedViewController {
         topController = presentedViewController
         }
         topController.present(navVC, animated: true, completion: nil)
         } else {
         UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
         APP_DELEGATE.window?.rootViewController = navVC
         }, completion: nil)
         }*/
    }
    
    func GoToBillingPage() {
        runAfterTime(time: 1) {
            let vc = loadVC(strStoryboardId: SB_ORDERPLACE, strVCId: "BillingDetailsVC") as! BillingDetailsVC
            vc.isFromPlaceOrder = true
            let navVC = UINavigationController(rootViewController:vc)
            navVC.isNavigationBarHidden = true
            navVC.modalPresentationStyle = .overCurrentContext
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(navVC, animated: true, completion: nil)
            }
        }
    }
    
    func GoToNotificationTab(_ moduleName: String) {
        let vc = loadVC(strStoryboardId: SB_NOTIFICATION, strVCId: "NotificationVC") as! NotificationVC
        vc.modalPresentationStyle = .fullScreen
        vc.isFromPush = true
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(vc, animated: true, completion: nil)
        }
    }
}

//MARK:- GET CURRENT LOCATION
extension AppDelegate: CLLocationManagerDelegate {
    func GetCurrentLocation()
    {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D = manager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
        lat_currnt = locValue.latitude
        long_currnt = locValue.longitude
        
        getCurrentCity { (strCity) in
            self.strCurrentCity = strCity ?? ""
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.startUpdatingLocation()
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
        }
    }
}
